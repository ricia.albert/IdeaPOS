<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Sidestock extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "stockAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Sidestock_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Sidestock_model->get_all_product($req);

		$this->load->view("Sidestock_view",$data);
	}

	public function add_sidestock_view(){
		$this->load->model("Sidestock_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$data["menuSub"] = $this->Sidestock_model->get_sub($pos);
		$this->load->view("Add_sidestock_view",$data);
	}

	public function delete(){
		$this->load->model("Sidestock_model");
		$delete = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$this->Sidestock_model->delete($delete,$pos);
		redirect(site_url("Sidestock"));
	}

	public function update(){
		$this->load->model("Sidestock_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$fromPos = $this->input->get("fromPos");
		$copy = $this->input->get("copy");
		$data["req"] = $this->input->get("menu");
		$data["pos"] = $this->input->get("pos");
		$data["copy"] = $copy;
		if ($copy == 1) {
			$data["posNm"] = $this->Sidestock_model->get_branch($pos)->POSNm;
		}
		$data["menuSub"] = $this->Sidestock_model->get_sub($pos);
		$data["viewProduct"] = $this->Sidestock_model->get_Product($req, ($copy==1) ? $fromPos : $pos);

		$this->load->view("Add_sidestock_view",$data);
	}

	public function update_product(){
		$this->load->model("Sidestock_model");
		$sideCode = $this->input->post("sideCode");
		$req = $this->input->post("req");
		$PosID = $this->input->post("pos");
		$sideName = $this->input->post("sideName");
		$Description = $this->input->post("Description");
		$sideQty = $this->input->post("sideQty");
		$sideHPP = $this->input->post("sideHPP");
		$sidePrice = $this->input->post("sidePrice");
		$sideMin = $this->input->post("sideMin");
		$menuVisible = $this->input->post("menuVisible");
		$sideCat = $this->input->post("sideCat");
		

		$dates = date("Y-m-d h:i:s");

			$data = array(
				"codeSide"=>$sideCode,
				"Side_name"=>$sideName,
				"Info"=>$Description,
				"Qty"=>$sideQty,
				"Minimum_stock"=>$sideMin,
				"Side_price"=>$sidePrice,
				"Bought_price"=>$sideHPP,
				"Availability"=>$menuVisible,
				"last_update"=>$dates,
				"userID"=>$this->session->staffID,
				"categoryID"=>$sideCat,
				"PosID" => $PosID
				);

		$this->Sidestock_model->update($data,$req,$PosID);
			
		redirect(site_url("Sidestock"));
	}

	public function add_product(){
		$this->load->model("Sidestock_model");
		//$userID = $this->input->post("userID");
		$sideCode = $this->input->post("sideCode");
		$PosID = $this->input->post("pos");
		$sideName = $this->input->post("sideName");
		$Description = $this->input->post("Description");
		$sideQty = $this->input->post("sideQty");
		$sideHPP = $this->input->post("sideHPP");
		$sidePrice = $this->input->post("sidePrice");
		$sideMin = $this->input->post("sideMin");
		$menuVisible = $this->input->post("menuVisible");
		$sideCat = $this->input->post("sideCat");

		/*$value = $this->db->query("SELECT `menuID` as value FROM `menu` ORDER BY `menuID` DESC LIMIT 1")
						->row()->value;
		$code = substr($value, 0,2);
		$incrmt = substr($value, 2,8);
		*/
		$msg= "";
		if ($sideName == "") {
			$msg = "Harap mengisi nama terlebih dahulu.";
		}
		elseif ($sideCode=="") {
			$msg = "Harap mengisi Code.";
		}
		elseif ($Description=="") {
			$msg = "Harap mengisi Info.";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["menuSub"] = $this->Sidestock_model->get_sub();
			$this->load->view("Add_sidestock_view", $data);
		}
		else{
			$dates = date("Y-m-d h:i:s");

			$data = array(
				"SideStockID" => $this->Sidestock_model->get_ID($PosID),
				"codeSide"=>$sideCode,
				"Side_name"=>$sideName,
				"Info"=>$Description,
				"Qty"=>$sideQty,
				"Minimum_stock"=>$sideMin,
				"Side_price"=>$sidePrice,
				"Bought_price"=>$sideHPP,
				"Availability"=>$menuVisible,
				"last_update"=>$dates,
				"userID"=>$this->session->staffID,
				"categoryID"=>$sideCat,
				"PosID" => $PosID
				);

			$this->Sidestock_model->add($data);
			
			redirect(site_url("Sidestock"));
		}
	}

	public function get_available_branch() {
		$this->load->model("Sidestock_model");
		$codeMenu = $this->input->post("codeMenu");
		$pos = $this->Sidestock_model->get_available_pos($codeMenu);
		$copy = $this->Sidestock_model->get_copy_pos($codeMenu);
		echo json_encode($pos)."<-sp->".json_encode($copy);
	}
}