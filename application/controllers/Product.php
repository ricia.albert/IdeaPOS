<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Product extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "productAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Product_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Product_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Product_view",$data);
	}

	public function add_product_view(){
		$this->load->model("Product_model");
		$this->load->model("Ingridients_model");
		$this->load->model("Brand_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$data["menuSub"] = $this->Product_model->get_sub($pos);
		$data["allMenu"] = $this->Product_model->get_all_ing($pos);
		$data["allBrand"] = $this->Brand_model->get_all_product($pos);
		$data["scalar"] = $this->Ingridients_model->get_scalar($pos);
		$data["scalarJson"] = json_encode($data["scalar"]);
		$this->load->view("Add_product_view",$data);
	}

	public function delete(){
		$this->load->model("Product_model");
		$delete = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$this->Product_model->delete($delete, $pos);
		$this->Product_model->delete_sub($delete, $pos);
		redirect(site_url("Product"));
	}

	public function update(){
		$this->load->model("Product_model");
		$this->load->model("Ingridients_model");
		$this->load->model("Brand_model");
		$data = array();

		$req = $this->input->get("menu");
		$code = $this->input->get("code");
		$pos = $this->input->get("pos");
		$fromPos = $this->input->get("fromPos");
		$copy = $this->input->get("copy");
		$data["req"] = $this->input->get("menu");
		$data["pos"] = $this->input->get("pos");
		$data["copy"] = $copy;
		if ($copy == 1) {
			$data["posNm"] = $this->Product_model->get_branch($pos)->POSNm;
		}
		$data["menuSub"] = $this->Product_model->get_sub($pos);
		$data["allMenu"] = $this->Product_model->get_all_ing($pos);
		$data["allBrand"] = $this->Brand_model->get_all_product($pos);
		$data["scalar"] = $this->Ingridients_model->get_scalar($pos);
		$data["scalarJson"] = json_encode($data["scalar"]);
		$data["menuChoosen"] = ($copy==1) ? $this->Product_model->get_all_choosen_by_code($req, $fromPos, $pos) : $this->Product_model->get_all_choosen($req, $pos);
		$data["viewProduct"] = $this->Product_model->get_Product($req, ($copy==1) ? $fromPos : $pos);

		$this->load->view("Add_product_view",$data);
	}

	public function update_product(){
		$this->load->model("Product_model");
		$this->load->model("Branch_model");
		$this->load->model("Ingridients_model");
		$menuID = $this->input->post("req");
		$codeMenu = $this->input->post("menuID");
		$nameMenu = $this->input->post("nameMenu");
		$description = $this->input->post("Description");
		$menuPrice = $this->input->post("menuPrice");
		$menuVisible = $this->input->post("menuVisible");
		$menuSub = $this->input->post("menuSub");
		$brandID = $this->input->post("brandID");
		$PosID = $this->input->post("pos");

		$dates = date("Y-m-d h:i:s");

			$data = array(
				"codeMenu"=>$codeMenu,
				"subCategoryID"=>$menuSub,
				"brandID" => $brandID,
				"menuNm"=>$nameMenu,
				"description"=>($description == "") ? "-" : $description,
				"price"=>$menuPrice,
				"last_update"=>$dates,
				"visible"=>$menuVisible,
				"userID"=>$this->session->staffID
				);
		$this->Product_model->update($menuID,$PosID,$data);

		$this->Product_model->delete_sub($menuID);
		$data1=array();

			$indexInsert = $this->input->post("indexInsert");
			//echo $indexInsert;
			for ($i=1; $i <= $indexInsert+1 ; $i++) { 
				$data1=array();
				$value = $this->db->query("SELECT `menuingID` as value FROM `menuing`
											WHERE PosID = ".$this->db->escape($PosID)."
										 ORDER BY `menuingID` DESC LIMIT 1")
						->row();
				if ($value != null) {
					$value = $value->value;
				} else {
					$value = 0;
				}
				$code = "MI";
				$incrmt = substr($value, 2,8);

				$incrmt += 1;
				//strval($incrmt);
				//echo "ini Code : ".$code." ini incr: ".strlen($incrmt);
				if (strlen($incrmt)==1) {
					$incrmt = '0000000'.$incrmt;
				}
				else if (strlen($incrmt)==2) {
					$incrmt = '000000'.$incrmt;
				}
				else if (strlen($incrmt)==3) {
					$incrmt = '00000'.$incrmt;
				}
				else if (strlen($incrmt)==4) {
					$incrmt = '0000'.$incrmt;
				}
				else if (strlen($incrmt)==5) {
					$incrmt = '000'.$incrmt;
				}
				else if (strlen($incrmt)==6) {
					$incrmt = '00'.$incrmt;
				}
				else if (strlen($incrmt)==7) {
					$incrmt = '0'.$incrmt;
				}

				$ingID = $code.$incrmt;
				
				if ($this->input->post("addCode".$i)!=""||$this->input->post("addCode".$i)!=null) {
					$addCode=$this->input->post("addCode".$i);
					$scalar=$this->input->post("addScalar".$i);
					$data1["menuIngID"] = $ingID;
					$data1["menuID"]=$menuID;
					$data1["ingID"]=$addCode;
					$data1["PosID"]=$PosID;
					$data1["scalarID"]=$scalar;
					//$data1["packagesID"]=$addCode;
					if ($this->input->post("addQty".$i)!=""||$this->input->post("addQty".$i)!=null) {
						$addQty=$this->input->post("addQty".$i);
						$data1["qty"]=$addQty;
					}
					$this->Product_model->add_sub($data1);
				}
				
				
			}
		
		// Update Mapped Product
		$branches = $this->Branch_model->get_non_storage_branch();
		foreach ($branches as $key => $value) {
			$this->Ingridients_model->updateCode($codeMenu, $value->POSID, array("bprice" => $menuPrice, "ingName" => $nameMenu));
		}

		redirect(site_url("Product"));
	}

	public function add_product() {
		$this->load->model("Product_model");
		$codeMenu = $this->input->post("menuID");
		$nameMenu = $this->input->post("nameMenu");
		$description = $this->input->post("Description");
		$menuPrice = $this->input->post("menuPrice");
		$menuVisible = $this->input->post("menuVisible");
		$menuSub = $this->input->post("menuSub");
		$brandID = $this->input->post("brandID");
		$PosID = $this->input->post("pos");		

		$msg= "";
		if ($nameMenu == ""||$nameMenu==null) {
			$msg = "Harap mengisi nama terlebih dahulu.";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["menuSub"] = $this->Product_model->get_sub();
			$this->load->view("Add_product_view", $data);
		}
		else{
			$dates = date("Y-m-d h:i:s");
			$menuID = $this->Product_model->get_ID($PosID);
			$data = array(
				"menuID" => $menuID,
				"codeMenu"=>$codeMenu,
				"subCategoryID"=>$menuSub,
				"brandID" => $brandID,
				"menuNm"=>$nameMenu,
				"description"=>($description == "") ? "-" : $description,
				"price"=>$menuPrice,
				"last_update"=>$dates,
				"visible"=>$menuVisible,
				"userID"=>$this->session->staffID,
				"PosID" => $PosID
				);

			$this->Product_model->add($data);

			$data1=array();

			$indexInsert = $this->input->post("indexInsert");
			//echo $indexInsert;
			for ($i=1; $i <= $indexInsert ; $i++) { 
				$data1=array();
				$value = $this->db->query("SELECT `menuingID` as value FROM `menuing` ORDER BY `menuingID` DESC LIMIT 1")
						->row()->value;
				$code = "MI";
				$incrmt = substr($value, 2,8);

				$incrmt += 1;
				//strval($incrmt);
				//echo "ini Code : ".$code." ini incr: ".strlen($incrmt);
				if (strlen($incrmt)==1) {
					$incrmt = '0000000'.$incrmt;
				}
				else if (strlen($incrmt)==2) {
					$incrmt = '000000'.$incrmt;
				}
				else if (strlen($incrmt)==3) {
					$incrmt = '00000'.$incrmt;
				}
				else if (strlen($incrmt)==4) {
					$incrmt = '0000'.$incrmt;
				}
				else if (strlen($incrmt)==5) {
					$incrmt = '000'.$incrmt;
				}
				else if (strlen($incrmt)==6) {
					$incrmt = '00'.$incrmt;
				}
				else if (strlen($incrmt)==7) {
					$incrmt = '0'.$incrmt;
				}

				$ingID = $code.$incrmt;
				
				if ($this->input->post("addCode".$i)!=""||$this->input->post("addCode".$i)!=null) {
					$addCode=$this->input->post("addCode".$i);
					$scalar=$this->input->post("addScalar".$i);
					$data1["menuIngID"] = $ingID;
					$data1["menuID"]=$menuID;
					$data1["PosID"] = $PosID;
					$data1["ingID"]=$addCode;
					$data1["scalarID"]=$scalar;
					//$data1["packagesID"]=$addCode;
					if ($this->input->post("addQty".$i)!=""||$this->input->post("addQty".$i)!=null) {
						$addQty=$this->input->post("addQty".$i);
						$data1["qty"]=$addQty;
					}
					$this->Product_model->add_sub($data1);
				}
				
				
			}
			
			redirect(site_url("Product"));
		}
	}

	public function get_available_branch() {
		$this->load->model("Product_model");
		$codeMenu = $this->input->post("codeMenu");
		$pos = $this->Product_model->get_available_pos($codeMenu);
		$copy = $this->Product_model->get_copy_pos($codeMenu);
		echo json_encode($pos)."<-sp->".json_encode($copy);
	}

	public function import() {
		$this->load->model("Product_model");
		$this->load->model("Sub_category_model");
		$this->load->model("Brand_model");
		$this->load->library("PHPExcel/Classes/PHPExcel");

		// Get Parameter
		$PosID = $this->input->post("PosID");

		// Upload File
		$config["upload_path"] = "./dist/upload/excel";
		$config["file_name"] = md5(time()).".xlsx";
		$config["allowed_types"] = "xlsx";
		$config["max_size"] = 3 * 1024;
		$this->load->library("upload", $config);
		if (!$this->upload->do_upload("fnImport")) {
			//$error = array("error" => $this->upload->display_errors());
			echo $this->upload->display_errors();
		} else {
			try{
				$reader = PHPExcel_IOFactory::createReader('Excel2007');
				$phpexcel = $reader->load($config["upload_path"]."/".$config["file_name"]);
			} catch(Zend_Exception $e) {
				echo $e->getMessage();
			}

			$sheet = $phpexcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++) {
				$menuID = $this->Product_model->get_ID($PosID);
				$menuNm = $sheet->getCell("A".$row)->getValue();
				$subCategoryID = $sheet->getCell("B".$row)->getValue();
				$description = $sheet->getCell("C".$row)->getValue();
				$price = $sheet->getCell("D".$row)->getValue();
				$visible = $sheet->getCell("E".$row)->getValue();
				$codeMenu = $sheet->getCell("F".$row)->getValue();
				$brandID = $sheet->getCell("G".$row)->getValue();

				if (!$this->Product_model->checkExists($menuNm) && $this->Sub_category_model->checkExists($subCategoryID)
					&& $this->Brand_model->checkExists($brandID)) {
					$data = array("menuID" => $menuID,
								  "menuNm" => $menuNm,
								  "subCategoryID" => $subCategoryID,
								  "description" => $description,
								  "price" => $price,
								  "visible" => $visible,
								  "codeMenu" => $codeMenu,
								  "brandID" => ($brandID == -1) ? null : $brandID,
								  "userID" => $this->session->staffID,
								  "POSID" => $PosID,
								  "last_update" => date('Y-m-d H:i'));
					$this->Product_model->add($data);
				}
			}
			redirect(site_url("Product"));
		}

		
	}
}