<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Sync extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "productAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Product_model");
		
		$branchList = $this->get_storage();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Product_model->get_all_product($req);
		
		$this->load->view("Sync_view",$data);
	}

	public function add() {
		$this->load->model("Product_model");
		$branchList = $this->get_non_storage();
		$branchList2 = $this->get_storage();
		$req = $this->input->get("req");
		$pos = $this->input->get("pos");

		$data=array();
		$data["pos"] = $pos;
		$data['view_branch'] = $branchList;
		$data["viewProduct"] = $this->Product_model->get_Product($req, $pos);
		$prod = $data["viewProduct"][0];
		$data["menuChoosen"] = $this->Product_model->get_same_code($prod->codeMenu);
		$data["allMenu"] = $this->Product_model->get_all_ing($pos);
		$data["get_branch"] = $branchList2;
		
		$this->load->view("Add_sync_view", $data);
	}

	
	public function update() {
		$this->load->model("Mapping_stock_model");
		$this->load->model("Ingridients_model");
		$indexInsert = $this->input->post("indexInsert");
		$codeMenu = $this->input->post("codeMenu"); 
		$menuID = $this->input->post("menuID");
		$posID = $this->input->post("posID");
		$price = $this->input->post("price");
		$name = $this->input->post("name");
		$codeIng = $this->input->post("codeIng");
		$dates = date("Y-m-d H:i:s");
		for ($i=1; $i <= $indexInsert+1 ; $i++) { 
			$data1 = array();
			if ($this->input->post("addCode".$i)!=""||$this->input->post("addCode".$i)!=null) {
				$addCode=$this->input->post("addCode".$i);
				$data1["ingID"]=$addCode;
					//$data1["packagesID"]=$addCode;
				if ($this->input->post("addPos".$i)!=""||$this->input->post("addPos".$i)!=null) {
					$addPos=$this->input->post("addPos".$i);
					$data1["PosID"]=$addPos;
				}
				if ($this->input->post("addName".$i)!=""||$this->input->post("addName".$i)!=null) {
					$addName=$this->input->post("addName".$i);
					$data1["ingName"]=$addName;
				}
				
				$this->Mapping_stock_model->add(array("menuID" => $menuID,"fromPos" => $posID, "ingID" => $data1["ingID"] ,"toPos" => $data1["PosID"] ,"last_update" => $dates));
				$this->Ingridients_model->addHist($data1["ingID"], $data1["PosID"], $this->session->staffID);
				$this->Ingridients_model->update($data1["ingID"], $data1["PosID"], array("ingName" => $name, "bprice" => $price ,"last_update" => $dates,"codeIng" => $codeMenu));
			}

		}
		redirect(site_url("Sync"));
	}


	public function syncall() {
		
		$this->load->model("Ingridients_model");
		$this->load->model("Sync_model");
		$this->load->model("Product_model");
		$dates = date("Y-m-d H:i:s");
		$pos = $this->input->get("pos");
		$data = $this->Sync_model->get_all_mapping_stock();
		
		foreach ($data as $row) {

			$req = $row->menuID;
			$req2 = $row->ingID;
			$getProductRow = $this->Product_model->get_Product_query($req, $pos);
			// $getStockRow = $this->Ingridients_model->get_product_query($req2, $row->toPos);
			$this->Ingridients_model->addHist($req2, $row->toPos, $this->session->staffID);
			$this->Ingridients_model->update($row->ingID, $row->toPos, array("ingName" => $getProductRow->menuNm, "bprice" => $getProductRow->price ,"last_update" => $dates,"codeIng" => $getProductRow->codeMenu));
		}

	}	
	
}