<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Payment extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Payment_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Payment_model->get_all_product($req);

		$this->load->view("Payment_view",$data);
	}

	public function add_position_view(){
		$this->load->model("Payment_model");

		$branch = $this->input->get("b");
    
		$data=array();
		$data["pos"] = $branch;
		$this->load->view("Add_payment_view",$data);
	}

	public function add_position(){
		$this->load->model("Payment_model");

		$msg="";

		$paymentName = $this->input->post("paymentName");
		$percentDiscount = $this->input->post("percentDiscount");
		$discount = $this->input->post("discount");
		$pos = $this->input->post("PosID");

		if ($paymentName == ""||$paymentName == null) {
			$msg = "Mohon isi nama pembayaran";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$this->load->view("Add_payment_view", $data);
		}

		$dates = date("Y-m-d h:i:s");


		$data = array(
			'paymentName' => $paymentName,
			'percentDiscount' => $percentDiscount,
			'discount' => $discount,
			'userID' => $this->session->staffID,
			"last_update" => $dates,
			'PosID' => $pos
			);
		$this->Payment_model->add($data);
		redirect(site_url("Payment"));

	}

	public function update(){
		$this->load->model("Payment_model");
		$data = array();

		$req = $this->input->get("req");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["viewProduct"] = $this->Payment_model->get_product($req,$pos);

		$this->load->view("Add_payment_view",$data);
	}

	public function update_position(){
		$this->load->model("Payment_model");

		$msg="";

		$paymentID = $this->input->post("paymentID");
		$paymentName = $this->input->post("paymentName");
		$percentDiscount = $this->input->post("percentDiscount");
		$discount = $this->input->post("discount");
		$pos = $this->input->post("pos");

		if ($paymentName == ""||$paymentName == null) {
			$msg = "Mohon isi nama pembayaran";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["position"] = $this->Payment_model->get_product($paymentID, $pos);
			$this->load->view("Add_payment_view", $data);
		}


		$dates = date("Y-m-d h:i:s");

		$data = array(
			'paymentName' => $paymentName,
			'percentDiscount' => $percentDiscount,
			'userID' => $this->session->staffID,
			"last_update" => $dates,
			'discount' => $discount
			);
		$this->Payment_model->update($paymentID,$pos,$data);
		redirect(site_url("Payment"));
	}

	public function delete(){
		$this->load->model("Payment_model");
		$req = $this->input->get("payment");
		$pos = $this->input->get("pos");
		$this->Payment_model->delete($req,$pos);
		redirect(site_url("Payment"));
	}


}