<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Product_detail_price extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "productAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Product_detail_price_model");
		if ($this->input->post("action")==null) {
			$req = "";	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data["name"] = $this->input->get("name");
		$data["menu"] = $this->input->get("menu");
		$data["pos"] = $this->input->get("pos");
		//$data['view_branch'] = $this->Product_detail_price_model->get_branch();
		$data['view_product'] = $this->Product_detail_price_model->get_all_product($data["menu"], $data["pos"]);

		$this->session->set_userdata("menu", $data["menu"]);
		$this->session->set_userdata("name", $data["name"]);
		$this->session->set_userdata("PosID", $data["pos"]);

		$this->load->view("Product_detail_price_view",$data);
	}

	public function add_detail_view(){
		$this->load->model("Product_detail_price_model");

		$data = array();
		$data["temp"] = $this->session->userdata("menu");
		$data["pos"] = $this->session->userdata("PosID");
		if (strpos($data["temp"], "MN") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type($data["pos"]);
		} else if (strpos($data["temp"], "PK") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type_pck($data["pos"]);
		} else if (strpos($data["temp"], "SS") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type_ss($data["pos"]);
		}
		$this->load->view("Add_detail_view",$data);
	}

	public function add_category(){
		$this->load->model("Product_detail_price_model");

		$msg="";
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		$data["pos"] = $this->session->userdata("PosID");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		if (strpos($menuID, "MN") === 0) {
			$data = array(
					'menuID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'PosID' => $data["pos"],
					'last_update' => $dates,
					'userID' => $this->session->staffID
				);
			$this->Product_detail_price_model->add($data);
		} else if (strpos($menuID, "PK") === 0) {
			$data = array(
					'packagesID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'PosID' => $data["pos"],
					'last_update' => $dates,
					'userID' => $this->session->staffID
				);
			$this->Product_detail_price_model->add_pck($data);
		} else if (strpos($menuID, "SS") === 0) {
			$data = array(
					'SideStockID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'PosID' => $data["pos"],
					'last_update' => $dates,
					'userID' => $this->session->staffID
				);
			$this->Product_detail_price_model->add_ss($data);
		}
		redirect(site_url("Product_detail_price?menu=".$this->session->userdata("menu")."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}

	public function update(){
		$this->load->model("Product_detail_price_model");
		$data = array();

		$req = $this->input->get("menu");
		$data["req"] = $req;
		$data["pos"] = $this->session->userdata("PosID");
		$menuID = $this->session->userdata("menu");
		$data["temp"] = $this->session->userdata("menu");

		if (strpos($menuID, "MN") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type($data["pos"]);
			$data["viewProduct"] = $this->Product_detail_price_model->get_product($req,$data["pos"]);
		} else if (strpos($menuID, "PK") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type_pck($data["pos"]);
			$data["viewProduct"] = $this->Product_detail_price_model->get_package($req,$data["pos"]);
		} else if (strpos($menuID, "SS") === 0) {
			$data['view_menu'] = $this->Product_detail_price_model->get_type_ss($data["pos"]);
			$data["viewProduct"] = $this->Product_detail_price_model->get_side($req,$data["pos"]);
		}
		$this->load->view("Add_detail_view",$data);
	}

	public function update_category(){
		$this->load->model("Product_detail_price_model");
		$id = $this->input->post("catID");
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		$pos = $this->session->userdata("PosID");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		if (strpos($menuID, "MN") === 0) {
			$data = array(
					'menuID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'last_update' => $dates
				);
			$this->Product_detail_price_model->update($id, $pos, $data);
		} else if (strpos($menuID, "PK") === 0) {
			$data = array(
					'packagesID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'last_update' => $dates,
				);
			$this->Product_detail_price_model->update_pck($id, $pos, $data);
		} else if (strpos($menuID, "SS") === 0) {
			$data = array(
					'SideStockID' => $menuID,
					'Info' => $detailInfo,
					'Price' => $detailPrice,
					'last_update' => $dates,
				);
			$this->Product_detail_price_model->update_ss($id, $pos, $data);
		}
		redirect(site_url("Product_detail_price?menu=".$this->session->userdata("menu")."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}

	public function delete(){
		$this->load->model("Product_detail_price_model");
		$req = $this->input->get("menu");
		$pos = $this->session->userdata("PosID");
		$menuID = $this->session->userdata("menu");

		if (strpos($menuID, "MN") === 0) {
			$this->Product_detail_price_model->delete($req,$pos);
		} else if (strpos($menuID, "PK") === 0) {
			$this->Product_detail_price_model->delete_pck($req,$pos);
		} else if (strpos($menuID, "SS") === 0) {
			$this->Product_detail_price_model->delete_ss($req,$pos);
		}
		redirect(site_url("Product_detail_price?menu=".$menuID."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}


}