<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Report extends Custom_CI_Controller
{
	var $corp;
	function __construct()
	{
		parent::__construct(true, "reportAuth"); 
		$this->load->helper("form");
		$this->corp = "SYN-PRINT";
	}

	public function index(){
		$this->load->model("Branch_model");
		$this->load->model("Transaction_model");
		$data = array();

		$data["view_branch"] = $this->Branch_model->get_all_branch();
		$data["view_price"] = $this->Transaction_model->get_price_type();

		$this->load->view("Report_view", $data);
	}

	public function exportTrans() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$debt = $this->input->post("debt");
		$stats = $this->input->post("stats");
		$price = $this->input->post("price");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Transaction_model->get_value("totalTransaksi","", $pos, $debt, $stats, $sd, $ed, $price);
		$total = $this->Transaction_model->get_total("totalTransaksi","", $pos, $debt, $stats, $sd, $ed, $price);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Data Transaksi");

		switch ($stats) {
			case 'A': $status = "Accepted"; break;
			case 'P': $status = "Processed"; break;
			case 'C': $status = "Canceled"; break;
		}


		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":L".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN DATA TRANSAKSI - ".strtoupper($status)." - ".(($debt == 1) ? "IN DEBT" : "NON DEBT")." - ".(($price == '') ? 'All' : $price));
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("Kode Transaksi");
		$objSheet->getCell("B5")->setValue("Tgl Transasi");
		$objSheet->getCell("C5")->setValue("Tgl Selesai");
		$objSheet->getCell("D5")->setValue("Jml Item");
		$objSheet->getCell("E5")->setValue("Jml Customer");
		$objSheet->getCell("F5")->setValue("Jenis Harga");
		$objSheet->getCell("G5")->setValue("Kasir");
		$objSheet->getCell("H5")->setValue("Total");
		$objSheet->getCell("I5")->setValue("PPN");
		$objSheet->getCell("J5")->setValue("Discount");
		$objSheet->getCell("K5")->setValue("Voucher");
		$objSheet->getCell("L5")->setValue("Net Total");
		$objSheet->getCell("M5")->setValue("Rounding");
		$objSheet->getCell("N5")->setValue("Catatan");
		$objSheet->getStyle("A1:N5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:N5")->applyFromArray($style);
		
		$index = 5;
		$total = 0;
		$subtotal = 0;
		$ppn = 0;
		$discount =0;
		$voucher = 0;
		$rounding = 0;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($value->transID);
			$objSheet->getCell("B".$index)->setValue(date('d M Y H:i', strtotime($value->transDate)));
			$objSheet->getCell("C".$index)->setValue(($value->settledDate == "") ? "" :date('d M Y H:i', strtotime($value->settledDate)));
			$objSheet->getCell("D".$index)->setValue($value->numOfItem);
			$objSheet->getCell("E".$index)->setValue($value->numOfCustomer);
			$objSheet->getCell("F".$index)->setValue($value->PriceType);
			$objSheet->getCell("G".$index)->setValue($value->employee_name);
			$objSheet->getCell("H".$index)->setValue($value->total);		
			$objSheet->getCell("I".$index)->setValue($value->ppn);		
			$objSheet->getCell("J".$index)->setValue($value->discount);		
			$objSheet->getCell("K".$index)->setValue($value->totalVoucher);		
			$objSheet->getCell("L".$index)->setValue($value->total + $value->ppn - $value->discount - $value->totalVoucher);
			$objSheet->getCell("M".$index)->setValue($value->rounding);
			$objSheet->getCell("N".$index)->setValue((!isset($value->notes)) ? "" : $value->notes);		
			$total += ($value->total + $value->ppn - $value->totalVoucher - $value->discount);
			$subtotal += $value->total;
			$discount += $value->discount;
			$voucher += $value->totalVoucher;
			$rounding += $value->rounding;
			$ppn += $value->ppn;
		}
		$index++;
		$objSheet->mergeCells("A".$index.":G".$index);
		$objSheet->getCell("A".$index)->setValue("Grand Total");
		$objSheet->getStyle("A".$index.":M".$index)->getFont()->setBold(true);
		$objSheet->getStyle("A".$index)->applyFromArray($style);
		$objSheet->getCell("H".$index)->setValue($subtotal);
		$objSheet->getCell("I".$index)->setValue($ppn);
		$objSheet->getCell("J".$index)->setValue($discount);
		$objSheet->getCell("K".$index)->setValue($voucher);
		$objSheet->getCell("L".$index)->setValue($total);
		$objSheet->getCell("M".$index)->setValue($rounding);
		$objSheet->getStyle("A5:N".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);
		$objSheet->getColumnDimension("G")->setAutoSize(true);
		$objSheet->getColumnDimension("H")->setAutoSize(true);
		$objSheet->getColumnDimension("I")->setAutoSize(true);
		$objSheet->getColumnDimension("J")->setAutoSize(true);
		$objSheet->getColumnDimension("K")->setAutoSize(true);
		$objSheet->getColumnDimension("L")->setAutoSize(true);
		$objSheet->getColumnDimension("M")->setAutoSize(true);
		$objSheet->getColumnDimension("N")->setAutoSize(true);

		// Download Filenya		
		$filename = "trans-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportMutasi() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Ingridients_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$debt = $this->input->post("debt");
		$stats = $this->input->post("stats");
		$price = $this->input->post("price");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$stock = $this->Ingridients_model->get_all_product($pos);
		$transfer_out = $this->Ingridients_model->get_transfer_out($pos, $sd, $ed);
		$transfer_in = $this->Ingridients_model->get_transfer_in($pos, $sd, $ed);
		$request_in = $this->Ingridients_model->get_request_in($pos, $sd, $ed);
		$inventory_in = $this->Ingridients_model->get_inventory_in($pos, $sd, $ed);
		$sales_stock = $this->Ingridients_model->get_sales_stock($pos, $sd, $ed);

		$data = array();
		foreach ($stock as $key) {
			$key->transfer_out = (isset($transfer_out[$key->ingID])) ? $transfer_out[$key->ingID]->totalQty : 0;
			$key->transfer_in = (isset($transfer_in[$key->ingID])) ? $transfer_in[$key->ingID]->totalQty : 0;
			$key->request_in = (isset($request_in[$key->ingID])) ? $request_in[$key->ingID]->totalQty : 0;
			$key->inventory_in = (isset($inventory_in[$key->ingID])) ? $inventory_in[$key->ingID]->totalQty : 0;
			$key->sales_stock = (isset($sales_stock[$key->ingID])) ? $sales_stock[$key->ingID]->totalQty : 0;
			array_push($data, $key);
		}

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Mutasi Stock");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":H".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN MUTASI STOCK");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("Kode Stock");
		$objSheet->getCell("B5")->setValue("Nama Stock");
		$objSheet->getCell("C5")->setValue("Current Qty");
		$objSheet->getCell("D5")->setValue("Stock In");
		$objSheet->getCell("E5")->setValue("Sold Out");
		$objSheet->getCell("F5")->setValue("Request Stock");
		$objSheet->getCell("G5")->setValue("Transfer In");
		$objSheet->getCell("H5")->setValue("Transfer Out");
		$objSheet->getStyle("A1:H5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		$styleRight = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:H5")->applyFromArray($style);

		
		$index = 5;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($value->codeIng);
			$objSheet->getCell("B".$index)->setValue($value->ingName);
			$objSheet->getCell("C".$index)->setValue(number_format($value->qty,0,",",".")." ".$value->scalarNm);
			$objSheet->getCell("D".$index)->setValue(number_format($value->inventory_in,0,",",".")." ".$value->scalarNm);
			$objSheet->getCell("E".$index)->setValue(number_format($value->sales_stock,0,",",".")." ".$value->scalarNm);
			$objSheet->getCell("F".$index)->setValue(number_format($value->request_in,0,",",".")." ".$value->scalarNm);
			$objSheet->getCell("G".$index)->setValue(number_format($value->transfer_in,0,",",".")." ".$value->scalarNm);
			$objSheet->getCell("H".$index)->setValue(number_format($value->transfer_out,0,",",".")." ".$value->scalarNm);	
		}
		$objSheet->getStyle("C5:H".$index)->applyFromArray($styleRight);
		$objSheet->getStyle("A5:H".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);
		$objSheet->getColumnDimension("G")->setAutoSize(true);
		$objSheet->getColumnDimension("H")->setAutoSize(true);
		// Download Filenya		
		$filename = "mutation-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportChange() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Transaction_model->get_change($pos, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Perubahan Transaksi");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":E".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN PERUBAHAN TRANSAKSI");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("Kode Transaksi");
		$objSheet->getCell("B5")->setValue("Jumlah Perubahan");
		$objSheet->getCell("C5")->setValue("Nominal Awal");
		$objSheet->getCell("D5")->setValue("Nominal Akhir");
		$objSheet->getCell("E5")->setValue("Selisih");
		$objSheet->getStyle("A1:E5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:E5")->applyFromArray($style);
		
		$index = 5;
		$total = 0;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($value->transID);
			$objSheet->getCell("B".$index)->setValue($value->Jml);
			$objSheet->getCell("C".$index)->setValue($value->Awal);
			$objSheet->getCell("D".$index)->setValue($value->Akhir);
			$objSheet->getCell("E".$index)->setValue($value->Selisih);
			$total += $value->Selisih;
		}
		$index++;
		$objSheet->mergeCells("A".$index.":D".$index);
		$objSheet->getCell("A".$index)->setValue("Grand Total");
		$objSheet->getStyle("A".$index.":E".$index)->getFont()->setBold(true);
		$objSheet->getStyle("A".$index)->applyFromArray($style);
		$objSheet->getCell("E".$index)->setValue($total);
		$objSheet->getStyle("A5:E".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);

		// Download Filenya		
		$filename = "change-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportHPP() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Corp_Model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$debt = $this->input->post("debt");
		$stats = $this->input->post("stats");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Corp_Model->get_transaction_hpp($pos, $debt, $stats, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Data HPP");

		switch ($stats) {
			case 'A': $status = "Accepted"; break;
			case 'P': $status = "Processed"; break;
			case 'C': $status = "Canceled"; break;
		}


		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN DETAIL HPP - ".strtoupper($status)." - ".(($debt == 1) ? "IN DEBT" : ""));
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("Kode Transaksi");
		$objSheet->getCell("B5")->setValue("Tgl Transasi");
		$objSheet->getCell("C5")->setValue("Tgl Selesai");
		$objSheet->getCell("D5")->setValue("Jml Item");
		$objSheet->getCell("E5")->setValue("Jml Customer");
		$objSheet->getCell("F5")->setValue("Kasir");
		$objSheet->getCell("G5")->setValue("Net HPP");
		$objSheet->getStyle("A1:G5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:G5")->applyFromArray($style);
		
		$index = 5;
		$total = 0;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($value->transID);
			$objSheet->getCell("B".$index)->setValue(date('d M Y H:i', strtotime($value->transDate)));
			$objSheet->getCell("C".$index)->setValue(($value->settledDate == "") ? "" : date('d M Y H:i', strtotime($value->settledDate)));
			$objSheet->getCell("D".$index)->setValue(number_format($value->numOfItem,0,".",","));
			$objSheet->getCell("E".$index)->setValue(number_format($value->numOfCustomer,0,".",","));
			$objSheet->getCell("F".$index)->setValue($value->employee_name);
			$objSheet->getCell("G".$index)->setValue(number_format(($value->hpp),0,".",","));		
			$total += ($value->hpp);
		}
		$index++;
		$objSheet->mergeCells("A".$index.":F".$index);
		$objSheet->getCell("A".$index)->setValue("Grand Total HPP");
		$objSheet->getStyle("A".$index.":G".$index)->getFont()->setBold(true);
		$objSheet->getStyle("A".$index)->applyFromArray($style);
		$objSheet->getCell("G".$index)->setValue(number_format($total, 2, ".", ","));
		$objSheet->getStyle("A5:G".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);
		$objSheet->getColumnDimension("G")->setAutoSize(true);

		// Download Filenya		
		$filename = "hpp-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportPettyCash() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Pettycash_Model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Pettycash_Model->get_all_product($pos, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Data Petty Cash");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN PETTY CASH");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Description");
		$objSheet->getCell("C5")->setValue("Tanggal");
		$objSheet->getCell("D5")->setValue("Debit");
		$objSheet->getCell("E5")->setValue("Credit");
		$objSheet->getCell("F5")->setValue("Balance");
		$objSheet->getStyle("A1:F5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:F5")->applyFromArray($style);
		
		$index = 5;
		$total = 0;
		$debit = 0;
		$credit = 0;
		$balance = 0;
		$totalDebit = 0;
		$totalCredit = 0;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue($value->pettyName);
			$objSheet->getCell("C".$index)->setValue(date('d M Y H:i', strtotime($value->pettyDate)));
			if ($value->typeCode == "C") {
				$credit = $value->pettyAmt;
				$debit = 0;
			} else {
				$debit = $value->pettyAmt;
				$credit = 0;
			}
			$balance += ($debit - $credit);
			$objSheet->getCell("D".$index)->setValue($debit);
			$objSheet->getCell("E".$index)->setValue($credit);
			$objSheet->getCell("F".$index)->setValue($balance);	
			$totalCredit += $credit;
			$totalDebit += $debit;
			$no++;
		}
		$index++;
		$objSheet->mergeCells("A".$index.":C".$index);
		$objSheet->getCell("A".$index)->setValue("Total Petty");
		$objSheet->getStyle("A".$index.":F".$index)->getFont()->setBold(true);
		$objSheet->getStyle("A".$index)->applyFromArray($style);
		$objSheet->getCell("D".$index)->setValue($totalDebit);
		$objSheet->getCell("E".$index)->setValue($totalCredit);
		$objSheet->getCell("F".$index)->setValue($balance);
		$objSheet->getStyle("A5:F".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);

		// Download Filenya		
		$filename = "petty-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportStock() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Ingridients_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");

		$data = $this->Ingridients_model->get_all_product($pos);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Data Persediaan");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN PERSEDIAAN STOCK");
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Nama Stock");
		$objSheet->getCell("C5")->setValue("Kategori");
		$objSheet->getCell("D5")->setValue("Deskripsi");
		$objSheet->getCell("E5")->setValue("Qty");
		$objSheet->getCell("F5")->setValue("Min Qty");
		$objSheet->getCell("G5")->setValue("Satuan");
		$objSheet->getStyle("A1:G5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:G5")->applyFromArray($style);
		
		$index = 5;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue($value->ingName);
			$objSheet->getCell("C".$index)->setValue($value->ingCateName);
			$objSheet->getCell("D".$index)->setValue($value->info);
			$objSheet->getCell("E".$index)->setValue($value->qty);
			$objSheet->getCell("F".$index)->setValue($value->minQty);
			$objSheet->getCell("G".$index)->setValue($value->scalarNm);
			$no++;
		}
		$objSheet->getStyle("A5:G".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);
		$objSheet->getColumnDimension("G")->setAutoSize(true);

		// Download Filenya		
		$filename = "stock-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportOpname() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Ingridients_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");

		$data = $this->Ingridients_model->get_stock_opname($pos, $sd);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Stock Opname");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":F".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN STOCK OPNAME");
		$objSheet->getCell("A3")->setValue("TANGGAL ".date('d M Y', strtotime($sd)));
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Kode Stock");
		$objSheet->getCell("C5")->setValue("Nama Stock");
		$objSheet->getCell("D5")->setValue("Qty 1");
		$objSheet->getCell("E5")->setValue("Qty 2");
		$objSheet->getCell("F5")->setValue("Qty Sistem");
		$objSheet->getStyle("A1:F5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:F5")->applyFromArray($style);
		
		$index = 5;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue($value->codeIng);
			$objSheet->getCell("C".$index)->setValue($value->ingName);
			$objSheet->getCell("D".$index)->setValue(number_format($value->jml_temp,2,".",",")." ".$value->scalarTemp);
			$objSheet->getCell("E".$index)->setValue(number_format($value->jml_barang,2,".",",")." ".$value->scalarBarang);
			$objSheet->getCell("F".$index)->setValue(number_format($value->jml_actual,2,".",",")." ".$value->scalarActual);
			$no++;
		}
		$objSheet->getStyle("A5:F".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);

		// Download Filenya		
		$filename = "opname-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportProductSell() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Corp_Model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Corp_Model->get_product_tren("", $pos, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Penjualan Per Produk");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN PENJUALAN PER PRODUK");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Kode Produk");
		$objSheet->getCell("C5")->setValue("Nama Kategori");
		$objSheet->getCell("D5")->setValue("Nama Produk");
		$objSheet->getCell("E5")->setValue("Harga");
		$objSheet->getCell("F5")->setValue("Qty");
		//$objSheet->getCell("F5")->setValue("Total");
		$objSheet->getStyle("A1:F5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:F5")->applyFromArray($style);
		
		$index = 5;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$value = (object)$value;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue($value->codeMenu);
			$objSheet->getCell("C".$index)->setValue($value->subCategoryNm);
			$objSheet->getCell("D".$index)->setValue($value->menuNM);
			$objSheet->getCell("E".$index)->setValue($value->price);
			$objSheet->getCell("F".$index)->setValue($value->jml);
			//$objSheet->getCell("F".$index)->setValue(number_format($value->total,2,".",","));
			$no++;
		}
		$objSheet->getStyle("A5:F".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);

		// Download Filenya		
		$filename = "favorite-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportOmset() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Corp_Model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Corp_Model->get_transaction_omset($pos, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Omset & Visit");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN OMSET & VISIT");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Tanggal");
		$objSheet->getCell("C5")->setValue("Jml Visit");
		$objSheet->getCell("D5")->setValue("Jml Transaksi");
		$objSheet->getCell("E5")->setValue("Total");
		$objSheet->getStyle("A1:E5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );
		$styleRight = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
				)
			);

		$objSheet->getStyle("A1:E5")->applyFromArray($style);
		
		$index = 5;
		$total_visit = 0;
		$total_trans = 0;
		$total = 0;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$value = (object)$value;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue(date('d M Y', strtotime($value->dates)));
			$objSheet->getCell("C".$index)->setValue(number_format($value->total_visit,0,".",","));
			$objSheet->getCell("D".$index)->setValue(number_format($value->transCount,2,".",","));
			$objSheet->getCell("E".$index)->setValue(number_format($value->total,2,".",","));
			$total_visit += $value->total_visit;
			$total_trans += $value->transCount;
			$total += $value->total;
			$no++;
		}
		$index++;
		$objSheet->mergeCells("A".$index.":B".$index);
		$objSheet->getCell("A".$index)->setValue("Grand Total");
		$objSheet->getStyle("A".$index.":E".$index)->getFont()->setBold(true);
		$objSheet->getCell("C".$index)->setValue(number_format($total_visit, 2, ".", ","));
		$objSheet->getCell("D".$index)->setValue(number_format($total_trans, 2, ".", ","));
		$objSheet->getCell("E".$index)->setValue(number_format($total, 2, ".", ","));
		$objSheet->getStyle("A5:E".$index)->applyFromArray($styleArray);
		$objSheet->getStyle("E6:E".$index)->applyFromArray($styleRight);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);

		// Download Filenya		
		$filename = "omset-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportUsage() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Transaction_model->get_stock_usage($pos, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Penggunaan Stock");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":G".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN PENGGUNAAN STOCK");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("No.");
		$objSheet->getCell("B5")->setValue("Kode Stock");
		$objSheet->getCell("C5")->setValue("Nama Stock");
		$objSheet->getCell("D5")->setValue("Harga Beli");
		$objSheet->getCell("E5")->setValue("Total Qty");
		$objSheet->getCell("F5")->setValue("Satuan");
		$objSheet->getStyle("A1:F5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:F5")->applyFromArray($style);
		
		$index = 5;
		$no = 1;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($no.".");
			$objSheet->getCell("B".$index)->setValue($value->codeIng);
			$objSheet->getCell("C".$index)->setValue($value->ingName);
			$objSheet->getCell("D".$index)->setValue(number_format($value->bprice,0,".",","));
			$objSheet->getCell("E".$index)->setValue(number_format($value->total_qty,0,".",","));
			$objSheet->getCell("F".$index)->setValue($value->scalarNm);
			$no++;
		}
		$objSheet->getStyle("A5:F".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);

		// Download Filenya		
		$filename = "usage-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}

	public function exportSummary() {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");
		$pos = $this->input->post("pos");
		$brc = $this->input->post("brc");
		$sd = $this->input->post("sd");
		$ed = $this->input->post("ed");

		$data = $this->Transaction_model->get_summary($pos, "A", $sd, $ed);
		$cash = $this->Transaction_model->get_cash($pos, "A", $sd, $ed);
		$payment = $this->Transaction_model->get_payment($pos, "A", $sd, $ed);
		$cancel = $this->Transaction_model->get_stats($pos, "C", $sd, $ed);
		$void = $this->Transaction_model->get_void($pos, $sd, $ed);
		$stats = $this->Transaction_model->get_stats($pos, "A", $sd, $ed);

		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Rangkuman Omset");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":B".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN RANGKUMAN OMSET");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		
		$objSheet->mergeCells("A5:B5");
		$objSheet->getCell("A5")->setValue("ALL VALUATION");
		$objSheet->getCell("A6")->setValue("Sales");
		$objSheet->getCell("A7")->setValue("Discount");
		$objSheet->getCell("A8")->setValue("Voucher");
		$objSheet->getCell("A9")->setValue("Tax / PPN");
		$objSheet->getCell("A10")->setValue("Rounding");
		$objSheet->getCell("A11")->setValue("Total");

		// Fill Data;
		$objSheet->getCell("B6")->setValue($data->total);
		$objSheet->getCell("B7")->setValue($data->discount);
		$objSheet->getCell("B8")->setValue($data->totalVoucher);
		$objSheet->getCell("B9")->setValue($data->ppn);
		$objSheet->getCell("B10")->setValue($data->rounding);
		$objSheet->getCell("B11")->setValue($data->total + $data->ppn + $data->rounding - $data->discount - $data->totalVoucher);


		// Payment Type
		$objSheet->mergeCells("A13:B13");
		$objSheet->getCell("A13")->setValue("PAYMENT TYPE");
		$objSheet->getCell("A14")->setValue("Cash");
		$objSheet->getCell("B14")->setValue($cash);
		$i = 15;
		foreach ($payment as $key => $value) {
			$objSheet->getCell("A".$i)->setValue($value->paymentName);
			$objSheet->getCell("B".$i)->setValue($value->Cash);
			$i++;
		}
		$objSheet->getCell("A".$i)->setValue("Total");
		$objSheet->getCell("B".$i)->setValue($data->total + $data->ppn + $data->rounding - $data->discount - $data->totalVoucher);


		// Void and Cancel
		$objSheet->mergeCells("A".($i+2).":B".($i+2));
		$objSheet->getCell("A".($i+2))->setValue("VOID INFORMATION");
		$objSheet->getCell("A".($i+3))->setValue("Canceled (Count)");
		$objSheet->getCell("B".($i+3))->setValue($cancel->Jml);
		$objSheet->getCell("A".($i+4))->setValue("Canceled (Total)");
		$objSheet->getCell("B".($i+4))->setValue($cancel->Total);
		$objSheet->getCell("A".($i+5))->setValue("Void (Count)");
		$objSheet->getCell("B".($i+5))->setValue($void->Jml);
		$objSheet->getCell("A".($i+6))->setValue("Void (Total)");
		$objSheet->getCell("B".($i+6))->setValue($void->Total);
		$i+=6;

		// Customer Data Info
		$objSheet->mergeCells("A".($i+2).":B".($i+2));
		$objSheet->getCell("A".($i+2))->setValue("CUSTOMER BASED INFORMATION");
		$objSheet->getCell("A".($i+3))->setValue("Bill Count");
		$objSheet->getCell("B".($i+3))->setValue($stats->Jml);
		$objSheet->getCell("A".($i+4))->setValue("Avg Bill");
		$objSheet->getCell("B".($i+4))->setValue(($stats->Jml == 0) ? $stats->Total : ($stats->Total / $stats->Jml));
		$objSheet->getCell("A".($i+5))->setValue("Guest Total");
		$objSheet->getCell("B".($i+5))->setValue($stats->jmlCust);
		$objSheet->getCell("A".($i+6))->setValue("Pcs Count");
		$objSheet->getCell("B".($i+6))->setValue($stats->jmlItem);
		$i+=6;


		$objSheet->getStyle("A1:A".$i)->getFont()->setBold(true);
		$objSheet->getColumnDimension('A')->setWidth(20);
		$objSheet->getColumnDimension('B')->setWidth(40);

		// Applying Style and Format
		$objSheet->getStyle("A1:A".$i)->applyFromArray($style);
		$objSheet->getStyle("A5:B".$i)->applyFromArray($styleArray);
		
	
		// Download Filenya		
		$filename = "summary-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		echo base_url()."dist/download/".$filename;
	}
}