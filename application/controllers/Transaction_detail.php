<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Transaction_detail extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "transactionAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Transaction_model");
		$this->load->model("Product_model");
		if ($this->input->get("d")==null) {
			$d = "totalTransaksi";
		} else {
			$d = $this->input->get("d");
		}


		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}

		if ($this->input->post("stats")==null) {
			$stats = "A";	
		}
		else{
			$stats = $this->input->post("stats");
		}

		if ($this->input->post("sd")==null) {
			$sd = date('Y-m-d');	
		}
		else{
			$sd = $this->input->post("sd");
		}

		if ($this->input->post("ed")==null) {
			$ed = date('Y-m-d', strtotime("+1 days"));	
		}
		else{
			$ed = $this->input->post("ed");
		}


		$data = array();
		$data['req'] = $d;
		$data["temp"] = $req;
		$data["sd"] = $sd;
		$data["ed"] = $ed;
		$data["stats"] = $stats;
		$data['view_branch'] = $branchList;
		$data['value'] = $this->Transaction_model->get_value($data["req"],"", $req,0,$stats, $sd, $ed);
		$data['total'] = $this->Transaction_model->get_total($data["req"],"", $req,0,$stats, $sd, $ed);
		$data['session'] = $this->get_auth($req);
		$this->load->view("Transaction_detail_view",$data);
	}

	public function debt(){
		$this->load->model("Transaction_model");
		$this->load->model("Product_model");
		if ($this->input->get("d")==null) {
			$d = "totalTransaksi";
		} else {
			$d = $this->input->get("d");
		}

		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		if ($this->input->post("stats")==null) {
			$stats = "A";	
		}
		else{
			$stats = $this->input->post("stats");
		}

		if ($this->input->post("sd")==null) {
			$sd = date('Y-m-d');	
		}
		else{
			$sd = $this->input->post("sd");
		}

		if ($this->input->post("ed")==null) {
			$ed = date('Y-m-d', strtotime("+1 days"));	
		}
		else{
			$ed = $this->input->post("ed");
		}

		$data = array();
		$data['req'] = $d;
		$data["temp"] = $req;
		$data["stats"] = $stats;
		$data["sd"] = $sd;
		$data["ed"] = $ed;
		$data['view_branch'] = $branchList;
		$data['value'] = $this->Transaction_model->get_value($data["req"],"", $req, 1, $stats, $sd, $ed);
		$data['total'] = $this->Transaction_model->get_total($data["req"],"", $req, 1, $stats, $sd, $ed);
		$this->load->view("Debt_view",$data);
	}

	function detail()
	{
		$this->load->model("Transaction_model");

		// Get Parameter
		$transID = $this->input->get("transID");
		$pos = $this->input->get("pos");

		$data = $this->Transaction_model->get_trans($transID, $pos);
		$detail = $this->Transaction_model->get_detail($transID, $pos);
		$data->detail = $detail;
		echo json_encode($data, JSON_NUMERIC_CHECK);
	}
}