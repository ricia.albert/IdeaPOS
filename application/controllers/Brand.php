<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Brand extends Custom_CI_Controller
{
  
  function __construct()
  {
    parent::__construct(true, "categoryAuth"); 
    $this->load->helper("form");
  }

  public function index(){
    $this->load->model("Brand_model");
    $branchList = $this->get_branch();
    if ($this->input->post("action")==null) {
      $req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;  
    }
    else{
      $req = $this->input->post("action");
    }
    $data = array();
    $data["temp"] = $req;
    $data['view_branch'] = $branchList;
    $data['view_product'] = $this->Brand_model->get_all_product($req);
    $data['session'] = $this->get_auth($req);

    //var_dump($data['view_product']);
    //exit();
    $this->load->view("Brand_view",$data);
  }

  public function add_brand_view(){
    $branch = $this->input->get("b");
    $data = array("branch" => $branch);
    $this->load->view("Add_brand_view", $data);
  }

  public function add_category(){
    $this->load->model("Brand_model");

    $msg="";
    $brandNm = $this->input->post("brandNm");
    $visible = $this->input->post("visible");
    $pos = $this->input->post("PosID");

    $dates = date("Y-m-d h:i:s");

    $data = array(
        'brandNm' => $brandNm,
        'last_update' => $dates,
        'visible' => $visible,
        'PosID' => $pos,
        'userID' => $this->session->staffID
      );

      $this->Brand_model->add($data);
      redirect(site_url("Brand"));
  }

  public function update(){
    $this->load->model("Brand_model");
    $data = array();

    $req = $this->input->get("menu");
    $pos = $this->input->get("pos");
    $data["req"] = $req;
    $data["pos"] = $pos;
    //$data["type"] = $this->category_model->get_type();
    $data["viewProduct"] = $this->Brand_model->get_product($req,$pos);

    $this->load->view("Add_brand_view",$data);
  }

  public function update_category(){
   $this->load->model("Brand_model");

    $msg="";
    $id = $this->input->post("catID");
    $brandNm = $this->input->post("brandNm");
    $visible = $this->input->post("visible");
    $pos = $this->input->post("pos");

    $dates = date("Y-m-d h:i:s");

    $data = array(
        'brandNm' => $brandNm,
        'last_update' => $dates,
        'visible' => $visible
      );

      //var_dump($data);
      //exit();
      $this->Brand_model->update($id,$pos,$data);
      redirect(site_url("Brand"));
  }

  public function delete(){
    $this->load->model("Brand_model");
    $req = $this->input->get("menu");
    $pos = $this->input->get("pos");

    $this->Brand_model->delete($req,$pos);
    redirect(site_url("Brand"));
  }


}