<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Package extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "productAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Package_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Package_model->get_all_product($req);

		$this->load->view("Package_view",$data);
	}

	public function add_package_view(){
		$this->load->model("Package_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$data["menuSub"] = $this->Package_model->get_all_menu($pos);
		$this->load->view("Add_package_view",$data);
	}

	public function update(){
		$this->load->model("Package_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$fromPos = $this->input->get("fromPos");
		$copy = $this->input->get("copy");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["copy"] = $copy;
		if ($copy == 1) {
			$data["posNm"] = $this->Package_model->get_branch($pos)->POSNm;
		}
		$data["menuChoosen"] = ($copy==1) ? array() : $this->Package_model->get_sub($req, $pos);
		$data["menuSub"] = $this->Package_model->get_all_menu($pos);
		$data["viewProduct"] = $this->Package_model->get_Product($req, ($copy==1) ? $fromPos : $pos);

		$this->load->view("Add_package_view",$data);
	}

	public function update_product(){
		$this->load->model("Package_model");
		$menuID = $this->input->post("req");
		$PosID = $this->input->post("pos");
		$packageCode = $this->input->post("packageCode");
		$nameMenu = $this->input->post("nameMenu");
		$Description = $this->input->post("Description");
		$totalPrice = $this->input->post("totalPrice");
		$menuPrice = $this->input->post("menuPrice");
		$menuVisible = $this->input->post("menuVisible");

		$dates = date("Y-m-d h:i:s");

			$data = array(
				"Packages_code"=>$packageCode,
				"Packages_name"=>$nameMenu,
				"Info"=>$Description,
				"Total_price"=>$totalPrice,
				"Packages_price"=>$menuPrice,
				"Availability"=>$menuVisible,
				"PosID" => $PosID
				);

		$this->Package_model->update($data,$menuID,$PosID);

		$this->Package_model->delete_sub($menuID,$PosID);
		$data1=array();

			$indexInsert = $this->input->post("indexInsert");
			//echo $indexInsert;
			for ($i=1; $i <= $indexInsert ; $i++) { 
				$data1=array();
				
				if ($this->input->post("addCode".$i)!=""||$this->input->post("addCode".$i)!=null) {
					$addCode=$this->input->post("addCode".$i);
					$data1["packagesID"] = $menuID;
					$data1["menuID"]=$addCode;
					//$data1["packagesID"]=$addCode;
					if ($this->input->post("addQty".$i)!=""||$this->input->post("addQty".$i)!=null) {
						$addQty=$this->input->post("addQty".$i);
						$data1["qty"]=$addQty;
					}
					$data1["PosID"] = $PosID;
					$this->Package_model->add_sub($data1);
				}
				
				
			}
			
		redirect(site_url("Package"));
	}

	public function add_package(){
		$this->load->model("Package_model");  //    
		//$userID = $this->input->post("userID");
		$PosID = $this->input->post("pos");
		$codeMenu = $this->input->post("packageCode");
		$nameMenu = $this->input->post("nameMenu");
		$Description = $this->input->post("Description");
		$totalPrice = $this->input->post("totalPrice");
		$menuPrice = $this->input->post("menuPrice");
		$menuVisible = $this->input->post("menuVisible");

		/*
		$value = $this->db->query("SELECT `packagesID` as value FROM `packages` ORDER BY `packagesID` DESC LIMIT 1")
						->row()->value;
		$code = substr($value, 0,2);
		$incrmt = substr($value, 2,8);
		*/
		$msg= "";
		if ( $codeMenu==null || $codeMenu == "") {
			$msg = "Harap mengisi Code terlebih dahulu.";
		}
		elseif ( $nameMenu==null || $nameMenu == "") {
			$msg = "Harap mengisi nama.";
		}
		elseif (is_int($menuPrice)) {
			$msg = "Harap mengisi harga dengan numeric.";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["menuSub"] = $this->Package_model->get_sub($menuID,$PosID);
			$this->load->view("Add_package_view", $data);
		}
		else{

			$menuID = $this->Package_model->get_ID($PosID);
			$dates = date("Y-m-d h:i:s");

			$data = array(
				"packagesID"=>$menuID,
				"Packages_code"=>$codeMenu,
				"Packages_name"=>$nameMenu,
				"Info"=>$Description,
				"Total_price"=>$totalPrice,
				"Packages_price"=>$menuPrice,
				"Availability"=>$menuVisible,
				"PosID" => $PosID
				);

			$this->Package_model->add($data);

			$data1=array();

			$indexInsert = $this->input->post("indexInsert");
			//echo $indexInsert;
			for ($i=1; $i <= $indexInsert ; $i++) { 
				$data1=array();
				
				if ($this->input->post("addCode".$i)!=""||$this->input->post("addCode".$i)!=null) {
					$addCode=$this->input->post("addCode".$i);
					$data1["packagesID"] = $menuID;
					$data1["menuID"]=$addCode;
					//$data1["packagesID"]=$addCode;
					if ($this->input->post("addQty".$i)!=""||$this->input->post("addQty".$i)!=null) {
						$addQty=$this->input->post("addQty".$i);
						$data1["qty"]=$addQty;
					}
					$data1["PosID"] = $PosID;
					$this->Package_model->add_sub($data1);
				}
				
				
			}
			
			redirect(site_url("Package"));
		}
	}
	public function delete(){
		$this->load->model("Package_model");
		$delete = $this->input->get("menu");
		$PosID = $this->input->get("pos");

		$this->Package_model->delete($delete,$PosID);
		$this->Package_model->delete_sub($delete,$PosID);
		redirect(site_url("Package"));
	}

	public function get_available_branch() {
		$this->load->model("Package_model");
		$codeMenu = $this->input->post("codeMenu");
		$pos = $this->Package_model->get_available_pos($codeMenu);
		$copy = $this->Package_model->get_copy_pos($codeMenu);
		echo json_encode($pos)."<-sp->".json_encode($copy);
	}
}