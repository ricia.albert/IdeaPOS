<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Branch extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Branch_model");
		$data = array();

		$data["view_branch"] = $this->Branch_model->get_all_branch();

		$this->load->view("Branch_view",$data);

	}

	public function Delete(){
		$this->load->model("Branch_model");
		$req = $this->input->get("d");
		$this->Branch_model->delete($req);
		$this->Branch_model->cleanEverything($req);

		redirect(site_url('Branch'));
	}

	public function Copy() {
		$this->load->model("Branch_model");

		// Get Parameter
		$from = $this->input->post("from");
		$to = $this->input->post("to");

		$this->Branch_model->copyDatabase($from, $to);
		echo 0;
	}

	public function Update(){
		$this->load->model("Branch_model");
		$req = $this->input->get("d");

		$data = array();
		$data["req"] = $this->input->get("d");
		$data["viewBranch"] = $this->Branch_model->get_all_branch_update($req);
		$this->load->view("Add_branch_view",$data);

	}

	public function Group() {
		$this->load->model("Branch_model");
		$req = $this->input->get("d");

		$data = array();
		$data["req"] = $this->input->get("d");
		$data["viewBranch"] = $this->Branch_model->get_all_branch_update($req);
		$data["branch"] = $this->Branch_model->get_all_branch();
		$data["branch_pos"] = $this->Branch_model->get_pos_transfer($data["req"]);
		$this->load->view("Group_view",$data);
	}

	public function addGroup() {
		$this->load->model("Branch_model");
		$POSID = $this->input->post("branchCode");
		$branchList = $this->Branch_model->get_all_branch();
		$pos_data = array();
		foreach ($branchList as $obj) {
			if ($this->input->post($obj->POSID)!= null) {
				$obj = array("POSID" => $POSID, "toPOSID" => $obj->POSID);
				array_push($pos_data, $obj);
			}
		}
		$this->Branch_model->delete_trf($POSID);
		$this->Branch_model->add_trf($pos_data);
		redirect(site_url("Branch"));
	}

	public function Reset() {
		$this->load->model("Branch_model");

		// Get Parameter
		$pos = $this->input->get("pos");

		$this->Branch_model->cleanEverything($pos);

		redirect(site_url("Branch"));
	}

	public function add_Branch_view(){
		$this->load->view("Add_branch_view");
	}
	public function add_branch(){     
		$this->load->model("Branch_model");
		$msg="";
		//$userID = $this->input->post("userID");
		$codeBranch = $this->input->post("codeBranch");
		$nameBranch = $this->input->post("nameBranch");
		$addressBranch = $this->input->post("addressBranch");
		$phoneBranch = $this->input->post("phoneBranch");
		$faxBranch = $this->input->post("faxBranch");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");
		$franchise = $this->input->post("franchise");
		$freestock = $this->input->post("freestock");
		$storage = $this->input->post("storage");

		$query = $this->db->select()
					->from("pos")
					->where("POSID",$codeBranch)
					->get();
		if ($query->num_rows()>0) {
			$msg = "Branch Code telah terdaftar.";
		}

		if ($codeBranch == "") {
				$msg = "Mohon code branch di isi.";
			}
			elseif ($nameBranch == "") {
				$msg = "Mohon nama Branch di isi.";
			}
			elseif ($addressBranch == "") {
				$msg = "Mohon Address/alamat di isi.";
			}
			elseif ($phoneBranch == "" && $faxBranch == "") {
				$msg = "Mohon isi antara phone atau fax.";
			}

			if ($msg != "") {
				$data = array("err" => $msg);
				$this->load->view("Add_branch_view", $data);
			}
			else {
				$dates = date("Y-m-d h:i:s");
				$data = array('POSID' => $codeBranch,
							'POSNm'=> $nameBranch,
							'Address'=> $addressBranch,
							'Telp'=>$phoneBranch,
							'Fax'=> $faxBranch,
							'latitude'=> $latitude,
							'longitude'=> $longitude,
							'last_update'=>$dates,
							'registered'=>$dates,
							"isFranchise" => ($franchise == null) ? 0 : $franchise,
							"isFree" => ($freestock == null) ? 0 : $freestock,
							"isGudang" => ($storage == null) ? 0 : $storage,
							'active'=> '1'
							);
				//var_dump($data);
				$this->Branch_model->add($data);

				redirect(site_url('Branch'));
			}
		
	}

	public function update_branch(){
		$this->load->model("Branch_model");
		$codeBranch = $this->input->post("codeBranch");
		$nameBranch = $this->input->post("nameBranch");
		$addressBranch = $this->input->post("addressBranch");
		$phoneBranch = $this->input->post("phoneBranch");
		$faxBranch = $this->input->post("faxBranch");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");
		$franchise = $this->input->post("franchise");
		$freestock = $this->input->post("freestock");
		$storage = $this->input->post("storage");

		$dates = date("Y-m-d HH:MM:SS");
		$data = array('POSID' => $codeBranch,
							'POSNm'=> $nameBranch,
							'Address'=> $addressBranch,
							'Telp'=>$phoneBranch,
							'Fax'=> $faxBranch,
							'latitude'=> $latitude,
							'longitude'=> $longitude,
							'last_update'=>$dates,
							"isFranchise" => ($franchise == null) ? 0 : $franchise,
							"isFree" => ($freestock == null) ? 0 : $freestock,
							"isGudang" => ($storage == null) ? 0 : $storage,
							'active'=> '1'
							);

		$this->Branch_model->update($data,$codeBranch);
		redirect(site_url('Branch'));
	}
}