<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Stock extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "stockAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Ingridients_model"); 
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}

		if ($this->input->post("sd")==null) {
			$sd = date('Y-m-d');	
		}
		else{
			$sd = $this->input->post("sd");
		}

		if ($this->input->post("ed")==null) {
			$ed = date('Y-m-d', strtotime("+1 days"));	
		}
		else{
			$ed = $this->input->post("ed");
		}


		$data = array();
		$data['req'] = $req;
		$data["temp"] = $req;
		$data["sd"] = $sd;
		$data["ed"] = $ed;
		$data['view_branch'] = $branchList;
		$stock = $this->Ingridients_model->get_all_product($req);
		$transfer_out = $this->Ingridients_model->get_transfer_out($req, $sd, $ed);
		$transfer_in = $this->Ingridients_model->get_transfer_in($req, $sd, $ed);
		$request_in = $this->Ingridients_model->get_request_in($req, $sd, $ed);
		$inventory_in = $this->Ingridients_model->get_inventory_in($req, $sd, $ed);
		$sales_stock = $this->Ingridients_model->get_sales_stock($req, $sd, $ed);

		$d = array();
		foreach ($stock as $key) {
			$key->transfer_out = (isset($transfer_out[$key->ingID])) ? $transfer_out[$key->ingID]->totalQty : 0;
			$key->transfer_in = (isset($transfer_in[$key->ingID])) ? $transfer_in[$key->ingID]->totalQty : 0;
			$key->request_in = (isset($request_in[$key->ingID])) ? $request_in[$key->ingID]->totalQty : 0;
			$key->inventory_in = (isset($inventory_in[$key->ingID])) ? $inventory_in[$key->ingID]->totalQty : 0;
			$key->sales_stock = (isset($sales_stock[$key->ingID])) ? $sales_stock[$key->ingID]->totalQty : 0;
			array_push($d, $key);
		}
		$data["value"] = $d;
		
		$this->load->view("Stock_view",$data);
	}
}