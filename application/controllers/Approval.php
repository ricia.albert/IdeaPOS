<?php
require_once "Custom_CI_Controller.php";

class Approval extends Custom_CI_Controller
{
	function __construct()
	{
		parent::__construct(true, "approvalAuth");
		$this->load->helper("form");
	}

	function index()
	{
		$this->load->model("Brand_model");
		$this->load->model("Approval_Model");
	    if ($this->input->post("action")==null) {
	      $req = "N";  
	    }
	    else{
	      $req = $this->input->post("action");
	    }
	    $data = array();
	    $data["temp"] = $req;
	    if ($req == "D") {
	    	$data['view_approval'] = $this->Approval_Model->getDiffStockRequest();
	    } else {
	    	$data['view_approval'] = $this->Approval_Model->getAllStockRequest($req);
		}
		$this->load->view("Approval_view", $data);
	}

	function price()
	{
		$this->load->model("Approval_Model");
		$this->Approval_Model->updateExpired();
		$branchList = $this->get_branch();
		if ($this->input->post("b")==null) {
			$b =  "";	
		}
		else{
			$b = $this->input->post("b");
		}

	    if ($this->input->post("action")==null) {
	      $req = "N";  
	    }
	    else{
	      $req = $this->input->post("action");
	    }
	    $data = array();
	    $data["b"] = $b;
	    $data["temp"] = $req;
	    $data['view_branch'] = $branchList;
		$data['view_approval'] = $this->Approval_Model->getAllPriceRequest($req, $b);
		$data['session'] = ($b == "") ? $this->session->userdata("approvalAuth") : $this->get_auth($b)->approvalAuth;
		
		$this->load->view("Price_view", $data);
	}

	function transfer()
	{
		$this->load->model("Approval_Model");
	    if ($this->input->post("action")==null) {
	      $req = "N";  
	    }
	    else{
	      $req = $this->input->post("action");
	    }
	    $data = array();
	    $data["temp"] = $req;
	    $data['view_approval'] = $this->Approval_Model->getAllTransferRequest($req);
		$this->load->view("Transfer_view", $data);
	}
}