<?php
require_once "Custom_CI_Controller.php";
/**
* 
*/
class Staff extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth");
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Staff_model");
		$branchList = $this->get_branch();
	    if ($this->input->post("action")==null) {
	      $req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID; 
	    }
	    else{
	      $req = $this->input->post("action");
	    }
	    $data = array();
	    $data["temp"] = $req;
	    $data['view_branch'] = $branchList;
	    $data['view_product'] = $this->Staff_model->get_all_product($req);
		$this->load->view("Staff_view",$data);
	}
	public function add_employee_view(){
		$this->load->model("Staff_model");
		$branch = $this->input->get("b");
		$data = array();
		$data["branch"] = $branch;
		$data["viewEmployee"] = $this->Staff_model->get_employee($branch);
		$this->load->view("Add_staff_view",$data);
	}

	public function Update(){
		$this->load->model("Staff_model");
		$data = array();

		$req = $this->input->get("d");
		$pos = $this->input->get("p");
		$data["req"] = $this->input->get("d");
		$data["viewUser"] = $this->Staff_model->get_product($req, $pos);
		$data["viewEmployee"] = $this->Staff_model->get_employee($pos);
		$data["auth"] = $this->Staff_model->get_auth($req, $pos);

		$this->load->view("Add_staff_view",$data);
	}

	public function update_user(){
		$this->load->model("Staff_model");
		$username = $this->input->post("username");
		$userID = $this->input->post("userID");
		$employeeID = $this->input->post("employeeID");
		$password = $this->input->post("password");
		$Active = $this->input->post("Active");
		$isExpired = $this->input->post("isExpired");
		$expired = $this->input->post("expired");
		$PosID = $this->input->post("PosID");
		$data = $this->input->post();
		$Admin_access = $this->isNull($data, "Admin_access", 0);
		$User_access = $this->isNull($data, "User_access", 0);
		$Trans_access = $this->isNull($data, "Trans_access", 0);
		$Add_User = $this->isNull($data, "Add_User", 0);
		$Edit_User = $this->isNull($data, "Edit_User", 0);
		$Delete_User = $this->isNull($data, "Delete_User", 0);
		$Cashier_Access = $this->isNull($data, "Cashier_Access", 0);
		$Menu_Access = $this->isNull($data, "Menu_Access", 0);
		$Add_Menu = $this->isNull($data, "Add_Menu", 0);
		$Edit_Menu = $this->isNull($data, "Edit_Menu", 0);
		$Delete_Menu = $this->isNull($data, "Delete_Menu", 0);
		$Stock_Access = $this->isNull($data, "Stock_Access", 0);
		$Add_Stock = $this->isNull($data, "Add_Stock", 0);
		$Edit_Stock = $this->isNull($data, "Edit_Stock", 0);
		$Delete_Stock = $this->isNull($data, "Delete_Stock", 0);
		$Guest_Access = $this->isNull($data, "Guest_Access", 0);
		$Add_Guest = $this->isNull($data, "Add_Guest", 0);
		$Edit_Guest = $this->isNull($data, "Edit_Guest", 0);
		$Delete_Guest = $this->isNull($data, "Delete_Guest", 0);
		$Hrd_Employee = $this->isNull($data, "Hrd_Employee", 0);
		$Report_Access = $this->isNull($data, "Report_Access", 0);
		$Setting_Access = $this->isNull($data, "Setting_Access", 0);
		$Category_Access = $this->isNull($data, "Category_Access", 0);
		$Opname_Access = $this->isNull($data, "Opname_Access", 0);
		$Add_Trans = $this->isNull($data, "Add_Trans", 0);
		$Edit_Trans = $this->isNull($data, "Edit_Trans", 0);
		$Cancel_Trans = $this->isNull($data, "Cancel_Trans", 0);
		$Finance_Access = $this->isNull($data, "Finance_Access", 0);
		$isExpired = $this->isNull($data, "isExpired", 0);

		$data = array(
				"Username"=>$username,
				"Pass"=>$password,
				"Active"=>$Active,
				"employeeID" => $employeeID,
				"Expired"=> $isExpired,
				"Expired_date"=>$expired,
				"PosID"=>$PosID
				);
		$auth = array(
						"userID" => $userID,
						"Admin_access" => $Admin_access,
						"User_access" => $User_access,
						"Trans_access" => $Trans_access,
						"Add_User" => $Add_User,
						"Edit_User" => $Edit_User,
						"Delete_User" => $Delete_User,
						"Cashier_Access" => $Cashier_Access,
						"Menu_Access" => $Menu_Access,
						"Add_Menu" => $Add_Menu,
						"Edit_Menu" => $Edit_Menu,
						"Delete_Menu" => $Delete_Menu,
						"Stock_Access" => $Stock_Access,
						"Add_Stock" => $Add_Stock,
						"Edit_Stock" => $Edit_Stock,
						"Delete_Stock" => $Delete_Stock,
						"Guest_Access" => $Guest_Access,
						"Add_Guest" => $Add_Guest,
						"Edit_Guest" => $Edit_Guest,
						"Delete_Guest" => $Delete_Guest,
						"Hrd_Employee" => $Hrd_Employee,
						"Report_Access" => $Report_Access,
						"Setting_Access" => $Setting_Access,
						"Category_Access" => $Category_Access,
						"Opname_Access" => $Opname_Access,
						"Add_Trans" => $Add_Trans,
						"Edit_Trans" => $Edit_Trans,
						"Cancel_Trans" => $Cancel_Trans,
						"Finance_Access" => $Finance_Access,
						"PosID"=>$PosID
					);

		$this->Staff_model->update($userID,$PosID,$data);
		$this->Staff_model->deleteAuth($userID, $PosID);
		$this->Staff_model->addAuth($auth);
			
		redirect(site_url("Staff"));
	}

	public function add_user(){
		$this->load->model("Staff_model");
		$username = $this->input->post("username");
		$employeeID = $this->input->post("employeeID");
		$password = $this->input->post("password");
		$Active = $this->input->post("Active");
		$isExpired = $this->input->post("isExpired");
		$expired = $this->input->post("expired");
		$PosID = $this->input->post("PosID");
		$data = $this->input->post();
		$Admin_access = $this->isNull($data, "Admin_access", 0);
		$User_access = $this->isNull($data, "User_access", 0);
		$Trans_access = $this->isNull($data, "Trans_access", 0);
		$Add_User = $this->isNull($data, "Add_User", 0);
		$Edit_User = $this->isNull($data, "Edit_User", 0);
		$Delete_User = $this->isNull($data, "Delete_User", 0);
		$Cashier_Access = $this->isNull($data, "Cashier_Access", 0);
		$Menu_Access = $this->isNull($data, "Menu_Access", 0);
		$Add_Menu = $this->isNull($data, "Add_Menu", 0);
		$Edit_Menu = $this->isNull($data, "Edit_Menu", 0);
		$Delete_Menu = $this->isNull($data, "Delete_Menu", 0);
		$Stock_Access = $this->isNull($data, "Stock_Access", 0);
		$Add_Stock = $this->isNull($data, "Add_Stock", 0);
		$Edit_Stock = $this->isNull($data, "Edit_Stock", 0);
		$Delete_Stock = $this->isNull($data, "Delete_Stock", 0);
		$Guest_Access = $this->isNull($data, "Guest_Access", 0);
		$Add_Guest = $this->isNull($data, "Add_Guest", 0);
		$Edit_Guest = $this->isNull($data, "Edit_Guest", 0);
		$Delete_Guest = $this->isNull($data, "Delete_Guest", 0);
		$Hrd_Employee = $this->isNull($data, "Hrd_Employee", 0);
		$Report_Access = $this->isNull($data, "Report_Access", 0);
		$Setting_Access = $this->isNull($data, "Setting_Access", 0);
		$Category_Access = $this->isNull($data, "Category_Access", 0);
		$Opname_Access = $this->isNull($data, "Opname_Access", 0);
		$Add_Trans = $this->isNull($data, "Add_Trans", 0);
		$Edit_Trans = $this->isNull($data, "Edit_Trans", 0);
		$Cancel_Trans = $this->isNull($data, "Cancel_Trans", 0);
		$Finance_Access = $this->isNull($data, "Finance_Access", 0);
		$isExpired = $this->isNull($data, "isExpired", 0);


		$value = $this->db->query("SELECT `userID` as value FROM `users` WHERE PosID='".$PosID."' ORDER BY `userID` DESC LIMIT 1")
						->row();
		$code = "US";

		if ($value == null)
			$incrmt = 0;
		else {
			$incrmt = substr($value->value, 3,8);
		}
		$incrmt += 1;

			if (strlen($incrmt)==1) {
				$incrmt = '0000000'.$incrmt;
			}
			else if (strlen($incrmt)==2) {
				$incrmt = '000000'.$incrmt;
			}
			else if (strlen($incrmt)==3) {
				$incrmt = '00000'.$incrmt;
			}
			else if (strlen($incrmt)==4) {
				$incrmt = '0000'.$incrmt;
			}
			else if (strlen($incrmt)==5) {
				$incrmt = '000'.$incrmt;
			}
			else if (strlen($incrmt)==6) {
				$incrmt = '00'.$incrmt;
			}
			else{
				$incrmt = '0'.$incrmt;
			}

			$userID = $code.$incrmt;


		$data = array(
				"userID"=>$userID,
				"employeeID"=>$employeeID,
				"Username"=>$username,
				"Pass"=>$password,
				"Active"=>$Active,
				"Expired"=> $isExpired,
				"Expired_date"=>$expired,
				"PosID"=>$PosID
				);
		$auth = array(
						"userID" => $userID,
						"Admin_access" => $Admin_access,
						"User_access" => $User_access,
						"Trans_access" => $Trans_access,
						"Add_User" => $Add_User,
						"Edit_User" => $Edit_User,
						"Delete_User" => $Delete_User,
						"Cashier_Access" => $Cashier_Access,
						"Menu_Access" => $Menu_Access,
						"Add_Menu" => $Add_Menu,
						"Edit_Menu" => $Edit_Menu,
						"Delete_Menu" => $Delete_Menu,
						"Stock_Access" => $Stock_Access,
						"Add_Stock" => $Add_Stock,
						"Edit_Stock" => $Edit_Stock,
						"Delete_Stock" => $Delete_Stock,
						"Guest_Access" => $Guest_Access,
						"Add_Guest" => $Add_Guest,
						"Edit_Guest" => $Edit_Guest,
						"Delete_Guest" => $Delete_Guest,
						"Hrd_Employee" => $Hrd_Employee,
						"Report_Access" => $Report_Access,
						"Setting_Access" => $Setting_Access,
						"Category_Access" => $Category_Access,
						"Opname_Access" => $Opname_Access,
						"Add_Trans" => $Add_Trans,
						"Edit_Trans" => $Edit_Trans,
						"Cancel_Trans" => $Cancel_Trans,
						"Finance_Access" => $Finance_Access,
						"PosID"=>$PosID
					);

		$this->Staff_model->add($data);
		$this->Staff_model->addAuth($auth);
			
		redirect(site_url("Staff"));
	}

	public function delete(){
		$this->load->model("Staff_model");
		$req = $this->input->get("staff");
		$PosID = $this->input->get("p");
		$this->Staff_model->delete($req, $PosID);
		$this->Staff_model->deleteAuth($req, $PosID);

		redirect(site_url("Staff"));
	}
}