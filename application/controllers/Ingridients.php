<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Ingridients extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "stockAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Ingridients_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Ingridients_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Ingridients_view",$data);
	}

	public function add_ingridients_view(){
		$this->load->model("Ingridients_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$data["category"] = $this->Ingridients_model->get_category($pos);
		$data["scalar"] = $this->Ingridients_model->get_scalar($pos);

		$this->load->view("Add_ingridients_view",$data);
	}

	public function add_ingridients(){
		$this->load->model("Ingridients_model");

		$msg="";

		$ingCode = $this->input->post("ingCode");
		$ingName = $this->input->post("ingName");
		$ingCat = $this->input->post("ingCat");
		$ingInfo = $this->input->post("ingInfo");
		$ingPrice = $this->input->post("ingPrice");
		$ingQty = $this->input->post("ingQty");
		$ingMin = $this->input->post("ingMin");
		$ingScalar = $this->input->post("ingScalar");
		$PosID = $this->input->post("pos");

		if ($ingCode == ""||$ingCode == null) {
			$msg = "Mohon isi code ingridients";
		}
		elseif ($ingName=="" || $ingName == null) {
			$msg = "Mohon isi nama ingridients";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["category"] = $this->Ingridients_model->get_category();
			$data["scalar"] = $this->Ingridients_model->get_scalar();
			$this->load->view("Add_ingridients_view", $data);
		}

		$dates = date("Y-m-d h:i:s");

		$data = array(
			'ingID' => $this->Ingridients_model->get_ID($PosID),
			'codeIng' => $ingCode,
			'cateIngID' => $ingCat,
			'ingName' => $ingName,
			'info' => $ingInfo,
			'qty' => $ingQty,
			'minQty' => $ingMin,
			'scalarID' => $ingScalar,
			'bprice' => $ingPrice, 
			'last_update' => $dates, 
			'userID' => $this->session->staffID,
			'PosID' => $PosID 
			);
		$this->Ingridients_model->add($data);
		redirect(site_url("Ingridients"));

	}

	public function update(){
		$this->load->model("Ingridients_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$fromPos = $this->input->get("fromPos");
		$copy = $this->input->get("copy");
		$data["req"] = $this->input->get("menu");
		$data["pos"] = $this->input->get("pos");
		$data["copy"] = $copy;
		if ($copy == 1) {
			$data["posNm"] = $this->Ingridients_model->get_branch($pos)->POSNm;
		}
		$data["category"] = $this->Ingridients_model->get_category($pos);
		$data["scalar"] = $this->Ingridients_model->get_scalar($pos);
		$data["viewProduct"] = $this->Ingridients_model->get_product($req, ($copy==1) ? $fromPos : $pos);

		$this->load->view("Add_ingridients_view",$data);
	}

	public function update_ingridients(){
		$this->load->model("Ingridients_model");

		$msg="";

		$ingID = $this->input->post("ingID");
		$ingCode = $this->input->post("ingCode");
		$ingName = $this->input->post("ingName");
		$ingCat = $this->input->post("ingCat");
		$ingInfo = $this->input->post("ingInfo");
		$ingPrice = $this->input->post("ingPrice");
		$ingQty = $this->input->post("ingQty");
		$ingMin = $this->input->post("ingMin");
		$ingScalar = $this->input->post("ingScalar");
		$PosID = $this->input->post("pos");

		if ($ingCode == ""||$ingCode == null) {
			$msg = "Mohon isi code ingridients";
		}
		elseif ($ingName=="" || $ingName == null) {
			$msg = "Mohon isi nama ingridients";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["category"] = $this->Ingridients_model->get_category();
			$data["scalar"] = $this->Ingridients_model->get_scalar();
			$this->load->view("Add_ingridients_view", $data);
		}

		$dates = date("Y-m-d h:i:s");

		$data = array(
			'codeIng' => $ingCode,
			'cateIngID' => $ingCat,
			'ingName' => $ingName,
			'info' => $ingInfo,
			'qty' => $ingQty,
			'minQty' => $ingMin,
			'scalarID' => $ingScalar,
			'bprice' => $ingPrice, 
			'last_update' => $dates
			);
		$this->Ingridients_model->update($ingID,$PosID,$data);
		redirect(site_url("Ingridients"));
	}

	public function delete(){
		$this->load->model("Ingridients_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Ingridients_model->delete($req,$pos);
		redirect(site_url("Ingridients"));
	}

	public function fast_update(){
		$this->load->model("Ingridients_model");
		$req = $this->input->get("menu");
		$ID = $this->input->get("ID");
		$PosID = $this->input->get("PosID");

		$this->Ingridients_model->fast_update($req,$ID,$PosID);
		redirect(site_url("Ingridients"));
	}

	public function get_available_branch() {
		$this->load->model("Ingridients_model");
		$codeMenu = $this->input->post("codeMenu");
		$pos = $this->Ingridients_model->get_available_pos($codeMenu);
		$copy = $this->Ingridients_model->get_copy_pos($codeMenu);
		echo json_encode($pos)."<-sp->".json_encode($copy);
	}
}