<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Product_distribution extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "productAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Supply_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Supply_model->get_all_supply($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Supply_view",$data);
	}
}