<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Shiftinout extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Shiftinout_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Shiftinout_model->get_all_product($req);

		//var_dump($data["view_product"]);
		//exit();

		$this->load->view("Shiftinout_view",$data);
	}

	public function add_detail_view(){
		$this->load->model("Shiftinout_model");
		$data = array();

		$data['view_menu'] = $this->Shiftinout_model->get_type();
		$this->load->view("Add_detail_view",$data);
	}

	public function add_category(){
		$this->load->model("Shiftinout_model");

		$msg="";
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
				'menuID' => $menuID,
				'Info' => $detailInfo,
				'Price' => $detailPrice,
				'last_update' => $dates,
				'userID' => $this->session->username
			);

			//var_dump($data);
			//exit();
			$this->Shiftinout_model->add($data);
			redirect(base_url()."product_detail_price","refresh");
	}

	public function update(){
		$this->load->model("Shiftinout_model");
		$data = array();

		$req = $this->input->get("menu");
		$data["req"] = $req;
		//$data["type"] = $this->category_model->get_type();
		$data['view_menu'] = $this->Shiftinout_model->get_type();
		$data["viewProduct"] = $this->Shiftinout_model->get_product($req);

		$this->load->view("add_detail_view",$data);
	}

	public function update_category(){
		$this->load->model("Shiftinout_model");
		$id = $this->input->post("catID");
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
				'menuID' => $menuID,
				'Info' => $detailInfo,
				'Price' => $detailPrice,
				'last_update' => $dates
			);

			//var_dump($data);
			//exit();
			$this->Shiftinout_model->update($id,$data);
			redirect(base_url()."product_detail_price","refresh");
	}

	public function delete(){
		$this->load->model("Shiftinout_model");
		$req = $this->input->get("menu");

		$this->Shiftinout_model->delete($req);
		redirect(base_url()."product_detail_price","refresh");
	}

	public function detail() {
		$this->load->model("Cashdrawer_model");
		$this->load->model("Shiftinout_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$data = array();
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["viewProduct"] = $this->Shiftinout_model->get_product($req, $pos);
		$data["cashDrawer"] = $this->Cashdrawer_model->get_cd($req, $pos);
		$this->load->view("Det_cashdrawer", $data);
	}
}