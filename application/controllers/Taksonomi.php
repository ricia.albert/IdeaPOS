<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Taksonomi extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "categoryAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Taksonomi_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Taksonomi_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Taksonomi_view",$data);
	}

	public function add_taksonomi_view(){
		$branch = $this->input->get("b");
		$data = array("branch" => $branch);
		$this->load->view("Add_taksonomi_view", $data);
	}

	public function add_category(){
		$this->load->model("Taksonomi_model");

		$msg="";
		$categoryCode = $this->input->post("categoryCode");
		$categoryName = $this->input->post("categoryName");
		$PosID = $this->input->post("PosID");
		//$req = $this->input->get("ID");
		$dates = date("Y-m-d h:i:s");

		$data = array(
				'typeCode' => $categoryCode,
				'typeText' => $categoryName,
				'PosID' => $PosID,
				'last_update' => $dates,
				'userID' => $this->session->staffID
			);

			//var_dump($data);
			//exit();
			$this->Taksonomi_model->add($data);
			redirect(site_url("Taksonomi"));
	}

	public function update(){
		$this->load->model("Taksonomi_model");
		$data = array();

		$req = $this->input->get("menu");
		$PosID = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $PosID;
		//$data["type"] = $this->category_model->get_type();
		$data["viewProduct"] = $this->Taksonomi_model->get_product($req, $PosID);

		$this->load->view("Add_taksonomi_view",$data);
	}

	public function update_category(){
		$this->load->model("Taksonomi_model");
		$categoryCode = $this->input->post("categoryCode");
		$categoryName = $this->input->post("categoryName");
		$id = $this->input->post("catID");
		$pos = $this->input->post("pos");
		//$req = $this->input->get("ID");

		$data = array(
				'typeCode' => $categoryCode,
				'typeText' => $categoryName,
			);

			//var_dump($data);
			//exit();
			$this->Taksonomi_model->update($id,$pos,$data);
			redirect(site_url("Taksonomi"));
	}

	public function delete(){
		$this->load->model("Taksonomi_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Taksonomi_model->delete($req,$pos);
		redirect(site_url("Taksonomi"));
	}


}