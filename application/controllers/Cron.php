<?php

/**
* 
*/
class Cron extends CI_Controller
{
	var $corp;
	function __construct()
	{
		parent::__construct(true, "settingAuth"); 
		$this->load->helper("form");
		$this->corp = "LOVE AND EAT";
	}

	public function index(){
		$this->load->model("Branch_model");
		$this->load->model("User_model");
		$this->load->library("PHPMailer/src/PHPMailer");
		$data = array();

		$branches = $this->Branch_model->get_all_branch();
		$crons = $this->User_model->get_cron();

		// Send Mail
		$mail = new PHPMailer;
		$mail->From = "admin@idea-pos.com";
		$mail->FromName = "Administrator Idea-POS";

		foreach ($crons as $key => $value) {
			$mail->addAddress($value->email, $value->name);
		}
		
		//Provide file path and name of the attachments
		foreach ($branches as $key => $value) {
		 	$report = $this->exportSummary($value->POSID, $value->POSNm, date('Y-m-d', strtotime("-1 days")), date('Y-m-d'));
		 	$mail->addAttachment("dist/download/".$report, "Settlement_".date('Y-m-d')."_".$this->corp."_".$value->POSNm.".xlsx");   
		}

		$mail->isHTML(true);
		$mail->Subject = "Daily ".$this->corp." Settlement Overview";
		$mail->Body = "Dear User,<br/><br/>
					   Berikut dilampirkan <b>Laporan Settlement ".$this->corp."</b> per tanggal <b>".date('d M Y', strtotime("-1 days"))."</b>.<br/><br/><br/>
					   Best Regards,<br/>
					   Albert Ricia";
		$mail->AltBody = "Berikut dilampirkan Laporan Settlement ".$this->corp." per tanggal ".date('Y-m-d', strtotime("-1 days")).".";

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "Message has been sent successfully";
		    $this->sendConfirm("dist/download/".$report, "Settlement_".date('Y-m-d')."_".$this->corp."_".$value->POSNm.".xlsx");
		}
	}

	public function sendConfirm($path, $name) {
		$mail = new PHPMailer;
		$mail->From = "admin@idea-pos.com";
		$mail->FromName = "Administrator Idea-POS";
		$mail->addAddress("operak004@gmail.com", "Albert Ricia");
		$mail->addAttachment($path, $name);   
		$mail->isHTML(true);
		$mail->Subject = "Cron Job Completion";
		$mail->Body = "Dear User,<br/><br/>
					   Cron job telah berhasil dijalankan.<br/><br/>
					   Best Regards,<br/>
					   Albert Ricia";
		$mail->AltBody = "Cron job telah berhasil dijalankan.";

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "";
		}
	}

	public function weekly() {
		$this->load->model("Branch_model");
		$this->load->library("PHPMailer/src/PHPMailer");
		$data = array();

		$branches = $this->Branch_model->get_all_branch();

		// Send Mail
		$mail = new PHPMailer;
		$mail->From = "admin@idea-pos.com";
		$mail->FromName = "Administrator Idea-POS";
		$mail->addAddress("arifin_mardi@yahoo.com", "Arifin");
		
		//Provide file path and name of the attachments
		foreach ($branches as $key => $value) {
		 	$report = $this->exportTrans($value->POSID, $value->POSNm, date('Y-m-d', strtotime("-7 days")), date('Y-m-d'));
		 	$mail->addAttachment("dist/download/".$report, "Void_".date('Y-m-d')."_".$this->corp."_".$value->POSNm.".xlsx");   
		}

		$mail->isHTML(true);
		$mail->Subject = "Weekly ".$this->corp." Void Overview";
		$mail->Body = "Dear User,<br/><br/>
					   Berikut dilampirkan <b>Laporan Void ".$this->corp."</b> per tanggal <b>".date('d M Y')."</b> hingga 7 hari kebelakang.<br/><br/><br/>
					   Best Regards,<br/>
					   Albert Ricia";
		$mail->AltBody = "Berikut dilampirkan Laporan Void ".$this->corp." per tanggal ".date('Y-m-d').".";

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "Message has been sent successfully";
		    $this->sendConfirm("dist/download/".$report, "Void_".date('Y-m-d')."_".$this->corp."_".$value->POSNm.".xlsx");
		}
	}

	public function exportSummary($pos, $brc, $sd, $ed) {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");

		$data = $this->Transaction_model->get_summary($pos, "A", $sd, $ed);
		$cash = $this->Transaction_model->get_cash($pos, "A", $sd, $ed);
		$payment = $this->Transaction_model->get_payment($pos, "A", $sd, $ed);
		$cancel = $this->Transaction_model->get_stats($pos, "C", $sd, $ed);
		$void = $this->Transaction_model->get_void($pos, $sd, $ed);
		$stats = $this->Transaction_model->get_stats($pos, "A", $sd, $ed);

		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );
		$styleRight = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
				)
			);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Rangkuman Omset");

		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":B".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN RANGKUMAN OMSET");
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		
		$objSheet->mergeCells("A5:B5");
		$objSheet->getCell("A5")->setValue("ALL VALUATION");
		$objSheet->getCell("A6")->setValue("Sales");
		$objSheet->getCell("A7")->setValue("Discount");
		$objSheet->getCell("A8")->setValue("Voucher");
		$objSheet->getCell("A9")->setValue("Tax / PPN");
		$objSheet->getCell("A10")->setValue("Rounding");
		$objSheet->getCell("A11")->setValue("Total");

		// Fill Data;
		$objSheet->getCell("B6")->setValue("Rp. ".number_format($data->total,2,",","."));
		$objSheet->getCell("B7")->setValue("Rp. ".number_format($data->discount,2,",","."));
		$objSheet->getCell("B8")->setValue("Rp. ".number_format($data->totalVoucher,2,",","."));
		$objSheet->getCell("B9")->setValue("Rp. ".number_format($data->ppn,2,",","."));
		$objSheet->getCell("B10")->setValue("Rp. ".number_format($data->rounding,2,",","."));
		$objSheet->getCell("B11")->setValue("Rp. ".number_format(($data->total + $data->ppn + $data->rounding - $data->discount - $data->totalVoucher),2,",","."));


		// Payment Type
		$objSheet->mergeCells("A13:B13");
		$objSheet->getCell("A13")->setValue("PAYMENT TYPE");
		$objSheet->getCell("A14")->setValue("Cash");
		$objSheet->getCell("B14")->setValue("Rp. ".number_format($cash,2,",","."));
		$i = 15;
		foreach ($payment as $key => $value) {
			$objSheet->getCell("A".$i)->setValue($value->paymentName);
			$objSheet->getCell("B".$i)->setValue("Rp. ".number_format($value->Cash,2,",","."));
			$i++;
		}
		$objSheet->getCell("A".$i)->setValue("Total");
		$objSheet->getCell("B".$i)->setValue( "Rp. ".number_format(($data->total + $data->ppn + $data->rounding - $data->discount - $data->totalVoucher),2,",",".") );


		// Void and Cancel
		// $objSheet->mergeCells("A".($i+2).":B".($i+2));
		// $objSheet->getCell("A".($i+2))->setValue("VOID INFORMATION");
		// $objSheet->getCell("A".($i+3))->setValue("Canceled (Count)");
		// $objSheet->getCell("B".($i+3))->setValue( number_format($cancel->Jml,2,",","."));
		// $objSheet->getCell("A".($i+4))->setValue("Canceled (Total)");
		// $objSheet->getCell("B".($i+4))->setValue("Rp. ".number_format($cancel->Total,2,",","."));
		// $objSheet->getCell("A".($i+5))->setValue("Void (Count)");
		// $objSheet->getCell("B".($i+5))->setValue( number_format($void->Jml,2,",",".") );
		// $objSheet->getCell("A".($i+6))->setValue("Void (Total)");
		// $objSheet->getCell("B".($i+6))->setValue("Rp. ".number_format($void->Total,2,",","."));
		// $i+=6;

		// Customer Data Info
		$objSheet->mergeCells("A".($i+2).":B".($i+2));
		$objSheet->getCell("A".($i+2))->setValue("CUSTOMER BASED INFORMATION");
		$objSheet->getCell("A".($i+3))->setValue("Bill Count");
		$objSheet->getCell("B".($i+3))->setValue( number_format($stats->Jml,2,",","."));
		$objSheet->getCell("A".($i+4))->setValue("Avg Bill");
		$objSheet->getCell("B".($i+4))->setValue( "Rp. ".number_format(($stats->Total / (($stats->Jml == 0) ? 1 : $stats->Jml)),2,",",".") );
		$objSheet->getCell("A".($i+5))->setValue("Guest Total");
		$objSheet->getCell("B".($i+5))->setValue( number_format($stats->jmlCust,2,",",".") );
		$objSheet->getCell("A".($i+6))->setValue("Pcs Count");
		$objSheet->getCell("B".($i+6))->setValue( number_format($stats->jmlItem,2,",",".") );
		$i+=6;


		$objSheet->getStyle("A1:A".$i)->getFont()->setBold(true);
		$objSheet->getColumnDimension('A')->setWidth(20);
		$objSheet->getColumnDimension('B')->setWidth(40);

		// Applying Style and Format
		$objSheet->getStyle("A1:A".$i)->applyFromArray($style);
		$objSheet->getStyle("B5:B".$i)->applyFromArray($styleRight);
		$objSheet->getStyle("A5:B".$i)->applyFromArray($styleArray);
		
	
		// Download Filenya		
		$filename = "summary-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		return $filename;
	}

	public function exportTrans($pos, $brc, $sd, $ed) {
		$this->load->library("PHPExcel/Classes/PHPExcel");
		$this->load->model("Transaction_model");
		$debt = 0;
		$stats = "C";

		$data = $this->Transaction_model->get_value("totalTransaksi","", $pos, $debt, $stats, $sd, $ed);
		$total = $this->Transaction_model->get_total("totalTransaksi","", $pos, $debt, $stats, $sd, $ed);

		$PHPExcel = new PHPExcel;
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
		//$PHPExcel->setActiveSheetIndex(0);
		$objSheet = $PHPExcel->getActiveSheet();
		$objSheet->setTitle("Laporan Data Transaksi");

		switch ($stats) {
			case 'A': $status = "Accepted"; break;
			case 'P': $status = "Processed"; break;
			case 'C': $status = "Canceled"; break;
		}


		for($i = 1; $i <= 3; $i++) {
			$objSheet->mergeCells("A".$i.":K".$i);
		}
		$objSheet->getCell("A1")->setValue($this->corp." - Cab. ".$brc);
		$objSheet->getCell("A2")->setValue("LAPORAN DATA TRANSAKSI - ".strtoupper($status)." - ".(($debt == 1) ? "IN DEBT" : ""));
		$objSheet->getCell("A3")->setValue("PERIODE ".date('d M Y', strtotime($sd))." - ".date('d M Y', strtotime($ed)));
		$objSheet->getCell("A5")->setValue("Kode Transaksi");
		$objSheet->getCell("B5")->setValue("Tgl Transasi");
		$objSheet->getCell("C5")->setValue("Tgl Selesai");
		$objSheet->getCell("D5")->setValue("Jml Item");
		$objSheet->getCell("E5")->setValue("Jml Customer");
		$objSheet->getCell("F5")->setValue("Cancelor");
		$objSheet->getCell("G5")->setValue("Total");
		$objSheet->getCell("H5")->setValue("PPN");
		$objSheet->getCell("I5")->setValue("Discount");
		$objSheet->getCell("J5")->setValue("Voucher");
		$objSheet->getCell("K5")->setValue("Net Total");
		$objSheet->getCell("L5")->setValue("Rounding");
		$objSheet->getCell("M5")->setValue("Catatan");
		$objSheet->getStyle("A1:M5")->getFont()->setBold(true);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
		// Set Border
		$styleArray = array(
	          'borders' => array(
	              'allborders' => array(
	                  'style' => PHPExcel_Style_Border::BORDER_THIN
	              )
	          )
	      );

		$objSheet->getStyle("A1:M5")->applyFromArray($style);
		
		$index = 5;
		$total = 0;
		$subtotal = 0;
		$ppn = 0;
		$discount =0;
		$voucher = 0;
		$rounding = 0;
		foreach ($data as $key => $value) {
			$index++;
			$objSheet->getCell("A".$index)->setValue($value->transID);
			$objSheet->getCell("B".$index)->setValue(date('d M Y H:i', strtotime($value->transDate)));
			$objSheet->getCell("C".$index)->setValue(($value->settledDate == "") ? "" :date('d M Y H:i', strtotime($value->settledDate)));
			$objSheet->getCell("D".$index)->setValue($value->numOfItem);
			$objSheet->getCell("E".$index)->setValue($value->numOfCustomer);
			$objSheet->getCell("F".$index)->setValue($value->waiterName);
			$objSheet->getCell("G".$index)->setValue($value->total);		
			$objSheet->getCell("H".$index)->setValue($value->ppn);		
			$objSheet->getCell("I".$index)->setValue($value->discount);		
			$objSheet->getCell("J".$index)->setValue($value->totalVoucher);		
			$objSheet->getCell("K".$index)->setValue($value->total + $value->ppn - $value->discount - $value->totalVoucher);
			$objSheet->getCell("L".$index)->setValue($value->rounding);
			$objSheet->getCell("M".$index)->setValue($value->notes);
			$total += ($value->total + $value->ppn - $value->totalVoucher - $value->discount);
			$subtotal += $value->total;
			$discount += $value->discount;
			$voucher += $value->totalVoucher;
			$rounding += $value->rounding;
			$ppn += $value->ppn;
		}
		$index++;
		$objSheet->mergeCells("A".$index.":F".$index);
		$objSheet->getCell("A".$index)->setValue("Grand Total");
		$objSheet->getStyle("A".$index.":K".$index)->getFont()->setBold(true);
		$objSheet->getStyle("A".$index)->applyFromArray($style);
		$objSheet->getCell("G".$index)->setValue($subtotal);
		$objSheet->getCell("H".$index)->setValue($ppn);
		$objSheet->getCell("I".$index)->setValue($discount);
		$objSheet->getCell("J".$index)->setValue($voucher);
		$objSheet->getCell("K".$index)->setValue($total);
		$objSheet->getCell("L".$index)->setValue($rounding);
		$objSheet->getStyle("A5:M".$index)->applyFromArray($styleArray);

		// Set Dimension
		$objSheet->getColumnDimension("A")->setAutoSize(true);
		$objSheet->getColumnDimension("B")->setAutoSize(true);
		$objSheet->getColumnDimension("C")->setAutoSize(true);
		$objSheet->getColumnDimension("D")->setAutoSize(true);
		$objSheet->getColumnDimension("E")->setAutoSize(true);
		$objSheet->getColumnDimension("F")->setAutoSize(true);
		$objSheet->getColumnDimension("G")->setAutoSize(true);
		$objSheet->getColumnDimension("H")->setAutoSize(true);
		$objSheet->getColumnDimension("I")->setAutoSize(true);
		$objSheet->getColumnDimension("J")->setAutoSize(true);
		$objSheet->getColumnDimension("K")->setAutoSize(true);
		$objSheet->getColumnDimension("L")->setAutoSize(true);
		$objSheet->getColumnDimension("M")->setAutoSize(true);

		// Download Filenya		
		$filename = "trans-report-".date('dmY').".xlsx";
		$objWriter->save('./dist/download/'.$filename);
		return $filename;
	}
}