<?php
require_once "Custom_CI_Controller.php";
/**
* 
*/
class Login extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(false); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->view("Login_view");
	}

	public function confirm(){
		$this->load->model("Login_model");
		$this->load->model("User_model");
		$username = $this->input->POST("username");
		$password = $this->input->post("password");
		$data = array();

		$req = $this->Login_model->check($username,$password);
		if ($req == true) {
			$user = $this->Login_model->get_user($username);
			$acc = $this->User_model->get_user_by_username($user->globaluser);
			$pos = $this->User_model->get_pos($user->userID);
			$auth = $this->User_model->get_all_auth_update($user->userID);
			$this->session->set_userdata("userID", $user->userID);
			$this->session->set_userdata("staffID", (($acc != null) ? $acc->userID : ""));
			$this->session->set_userdata("username", $username);
			$this->session->set_userdata("emp_name", $user->employee_name);
			$this->session->set_userdata("userAuth", $auth->userAuth);
			$this->session->set_userdata("approvalAuth", $auth->approvalAuth);
			$this->session->set_userdata("branchAuth", $auth->branchAuth);
			$this->session->set_userdata("requestAuth", $auth->requestAuth);
			$this->session->set_userdata("productAuth", $auth->productAuth);
			$this->session->set_userdata("stockAuth", $auth->stockAuth);
			$this->session->set_userdata("categoryAuth", $auth->categoryAuth);
			$this->session->set_userdata("customerAuth", $auth->customerAuth);
			$this->session->set_userdata("transactionAuth", $auth->transactionAuth);
			$this->session->set_userdata("reportAuth", $auth->reportAuth);
			$this->session->set_userdata("settingAuth", $auth->settingAuth);
			$this->session->set_userdata("pos", $pos);
			redirect(site_url("Home"));
		}
		else{
			$data["err"] = 'Wrong username or password';
			$this->load->view("Login_view",$data);
		}
	}
}