<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Additional_menu extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Additional_menu_model");
		if ($this->input->post("action")==null) {
			$req = "";	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data["name"] = $this->input->get("name");
		$data["menu"] = $this->input->get("menu");
		$data["pos"] = $this->input->get("pos");
		//$data['view_branch'] = $this->Additional_menu_model->get_branch();
		$data['view_product'] = $this->Additional_menu_model->get_all_product($data["menu"], $data["pos"]);

		$this->session->set_userdata("menu", $data["menu"]);
		$this->session->set_userdata("name", $data["name"]);
		$this->session->set_userdata("PosID", $data["pos"]);

		$this->load->view("Additional_menu_view",$data);
	}

	public function add_additional_view(){
		$this->load->model("Additional_menu_model");
		$data = array();

		$data["pos"] = $this->session->userdata("PosID");

		$data['view_menu'] = $this->Additional_menu_model->get_type($data["pos"]);
		$this->load->view("Add_additional_view",$data);
	}

	public function add_category(){
		$this->load->model("Additional_menu_model");

		$msg="";
		$additionalCode = $this->input->post("additionalCode");
		$specialPrice = $this->input->post("specialPrice");
		$useSpecial = $this->input->post("useSpecial");
		//$req = $this->input->get("ID");

		$data = array(
				'menuID' => $this->session->userdata("menu"),
				'toppingID' => $additionalCode,
				'specialPrice' => $specialPrice,
				'useDefaultPrice' => ($useSpecial == null) ? 0 :$useSpecial,
				'PosID' => $this->session->userdata("PosID")
			);

		//var_dump($data);
		//exit();
		$this->Additional_menu_model->add($data);
		redirect(site_url("Additional_menu?menu=".$this->session->userdata("menu")."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}

	public function update(){
		$this->load->model("Additional_menu_model");
		$data = array();

		$req = $this->input->get("menu");
		$data["req"] = $req;
		$data["pos"] = $this->session->userdata("PosID");
		//$data["type"] = $this->category_model->get_type();
		$data['view_menu'] = $this->Additional_menu_model->get_type($data["pos"]);
		$data["viewProduct"] = $this->Additional_menu_model->get_product($req, $data["pos"]);

		$this->load->view("Add_additional_view",$data);
	}

	public function update_category(){
		$this->load->model("Additional_menu_model");
		$id = $this->input->post("catID");
		$additionalCode = $this->input->post("additionalCode");
		$specialPrice = $this->input->post("specialPrice");
		$useSpecial = $this->input->post("useSpecial");
		$pos = $this->session->userdata("PosID");
		//$req = $this->input->get("ID");

		$data = array(
				'toppingID' => $additionalCode,
				'specialPrice' => $specialPrice,
				'useDefaultPrice' => ($useSpecial == null) ? 0 :$useSpecial
			);

		$this->Additional_menu_model->update($id,$pos,$data);
		redirect(site_url("Additional_menu?menu=".$this->session->userdata("menu")."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}

	public function delete(){
		$this->load->model("Additional_menu_model");
		$req = $this->input->get("menu");
		$pos = $this->session->userdata("PosID");

		$this->Additional_menu_model->delete($req,$pos);
		redirect(site_url("Additional_menu?menu=".$this->session->userdata("menu")."&pos=".$this->session->userdata("PosID")."&name=".$this->session->userdata("name")));
	}


}