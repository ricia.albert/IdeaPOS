<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Cashdrawer extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Cashdrawer_model");
		$branchList = $this->Cashdrawer_model->get_branch();
		if ($this->input->post("action")==null) {
			$req = $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $this->Cashdrawer_model->get_branch();
		$data['view_product'] = $this->Cashdrawer_model->get_all_product($req);

		//var_dump($data["view_product"]);
		//exit();

		$this->load->view("Cashdrawer_view",$data);
	}

	public function add_detail_view(){
		$this->load->model("Cashdrawer_model");
		$data = array();

		$data['view_menu'] = $this->Cashdrawer_model->get_type();
		$this->load->view("add_detail_view",$data);
	}

	public function add_category(){
		$this->load->model("Cashdrawer_model");

		$msg="";
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
				'menuID' => $menuID,
				'Info' => $detailInfo,
				'Price' => $detailPrice,
				'last_update' => $dates,
				'userID' => $this->session->username
			);

			//var_dump($data);
			//exit();
			$this->Cashdrawer_model->add($data);
			redirect(base_url()."product_detail_price","refresh");
	}

	public function update(){
		$this->load->model("Cashdrawer_model");
		$data = array();

		$req = $this->input->get("menu");
		$data["req"] = $req;
		//$data["type"] = $this->category_model->get_type();
		$data['view_menu'] = $this->Cashdrawer_model->get_type();
		$data["viewProduct"] = $this->Cashdrawer_model->get_product($req);

		$this->load->view("add_detail_view",$data);
	}

	public function update_category(){
		$this->load->model("Cashdrawer_model");
		$id = $this->input->post("catID");
		$menuID = $this->input->post("menuID");
		$detailInfo = $this->input->post("detailInfo");
		$detailPrice = $this->input->post("detailPrice");
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
				'menuID' => $menuID,
				'Info' => $detailInfo,
				'Price' => $detailPrice,
				'last_update' => $dates
			);

			//var_dump($data);
			//exit();
			$this->Cashdrawer_model->update($id,$data);
			redirect(base_url()."product_detail_price","refresh");
	}

	public function delete(){
		$this->load->model("Cashdrawer_model");
		$req = $this->input->get("menu");

		$this->Cashdrawer_model->delete($req);
		redirect(base_url()."product_detail_price","refresh");
	}


}


?>