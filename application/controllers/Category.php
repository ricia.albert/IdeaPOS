<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Category extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "categoryAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Category_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Category_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Category_view",$data);
	}

	public function add_category_view(){
		$this->load->model("Category_model");

		$branch = $this->input->get("b");
    
		$data=array();
		$data["type"] = $this->Category_model->get_type($branch);
		$data["branch"] =  $branch;
		$this->load->view("Add_category_view",$data);
	}

	public function add_category(){
		$this->load->model("Category_model");

		$msg="";

		$categoryName = $this->input->post("categoryName");
		$typeID = $this->input->post("typeID");
		$visible = $this->input->post("visible");
		$pos = $this->input->post("PosID");

		if ($categoryName == ""||$categoryName == null) {
			$msg = "Mohon isi code category";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["category"] = $this->Category_model->get_category();
			$data["scalar"] = $this->Category_model->get_scalar();
			$this->load->view("Add_category_view", $data);
		}

		$dates = date("Y-m-d h:i:s");

		$data = array(
			'typeID' => $typeID,
			'categoryNm' => $categoryName,
			'visible' => $visible,
			'last_update' => $dates,
			'PosID' => $pos,
			'userID' => $this->session->staffID 
			);
		$this->Category_model->add($data);
		redirect(site_url("Category"));

	}

	public function update(){
		$this->load->model("Category_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["type"] = $this->Category_model->get_type($pos);
		$data["viewProduct"] = $this->Category_model->get_product($req,$pos);

		$this->load->view("Add_category_view",$data);
	}

	public function update_category(){
		$this->load->model("Category_model");

		$msg="";

		$categoryName = $this->input->post("categoryName");
		$typeID = $this->input->post("typeID");
		$catID = $this->input->post("catID");
		$visible = $this->input->post("visible");
		$pos = $this->input->post("pos");

		if ($categoryName == ""||$categoryName == null) {
			$msg = "Mohon isi code category";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["category"] = $this->Category_model->get_category();
			$data["scalar"] = $this->Category_model->get_scalar();
			$this->load->view("Add_category_view", $data);
		}


		$dates = date("Y-m-d h:i:s");

		$data = array(
			'typeID' => $typeID,
			'categoryNm' => $categoryName,
			'visible' => $visible,
			'last_update' => $dates, 
			'userID' => $this->session->staffID 
			);
		$this->Category_model->update($catID,$pos,$data);
		redirect(site_url("Category"));
	}

	public function delete(){
		$this->load->model("Category_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$this->Category_model->delete($req,$pos);
		redirect(site_url("Category"));
	}


}