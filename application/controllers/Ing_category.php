<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Ing_category extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "categoryAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Ing_category_model");

		$branchList = $this->get_branch();
		$data = array();
		$data['view_branch'] = $branchList;
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data["temp"] = $req;
		$data['view_product'] = $this->Ing_category_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);
		
		$this->load->view("Ing_category_view",$data);
	}

	public function add_category(){
		$this->load->model("Ing_category_model");

		$msg="";
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		//$req = $this->input->get("ID");

		$value = $this->db->query("SELECT `cateIngID` as value FROM `ingcate` WHERE PosID='".$pos."' ORDER BY `cateIngID` DESC LIMIT 1")
						->row()->value;
		$code = substr($value, 0,2);
		$incrmt = substr($value, 2,8);

		$incrmt += 1;

			if (strlen($incrmt)==1) {
				$incrmt = '0000000'.$incrmt;
			}
			else if (strlen($incrmt)==2) {
				$incrmt = '000000'.$incrmt;
			}
			else if (strlen($incrmt)==3) {
				$incrmt = '00000'.$incrmt;
			}
			else if (strlen($incrmt)==4) {
				$incrmt = '0000'.$incrmt;
			}
			else if (strlen($incrmt)==5) {
				$incrmt = '000'.$incrmt;
			}
			else if (strlen($incrmt)==6) {
				$incrmt = '00'.$incrmt;
			}
			else if (strlen($incrmt)==7) {
				$incrmt = '0'.$incrmt;
			}
			else{
			}

			$userID = $code.$incrmt;
			$dates = date("Y-m-d h:i:s");

			$data = array(
				'cateIngID' => $userID,
				'ingCateName' => $req,
				'PosID' => $pos,
				'last_update' => $dates, 
				'userID' => $this->session->staffID 
			);

			$this->Ing_category_model->add($data);
			redirect(site_url("Ing_category"));

	}

	public function update_category(){
		$this->load->model("Ing_category_model");
		$req = $this->input->get("menu");
		$ID = $this->input->get("id");
		$pos = $this->input->get("pos");

		$dates = date("Y-m-d h:i:s");

		$data = array(
				'ingCateName' => $req,
				'last_update' => $dates
			);
		//var_dump($data);
		//exit();
		$this->Ing_category_model->update($ID,$pos,$data);
		redirect(site_url("Ing_category"));
	}

	public function delete(){
		$this->load->model("Ing_category_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Ing_category_model->delete($req,$pos);
		redirect(site_url("Ing_category"));
	}

}