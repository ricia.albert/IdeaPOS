<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Member extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "customerAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Member_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Member_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Member_view",$data);
	}

	public function add_member_view(){
		$this->load->model("Member_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$this->load->view("Add_member_view", $data);
	}
	// function Special price
	public function special(){
		$this->load->model("Member_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$data = array();
		//$data["custID"] = $req;
		$data["viewUser"] = $this->Member_model->get_member($req,$pos);
		$data["viewMenu"] = $this->Member_model->get_all_menu($req,$pos);
		$this->load->view("Special_price_view",$data);
	}

	public function add_special_view(){
		$this->load->model("Member_model");
		$req = $this->input->get("guestID");
		$pos = $this->input->get("pos");
		//echo $req;
		//exit();
		$data = array();
		$data["guestID"] = $req;
		$data["pos"] = $pos;
		$data["viewUser"] = $this->Member_model->get_special_member($req, $pos);
		//var_dump("viewUser");
		//exit();
		$data["viewMenu"] = $this->Member_model->get_true_menu($pos);
		$this->load->view("Add_special_view",$data);
	}

	public function add_special(){
		$this->load->model("Member_model");
		$guestID = $this->input->post("guestID");
		$menuID = $this->input->post("menuID");
		$specialPrice = $this->input->post("specialPrice");

		$PosID = $this->input->post("pos");

		$dates = date("Y-m-d h:i:s");

		

		//var_dump($data);
		//exit();
		if (!$this->Member_model->check_member_price($guestID, $menuID, $PosID)) {
			$data = array(
							"guestID" =>$guestID ,
							"menuID" =>$menuID ,
							"priceOffer" =>$specialPrice ,
							"last_update" =>$dates ,
							"userID" =>$this->session->staffID,
							"PosID" =>$PosID
						);
			$this->Member_model->add_special($data);
			redirect(site_url("Member/special?menu=".$guestID."&pos=".$PosID));
		} else {
			$data = array("err" => "There is already special price of this product.",	
						  "pos" => $PosID,
						  "guestID" => $guestID,
						  "viewUser" => $this->input->post("name"),
						  "viewMenu" => $this->Member_model->get_true_menu($PosID));
			$this->load->view("Add_special_view", $data);
		}
	}

	public function fast_update(){
		$this->load->model("Member_model");
		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$guest = $this->input->get("guest");
		$ID = $this->input->get("ID");

		//echo $req."+".$ID;exit();

		$this->Member_model->fast_update($req,$ID,$pos);
		redirect(site_url("Member/special?menu=".$guest."&pos=".$pos));
	}

	public function delete(){
		$this->load->model("Member_model");
		$delete = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Member_model->delete($delete, $pos);
		redirect(site_url("Member"));
	}

	//End

	public function delete_special(){
		$this->load->model("Member_model");
		$guest = $this->input->get("guest");
		$delete = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Member_model->delete_special($delete, $pos);
		redirect(site_url("Member/special?menu=".$guest."&pos=".$pos));
	}

	public function update(){
		$this->load->model("Member_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["viewUser"] = $this->Member_model->get_member($req, $pos);

		$this->load->view("Add_member_view",$data);
	}

	public function update_member(){
		$this->load->model("Member_model");
		$guestName = $this->input->post("guestName");
		$guestBorn = $this->input->post("guestBorn");
		$guestType = $this->input->post("guestType");
		$PosID = $this->input->post("pos");
		$guestAddress = $this->input->post("guestAddress");
		$guestPhone = $this->input->post("guestPhone");
		$guestEmail = $this->input->post("guestEmail");
		$statusGuest = $this->input->post("statusGuest");
		$gender = $this->input->post("gender");
		$guestID = $this->input->post("guestID");
		$guestLimit = $this->input->post("guestLimit");
		

		$dates = date("Y-m-d h:i:s");

		$data = array(
			"guestID"=>$guestID,
			"Guest_name"=>$guestName,
			"guest_type" => $guestType,
			"Address"=>$guestAddress,
			"Phone"=>$guestPhone,
			"Email"=>$guestEmail,
			"register_date"=>$dates,
			"Info"=>$statusGuest,
			"last_update"=>$dates,
			"limit_debt" => $guestLimit,
			"PosID" => $PosID
			);

		if ($guestType == "P") {
			$data["Sex"] = $gender;
			$data["Born"] = date('Y-m-d', strtotime($guestBorn));
		}

		$this->Member_model->update($data,$guestID,$PosID);

		redirect(site_url("Member"));
	}

	public function add_member(){
		$this->load->model("Member_model");
		$guestName = $this->input->post("guestName");
		$PosID = $this->input->post("pos");
		$guestType = $this->input->post("guestType");
		$guestBorn = $this->input->post("guestBorn");
		$guestAddress = $this->input->post("guestAddress");
		$guestPhone = $this->input->post("guestPhone");
		$guestEmail = $this->input->post("guestEmail");
		$statusGuest = $this->input->post("statusGuest");
		$gender = $this->input->post("gender");
		$guestLimit = $this->input->post("guestLimit");

		$msg= "";
		if ($guestName == ""||$guestName==null) {
			$msg = "Harap mengisi nama terlebih dahulu.";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["menuSub"] = $this->Member_model->get_sub();
			$this->load->view("Add_member_view", $data);
		}
		else{

			$value = $this->db->query("SELECT `guestID` as value FROM `guest_book` WHERE PosID=".$this->db->escape($PosID)." ORDER BY `guestID` DESC LIMIT 1")
						->row()->value;

			$guestCode = $value+1;

			$dates = date("Y-m-d h:i:s");

			$data = array(
				"guestID"=>$guestCode,
				"Guest_name"=>$guestName,
				"guest_type" => $guestType,
				"Address"=>$guestAddress,
				"Phone"=>$guestPhone,
				"Email"=>$guestEmail,
				"register_date"=>$dates,
				"Info"=>$statusGuest,
				"last_update"=>$dates,
				"limit_debt" => $guestLimit,
				"PosID" => $PosID
				);

			if ($guestType == "P") {
				$data["Sex"] = $gender;
				$data["Born"] = date('Y-m-d', strtotime($guestBorn));
			}

			$this->Member_model->add($data);
			
			redirect(site_url("Member"));
		}
	}
}