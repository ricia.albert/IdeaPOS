<?php

class Custom_CI_Controller extends CI_Controller
{
	function __construct($login = true, $session = "")
	{
		parent::__construct();

		if ($login) {
			if ($this->session->userdata("username") == "" || $this->session->userdata("username") == null) {
				redirect(site_url("login"));
			}
		} else {
			if ($this->session->userdata("username") != "" && $this->session->userdata("username") != null) {
				redirect(site_url("home"));
			}
		}

		if ($session != "") {
			if ($this->session->userdata($session) == AUTH_NONE) {
				redirect(site_url("home"));
			}
		}
	}

	public function isNull($x, $index, $default) {
		return (!isset($x[$index])) ? $default : $x[$index];
	}

	public function isNullData($data, $default) {
		return (!isset($data)) ? $default : $data;
	}

	public function get_branch() {
		$this->load->model("Branch_model");
		$pos = $this->session->userdata("pos");
		$branches = $this->Branch_model->get_branch($pos);
		return $branches;
	}

	public function get_storage() {
		$this->load->model("Branch_model");
		$pos = $this->session->userdata("pos");
		$branches = $this->Branch_model->get_storage($pos);
		return $branches;
	}

	public function get_non_storage() {
		$this->load->model("Branch_model");
		$pos = $this->session->userdata("pos");
		$branches = $this->Branch_model->get_non_storage($pos);
		return $branches;
	}

	public function get_auth($PosID) {
		$this->load->model("User_model");
		$userID = $this->session->userdata("userID");
		$auth = $this->User_model->get_pos_auth_branch($userID, $PosID);
		return $auth;
	}
}