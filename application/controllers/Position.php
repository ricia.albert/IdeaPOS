<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Position extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Position_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Position_model->get_all_product($req);

		$this->load->view("Position_view",$data);
	}

	public function add_position_view(){
		$this->load->model("Position_model");

		$branch = $this->input->get("b");
    
		$data=array();
		$data["pos"] = $branch;
		$this->load->view("Add_position_view",$data);
	}

	public function add_position(){
		$this->load->model("Position_model");

		$msg="";

		$positionName = $this->input->post("positionName");
		$info = $this->input->post("info");
		$pos = $this->input->post("PosID");

		if ($positionName == ""||$positionName == null) {
			$msg = "Mohon isi nama posisi";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$this->load->view("Add_position_view", $data);
		}

		$dates = date("Y-m-d h:i:s");

		$value = $this->db->query("SELECT `positionID` as value FROM `position` ORDER BY `positionID` DESC LIMIT 1")
						->row();
		$code = "PO";
		if ($value == null) 
			$incrmt = 0;
		else {
			$incrmt = substr($value->value, 3,8);
		}

		$incrmt += 1;
		if (strlen($incrmt)==1) {
			$incrmt = '0000000'.$incrmt;
		}
		else if (strlen($incrmt)==2) {
			$incrmt = '000000'.$incrmt;
		}
		else if (strlen($incrmt)==3) {
			$incrmt = '00000'.$incrmt;
		}
		else if (strlen($incrmt)==4) {
			$incrmt = '0000'.$incrmt;
		}
		else if (strlen($incrmt)==5) {
			$incrmt = '000'.$incrmt;
		}
		else if (strlen($incrmt)==6) {
			$incrmt = '00'.$incrmt;
		}
		else if (strlen($incrmt)==7) {
			$incrmt = '0'.$incrmt;
		}

		$positionID = $code.$incrmt;


		$data = array(
			'positionID' => $positionID,
			'position_name' => $positionName,
			'info' => $info,
			'PosID' => $pos
			);
		$this->Position_model->add($data);
		redirect(site_url("Position"));

	}

	public function update(){
		$this->load->model("Position_model");
		$data = array();

		$req = $this->input->get("req");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["viewProduct"] = $this->Position_model->get_product($req,$pos);

		$this->load->view("Add_position_view",$data);
	}

	public function update_position(){
		$this->load->model("Position_model");

		$msg="";

		$positionID = $this->input->post("positionID");
		$positionName = $this->input->post("positionName");
		$info = $this->input->post("info");
		$pos = $this->input->post("pos");

		if ($positionName == ""||$positionName == null) {
			$msg = "Mohon isi nama posisi";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["position"] = $this->Position_model->get_product($positionID, $pos);
			$this->load->view("Add_position_view", $data);
		}


		$dates = date("Y-m-d h:i:s");

		$data = array(
			'position_name' => $positionName,
			'info' => $info
			);
		$this->Position_model->update($positionID,$pos,$data);
		redirect(site_url("Position"));
	}

	public function delete(){
		$this->load->model("Position_model");
		$req = $this->input->get("position");
		$pos = $this->input->get("pos");
		$this->Position_model->delete($req,$pos);
		redirect(site_url("Position"));
	}


}