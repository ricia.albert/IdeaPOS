<?php
require_once "Custom_CI_Controller.php";

class Pettycash extends Custom_CI_Controller
{
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Pettycash_Model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		if ($this->input->post("sd")==null) {
			$sd = date('Y-m-d');	
		}
		else{
			$sd = $this->input->post("sd");
		}

		if ($this->input->post("ed")==null) {
			$ed = date('Y-m-d', strtotime("+1 days"));	
		}
		else{
			$ed = $this->input->post("ed");
		}

		$data = array();
		$data["temp"] = $req;
		$data["sd"] = $sd;
		$data["ed"] = $ed;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Pettycash_Model->get_all_product($req, $sd, $ed);

		//var_dump($data["view_product"]);
		//exit();

		$this->load->view("Pettycash_view",$data);
	}

	public function add_pettycash_view(){
		$this->load->model("Pettycash_Model");
		$pos = $this->input->get("pos");
		$data = array();
		$data["pos"] = $pos;
		$data['view_menu'] = $this->Pettycash_Model->get_type($pos);
		$this->load->view("Add_pettycash_view",$data);
	}

	public function add_pettycash(){
		$this->load->model("Pettycash_Model");

		$msg="";
		$pettyName = $this->input->post("pettyName");
		$pettyDate = $this->input->post("pettyDate");
		$pettyType = $this->input->post("pettyType");
		$pettyAmt = $this->input->post("pettyAmt");
		$description = $this->input->post("description");
		$PosID = $this->input->post('pos');
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
				'pettyName' => $pettyName,
				'pettyDate' => date('Y-m-d',strtotime($pettyDate)),
				'pettyTypeID' => $pettyType,
				'pettyAmt' => $pettyAmt,
				'pettyDescr' => $description,
				'created_date' => $dates,
				'PosID' => $PosID,
				'userID' => $this->session->staffID
			);

		$this->Pettycash_Model->add($data);
		redirect(site_url("Pettycash"));
	}

	public function update(){
		$this->load->model("Pettycash_Model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data['view_menu'] = $this->Pettycash_Model->get_type($pos);
		$data["viewProduct"] = $this->Pettycash_Model->get_pettycash($req, $pos);

		$this->load->view("Add_pettycash_view",$data);
	}

	public function update_pettycash(){
		$this->load->model("Pettycash_Model");
		$id = $this->input->post("req");
		$pettyName = $this->input->post("pettyName");
		$pettyDate = $this->input->post("pettyDate");
		$pettyType = $this->input->post("pettyType");
		$pettyAmt = $this->input->post("pettyAmt");
		$description = $this->input->post("description");
		$PosID = $this->input->post('pos');
		//$req = $this->input->get("ID");

		$dates = date("Y-m-d h:i:s");
		$data = array(
						'pettyName' => $pettyName,
						'pettyDate' => date('Y-m-d',strtotime($pettyDate)),
						'pettyTypeID' => $pettyType,
						'pettyAmt' => $pettyAmt,
						'pettyDescr' => $description,
						'created_date' => $dates,
						'userID' => $this->session->staffID
					);

		$this->Pettycash_Model->update($id, $PosID, $data);
		redirect(site_url("Pettycash"));
	}

	public function delete(){
		$this->load->model("Pettycash_Model");
		$req = $this->input->get("req");
		$pos = $this->input->get("pos");

		$this->Pettycash_Model->delete($req, $pos);
		redirect(site_url("Pettycash"));
	}
}