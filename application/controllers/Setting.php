<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Setting extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "settingAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Setting_model");
		$branchList = $this->get_branch();
		if ($this->input->get("brancOption")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->get("brancOption");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_setting'] = $this->Setting_model->get_setting($req);
		$data['session'] = $this->get_auth($req);
		
		$this->load->view("Setting_view",$data);
	}

	public function update(){
		$this->load->model("Setting_model");

		$msg="";
	
		$data = array();
		$data = $this->input->post();
		$pos = $this->input->post("pos");
		$data["Notif"] = $this->isNull($data, "Notif", "False");
		$data["signBlackList"] = $this->isNull($data, "signBlackList", 0);
		$data["e_ppn"] = $this->isNull($data, "e_ppn", "False");
		$data["affectStock"] = $this->isNull($data, "affectStock", 0);
		$data["StrictPolicy"] = $this->isNull($data, "StrictPolicy", 0);
		$data["vCutAfterTax"] = $this->isNull($data, "vCutAfterTax", 0);
		$data["canBargain"] = $this->isNull($data, "canBargain", 0);
		// $data["bargainConfirm"] = $this->isNull($data, "bargainConfirm", 0);
		$data["expireBargain"] = $this->isNull($data, "expireBargain", 0);
		$data["allowCredit"] = $this->isNull($data, "allowCredit", 0);
		$data["allowPriceOffer"] = $this->isNull($data, "allowPriceOffer", 0);
		$data["secondaryTab"] = $this->isNull($data, "secondaryTab", 0);
		$data["tersierTab"] = $this->isNull($data, "tersierTab", 0);
		$data["debtTab"] = $this->isNull($data, "debtTab", 0);
		$data["rounding"] = $this->isNull($data, "rounding", 0);
		unset($data["pos"]);
		$this->Setting_model->update($pos,$data);
		redirect(site_url("Setting?brancOption=".$pos));
	}
}