<?php
require_once "Custom_CI_Controller.php";
/**
* 
*/
class Employee extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth");
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Employee_model");
		$branchList = $this->get_branch();
	    if ($this->input->post("action")==null) {
	      $req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;
	    }
	    else{
	      $req = $this->input->post("action");
	    }
	    $data = array();
	    $data["temp"] = $req;
	    $data['view_branch'] = $branchList;
	    $data['view_product'] = $this->Employee_model->get_all_product($req);
		$this->load->view("Employee_view",$data);
	}
	public function add_employee_view(){
		$this->load->model("Employee_model");
		$branch = $this->input->get("b");
		$data = array();
		$data["branch"] = $branch;
		$data["position"] = $this->Employee_model->get_position($branch);
		$this->load->view("Add_employee_view", $data);
	}

	public function Update(){
		$this->load->model("Employee_model");
		$data = array();

		$req = $this->input->get("d");
		$pos = $this->input->get("p");
		$data["req"] = $this->input->get("d");
		$data["viewUser"] = $this->Employee_model->get_product($req, $pos);
		$data["position"] = $this->Employee_model->get_position($pos);
		$this->load->view("Add_employee_view",$data);
	}

	public function update_user(){
		$this->load->model("Employee_model");
		$employeeName = $this->input->post("employeeName");
		$employeeID = $this->input->post("employeeID");
		$employeePosition = $this->input->post("employeePosition");
		$employeeType = $this->input->post("employeeType");
		$employeeKTP = $this->input->post("employeeKTP");
		$employeeRek = $this->input->post("employeeRek");
		$employeeAddress = $this->input->post("employeeAddress");
		$employeeStatus = $this->input->post("employeeStatus");
		$gender = $this->input->post("gender");
		$employeePhone = $this->input->post("employeePhone");
		$work = $this->input->post("work");
		$end = $this->input->post("end");
		$PosID = $this->input->post("PosID");

		$data = array(
				"employee_name"=>$employeeName,
				"Position"=>$employeePosition,
				"Type"=>$employeeType,
				"No_KTP"=>$employeeKTP,
				"Path"=>"default",
				"Sex"=>$gender,
				"No_Rek"=>$employeeRek,
				"Status"=>$employeeStatus,
				"Address"=>$employeeAddress,
				"Phone"=>$employeePhone,
				"Work_date"=>$work,
				"End_date"=>$end,
				"PosID"=>$PosID
				);

		$this->Employee_model->update($employeeID,$PosID,$data);
			
		redirect(site_url("Employee"));
	}

	public function add_user(){
		$this->load->model("Employee_model");
		$employeeName = $this->input->post("employeeName");
		$employeeID = $this->input->post("employeeID");
		$employeePosition = $this->input->post("employeePosition");
		$employeeType = $this->input->post("employeeType");
		$employeeKTP = $this->input->post("employeeKTP");
		$employeeRek = $this->input->post("employeeRek");
		$employeeAddress = $this->input->post("employeeAddress");
		$employeeStatus = $this->input->post("employeeStatus");
		$gender = $this->input->post("gender");
		$employeePhone = $this->input->post("employeePhone");
		$work = $this->input->post("work");
		$end = $this->input->post("end");
		$PosID = $this->input->post("PosID");

		$value = $this->db->query("SELECT `employeeID` as value FROM `employee` WHERE PosID='".$PosID."' ORDER BY `employeeID` DESC LIMIT 1")
						->row();
		$code = "EP";
		if ($value == null) 
			$incrmt = 0;
		else {
			$incrmt = substr($value->value, 3,8);
		}
		$incrmt += 1;

			if (strlen($incrmt)==1) {
				$incrmt = '0000000'.$incrmt;
			}
			else if (strlen($incrmt)==2) {
				$incrmt = '000000'.$incrmt;
			}
			else if (strlen($incrmt)==3) {
				$incrmt = '00000'.$incrmt;
			}
			else if (strlen($incrmt)==4) {
				$incrmt = '0000'.$incrmt;
			}
			else if (strlen($incrmt)==5) {
				$incrmt = '000'.$incrmt;
			}
			else if (strlen($incrmt)==6) {
				$incrmt = '00'.$incrmt;
			}
			else{
				$incrmt = '0'.$incrmt;
			}

			$employeeID = $code.$incrmt;

		$data = array(
				"employeeID"=>$employeeID,
				"employee_name"=>$employeeName,
				"Position"=>$employeePosition,
				"Type"=>$employeeType,
				"No_KTP"=>$employeeKTP,
				"Path"=>"default",
				"Sex"=>$gender,
				"No_Rek"=>$employeeRek,
				"Status"=>$employeeStatus,
				"Address"=>$employeeAddress,
				"Phone"=>$employeePhone,
				"Work_date"=>$work,
				"End_date"=>$end,
				"PosID"=>$PosID
				);

		$this->Employee_model->add($data);
			
		redirect(site_url("Employee"));
	}

	public function Delete(){
		$this->load->model("Employee_model");
		$req = $this->input->get("d");
		$PosID = $this->input->get("p");
		$this->Employee_model->delete($req, $PosID);

		redirect(site_url("Employee"));
	}
}