<?php
require_once "Custom_CI_Controller.php";

/**
* 
*/
class Supplier extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "branchAuth"); 
		$this->load->helper("form");
	}

	public function index(){
		$this->load->model("Supplier_model");
		$branchList = $this->get_branch();
		if ($this->input->post("action")==null) {
			$req = (count($branchList) <= 0) ? "" : $branchList[0]->POSID;	
		}
		else{
			$req = $this->input->post("action");
		}
		$data = array();
		$data["temp"] = $req;
		$data['view_branch'] = $branchList;
		$data['view_product'] = $this->Supplier_model->get_all_product($req);
		$data['session'] = $this->get_auth($req);

		$this->load->view("Supplier_view",$data);
	}

	public function add_member_view(){
		$this->load->model("Supplier_model");
		$pos = $this->input->get("pos");
		$data=array();
		$data["pos"] = $pos;
		$this->load->view("Add_supplier_view", $data);
	}
	

	public function delete(){
		$this->load->model("Supplier_model");
		$delete = $this->input->get("menu");
		$pos = $this->input->get("pos");

		$this->Supplier_model->delete($delete, $pos);
		redirect(site_url("Supplier"));
	}

	//End

	public function update(){
		$this->load->model("Supplier_model");
		$data = array();

		$req = $this->input->get("menu");
		$pos = $this->input->get("pos");
		$data["req"] = $req;
		$data["pos"] = $pos;
		$data["viewUser"] = $this->Supplier_model->get_member($req, $pos);

		$this->load->view("Add_supplier_view",$data);
	}

	public function update_member(){
		$this->load->model("Supplier_model");
		$guestName = $this->input->post("guestName");
		$guestType = $this->input->post("guestType");
		$PosID = $this->input->post("pos");
		$guestAddress = $this->input->post("guestAddress");
		$guestPhone = $this->input->post("guestPhone");
		$guestEmail = $this->input->post("guestEmail");
		$active = $this->input->post("active");
		$blacklist = $this->input->post("blacklist");
		$info = $this->input->post("info");
		$guestID = $this->input->post("guestID");
		$cpName = $this->input->post("cpName");
		

		$dates = date("Y-m-d h:i:s");

		$data = array(
			"supplier_name"=>$guestName,
			"Type" => $guestType,
			"Address"=>$guestAddress,
			"Phone"=>$guestPhone,
			"CP_email"=>$guestEmail,
			"last_update"=>$dates,
			"Info"=>$info,
			"active"=>$active,
			"isBlacklist"=>$blacklist,
			"CP_name" => $cpName,
			"PosID" => $PosID
			);

		$this->Supplier_model->update($data,$guestID,$PosID);

		redirect(site_url("Supplier"));
	}

	public function add_member(){
		$this->load->model("Supplier_model");
		$guestName = $this->input->post("guestName");
		$guestType = $this->input->post("guestType");
		$PosID = $this->input->post("pos");
		$guestAddress = $this->input->post("guestAddress");
		$guestPhone = $this->input->post("guestPhone");
		$guestEmail = $this->input->post("guestEmail");
		$active = $this->input->post("active");
		$blacklist = $this->input->post("blacklist");
		$info = $this->input->post("info");
		$guestID = $this->input->post("guestID");
		$cpName = $this->input->post("cpName");

		$msg= "";
		if ($guestName == ""||$guestName==null) {
			$msg = "Harap mengisi nama terlebih dahulu.";
		}

		if ($msg != "") {
			$data = array("err" => $msg);
			$data["pos"] = $PosID;
			$this->load->view("Add_supplier_view", $data);
		}
		else{

			$value = $this->db->query("SELECT `ID_supplier` as value FROM `supplier` ORDER BY `ID_supplier` DESC LIMIT 1")
						->row()->value;
			$code = "SU";
			$incrmt = substr($value, 2,8);

			$incrmt += 1;
			//strval($incrmt);
			//echo "ini Code : ".$code." ini incr: ".strlen($incrmt);
			if (strlen($incrmt)==1) {
				$incrmt = '0000000'.$incrmt;
			}
			else if (strlen($incrmt)==2) {
				$incrmt = '000000'.$incrmt;
			}
			else if (strlen($incrmt)==3) {
				$incrmt = '00000'.$incrmt;
			}
			else if (strlen($incrmt)==4) {
				$incrmt = '0000'.$incrmt;
			}
			else if (strlen($incrmt)==5) {
				$incrmt = '000'.$incrmt;
			}
			else if (strlen($incrmt)==6) {
				$incrmt = '00'.$incrmt;
			}
			else if (strlen($incrmt)==7) {
				$incrmt = '0'.$incrmt;
			}

			$ID_supplier = $code.$incrmt;

			$dates = date("Y-m-d h:i:s");

			$data = array(
				"ID_supplier"=>$ID_supplier,
				"supplier_name"=>$guestName,
				"Type" => $guestType,
				"Address"=>$guestAddress,
				"Phone"=>$guestPhone,
				"CP_email"=>$guestEmail,
				"last_update"=>$dates,
				"Info"=>$info,
				"active"=>$active,
				"isBlacklist"=>$blacklist,
				"CP_name" => $cpName,
				"PosID" => $PosID
				);

			$this->Supplier_model->add($data);
			
			redirect(site_url("Supplier"));
		}
	}
}