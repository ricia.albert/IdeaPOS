<?php
require_once "Custom_CI_Controller.php";
/**
* 
*/
class User extends Custom_CI_Controller
{
	
	function __construct()
	{
		parent::__construct(true, "userAuth"); 
		$this->load->helper('form');
	}

	public function index(){
		$this->load->model("User_model");
		$data = array();

		$data["view_user"] = $this->User_model->get_all_user();

		$this->load->view("User_view",$data);
	}			

	public function Delete(){
		$this->load->model("User_model");
		$req = $this->input->get("d");
		$this->User_model->delete_user($req);

		redirect(site_url("User"));
	}

	public function Update(){
		$this->load->model("User_model");
		$data = array();

		$req = $this->input->get("d");
		$data["req"] = $this->input->get("d");
		$data["viewUser"] = $this->User_model->get_all_user_update($req);
		$data["viewAuth"] = $this->User_model->get_all_auth_update($req);
		$data["branch"] = $this->User_model->get_all_branch();
		$data["pos"] = $this->User_model->get_pos($req);
		$data["posAuth"] = $this->User_model->get_pos_auth($req);

		$this->load->view("Add_view_user",$data);
	}

	public function add_user_view(){
		$this->load->model("User_model");
		$data["branch"] = $this->User_model->get_all_branch();
		$this->load->view("Add_view_user.php", $data);	
	}
	
	public function add_user(){
		$this->load->model("User_model");
		//$userID = $this->input->post("userID");
		$nameUser = $this->input->post("nameUser");
		$globalUser = $this->input->post("globalUser");
		$addressUser = $this->input->post("addressUser");
		$phoneUser = $this->input->post("phoneUser");
		$emailUser = $this->input->post("emailUser");
		$username = $this->input->post("username");
		$password = sha1($this->input->post("password"));
		$gender = $this->input->post("gender");
		$position = $this->input->post("position");
		$userAuth = $this->input->post("userAuth");
		$branchAuth = $this->input->post("branchAuth");
		$categoryAuth = $this->input->post("categoryAuth");
		$stockAuth = $this->input->post("stockAuth");
		$productAuth = $this->input->post("productAuth");
		$transAuth = $this->input->post("transAuth");
		$settingAuth = $this->input->post("settingAuth");
		$customerAuth = $this->input->post("customerAuth");
		$reportAuth = $this->input->post("reportAuth");
		$requestAuth = $this->input->post("requestAuth");
		$approvalAuth = $this->input->post("approvalAuth");

		$branchList = $this->User_model->get_all_branch();
		$value = $this->db->query("SELECT `userID` as value FROM `user_web` ORDER BY `userID` DESC LIMIT 1")
						->row()->value;
		$code = substr($value, 0,3);
		$incrmt = substr($value, 3,8);

		$msg= "";
		if (strlen($nameUser) < 3) {
			$msg = "Harap mengisi nama dan harus mengandung minimal 3 karakter.";
		}
		elseif ($addressUser == "") {
			$msg = "Harap mengisi alamat.";
		}
		elseif ($phoneUser == "") {
			$msg = "Harap mengisi no telpon.";
		}
		elseif (!filter_var($emailUser,FILTER_VALIDATE_EMAIL)) {
			$msg = "Harap mengisi email sesuai dengan format.";
		}
		elseif ($username == "") {
			$msg = "Harap mengisi username";
		}
		elseif ($password == "" || strlen($password) < 6 ) {
			$msg = "Harap mengisi password dengan panjang lebih dari 6.";
		}

		if ($msg != "") {
			$data = array("err" => $msg,
						  "branch" => $this->User_model->get_all_branch());
			$this->load->view("Add_view_user", $data);
		}
		else{

			$incrmt += 1;

			if (strlen($incrmt)==1) {
				$incrmt = '000000'.$incrmt;
			}
			else if (strlen($incrmt)==2) {
				$incrmt = '00000'.$incrmt;
			}
			else if (strlen($incrmt)==3) {
				$incrmt = '0000'.$incrmt;
			}
			else if (strlen($incrmt)==4) {
				$incrmt = '000'.$incrmt;
			}
			else if (strlen($incrmt)==5) {
				$incrmt = '00'.$incrmt;
			}
			else if (strlen($incrmt)==6) {
				$incrmt = '0'.$incrmt;
			}
			else{
			}

			$userID = $code.$incrmt;

			$data = array(
				"userID"=>$userID,
				"username"=>$username,
				"password"=>$password,
				"globaluser" => $globalUser,
				"employee_name"=>$nameUser,
				"Position"=>$position,
				"Sex"=>$gender,
				"Email"=>$emailUser,
				"Address"=>$addressUser,
				"Phone"=>$phoneUser
				);
			$auth_data = array(
								"userID" => $userID,
								"userAuth" => $userAuth,
								"approvalAuth" => $approvalAuth,
								"branchAuth" => $branchAuth,
								"requestAuth" => $requestAuth,
								"productAuth" => $productAuth,
								"stockAuth" => $stockAuth,
								"categoryAuth" => $categoryAuth,
								"customerAuth" => $customerAuth,
								"transactionAuth" => $transAuth,
								"reportAuth" => $reportAuth,
								"settingAuth" => $settingAuth
								);
			$pos_data = array();
			foreach ($branchList as $obj) {
				if ($this->input->post($obj->POSID)!= null) {
					$categoryAuth = $this->input->post("categoryAuth_".$obj->POSID);
					$stockAuth = $this->input->post("stockAuth_".$obj->POSID);
					$productAuth = $this->input->post("productAuth_".$obj->POSID);
					$transAuth = $this->input->post("transAuth_".$obj->POSID);
					$settingAuth = $this->input->post("settingAuth_".$obj->POSID);
					$customerAuth = $this->input->post("customerAuth_".$obj->POSID);
					$reportAuth = $this->input->post("reportAuth_".$obj->POSID);
					$requestAuth = $this->input->post("requestAuth_".$obj->POSID);
					$approvalAuth = $this->input->post("approvalAuth_".$obj->POSID);

					$obj = array("userID" => $userID, 
								 "PosID" => $obj->POSID,
								 "categoryAuth" => $categoryAuth,
								 "settingAuth" => $settingAuth,
								 "stockAuth" => $stockAuth,
								 "productAuth" => $productAuth,
								 "transactionAuth" => $transAuth,
								 "settingAuth" => $settingAuth,
								 "approvalAuth" => $approvalAuth,
								 "requestAuth" => $requestAuth,
								 "reportAuth" => $reportAuth,
								 "customerAuth" => $customerAuth,
								 "created_date" => date('Y-m-d H:i'));
					array_push($pos_data, $obj);
				}
			}

			$this->User_model->add($data);
			$this->User_model->add_auth($auth_data);
			$this->User_model->add_pos($pos_data);
			
			redirect(site_url("User"));
		}
	}

	public function update_user(){
		$this->load->model("User_model");
		$userID = $this->input->post("userID");
		$nameUser = $this->input->post("nameUser");
		$globalUser = $this->input->post("globalUser");
		$addressUser = $this->input->post("addressUser");
		$phoneUser = $this->input->post("phoneUser");
		$emailUser = $this->input->post("emailUser");
		$username = $this->input->post("username");
		$gender = $this->input->post("gender");
		$position = $this->input->post("position");
		$userAuth = $this->input->post("userAuth");
		$branchAuth = $this->input->post("branchAuth");
		$categoryAuth = $this->input->post("categoryAuth");
		$stockAuth = $this->input->post("stockAuth");
		$productAuth = $this->input->post("productAuth");
		$transAuth = $this->input->post("transAuth");
		$settingAuth = $this->input->post("settingAuth");
		$customerAuth = $this->input->post("customerAuth");
		$reportAuth = $this->input->post("reportAuth");
		$requestAuth = $this->input->post("requestAuth");
		$approvalAuth = $this->input->post("approvalAuth");
		$branchList = $this->User_model->get_all_branch();

		$data = array(
				"username"=>$username,
				"globaluser" => $globalUser,
				"employee_name"=>$nameUser,
				"Position"=>$position,
				"Sex"=>$gender,
				"Email"=>$emailUser,
				"Address"=>$addressUser,
				"Phone"=>$phoneUser
				);
		$auth_data = array(
							"userAuth" => $userAuth,
							"approvalAuth" => $approvalAuth,
							"branchAuth" => $branchAuth,
							"requestAuth" => $requestAuth,
							"productAuth" => $productAuth,
							"stockAuth" => $stockAuth,
							"categoryAuth" => $categoryAuth,
							"customerAuth" => $customerAuth,
							"transactionAuth" => $transAuth,
							"reportAuth" => $reportAuth,
							"settingAuth" => $settingAuth
							);
		$pos_data = array();
		foreach ($branchList as $obj) {
			if ($this->input->post($obj->POSID)!= null) {
				$categoryAuth = $this->input->post("categoryAuth_".$obj->POSID);
				$stockAuth = $this->input->post("stockAuth_".$obj->POSID);
				$productAuth = $this->input->post("productAuth_".$obj->POSID);
				$transAuth = $this->input->post("transAuth_".$obj->POSID);
				$settingAuth = $this->input->post("settingAuth_".$obj->POSID);
				$customerAuth = $this->input->post("customerAuth_".$obj->POSID);
				$reportAuth = $this->input->post("reportAuth_".$obj->POSID);
				$requestAuth = $this->input->post("requestAuth_".$obj->POSID);
				$approvalAuth = $this->input->post("approvalAuth_".$obj->POSID);

				$obj = array("userID" => $userID, 
							 "PosID" => $obj->POSID,
							 "categoryAuth" => $categoryAuth,
							 "settingAuth" => $settingAuth,
							 "stockAuth" => $stockAuth,
							 "productAuth" => $productAuth,
							 "transactionAuth" => $transAuth,
							 "settingAuth" => $settingAuth,
							 "approvalAuth" => $approvalAuth,
							 "requestAuth" => $requestAuth,
							 "reportAuth" => $reportAuth,
							 "customerAuth" => $customerAuth,
							 "created_date" => date('Y-m-d H:i'));
				array_push($pos_data, $obj);
			}
		}
		$this->User_model->update($data,$userID);
		$this->User_model->update_auth($auth_data, $userID);
		$this->User_model->delete_pos($userID);
		$this->User_model->add_pos($pos_data);

		redirect(site_url("User"));
	}

	public function chg_pass() {
		$userID = $this->input->get("d");
		$data = array("userID" => $userID);
		$this->load->view("Change_pass_user", $data);
	}

	public function submitpass() {
        $this->load->model("User_model");

        // Get Parameter
        $userID = $this->input->post("userID");
        $newpass = $this->input->post("newpass");
        $confirm = $this->input->post("confirm");

        $data = array();
        $err = "";
        if ($newpass == "") {
            $err = "New password must be filled !";
        } else if ($confirm == "") {
            $err = "Confirmation must be filled !";
        } else if ($newpass != $confirm) {
            $err = "New password and confirmation do not match";
        } else {
            $col = array("password" => sha1($newpass));
            $this->User_model->update($col, $userID);
            redirect(site_url("User"));
        }
        if ($err != "") {
            $data["err"] = $err;
            $data["userID"] = $userID;
            $this->load->view("Change_pass_user", $data);
        }
    }
}