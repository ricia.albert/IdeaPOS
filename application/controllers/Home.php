<?php
require_once 'Custom_CI_Controller.php';

class Home extends Custom_CI_Controller{

	function __construct() { 
         parent::__construct();
    }

    public function logout(){
		$this->session->unset_userdata("username");
		redirect(site_url("Login"));
    }

    public function index() {
    	$this->load->model("Corp_Model");
    	$this->load->model("Approval_Model");
        $branchList = $this->get_branch();
    	$data = array();
    	$m = "";
        if ($this->input->get("t")==null) {
            $t = "day";  
        }
        else{
            $t = $this->input->get("t");
        }

        if ($this->input->get("action")==null) {
            $req = "";  
        }
        else{
            $req = $this->input->get("action");
        }

        if ($this->input->get("sd")==null) {
            $sd = date('Y-m-d');    
        }
        else{
            $sd = $this->input->get("sd");
        }

        if ($this->input->get("ed")==null) {
            $ed = date('Y-m-d', strtotime("+1 days"));  
        }
        else{
            $ed = $this->input->get("ed");
        }

    	$data["branch_count"] = $this->Corp_Model->get_branch_count();
    	$data["transaction_all"] = $this->Corp_Model->get_transaction_sum($m, $req, $sd, $ed);
    	$data["transaction_sum"] = $this->Corp_Model->get_trans_sccs($m, $req, $sd, $ed);
    	
    	$data["transaction_count"] = $this->Corp_Model->get_transaction_count($m, $req, $sd, $ed);
    	$data["member_count"] = $this->Corp_Model->get_member_count($m, $req, $sd, $ed);

    	$data["transaction_sccs"] = $this->Corp_Model->get_trans_sccs($m, $req, $sd, $ed);
    	$data["transaction_pending"] = $this->Corp_Model->get_transaction_pending($m, $req, $sd, $ed);
    	$data["transaction_cancel"] = $this->Corp_Model->get_transaction_cancel($m, $req, $sd, $ed);
        $data["transaction_debt"] = $this->Corp_Model->get_transaction_debt($m, $req, $sd, $ed);
        $data["transaction_limit"] = (($data["transaction_all"] == 0) ? 1 : $data["transaction_all"]);

		$data["trans_all"] = $this->Corp_Model->max_trans_all($m, $req, $sd, $ed);
		$data["total_cost"] = $this->Corp_Model->get_transaction_cost($m, $req, $sd, $ed);
		$data["param"] = $t;

		$data["product_tren"] = $this->Corp_Model->get_product_tren($m, $req, $sd, $ed);
		$data["guest"] = $this->Corp_Model->get_guest();
        $tr = $this->Corp_Model->get_monthly($t, $req, $sd, $ed);
		$data["monthly_trans"] = json_encode($tr["total"], JSON_NUMERIC_CHECK);
        $data["monthly_debt"] = json_encode($tr["debt"], JSON_NUMERIC_CHECK);

		$data["itemCharts"] = json_encode($this->Corp_Model->get_item_pie($m, $req, $sd, $ed),JSON_NUMERIC_CHECK);// kebalik antara item sama customer chart
		$data["customerCharts"] = json_encode($this->Corp_Model->get_customer_pie($m, $req, $sd, $ed),JSON_NUMERIC_CHECK);
		$data["ppn"] = json_encode($tr["ppn"],JSON_NUMERIC_CHECK);
		$data["branch"] = json_encode($this->Corp_Model->get_all_branch(),JSON_NUMERIC_CHECK);
		$data["total_visit"] = $this->Corp_Model->get_total_visit($m, $req, $sd, $ed);
		$data["total_omzet"] = $this->Corp_Model->get_total_omzet($m, $req, $sd, $ed);
		$data["total_profit"] = $this->Corp_Model->get_total_profit($m, $req, $sd, $ed);

		$data["request_stock"] = $this->Approval_Model->getStockRequest();
        $data['view_branch'] = $branchList;
        $data["temp"] = $req;
        $data["sd"] = $sd;
        $data["ed"] = $ed;


		//$data["branch_revenue"] = $this->Corp_Model->get_branch_revenue($POSID);
		//$data["branch_cost"] = $this->Corp_Model->get_branch_cost($POSID);
		//$data["branch_profit"] = $this->Corp_Model->get_branch_profit($POSID);

		$data['timeStamp']=json_encode($tr["time"]);

    	$this->load->view("Index", $data);

    } 

    public function user(){
    	$this->load->view("user");
    }

    public function get_info_branch()
    {
    	$this->load->model("Corp_Model");

		// Get Parameter
		$POSID = $this->input->post("POSID");
        $sd = $this->input->post("sd");
        $ed = $this->input->post("ed");
		//$cost = $this->Corp_Model->get_branch_cost("POSID");


		$rev = $this->Corp_Model->get_branch_revenue($POSID, $sd, $ed);
		$rev->total = number_format($rev->total,2,",",".");
		$rev->cost = number_format($rev->cost,2,",",".");
		//$profit = $rev->total - $rev->cost;
		$rev->profit = number_format($rev->profit,2,",",".");
		//$hpp = $this->Corp_Model->get_branch_cost($POSID);
    	echo json_encode($rev,JSON_NUMERIC_CHECK);
    }

    public function get_new_request()
    {
        $this->load->model("Approval_Model");

        // Get Data
        $approvals = $this->Approval_Model->getStockRequest();
        $json = json_encode($approvals);
        echo $json;
    }

    public function get_request()
    {
        $this->load->model("Approval_Model");

        // Get Parameter
        if ($this->input->get("s") != null) {
            $stats = $this->input->get("s");
        } else {
            $stats = Approval_Model::$STAT_NEW;
        }
        
        // Get Data
        if ($stats == "D") {
            $approvals = $this->Approval_Model->getDiffStockRequest();
        } else {
            $approvals = $this->Approval_Model->getAllStockRequest($stats);
        }
        $json = json_encode($approvals);
        echo $json;
    }

    public function get_transfer()
    {
        $this->load->model("Approval_Model");

        // Get Parameter
        if ($this->input->get("s") != null) {
            $stats = $this->input->get("s");
        } else {
            $stats = Approval_Model::$STAT_NEW;
        }
        
        // Get Data
        $approvals = $this->Approval_Model->getAllTransferRequest($stats);
        $json = json_encode($approvals);
        echo $json;
    }


    public function get_price()
    {
        $this->load->model("Approval_Model");
        $this->Approval_Model->updateExpired();
        // Get Parameter
        if ($this->input->get("s") != null) {
            $stats = $this->input->get("s");
        } else {
            $stats = Approval_Model::$STAT_NEW;
        }

        if ($this->input->get("p") != null) {
            $pos = $this->input->get("p");
        } else {
            $pos = "";
        }
        
        // Get Data
        $approvals = $this->Approval_Model->getAllPriceRequest($stats, $pos);
        $json = json_encode($approvals);
        echo $json;
    }

    public function view_request()
    {
    	$this->load->model("Approval_Model");

    	// Get Parameter
    	$approvalID = $this->input->post("approvalID");

    	$approval = $this->Approval_Model->viewTransferReq($approvalID);
    	echo json_encode($approval);
    }

    public function approve_request()
    {
    	$this->load->model("Approval_Model");

    	// Get Parameter
    	$approvalID = $this->input->post("approvalID");
    	$json = $this->input->post("json");
        $POSID = $this->input->post("POSID");

        $approval = json_decode($json);
        $this->Approval_Model->updateTransfer($approvalID, $approval, $POSID);
    }

    public function reject_request()
    {
        $this->load->model("Approval_Model");

        // Get Parameter
        $approvalID = $this->input->post("approvalID");
        $POSID = $this->input->post("POSID");

        $this->Approval_Model->rejectTransfer($approvalID, $POSID);
    }

    public function approve_price()
    {
        $this->load->model("Approval_Model");

        // Get Parameter
        $approvalID = $this->input->post("approvalID");
        $json = $this->input->post("json");
        $POSID = $this->input->post("POSID");

        $approval = json_decode($json);
        $this->Approval_Model->updatePrice($approvalID, $approval, $POSID);
    }

    public function reject_price()
    {
        $this->load->model("Approval_Model");

        // Get Parameter
        $approvalID = $this->input->post("approvalID");
        $POSID = $this->input->post("POSID");

        $this->Approval_Model->rejectPrice($approvalID, $POSID);
    }

    public function changepass(){
        $this->load->view("Change_pass_view");
    }

    public function submitpass() {
        $this->load->model("User_model");

        // Get Parameter
        $userID = $this->session->userdata("userID");
        $oldpass = $this->input->post("oldpass");
        $newpass = $this->input->post("newpass");
        $confirm = $this->input->post("confirm");

        $data = array();
        $err = "";
        $user = $this->User_model->get_all_user_id($userID);
        if ($oldpass == "") {
            $err = "Old password must be filled !";
        } else if ($newpass == "") {
            $err = "New password must be filled !";
        } else if ($confirm == "") {
            $err = "Confirmation must be filled !";
        } else if ($user->password != sha1($oldpass)) {
            $err = "Old password does not match!";
        } else if ($newpass != $confirm) {
            $err = "New password and confirmation do not match";
        } else {
            $col = array("password" => sha1($newpass));
            $this->User_model->update($col, $userID);
            $err = "Password has been changed successfully !";
        }
        if ($err != "") {
            $data["err"] = $err;
            $this->load->view("Change_pass_view", $data);
        }
    }
}