<?php

/**
* 
*/
class Cashdrawer_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req){
		$query = $this->db->query("SELECT * FROM `menupricelist` WHERE `menuPriceID`='".$req."'");
		return $query->result();
	}

	public function get_all_product($req){
		if ($req == "") {
			$query = $this->db->query("SELECT c.PosID,s.shiftDate,c.moneyNm,c.qty 
										FROM cashdrawer c JOIN shiftinout s on c.shiftID = s.shiftID AND c.PosID = s.PosID 
										ORDER by c.PosID,c.shiftID ");
		}
		else{
			$query = $this->db->query("SELECT c.PosID,s.shiftDate,c.moneyNm,c.qty 
										FROM cashdrawer c JOIN shiftinout s on c.shiftID = s.shiftID AND c.PosID = s.PosID
										WHERE c.PosID='".$req."' ORDER by c.PosID,c.shiftID ");
		}
		return $query->result();
	}

	public function get_cd($shiftID, $pos){
		$query = $this->db->query("SELECT c.PosID,s.shiftDate,c.moneyNm, value, c.qty, total
									FROM cashdrawer c JOIN shiftinout s on c.shiftID = s.shiftID AND c.PosID = s.PosID
									WHERE c.shiftID='".$shiftID."' AND c.PosID=".$this->db->escape($pos)." ORDER by c.PosID,c.shiftID ");
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `menu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("menupricelist",$data);
	}

	public function update($req,$data){
		$this->db->set($data);
		$this->db->where("menuPriceID",$req);
		$this->db->update("menupricelist");
	}

	public function delete($req){
		$this->db->where("menuPriceID",$req);
		$this->db->delete("menupricelist");
	}

}