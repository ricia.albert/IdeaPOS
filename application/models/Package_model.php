<?php

/**
* 
*/
class Package_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_Product($req, $pos){
		$query = $this->db->query("SELECT * FROM packages WHERE packagesID ='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_menu($pos){
		$query = $this->db->query("SELECT * FROM menu WHERE `visible`='1' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_available_pos($menuCode) {
		$query = $this->db->query("SELECT m.PosID, p.PosNm FROM packages AS m JOIN pos AS p ON m.PosID = p.PosID WHERE Packages_code=".$this->db->escape($menuCode));
		return $query->result();
	}

	public function get_copy_pos($menuCode) {
		$query = $this->db->query("SELECT PosID, PosNm FROM pos
									WHERE PosID NOT IN(SELECT PosID FROM packages
									WHERE Packages_code=".$this->db->escape($menuCode).")");
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM packages WHERE POSID = ".$this->db->escape($req));
		return $query->result();
	}
	public function get_ID($PosID){
		$query = $this->db->query("SELECT RIGHT(MAX(packagesID),8) AS ID FROM packages WHERE PosID=".$this->db->escape($PosID));
		$row = $query->row();
		$index = $row->ID + 1;
		return "PK".str_pad($index,8,"0",STR_PAD_LEFT);
	}
	public function get_sub($req, $pos){
		$query = $this->db->query("SELECT ps.`menuID`, m.`menuNm`,ps.`qty`,m.`price` FROM `packages_sub` ps 
									JOIN `menu` m ON ps.menuID = m.menuID AND m.PosID = ps.PosID
									WHERE ps.`packagesID`='".$req."' AND ps.PosID = ".$this->db->escape($pos));//SELECT `subCategoryID`,`subCategoryNm` FROM ``
		return $query->result();
	}
	public function add($data){
		$this->db->insert('packages',$data);
	}
	public function add_sub($data){
		$this->db->insert('packages_sub',$data);
	}
	public function update($data,$menuID,$PosID){
		$this->db->set($data);
		$this->db->where("packagesID",$menuID);
		$this->db->where("PosID", $PosID);
		$this->db->update("packages");
	}
	public function update_sub($data,$menuID,$PosID){
		$this->db->set($data);
		$this->db->where("packagesID",$menuID);
		$this->db->where("PosID", $PosID);
		$this->db->update("packages_sub");
	}
	public function delete($data,$PosID){
		$this->db->where("packagesID",$data);
		$this->db->where("PosID", $PosID);
		$this->db->delete("packages");
	}
	public function delete_sub($data,$PosID){
		$this->db->where("packagesID",$data);
		$this->db->where("PosID", $PosID);
		$this->db->delete("packages_sub");
	}
}