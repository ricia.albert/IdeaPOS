<?php

/**
* 
*/
class Setting_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_setting($pos){
		$query = $this->db->query("SELECT * FROM `settings` WHERE PosID=".$this->db->escape($pos));
		return $query->row();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function update($pos,$data){
		$this->db->set($data);
		$this->db->where("PosID",$pos);
		$this->db->update("settings");
	}
}