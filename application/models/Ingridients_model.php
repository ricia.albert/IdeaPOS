<?php

/**
* 
*/
class Ingridients_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $pos){
		$query = $this->db->query("SELECT * FROM `ingridients` WHERE `ingID`='".$req."' AND PosID='".$pos."'");
		return $query->result();
	}

	public function get_product_query($req, $pos){
		$query = $this->db->query("SELECT * FROM `ingridients` WHERE `ingID`='".$req."' AND PosID='".$pos."'");
		return $query->row();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_stock_opname($pos, $tgl){
		$query = $this->db->query("SELECT opnamedetID, sod.opnameID, sod.ingID, i.codeIng, i.ingName, sod.jml_temp, sod.scalarID_temp, sc1.scalarNm AS scalarTemp, sc1.ratio AS ratioTemp, sod.jml_actual, sod.scalarID_actual
									, sc.scalarNm AS scalarActual,sc.ratio AS ratioActual, sod.jml_barang, sod.scalarID_barang, sc2.scalarNm AS scalarBarang, 
									sc2.ratio AS ratioBarang, COALESCE(sod.price,0) AS bprice, sod.last_update, sod.userID FROM stockopnamedetail AS sod 
									JOIN stockopname AS sop ON sod.opnameID = sop.opnameID AND sod.PosID = sop.PosID
									JOIN ingridients AS i ON sod.ingID = i.ingID AND sod.PosID = i.PosID
									JOIN scalar AS sc1 ON sod.scalarID_temp = sc1.scalarID AND sod.PosID = sc1.PosID
									JOIN scalar AS sc ON sod.scalarID_actual = sc.scalarID AND sod.PosID = sc.PosID
									JOIN scalar AS sc2 ON sod.scalarID_barang = sc2.scalarID AND sod.PosID = sc2.PosID
									WHERE sop.posID =".$this->db->escape($pos)." AND sop.opnameDate='".date('Y-m-d', strtotime($tgl))."'");
		return $query->result();
	}

	public function get_all_product($req){
		if ($req == "") {
			$query = $this->db->query("SELECT i.ingID,ic.ingCateName,i.ingName,i.info,i.qty,i.minQty,s.scalarNm,i.bprice,i.PosID, i.codeIng 
									FROM ingridients i 
									LEFT JOIN scalar s on i.scalarID = s.scalarID AND i.PosID = s.PosID 
									LEFT JOIN ingcate ic ON i.cateIngID = ic.cateIngID AND i.PosID = ic.PosID");
		} else {
			$query = $this->db->query("SELECT i.ingID,ic.ingCateName,i.ingName,i.info,i.qty,i.minQty,s.scalarNm,i.bprice,i.PosID, i.codeIng 
									FROM ingridients i 
									LEFT JOIN scalar s on i.scalarID = s.scalarID AND i.PosID = s.PosID 
									LEFT JOIN ingcate ic ON i.cateIngID = ic.cateIngID AND i.PosID = ic.PosID
									WHERE i.PosID=".$this->db->escape($req));
		}
		return $query->result();
	}

	public function get_available_pos($menuCode) {
		$query = $this->db->query("SELECT m.PosID, p.PosNm FROM ingridients AS m JOIN pos AS p ON m.PosID = p.PosID WHERE codeIng=".$this->db->escape($menuCode));
		return $query->result();
	}

	public function get_copy_pos($menuCode) {
		$query = $this->db->query("SELECT PosID, PosNm FROM pos
									WHERE PosID NOT IN(SELECT PosID FROM ingridients
									WHERE codeIng=".$this->db->escape($menuCode).")");
		return $query->result();
	}

	public function get_ID($PosID){
		$query = $this->db->query("SELECT RIGHT(MAX(ingID),12) AS ID FROM ingridients WHERE PosID=".$this->db->escape($PosID));
		$row = $query->row();
		$index = $row->ID + 1;
		return "IG".str_pad($index,12,"0",STR_PAD_LEFT);
	}


	public function get_category($pos){
		$query = $this->db->query("SELECT distinct cateIngID, ingCateName FROM `ingcate` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_scalar($pos){
		$query = $this->db->query("SELECT distinct scalarID, scalarNm, ratioID FROM `scalar` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}


	// Mutation Stock
	public function get_transfer_out($PosID, $sd, $ed) {
		$sql = "SELECT td.ingID, i.ingName, SUM(td.qty_real * (s2.ratio / s.ratio)) AS totalQty, i.scalarID, s.scalarNm from transferdetail as td 
				join transfer as tr on td.transferID = tr.transferID AND td.PosID = tr.PosID
				join approval as a on tr.transferID = a.fID AND a.PosID = tr.PosID
				join ingridients as i on td.ingID = i.ingID AND td.PosID = i.PosID
				join scalar as s on i.scalarID = s.scalarID AND i.PosID = s.PosID
				join scalar as s2 on td.scalarID = s2.scalarID AND td.PosID = s2.PosID
				WHERE a.approvalStats='V' AND  tr.PosID = ".$this->db->escape($PosID)." AND tr.transferDt BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
				GROUP BY td.ingID";
		$query = $this->db->query($sql);
		$res = $query->result();

		$data = array();
		foreach ($res as $r) :
			$data[$r->ingID] = $r; 
		endforeach;
		return $data;
	}

	public function get_transfer_in($PosID, $sd, $ed) {
		$sql = "SELECT td.ingID, i.ingName, SUM(td.qty_real * (s2.ratio / s.ratio)) AS totalQty, i.scalarID, s.scalarNm, td.PosID from transferdetail as td 
				join transfer as tr on td.transferID = tr.transferID AND td.PosID = tr.PosID
				join approval as a on tr.transferID = a.fID AND a.PosID = tr.PosID
				join ingridients as i on td.ingID = i.ingID AND td.PosID = i.PosID
				join scalar as s on i.scalarID = s.scalarID AND i.PosID = s.PosID
				join scalar as s2 on td.scalarID = s2.scalarID AND td.PosID = s2.PosID
				WHERE a.approvalStats='V' AND a.approvalType='T' AND  tr.toPOSID = ".$this->db->escape($PosID)." AND tr.transferDt BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
				GROUP BY td.ingID";
		$query = $this->db->query($sql);
		$res = $query->result();

		$data = array();
		foreach ($res as $r) :
			$data[$r->ingID] = $r; 
		endforeach;
		return $data;
	}

	public function get_request_in($PosID, $sd, $ed) {
		$sql = "SELECT td.ingID, i.ingName, SUM(td.qty_real * (s2.ratio / s.ratio)) AS totalQty, i.scalarID, s.scalarNm, td.PosID from transferdetail as td 
				join transfer as tr on td.transferID = tr.transferID AND td.PosID = tr.PosID
				join approval as a on tr.transferID = a.fID AND a.PosID = tr.PosID
				join ingridients as i on td.ingID = i.ingID AND td.PosID = i.PosID
				join scalar as s on i.scalarID = s.scalarID AND i.PosID = s.PosID
				join scalar as s2 on td.scalarID = s2.scalarID AND td.PosID = s2.PosID
				WHERE a.approvalStats='V' AND a.approvalType='S' AND  tr.toPOSID = ".$this->db->escape($PosID)." AND tr.transferDt BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
				GROUP BY td.ingID";
		$query = $this->db->query($sql);
		$res = $query->result();

		$data = array();
		foreach ($res as $r) :
			$data[$r->ingID] = $r; 
		endforeach;
		return $data;
	}

	public function get_inventory_in($PosID, $sd, $ed) {
		$sql = "SELECT i.ingID, sp.product_name, SUM(sp.ammount * (s2.ratio / s.ratio)) AS totalQty, sp.Scalar from stock_payment AS sp
				JOIN stock_check_payment AS scp ON scp.ID_check_payment = sp.ID_check_payment AND scp.PosID = sp.PosID
				JOIN ingridients AS i ON sp.product_name= i.ingName AND scp.PosID = i.PosID
				JOIN scalar as s on i.scalarID = s.scalarID AND i.PosID = s.PosID
				JOIN scalar as s2 on sp.scalar = s2.scalarID AND sp.PosID = s2.PosID
				WHERE scp.PosID = ".$this->db->escape($PosID)." AND scp.receive_date BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
				GROUP BY sp.product_name, sp.Scalar";
		$query = $this->db->query($sql);
		$res = $query->result();

		$data = array();
		foreach ($res as $r) :
			$data[$r->ingID] = $r; 
		endforeach;
		return $data;
	}
	
	public function get_sales_stock($PosID, $sd, $ed) {
		$sql = "SELECT i.ingID, i.ingName, sum((mi.qty * tod.qty) * (s2.ratio / s.ratio)) AS totalQty, i.scalarID, s.scalarNm, tod.PosID from transorderdetail as tod
				join transorder as tor on tod.transID = tor.transID AND tod.PosID = tor.PosID
				join menuing as mi on tod.menuID = mi.menuID AND tod.PosID = mi.PosID
				join ingridients as i on mi.ingID = i.ingID AND i.PosID = mi.PosID
				join scalar as s on i.scalarID = s.scalarID AND i.PosID = s.PosID
				join scalar as s2 on mi.scalarID = s2.scalarID AND i.PosID = s.PosID
				WHERE tor.PosID = ".$this->db->escape($PosID)." AND tor.transStatus='A' AND tor.settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
				GROUP BY i.ingID";
		$query = $this->db->query($sql);
		$res = $query->result();

		$data = array();
		foreach ($res as $r) :
			$data[$r->ingID] = $r; 
		endforeach;
		return $data;
	}



/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("ingridients",$data);
	}

	public function addHist($id, $pos, $userID){
		$ing = $this->get_product_query($id, $pos);
		$data = array("ingID" => $id,
					  "PosID" => $pos,
					  "codeIng" => $ing->codeIng,
					  "created_date" => date('Y-m-d H:i'),
					  "userID" => $userID);
		$this->db->insert("hist_ing_code",$data);
	}

	public function update($req,$PosID,$data){
		$this->db->set($data);
		$this->db->where("ingID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->update("ingridients");
	}

	public function updateCode($code,$PosID,$data){
		$this->db->set($data);
		$this->db->where("codeIng",$code);
		$this->db->where("PosID",$PosID);
		$this->db->update("ingridients");
	}

	public function clearCode($codeIng,$data){
		$this->db->set($data);
		$this->db->where("codeIng",$codeIng);
		$this->db->update("ingridients");
	}

	public function delete($req,$PosID){
		$this->db->where("ingID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->delete("ingridients");
	}

	public function fast_update($req,$id,$PosID){
		$this->db->set('qty',$req);
		$this->db->where("ingID",$id);
		$this->db->where("PosID",$PosID);
		$this->db->update("ingridients");
	}
}