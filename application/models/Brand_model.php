<?php

/**
* 
*/
class Brand_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT * FROM `brand` WHERE `brandID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `brand` WHERE PosID = '".$req."' ");
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `categorymenu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/

	public function checkExists($id) {
		$this->db->where("brandID", $id);
		$query = $this->db->get("brand");
		return ($query->row() != null) ? true : false;
	}
	
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("brand",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("brandID",$req);
		$this->db->where("PosID", $pos);
		$this->db->update("brand");
	}

	public function delete($req,$pos){
		$this->db->where("brandID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("brand");
	}

}