<?php

//require("../plugins/classes/PHPExcel.php");
/**
* 
*/
class Csv_export_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function letter($c){
		$c = intval($c);
		if ($c <= 0) return '';
		$letter = '';
		while($c != 0){
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}

	public function export($req,$time){
		$date=date("d-m-Y");

		//require_once("report_function.php");
		require_once'plugins/classes/PHPExcel.php';


		if ($time == "") {
			$time = "day";
		}

		$objPHPExcel = new PHPExcel();
		$value = $this->get_value($req,$time);
		if ($req=="cabang") {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'1', "Report Cabang");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		}
		else if ($req=="totalTransaksi") {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'1', "Report Transaksi");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		}
		else if ($req=="newMember") {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'1', "Report Member");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		}
		else if ($req=="sumTransaki") {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'1', "Report Total Transaksi");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
		}

		if ($req=="cabang") {
			$row=3;
			$grand=0;

			$col=1;
			$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).'1', "Branch Code");$col++;
			$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).'2', "Branch Name");$col++;
			$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).'3', "Branch Address");$col++;
			$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).'4', "Branch Phone");$col++;
			$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).'5', "Branch Fax");$col++;
			$row++;
			foreach ($value as $key) {
				$col=1;
				
				$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).$row, $key->POSID);$col++;
				$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).$row, $key->POSNm);$col++;
				$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).$row, $key->Address);$col++;
				$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).$row, $key->Telp);$col++;
				$objPHPExcel->getActiveSheet()->setCellValue($this->letter($col).$row, $key->Fax);$col++;
				$row++;		
			}
		}
		else if ($req=="totalTransaksi") {
			# code...
		}
		else if ($req=="newMember") {
			# code...
		}
		else if ($req=="sumTransaki") {
			# code...
		}

		
		
		
		

		
				//$row+=1;
				//$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Total Sales");
				//$objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $grand);
				//$objPHPExcel->getActiveSheet()->mergeCells("A".$row.":G".$row);

		
		foreach(range('A','H') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		if ($req=="cabang") {
			//header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			//header("Content-Disposition: attachment; filename=\"cabang report ".$date.".xlsx\"");
			//header("Cache-Control: max-age=0");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_clean();
			//echo str_replace(".php", ".xlsx", __FILE__);

			$objWriter->save(str_replace(".php", ".xlsx", __FILE__));
		}
		/*
		else if ($req=="totalTransaksi") {
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header("Content-Disposition: attachment; filename=\"transaksi report ".$date.".xlsx\"");
			header("Cache-Control: max-age=0");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_clean();
			$objWriter->save("php://output");
		}
		else if ($req=="newMember") {
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header("Content-Disposition: attachment; filename=\"member report ".$date.".xlsx\"");
			header("Cache-Control: max-age=0");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_clean();
			$objWriter->save("php://output");		}
		else if ($req=="sumTransaki") {
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header("Content-Disposition: attachment; filename=\"total transaksi report ".$date.".xlsx\"");
			header("Cache-Control: max-age=0");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			ob_clean();
			$objWriter->save("php://output");
		}
		*/
	}


	public function get_value($req,$time){
		if ($req=="cabang") {
			$str="SELECT * FROM `pos` where `active` = '1'";
		}
		else{
			if ($time=="day") {
				$str="";
			}
			else if($time == "month"){

			}
			else if ($time == "year") {
				
			}
		}
		$query = $this->db->query($str);
		return $query->result();

	}

}



?>