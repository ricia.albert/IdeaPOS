<?php

/**
* 
*/
class Ing_category_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT * FROM `ingcate` WHERE `cateIngID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `ingcate` WHERE PosID=".$this->db->escape($req));
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `ingcate` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("ingcate",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("cateIngID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("ingcate");
	}

	public function delete($req,$pos){
		$this->db->where("cateIngID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("ingcate");
	}

}