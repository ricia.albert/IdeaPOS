<?php

/**
* 
*/
class Taksonomi_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $PosID){
		$query = $this->db->query("SELECT * FROM `menutype` WHERE `typeID`='".$req."' AND PosID=".$this->db->escape($PosID));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `menutype` WHERE PosID=".$this->db->escape($req));
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `categorymenu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("menutype",$data);
	}

	public function update($req,$PosID,$data){
		$this->db->set($data);
		$this->db->where("typeID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->update("menutype");
	}

	public function delete($req,$PosID){
		$this->db->where("typeID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->delete("menutype");
	}

}