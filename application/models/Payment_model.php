<?php

/**
* 
*/
class Payment_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT * FROM `payment` WHERE `paymentID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `payment` WHERE PosID=".$this->db->escape($req));
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function add($data){
		//$this->db->set($data);
		$this->db->insert("payment",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("paymentID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("payment");
	}

	public function delete($req,$pos){
		$this->db->where("paymentID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("payment");
	}

}