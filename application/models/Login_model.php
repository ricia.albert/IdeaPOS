<?php

/**
* 
*/
class Login_model extends CI_Model
{
	
	function __construct()
	{
		# code...
		parent::__construct(); 
	}

	function check($username,$password){
		$query =$this->db->select()
				->from("user_web")
				->where("username",$username)
				->where("password",sha1($password))
				->where("status",'1')
				->get();

		if ($query->num_rows()>0) {
			return true;
		}
		else{
			return false;
		}
	}

	function get_user($username){
		$query =$this->db->select()
				->from("user_web")
				->where("username",$username)
				->where("status",'1')
				->get();

		return $query->row();
	}
}