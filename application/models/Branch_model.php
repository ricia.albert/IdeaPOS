<?php

/**
* 
*/
class Branch_model extends CI_Model
{
	
	function __construct()
	{
		# code...
		parent::__construct(); 
	}

	public function get_all_branch(){
		$query = $this->db->get("pos");
		return $query->result();
	}

	public function get_pos_transfer($POSID){
		$this->db->where("POSID", $POSID);
		$query = $this->db->get("pos_transfer");
		$result = $query->result();
		$pos = array();
		foreach ($result as $key) {
			array_push($pos, $key->toPOSID);
		}
		return $pos;
	}

	public function add_trf($data) {
		foreach ($data as $d) :
			$this->db->insert("pos_transfer", $d);
		endforeach;
	}

	public function delete_trf($POSID) {
		$this->db->where("POSID", $POSID);
		$this->db->delete("pos_transfer");
	}

	public function get_branch($branchList) {
		$this->db->where("active", 1);
		if (count($branchList) > 0) {
			foreach ($branchList as $branch) {
				$this->db->or_where("POSID", $branch);
			}
		} else {
			$this->db->where("POSID", "");
		}
		$query = $this->db->get("pos");
		return $query->result();
	}

	public function get_storage($branchList) {
		$this->db->where("active", 1);
		$this->db->where("isGudang", 1);
		if (count($branchList) > 0) {
			$this->db->group_start();
			foreach ($branchList as $branch) {
				$this->db->or_where("POSID", $branch);
			}
			$this->db->group_end();
		} else {
			$this->db->where("POSID", "");
		}

		$query = $this->db->get("pos");
		return $query->result();
	}

	public function get_non_storage($branchList) {
		$this->db->where("active", 1);
		$this->db->where("isGudang", 0);
		if (count($branchList) > 0) {
			$this->db->group_start();
			foreach ($branchList as $branch) {
				$this->db->or_where("POSID", $branch);
			}
			$this->db->group_end();
		} else {
			$this->db->where("POSID", "");
		}
		$query = $this->db->get("pos");
		return $query->result();
	}

	public function get_non_storage_branch() {
		$this->db->where("active", 1);
		$this->db->where("isGudang", 0);
		$query = $this->db->get("pos");
		return $query->result();
	}

	public function delete($r){
		//$this->db->set("status",'0');
		$this->db->where("POSID",$r);
		$this->db->delete("pos");
	}

	public function cleanEverything($POSID) {
		$query = $this->db->query("CALL sp_RemoveBranch(".$this->db->escape($POSID).");");
		return $query;
	}

	public function copyDatabase($from, $to) {
		$query = $this->db->query("CALL sp_CopyDatabase(".$this->db->escape($from).",".$this->db->escape($to).");");
		return $query;
	}

	public function add($data){
		if($this->db->insert('pos',$data)){
			return true;
		}
	}

	public function get_all_branch_update($data){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `POSID` = '".$data."'");
		return $query->result();

	}

	public function update($data,$id){
		$this->db->set($data);
		$this->db->where("POSID",$id);
		$this->db->update("pos");
	}
}