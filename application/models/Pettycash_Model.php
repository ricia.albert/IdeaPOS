<?php

/**
* 
*/
class Pettycash_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_pettycash($req, $pos){
		$query = $this->db->query("SELECT s.*, pt.typeName FROM pettycash AS s
									JOIN pettycashtype AS pt ON s.pettyTypeID = pt.pettyTypeID AND s.PosID = pt.PosID
								   WHERE `pettyCashID`='".$req."' AND s.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req, $start, $end){
		$query = $this->db->query("SELECT s.*, pt.typeCode, pt.typeName FROM pettycash AS s
									JOIN pettycashtype AS pt ON s.pettyTypeID = pt.pettyTypeID AND s.PosID = pt.PosID
								   WHERE s.pettyDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($start)))." AND ".$this->db->escape(date('Y-m-d', strtotime($end)))." AND s.PosID=".$this->db->escape($req));
		return $query->result();
	}

	public function get_type($pos) {
		$query = $this->db->query("SELECT * FROM `pettycashtype` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function add($data){
		//$this->db->set($data);
		$this->db->insert("pettycash",$data);
	}

	public function update($req, $pos, $data){
		$this->db->set($data);
		$this->db->where("pettyCashID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("pettycash");
	}

	public function delete($req, $pos){
		$this->db->where("pettyCashID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("pettycash");
	}

}