<?php

/**
* 
*/
class Product_detail_price_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT menuPriceID, menuID, Info, Price, last_update, userID, PosID FROM `menupricelist` WHERE `menuPriceID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_package($req,$pos){
		$query = $this->db->query("SELECT pckPriceID AS menuPriceID, packagesID AS menuID, Info, Price, last_update, userID, PosID FROM `packagespricelist` WHERE `pckPriceID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_side($req,$pos){
		$query = $this->db->query("SELECT SSPriceID AS menuPriceID, SideStockID AS menuID, Info, Price, last_update, userID, PosID FROM `sidestockpricelist` WHERE `SSPriceID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($menu, $pos){
		if (strpos($menu, "MN") === 0) {
			$query = $this->db->query("SELECT mp.menuPriceID,mp.menuID,mp.Info,mp.Price,u.Username 
									FROM menupricelist mp 
									LEFT JOIN users u on mp.userID = u.userID AND mp.PosID = u.PosID
									WHERE mp.PosID=".$this->db->escape($pos)." AND mp.menuID=".$this->db->escape($menu));
		} else if (strpos($menu, "PK") === 0) {
			$query = $this->db->query("SELECT mp.pckPriceID as menuPriceID,mp.packagesID as menuID,
									mp.Info,mp.Price,u.Username 
									FROM packagespricelist mp 
									LEFT JOIN users u on mp.userID = u.userID AND mp.PosID = u.PosID
									WHERE mp.PosID=".$this->db->escape($pos)." AND mp.packagesID=".$this->db->escape($menu));
		} else if (strpos($menu, "SS") === 0) {
			$query = $this->db->query("SELECT mp.SSPriceID as menuPriceID,mp.SideStockID as menuID,
									mp.Info,mp.Price,u.Username 
									FROM sidestockpricelist mp 
									LEFT JOIN users u on mp.userID = u.userID AND mp.PosID = u.PosID
									WHERE mp.PosID=".$this->db->escape($pos)." AND mp.SideStockID=".$this->db->escape($menu));
		}
		return $query->result();
	}

	public function get_type($pos){
		$query = $this->db->query("SELECT * FROM `menu` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_type_pck($pos){
		$query = $this->db->query("SELECT packagesID as menuID, packages_name as menuNm, packages_price as price FROM `packages` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_type_ss($pos){
		$query = $this->db->query("SELECT SideStockID as menuID, Side_name as menuNm, Side_price as price FROM `sidestock` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}


	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("menupricelist",$data);
	}

	public function add_pck($data){
		//$this->db->set($data);
		$this->db->insert("packagespricelist",$data);
	}

	public function add_ss($data){
		//$this->db->set($data);
		$this->db->insert("sidestockpricelist",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("menuPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("menupricelist");
	}

	public function update_pck($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("pckPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("packagespricelist");
	}

	public function update_ss($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("SSPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("sidestockpricelist");
	}

	public function delete($req,$pos){
		$this->db->where("menuPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("menupricelist");
	}

	public function delete_pck($req,$pos){
		$this->db->where("pckPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("packagespricelist");
	}

	public function delete_ss($req,$pos){
		$this->db->where("SSPriceID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("sidestockpricelist");
	}

}