<?php

/**
* 
*/
class Member_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `guest_book` WHERE PosID = ".$this->db->escape($req));
		return $query->result();
	}

	public function get_member($req, $pos){
		$query = $this->db->query("SELECT * FROM guest_book WHERE guestID = '".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function check_member_price($custID, $menuID, $pos) {
		$query = $this->db->query("SELECT menuID, priceOffer, guestID, PosID 
								   FROM guest_price 
								   WHERE guestID = ".$this->db->escape($custID)." 
								   AND PosID=".$this->db->escape($pos)."
								   AND menuID=".$this->db->escape($menuID));
		return ($query->row() != null) ? true : false;
	}

	public function add($data){
		if ($this->db->insert('guest_book',$data)) {
			return true;
		}
	}
	public function update($data,$menuID, $PosID){
		$this->db->set($data);
		$this->db->where("guestID",$menuID);
		$this->db->where("PosID",$PosID);
		$this->db->update("guest_book");
	}

	public function delete($data, $pos){
		$this->db->where("guestID",$data);
		$this->db->where("PosID",$pos);
		$this->db->delete("guest_book");
	}

	// special price Function

	public function get_all_menu($req, $pos){
		$query  = $this->db->query("SELECT gp.priceID,m.menuID,m.menuNm,gp.priceOffer,p.POSNm, gp.guestID, gp.PosID 
									FROM guest_price gp 
									JOIN menu m ON gp.menuID = m.menuID AND gp.PosID = m.PosID 
									LEFT JOIN pos p ON gp.PosID = p.POSID AND gp.PosID = p.PosID
									WHERE gp.guestID='".$req."' AND gp.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_special_member($req, $pos){
		$query = $this->db->query("SELECT Guest_name as guets 
									FROM guest_book 
									WHERE guestID = '".$req."' AND PosID=".$this->db->escape($pos));
		return $query->row()->guets;
	}

	public function get_true_menu($pos){
		$this->db->where("PosID", $pos);
		$query = $this->db->get("menu");
		return $query->result();
	}

	public function add_special($data){
		$this->db->insert("guest_price",$data);
	}

	public function get_special($req, $pos){
		$query=$this->db->query("SELECT gp.menuID,gp.priceOffer,gb.Guest_name,  gp.guestID, gp.PosID 
								FROM guest_price gp 
								JOIN guest_book gb ON gp.guestID = gb.guestID AND gp.PosID = gb.PosID 
								WHERE gp.priceID='".$req."' AND gp.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function fast_update($req,$id, $pos){
		$this->db->set('priceOffer',$req);
		$this->db->where("priceID",$id);
		$this->db->where("PosID", $pos);
		$this->db->update("guest_price");
	}
	public function delete_special($data, $pos){
		$this->db->where("priceID",$data);
		$this->db->where("PosID", $pos);
		$this->db->delete("guest_price");
	}
}