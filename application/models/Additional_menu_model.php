<?php

/**
* 
*/
class Additional_menu_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $pos){
		$query = $this->db->query("SELECT mt.*, m.menuNm FROM `menutopping` AS mt JOIN menu AS m ON mt.toppingID = m.menuID AND mt.PosID = m.PosID WHERE mt.`menuToppingID`='".$req."' AND mt.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($menu, $pos){
		$query = $this->db->query("SELECT mt.*, m.menuNm FROM `menutopping` AS mt JOIN menu AS m ON mt.toppingID = m.menuID AND mt.PosID = m.PosID WHERE mt.menuID = ".$this->db->escape($menu)." AND mt.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_type($pos){
		$query = $this->db->query("SELECT * FROM `menu` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("menutopping",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("menuToppingID",$req);
		$this->db->where("PosID", $pos);
		$this->db->update("menutopping");
	}

	public function delete($req,$pos){
		$this->db->where("menuToppingID",$req);
		$this->db->where("PosID", $pos);
		$this->db->delete("menutopping");
	}

}