<?php

/**
* 
*/
class Transaction_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct(); 
	}

     public function get_total($req,$time, $pos, $isDebt = 0, $stats="A", $sd = "", $ed = "", $price = "") {
          if($time=="day"){
               $str="SELECT SUM(COALESCE(total,0) + COALESCE(ppn,0) - COALESCE(discount,0) - COALESCE(totalVoucher,0)) AS total FROM `transorder` where transDate BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' ";
          }
          elseif($time=="month"){
               $str="SELECT SUM(COALESCE(total,0) + COALESCE(ppn,0)  - COALESCE(discount,0) - COALESCE(totalVoucher,0)) AS total FROM `transorder` where  month(transDate) = '".date('m')."' ";
          }
          elseif($time=="year"){
               $str="SELECT SUM(COALESCE(total,0) + COALESCE(ppn,0)  - COALESCE(discount,0) - COALESCE(totalVoucher,0)) AS total FROM `transorder` where year(transDate) = '".date('Y')."' ";
          } else {
               $str="SELECT SUM(COALESCE(total,0) + COALESCE(ppn,0)  - COALESCE(discount,0) - COALESCE(totalVoucher,0)) AS total FROM `transorder` 
                    where PosID=".$this->db->escape($pos)."  AND COALESCE(PriceType,'Normal') LIKE ".$this->db->escape("%".$price."%")." AND COALESCE(isDebt,0)=".$isDebt." AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          }
          $query = $this->db->query($str);
          $row = $query->row();
          return $row->total;
     }

     public function get_summary($pos, $stats="A", $sd = "", $ed = "") {
          $str="SELECT COALESCE(SUM(COALESCE(total,0)),0) AS total, COALESCE(SUM(COALESCE(ppn,0)),0) AS ppn, COALESCE(SUM(COALESCE(discount,0)),0) AS discount, COALESCE(SUM(COALESCE(totalVoucher,0)),0) AS totalVoucher
               FROM `transorder` where PosID=".$this->db->escape($pos)."  AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          $query = $this->db->query($str);
          return $query->row();
     }

     public function get_price_type()
     {
          $str = "SELECT DISTINCT info FROM menupricelist";
          $query = $this->db->query($str);
          return $query->result();
     }

     public function get_change($pos, $sd = "", $ed = "") {
          $str="SELECT tor.transID, tor.PosID, ord.Jml, ord.total AS Awal, ord2.total AS Akhir, (ord.total - ord2.total) as Selisih from transorder as tor
               join (
                    select OS.transID, orderMin.Jml, OS.total, OS.PosID from orderstate as OS  
                    join (
                    select transID, PosID, COUNT(stateID) as Jml, MIN(stateID) as minState from orderstate 
                    group by transID, PosID
                    ) as orderMin on OS.stateID = orderMin.minState AND OS.PosID = orderMin.PosID 
               )as ord on tor.transID = ord.transID AND tor.PosID = ord.PosID
               join (
                    select OS.transID, OS.total, OS.PosID from orderstate as OS  
                    join (
                    select transID, PosID, MAX(stateID) as maxState from orderstate 
                    group by transID, PosID
                    ) as orderMax on OS.stateID = orderMax.maxState AND OS.PosID = orderMax.PosID 
               )as ord2 on tor.transID = ord2.transID AND tor.PosID = ord2.PosID
               where ord2.total < ord.total AND tor.PosID=".$this->db->escape($pos)." AND tor.transStatus='A' AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          $query = $this->db->query($str);
          return $query->row();
     }

     public function get_stats($pos, $stats="A", $sd = "", $ed = "") {
          $str="SELECT SUM(numOfItem) AS jmlItem, SUM(numOfCustomer) AS jmlCust, COUNT(transID) AS Jml, COALESCE(SUM(COALESCE(total,0)),0) AS Total, COALESCE(SUM(COALESCE(ppn,0)),0) AS ppn, COALESCE(SUM(COALESCE(discount,0)),0) AS discount, COALESCE(SUM(COALESCE(totalVoucher,0)),0) AS totalVoucher 
               FROM `transorder` where PosID=".$this->db->escape($pos)."  AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          $query = $this->db->query($str);
          return $query->row();
     }

     public function get_void($pos, $sd = "", $ed = "") {
          $str="SELECT COUNT(transID) AS Jml, COALESCE(SUM(COALESCE(total,0)),0) AS Total, COALESCE(SUM(COALESCE(ppn,0)),0) AS ppn, COALESCE(SUM(COALESCE(discount,0)),0) AS discount, COALESCE(SUM(COALESCE(totalVoucher,0)),0) AS totalVoucher 
               FROM `transorder` where isDebt = 1 AND PosID=".$this->db->escape($pos)."  AND transStatus=".$this->db->escape("A")." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          $query = $this->db->query($str);
          return $query->row();
     }

     public function get_cash($pos, $stats="A", $sd = "", $ed = "") {
          $str="SELECT SUM(COALESCE(total,0) + COALESCE(ppn,0)   - COALESCE(discount,0) - COALESCE(totalVoucher,0)) AS Cash
               FROM `transorder` where paymentID IS NULL AND PosID=".$this->db->escape($pos)."  AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
          $query = $this->db->query($str);
          return $query->row()->Cash;
     }

     public function get_payment($pos, $stats="A", $sd = "", $ed = "") {
          $str="SELECT p.paymentName, SUM(COALESCE(total,0) + COALESCE(ppn,0)   - COALESCE(t.discount,0) - COALESCE(totalVoucher,0)) AS Cash
               FROM `transorder` AS t JOIN payment AS p ON t.paymentID = p.paymentID AND t.PosID = p.PosID
               where t.PosID=".$this->db->escape($pos)."  AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))."
               GROUP BY p.paymentName";
          $query = $this->db->query($str);
          return $query->result();
     }

	public function get_value($req,$time, $pos, $isDebt = 0, $stats="A", $sd = "", $ed = "", $price = ""){
		//echo "ini req : ".$req. " ini time : ".$time;
		// if ($time == "") {
		// 	$time = "day";
		// }
		//echo "ini req : ".$req. " ini time : ".$time;
		if ($req=="cabang") {
			$str = "SELECT * FROM `pos` where `active` = '1' ";
		}
		elseif ($req == "totalTransaksi") {
			if($time=="day"){
     			$str="SELECT `transID`, `transDate`, `settledDate`, `employeeID`, `paymentID`, `atasNama`, `numOfItem`, `numOfCustomer`, `tableID`, `total`, COALESCE(`ppn`,0) AS ppn, COALESCE(`discount`,0) AS discount, COALESCE(`discountMember`,0) AS discountMember, COALESCE(`totalVoucher`,0) AS totalVoucher, `codeMember`, `transStatus`, `isTakeAway`, `TAQueue`, `isDelivery`, `DVQueue`, `CustomerNm`, `CustomerAddr`, `CustomerTelp`, `deliveryFee`, COALESCE(`PriceType`,'Normal') AS PriceType, `FlagEPos`, `EPosID`, `PosID`, `customerID`, `hasVoid`, `voidDt`, `waiterID`, `isDebt`, `DueDt`, `loanID`, `approvalID`, `isSign`, `isApproved`, `isBargain`, `bargainDt` FROM `transorder` where transDate BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' ";
     		}
     		elseif($time=="month"){
     			$str="SELECT `transID`, `transDate`, `settledDate`, `employeeID`, `paymentID`, `atasNama`, `numOfItem`, `numOfCustomer`, `tableID`, `total`, COALESCE(`ppn`,0) AS ppn, COALESCE(`discount`,0) AS discount, COALESCE(`discountMember`,0) AS discountMember, COALESCE(`totalVoucher`,0) AS totalVoucher, `codeMember`, `transStatus`, `isTakeAway`, `TAQueue`, `isDelivery`, `DVQueue`, `CustomerNm`, `CustomerAddr`, `CustomerTelp`, `deliveryFee`, COALESCE(`PriceType`,'Normal') AS PriceType, `FlagEPos`, `EPosID`, `PosID`, `customerID`, `hasVoid`, `voidDt`, `waiterID`, `isDebt`, `DueDt`, `loanID`, `approvalID`, `isSign`, `isApproved`, `isBargain`, `bargainDt` FROM `transorder` where  month(transDate) = '".date('m')."' ";
     		}
     		elseif($time=="year"){
     			$str="SELECT `transID`, `transDate`, `settledDate`, `employeeID`, `paymentID`, `atasNama`, `numOfItem`, `numOfCustomer`, `tableID`, `total`, COALESCE(`ppn`,0) AS ppn, COALESCE(`discount`,0) AS discount, COALESCE(`discountMember`,0) AS discountMember, COALESCE(`totalVoucher`,0) AS totalVoucher, `codeMember`, `transStatus`, `isTakeAway`, `TAQueue`, `isDelivery`, `DVQueue`, `CustomerNm`, `CustomerAddr`, `CustomerTelp`, `deliveryFee`, COALESCE(`PriceType`,'Normal') AS PriceType, `FlagEPos`, `EPosID`, `PosID`, `customerID`, `hasVoid`, `voidDt`, `waiterID`, `isDebt`, `DueDt`, `loanID`, `approvalID`, `isSign`, `isApproved`, `isBargain`, `bargainDt` FROM `transorder` where year(transDate) = '".date('Y')."' ";
     		} else {
                    $str="SELECT `transID`, `transDate`, `settledDate`, t.`employeeID`, `paymentID`, `atasNama`, `numOfItem`, `numOfCustomer`, t.`tableID`, `total`, COALESCE(`ppn`,0) AS ppn, COALESCE(`discount`,0) AS discount, COALESCE(`discountMember`,0) AS discountMember, COALESCE(`totalVoucher`,0) AS totalVoucher, `codeMember`, `transStatus`, `isTakeAway`, `TAQueue`, `isDelivery`, `DVQueue`, `CustomerNm`, `CustomerAddr`, `CustomerTelp`, `deliveryFee`, `notes`, COALESCE(`PriceType`,'Normal') AS PriceType, `FlagEPos`, `EPosID`, t.`PosID`, `customerID`, `hasVoid`, `voidDt`, `waiterID`, `isDebt`, `DueDt`, `loanID`, `approvalID`, `isSign`, `isApproved`, `isBargain`, `bargainDt`, e.employee_name, COALESCE(e2.employee_name,e.employee_name) AS waiterName,
                    r.tableNm
                    FROM `transorder` AS t 
                    JOIN restotable AS r ON t.tableID = r.tableID AND t.PosID = r.PosID
                    JOIN employee AS e ON t.employeeID = e.employeeID AND t.PosID = e.PosID 
                    LEFT JOIN employee AS e2 ON t.waiterID = e2.employeeID AND t.PosID = e2.PosID 
                    where t.PosID=".$this->db->escape($pos)." AND COALESCE(t.PriceType,'Normal') LIKE ".$this->db->escape("%".$price."%")." AND COALESCE(isDebt,0)=".$isDebt." AND transStatus=".$this->db->escape($stats)." AND transDate BETWEEN ".$this->db->escape(date('Y-m-d', strtotime($sd))). " AND ".$this->db->escape(date('Y-m-d', strtotime($ed)));
               }
		}
		elseif ($req == "newMember") {
			if($time=="day")
     		{
     			$str="SELECT * FROM `guest_book` WHERE register_date BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' ";
     		}
     		elseif($time=="month")
     		{
     			$str="SELECT * FROM `guest_book` WHERE month(register_date) = '".date('m')."' ";
     		}
     		elseif($time=="year"){
     			$str="SELECT * FROM `guest_book` WHERE year(register_date) = '".date("Y")."' ";
     		}
		}
		elseif ($req=="sumTransaki") {
			if($time=="day")
     		{
     			$str="SELECT POSID,count(transID) AS total FROM `transorder` WHERE transDate BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' GROUP BY POSID  ";
     		}
     		elseif($time=="month")
     		{
     			$str="SELECT POSID,count(transID) AS total FROM `transorder` WHERE month(transDate) = '".date('m')."' GROUP BY POSID  ";
     		}
     		elseif($time=="year"){
     			$str="SELECT POSID,count(transID) AS total FROM `transorder` WHERE year(transDate) = '".date("Y")."' GROUP BY POSID ";
     		}
     		
		}
		else{
			//echo $time;
			if($time=="day"){
     			$str="SELECT td.POSID,td.transID,td.transDate,td.settledDate FROM transorder td JOIN transorderdetail tdr on td.transID = tdr.transID WHERE tdr.menuID = 'MN00000012' AND td.transDate BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' ";
     		}
     		elseif($time=="month"){
     			$str="SELECT td.POSID,td.transID,td.transDate,td.settledDate FROM transorder td JOIN transorderdetail tdr on td.transID = tdr.transID WHERE tdr.menuID = 'MN00000012' AND month(td.transDate) = '".date("m")."' AND year(td.transDate)='".date("Y")."' ";//AND year(td.transDate)='2017'
     		}
     		elseif($time=="year"){
     			$str="SELECT td.POSID,td.transID,td.transDate,td.settledDate FROM transorder td JOIN transorderdetail tdr on td.transID = tdr.transID WHERE tdr.menuID = 'MN00000012' AND year(td.transDate) = '".date("Y")."' ";
     		}
		}
		// echo $str;
		//exit();
		$query = $this->db->query($str);
		return $query->result();
	}

     public function get_detail($transID, $pos)
     {
          $this->db->where("transID", $transID);
          $this->db->where("PosID", $pos);
          $query = $this->db->get("transorderdetail");
          return $query->result();
     }

     public function get_trans($transID, $pos)
     {
          $sql = "SELECT *,COALESCE(tor.discount,0) AS totalDiscount,
                    CASE tor.transStatus
                    WHEN 'A' THEN 'Accepted'
                    WHEN 'C' THEN 'Canceled'
                    WHEN 'P' THEN 'Pending'
                    END AS Status, e.employee_name, e2.employee_name AS waiterName,
                    r.tableNm, m.roomNm FROM transorder AS tor 
                    LEFT JOIN restotable AS r ON tor.tableID = r.tableID AND tor.PosID = r.PosID
                    LEFT JOIN restoroom AS m ON m.roomID = r.roomID AND m.PosID = r.PosID
                    LEFT JOIN employee AS e ON tor.employeeID = e.employeeID AND tor.PosID = e.PosID
                    LEFT JOIN employee AS e2 ON tor.waiterID = e2.employeeID AND tor.PosID = e2.PosID
                  LEFT JOIN payment AS p ON tor.paymentID = p.paymentID
                  WHERE tor.transID=".$this->db->escape($transID)." AND tor.PosID=".$this->db->escape($pos);
          $query = $this->db->query($sql);
          return $query->row();
     }

      public function get_stock_usage($pos, $sd = "", $ed = "")
     {
          $sql = "SELECT i.ingName, i.codeIng, i.bprice, SUM((mi.qty * tod.qty) * (s2.ratio / s.ratio)) As total_qty, 
                    s2.scalarNm, SUM(i.bprice * ((mi.qty * tod.qty) * (s2.ratio / s.ratio))) as total 
                    from transorderdetail as tod 
                    join transorder as tor on tod.transID = tor.transID AND tod.PosID = tor.PosID
                    join menu as m on tod.menuID = m.menuID AND m.PosID = tod.PosID
                    join menuing as mi on m.menuID = mi.menuID AND m.PosID = mi.PosID
                    join ingridients as i on mi.ingID = i.ingID AND i.PosID = mi.PosID
                    join scalar as s on i.scalarID = s.scalarID AND s.PosID = i.PosID
                    join scalar as s2 on mi.scalarID = s2.scalarID AND mi.PosID = s2.PosID
                    WHERE tor.transStatus = 'A' AND tor.PosID=".$this->db->escape($pos)." AND tor.settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'
                    group by i.ingName, i.codeIng, i.bprice, s2.scalarNm";
          $query = $this->db->query($sql);
          return $query->result();
     }
}