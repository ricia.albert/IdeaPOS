<?php

class Corp_Model extends CI_Model{
	function __construct() { 
      parent::__construct(); 
         //$this->load->database(); ".date('Y')."
 } 

 function get_branch_count() {
    $query = $this->db->get("pos");
    return $query->num_rows();
}

function get_transaction_sum($m, $pos, $sd, $ed){
    $str = "";
     if($m=="day")
     {
         $str="transDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="month(transDate) = '".date('m')."' and year(transDate) = '".date('Y')."'";
    }
    else if($m=="year") {
         $str="year(transDate) = '".date('Y')."' ";
    } 
    else {
        $str="transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("SUM(total) AS total")
                   ->from("transorder")
                   ->where ($str)
                   ->where ("posID LIKE '".$pos."%'")
                   ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_transaction_count($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="settledDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="month(settledDate) = '".date('m')."' ";
    }
    else if($m=="year") {
         $str="year(settledDate) = '".date('Y')."' ";
    } 
    else {
        $str="settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("count(transID) AS total")
    ->from("transorder")
    ->where("transStatus=".$this->db->escape("A"))
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_member_count($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="register_date BETWEEN ".date('Y-m-d')." and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="month(register_date) = '".date('m')."' ";
    }
    else if($m=="year") {
         $str="year(register_date) = '".date('Y')."' ";
    }
    else {
        $str="register_date BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("count(guestID) AS total")
    ->from("guest_book")
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_trans_sccs($m, $pos, $sd, $ed){
    if($m=="day")
    {
     $str="settledDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="month(settledDate) = '".date('m')."' and year(settledDate) = '".date('Y')."'";
    }
    else if($m=="year") {
         $str="year(settledDate) = '".date('Y')."' ";
    }
    else {
        $str="settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where("transStatus='A'")
    ->where($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function max_trans_sccs($m){
    if($m=="day")
    {
         $str="transStatus='A' and transDate = date_sub('".date('Y-m-d')."',interval -1 day )  ";
    }
    else if($m=="month")
    {
         $str="transStatus='A' and month(transDate) = date_sub('".date('m')."',interval -1 month ) ";
    }
    else{
         $str=" transStatus='A' and year(transDate) = date_sub('".date('Y')."',interval -1 year ) ";
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}
function max_trans_pending($m){
    if($m=="day")
    {
         $str="transStatus='P' and COALESCE(isDebt,0) = 0 and transDate = date_sub('".date('Y-m-d')."',interval -1 day )  ";
    }
    else if($m=="month")
    {
         $str="transStatus='P' and COALESCE(isDebt,0) = 0 and month(transDate) = date_sub('".date('m')."',interval -1 month ) ";
    }
    else {
         $str=" transStatus='P' and COALESCE(isDebt,0) = 0 and year(transDate) = date_sub('".date('Y')."',interval -1 year ) ";
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_transaction_pending($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="transStatus='P' and COALESCE(isDebt,0) = 0 and transDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="transStatus='P' and COALESCE(isDebt,0) = 0 and month(transDate) = '".date('m')."'  ";
    }
    else if($m=="year") {
         $str=" transStatus='P' and COALESCE(isDebt,0) = 0 and year(transDate) = '".date('Y')."'  ";
    } else {
        $str="transStatus='P' and COALESCE(isDebt,0) = 0 and transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_transaction_debt($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="transStatus='P' and isDebt = 1 and transDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="transStatus='P' and isDebt = 1 and month(transDate) = '".date('m')."'  ";
    }
    else if ($m== "year") {
         $str=" transStatus='P' and isDebt = 1 and year(transDate) = '".date('Y')."'  ";
    }
    else {
        $str="transStatus='P' and isDebt = 1 and transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function max_trans_cancel($m){
    if($m=="day")
    {
         $str="transStatus='C' and transDate = date_sub('".date('Y-m-d')."',interval -1 day )  ";
    }
    else if($m=="month")
    {
         $str="transStatus='C' and month(transDate) = date_sub('".date('m')."',interval -1 month ) ";
    }
    else{
         $str=" transStatus='C' and year(transDate) = date_sub('".date('Y')."',interval -1 year ) ";
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}
function get_transaction_cancel($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="transStatus='C' and transDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="transStatus='C' and month(transDate) = '".date('m')."'  ";
    }
    else if($m=="year"){
         $str=" transStatus='C' and year(transDate) = '".date('Y')."'  ";
    } else {
         $str="transStatus='C' and transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)));
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function max_trans_all($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str="transDate = date_sub('".date('Y-m-d')."',interval -1 day )  ";
    }
    else if($m=="month")
    {
         $str="month(transDate) = date_sub('".date('m')."',interval -1 month ) ";
    }
    else if($m=="year"){
         $str="year(transDate) = date_sub('".date('Y')."',interval -1 year ) ";
    } else {
         $str="transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'";
    }
    $query = $this->db->select("SUM(total) AS total")
    ->from("transorder")
    ->where ($str)
    ->where("PosID LIKE '".$pos."%'")
    ->get();
    if ($query->row()->total == '') {
        return 0;
   }
   else{
        return $query->row()->total;
   }
}

function get_transaction_omset($pos, $sd, $ed){
    $str="SELECT DATE_FORMAT(transDate, '%Y-%m-%d') AS dates, SUM(numOfCustomer) AS total_visit, COUNT(transID) AS transCount, 
            SUM(COALESCE(total,0)+COALESCE(ppn,0)+COALESCE(rounding,0)-COALESCE(discount,0)-COALESCE(totalVoucher,0)) AS total
            FROM transorder
            WHERE transStatus = 'A' AND PosID='".$pos."' AND transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and '".date('Y-m-d', strtotime($ed))."'
            GROUP BY dates";
   $query = $this->db->query($str);
   return $query->result();
}

function get_transaction_cost($m, $pos, $sd, $ed){
    if($m=="day")
    {
         $str=" SELECT SUM(i.bprice * (mi.qty * tod.qty) * s2.ratio / s.ratio) AS total
                FROM transorder tk 
                LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                JOIN scalar as s on s.scalarID = i.scalarID AND i.PosID = s.PosID
                JOIN scalar as s2 on s2.scalarID = mi.scalarID AND mi.PosID = s2.PosID
                WHERE tk.transStatus = 'A' AND tk.settledDate BETWEEN '".date('Y-m-d')."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
    }
    else if($m=="month")
    {
         $str="SELECT SUM(i.bprice * (mi.qty * tod.qty) * s2.ratio / s.ratio) AS total
                FROM transorder tk 
                LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                JOIN scalar as s on s.scalarID = i.scalarID AND i.PosID = s.PosID
                JOIN scalar as s2 on s2.scalarID = mi.scalarID AND mi.PosID = s2.PosID
                WHERE tk.transStatus = 'A' AND month(tk.settledDate) = '".date('m')."' AND year(settledDate) = '".date('Y')."'";
    }
    else if($m=="year"){
         $str="SELECT SUM(i.bprice * (mi.qty * tod.qty) * s2.ratio / s.ratio) AS total
                FROM transorder tk 
                LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                JOIN scalar as s on s.scalarID = i.scalarID AND i.PosID = s.PosID
                JOIN scalar as s2 on s2.scalarID = mi.scalarID AND mi.PosID = s2.PosID
                WHERE tk.transStatus = 'A' AND year(settledDate) = '".date('Y')."' ";
    } else {
          $str=" SELECT SUM(i.bprice * (mi.qty * tod.qty) * s2.ratio / s.ratio) AS total
                FROM transorder tk 
                LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                JOIN scalar as s on s.scalarID = i.scalarID AND i.PosID = s.PosID
                JOIN scalar as s2 on s2.scalarID = mi.scalarID AND mi.PosID = s2.PosID
                WHERE tk.transStatus = 'A' AND tk.settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND tk.PosID LIKE '".$pos."%'";
    }

     					//SELECT SUM(m.price) as total FROM `transorder` tok JOIN `transorderdetail` tod ON tok.transID=tod.transID JOIN `menu` m on tod.menuID=m.menuID WHERE tok.transDate BETWEEN'2015-06-06 00:00:00' AND '2015-06-06 23:59:59'
     	/*$query = $this->db->select("SUM(m.price) as total")
     					->from("`transorder` tok JOIN `transorderdetail` tod ON tok.transID=tod.transID JOIN `menu` m on tod.menuID=m.menuID")
     					->where ("tok.transDate BETWEEN'2015-06-06 00:00:00' AND '2015-06-06 23:59:59'")
     					->get();*/
                             $query = $this->db->query($str);
                             if ($query->row()->total == '') {
                                 return 0;
                            }
                            else{
                                 return $query->row()->total;
                            }
                       }

                       function get_product_tren($m, $pos = "", $sd = "", $ed = ""){
                        if($m=="day")
                        {
                             $str="SELECT tod.menuID, m.codeMenu,tod.menuNM,SUM(tod.qty) as total FROM `transorder` tok join `transorderdetail` tod on tok.transID=tod.transID and tok.PosID = tod.PosID JOIN menu AS m On tod.menuID = m.menuID and tod.PosID = m.PosID WHERE tok.transDate BETWEEN '".date('Y-m-d')."' AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))." GROUP BY tod.menuID ORDER BY total DESC LIMIT 14 ";
                        }
                        else if($m=="month")
                        {
                             $str="SELECT tod.menuID, m.codeMenu,tod.menuNM,SUM(tod.qty) as total FROM `transorder` tok join `transorderdetail` tod on tok.transID=tod.transID and tok.PosID = tod.PosID JOIN menu AS m On tod.menuID = m.menuID and tod.PosID = m.PosID WHERE month(tok.transDate) = '".date('m')."' AND year(tok.transDate)='".date("Y")."' GROUP BY tod.menuID ORDER BY total DESC LIMIT 14 ";
                        }
                        else if($m=="year"){
                             $str="SELECT tod.menuID, m.codeMenu,tod.menuNM,SUM(tod.qty) as total FROM `transorder` tok join `transorderdetail` tod on tok.transID=tod.transID and tok.PosID = tod.PosID JOIN menu AS m On tod.menuID = m.menuID and tod.PosID = m.PosID WHERE year(tok.transDate) = '".date('Y')."' GROUP BY tod.menuID ORDER BY total DESC LIMIT 14 ";
                        } else {
                          $str="SELECT tod.menuID, tod.PosID, m.codeMenu, c.subCategoryNm, tod.menuNM, m.price, SUM(tod.qty) as jml, SUM(tod.price * tod.qty) AS total FROM `transorder` tok join `transorderdetail` tod 
                          on tok.transID=tod.transID and tok.PosID = tod.PosID 
                          JOIN menu AS m On tod.menuID = m.menuID and tod.PosID = m.PosID 
                          JOIN subcategory AS c ON m.subCategoryID = c.subCategoryID AND m.PosID = c.PosID
                          WHERE tok.transStatus='A' AND tod.PosID LIKE '".$pos."%' AND tok.transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."' 
                          GROUP BY m.codeMenu
                          ORDER BY jml DESC, subCategoryNm ASC";
                        }
                        $query = $this->db->query($str);
                        return $query->result_array();
                  }

                  function get_guest(){
                   // if($m=="day")
                   // {
                   //      $str="last_update BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59' ";
                   // }
                   // else if($m=="month")
                   // {
                   //      $str="month(last_update) = '".date('m')."' ";
                   // }
                   // else{
                   //      $str="year(last_update) = '".date('Y')."' ";
                   // }
                   $query = $this->db->select("Guest_Name, Email")
                   ->from("guest_book")
                   // ->where ($str)
                   ->get();
                   if ($query->num_rows()<1) {

                   }
                   else{
                       return $query->result();
                  }
             }

             function get_monthly($m, $pos, $sd, $ed){
              if ($m == 'day') {
                  $query = "SELECT a.trdate, a.total, a.ppn, COALESCE(b.debt,0) AS debt FROM (
                            SELECT DATE_FORMAT(settledDate, '%Y-%m-%d') AS trdate, SUM(total) AS total, SUM(ppn) AS ppn 
                            from `transorder` 
                            WHERE transStatus = 'A'
                            AND settledDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS a LEFT JOIN (
                            SELECT DATE_FORMAT(transDate, '%Y-%m-%d') AS trdate, SUM(total) AS debt 
                            from `transorder` 
                            WHERE transStatus = 'P' and isDebt = 1
                            AND transDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS b ON a.trdate = b.trdate";
             }
             elseif ($m == 'month') {
                  $query="SELECT a.trdate, a.total, a.ppn, COALESCE(b.debt,0) AS debt FROM (
                            SELECT DATE_FORMAT(settledDate, '%Y-%m') AS trdate, SUM(total) AS total, SUM(ppn) AS ppn 
                            from `transorder` 
                            WHERE transStatus = 'A'
                            AND settledDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS a LEFT JOIN (
                            SELECT DATE_FORMAT(transDate, '%Y-%m') AS trdate, SUM(total) AS debt 
                            from `transorder` 
                            WHERE transStatus = 'P' and isDebt = 1
                            AND transDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS b ON a.trdate = b.trdate";
             }
             elseif ($m == 'year'){
                  $query = "SELECT a.trdate, a.total, a.ppn, COALESCE(b.debt,0) AS debt FROM (
                            SELECT DATE_FORMAT(settledDate, '%Y') AS trdate, SUM(total) AS total, SUM(ppn) AS ppn 
                            from `transorder` 
                            WHERE transStatus = 'A'
                            AND settledDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS a LEFT JOIN (
                            SELECT DATE_FORMAT(transDate, '%Y') AS trdate, SUM(total) AS debt 
                            from `transorder` 
                            WHERE transStatus = 'P' and isDebt = 1
                            AND transDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS b ON a.trdate = b.trdate";
             } else {
              $query = "SELECT a.trdate, a.total, a.ppn, COALESCE(b.debt,0) AS debt FROM (
                            SELECT DATE_FORMAT(settledDate, '%Y-%m-%d') AS trdate, SUM(total) AS total, SUM(ppn) AS ppn 
                            from `transorder` 
                            WHERE transStatus = 'A'
                            AND settledDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS a LEFT JOIN (
                            SELECT DATE_FORMAT(transDate, '%Y-%m-%d') AS trdate, SUM(total) AS debt 
                            from `transorder` 
                            WHERE transStatus = 'P' and isDebt = 1
                            AND transDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                            AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                            GROUP BY trdate
                            ORDER BY trdate ASC
                        ) AS b ON a.trdate = b.trdate";
             }
             $query = $this->db->query($query);
             $total = array();
             $ppn = array();
             $debt = array();
             $timestamp = array();
             $res = $query->result();
             foreach ($res as $obj) {
                  array_push($total, $obj->total);
                  array_push($ppn, $obj->ppn);
                  array_push($debt, $obj->debt);
                  array_push($timestamp, $obj->trdate);
             }
             $data = array("total" => $total, "ppn" => $ppn, "debt" => $debt, "time" => $timestamp);
             return $data;

        }

        function get_monthly_debt($m, $pos, $sd, $ed){
              if ($m == 'day') {
                  $query = "SELECT a.date, a.mon, a.year, day, total FROM (
                              SELECT lt.Day , COALESCE(tor.date, day(NOW())) AS date,
                              COALESCE(tor.mon, MONTH(NOW())) AS mon, 
                              COALESCE(tor.year, YEAR(NOW())) AS year, 
                              COALESCE(SUM(tor.total),0) as total FROM ltdays lt LEFT JOIN (
                                                  select YEAR(transDate) AS year, MONTH(transDate) AS mon, DAY(transDate) AS date, WEEKDAY(transDate) AS week, SUM(total) AS total from `transorder` 
                                                  WHERE transStatus = 'P' AND isDebt = 1 AND year(transDate) =".$this->db->escape(date('Y'))."
                                                  AND transDate between date_add(".$this->db->escape(date('Y-m-d')).", INTERVAL -6 DAY) 
                                                  AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))."
                                                  GROUP BY YEAR(transDate),MONTH(transDate),DAY(transDate)
                                                  ) tor on lt.ID =(tor.week+1) GROUP BY lt.Day 
                              ) AS a
                              ORDER BY a.year ASC, a.mon ASC, a.date ASC";
             }
             elseif ($m == 'month') {
                  $query="SELECT lt.ltID, lt.month_name as month,COALESCE(SUM(total),0) as total  FROM `ltmonth` lt LEFT JOIN ( select * from `transorder` WHERE transStatus = 'P' AND isDebt = 1 AND year(transDate)='".date('Y')."') toa on lt.ltid=month(toa.transDate) GROUP BY lt.month_name ORDER BY lt.ltID";
             }
             elseif ($m == 'year'){
                  $query = "SELECT year(transDate) as date, sum(total) as total FROM transorder where transStatus = 'P' AND isDebt = 1 GROUP BY date ORDER BY date";
             } else {
                  $query = "SELECT a.date, a.mon, a.year, day, total FROM (
                              SELECT lt.Day , COALESCE(tor.date, day(NOW())) AS date,
                              COALESCE(tor.mon, MONTH(NOW())) AS mon, 
                              COALESCE(tor.year, YEAR(NOW())) AS year, 
                              COALESCE(SUM(tor.total),0) as total FROM ltdays lt LEFT JOIN (
                                                  select YEAR(transDate) AS year, MONTH(transDate) AS mon, DAY(transDate) AS date, WEEKDAY(transDate) AS week, SUM(total) AS total from `transorder` 
                                                  WHERE transStatus = 'P' AND isDebt = 1 AND year(transDate) =".$this->db->escape(date('Y'))."
                                                  AND transDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))."
                                                  AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                                                  GROUP BY YEAR(transDate),MONTH(transDate),DAY(transDate)
                                                  ) tor on lt.ID =(tor.week+1) GROUP BY lt.Day 
                              ) AS a
                              ORDER BY a.year ASC, a.mon ASC, a.date ASC";
             }
             $query = $this->db->query($query);
             $data = array();
             $res = $query->result();
             foreach ($res as $obj) {
                  array_push($data, $obj->total);
             }

             return $data;

        }

        function get_item_pie($m, $pos, $sd, $ed){
     	//echo $m;
         $dateTemp = $m;
         $grand = 0 ;
         $index = 0 ;
         if ($m=='day') {
             $query = "SELECT CustomerNm,SUM(total) as total FROM `transorder` where transDate BETWEEN '".date("Y-m-d")."' AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))." GROUP BY CustomerNm";
        }
        elseif ($m=='month') {
             $query = "SELECT CustomerNm,SUM(total) as total FROM `transorder` where month(`transDate`) = '".date("m")."' and year(`transDate`) = '".date("Y")."'  GROUP BY CustomerNm";
        }
        elseif ($m=='year') {
             $query = "SELECT CustomerNm,SUM(total) as total FROM `transorder` where year(`transDate`) = '".date('Y')."' GROUP BY CustomerNm";
        } else {
            $query = "SELECT CustomerNm,SUM(total) as total FROM `transorder` where transDate BETWEEN '".date("Y-m-d", strtotime($sd))."' AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%' GROUP BY CustomerNm";
        }
        $query = $this->db->query($query);
        $pembanding = $this->get_transaction_sum($dateTemp, $pos, $sd, $ed);
     	//echo $dateTemp;
        $data = array();
        $res = $query->result();
        foreach ($res as $key) {
             $index++;
             if (count($data)>3) {
                 $customer = 'Other';
            }
            else{
                 if ($key->CustomerNm == "") {
                     $customer =  'non-member';
                }
                else{
                     $customer=$key->CustomerNm;
                }
           }

           if ($customer != "Other") {
            if ($pembanding == 0) {
                $pembanding = 1;
           }
           $total = ($key->total / $pembanding)*100 ;
	     		//echo "Total : ".number_format($total)." key : ".number_format($key->total)." pembanding : ".$pembanding;
           $temp = (object)array("name" => $customer, "y" => $total);
           array_push($data, $temp);
      }
      else{
       if ($index != $query->num_rows()) {
           $total = ($key->total / $pembanding)*100 ;
           $grand += $total;
      }
      else{
           if ($pembanding == 0) {
               $pembanding = 1;
          }
          $total = ($key->total / $pembanding)*100 ;
          $grand += $total;
		     		//echo "Total : ".number_format($total)." key : ".number_format($key->total)." pembanding : ".$pembanding;
          $temp = (object)array("name" => $customer, "y" => $total);
          array_push($data, $temp);
     }
}




}

return $data;
}

function get_transaction_hpp($pos, $debt, $stats, $sd, $ed){

  $str=" SELECT tk.transID, tk.transDate, tk.settledDate, tk.numOfItem, tk.numOfCustomer, e.employee_name, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
            FROM transorder tk 
            LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
            LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
            left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
            LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
            JOIN employee e ON tk.employeeID = e.employeeID AND tk.PosID = e.PosID
            JOIN scalar as s on s.scalarID = i.scalarID AND i.PosID = s.PosID
            JOIN scalar as s2 on s2.scalarID = mi.scalarID AND mi.PosID = s2.PosID
            WHERE tk.transStatus = '".$stats."' AND tk.isDebt=".$debt." AND tk.transDate BETWEEN '".date('Y-m-d', strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))."
            GROUP BY tk.transID";

   $query = $this->db->query($str);
   return $query->result();
 }

function get_customer_pie($m, $pos, $sd, $ed){
    $dateTemp = $m;
    $grand = 0;
    $index = 0;
    if ($m=='day') {
        $query = "SELECT tod.menuNM as menuName, SUM(tod.price) as total FROM `transorder` tor JOIN transorderdetail tod ON tor.transID = tod.transID WHERE transDate BETWEEN '".date("Y-m-d")."' AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))." GROUP BY tod.menuNM ORDER by total DESC";
   }
   elseif ($m=='month') {
        $query = "SELECT tod.menuNM as menuName , SUM(tod.price) as total FROM `transorder` tor JOIN transorderdetail tod ON tor.transID = tod.transID WHERE month(transDate) = '".date("m")."' and year(transDate) = '".date("Y")."' GROUP BY tod.menuNM ORDER by total DESC";
   }
   elseif ($m=='year') {
        $query = "SELECT tod.menuNM as menuName, SUM(tod.price) as total FROM `transorder` tor JOIN transorderdetail tod ON tor.transID = tod.transID WHERE YEAR(transDate) = '".date('Y')."' GROUP BY tod.menuNM ORDER by total DESC";
   }
   else{
        $query = "SELECT tod.menuNM as menuName, SUM(tod.price) as total FROM `transorder` tor JOIN transorderdetail tod ON tor.transID = tod.transID WHERE transDate BETWEEN '".date("Y-m-d", strtotime($sd))."' AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND tod.PosID LIKE '".$pos."%' GROUP BY tod.menuNM ORDER by total DESC";
   }
   $query = $this->db->query($query);
   $pembanding = $this->get_transaction_sum($dateTemp, $pos, $sd, $ed);
     	//echo $dateTemp;
   $data = array();
   $res = $query->result();
   foreach ($res as $key) {
        $index++;
        if (count($data)>3) {
            $item = 'Other';
       }
       else{
            $item=$key->menuName;
       }
       if ($item != 'Other') {
            if ($pembanding == 0) {
                $pembanding = 1;
           }
           $total = ($key->total / $pembanding)*100 ;
	     		//echo "Total : ".number_format($total)." key : ".number_format($key->total)." pembanding : ".$pembanding;
           $temp = (object)array("name" => $item, "y" => $total);
           array_push($data, $temp);
      }
      else{
       if ($index != $query->num_rows()) {
           $total = ($key->total / $pembanding)*100 ;
           $grand += $total;
      }
      else{
           if ($pembanding == 0) {
               $pembanding = 1;
          }
          $total = ($key->total / $pembanding)*100 ;
          $grand += $total;
		     		//echo "Total : ".number_format($total)." key : ".number_format($key->total)." pembanding : ".$pembanding;
          $temp = (object)array("name" => $item, "y" => $total);
          array_push($data, $temp);
     }
}

}

return $data;
}

public function get_year_time_stamp($m){
    if ($m=="day") {
        $string="SELECT day AS date, total FROM (
                              SELECT lt.Day , COALESCE(tor.date, day(NOW())) AS date,
                              COALESCE(tor.mon, MONTH(NOW())) AS mon, 
                              COALESCE(tor.year, YEAR(NOW())) AS year, 
                              COALESCE(SUM(tor.total),0) as total FROM ltdays lt LEFT JOIN (
                                                  select YEAR(settledDate) AS year, MONTH(settledDate) AS mon, DAY(settledDate) AS date, WEEKDAY(settledDate) AS week, SUM(total) AS total from `transorder` 
                                                  WHERE transStatus = 'A' AND year(settledDate) =".$this->db->escape(date('Y'))."
                                                  AND settledDate between date_add(".$this->db->escape(date('Y-m-d')).", INTERVAL -6 DAY) 
                                                  AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))."
                                                  GROUP BY YEAR(settledDate),MONTH(settledDate),DAY(settledDate)
                                                  ) tor on lt.ID =(tor.week+1) GROUP BY lt.Day 
                              ) AS a
                              ORDER BY a.year ASC, a.mon ASC, a.date ASC";
   }
   elseif ($m=="month") {

   }
   else{
        $string="SELECT year(transDate) as date FROM `transorder` GROUP BY date ORDER BY date";
   }
   $query = $this->db->query($string);

   $data = array();
   $res = $query->result();

   foreach ($res as $key) {
        array_push($data, $key->date);
   }

     	//var_dump($data);

   return $data;
}

public function get_ppn($m, $pos, $sd, $ed){
    if ($m == 'day') {
        $query = "SELECT a.date, a.mon, a.year, day, total FROM (
                              SELECT lt.Day , COALESCE(tor.date, day(NOW())) AS date,
                              COALESCE(tor.mon, MONTH(NOW())) AS mon, 
                              COALESCE(tor.year, YEAR(NOW())) AS year, 
                              COALESCE(SUM(tor.total),0) as total FROM ltdays lt LEFT JOIN (
                                                  select YEAR(settledDate) AS year, MONTH(settledDate) AS mon, DAY(settledDate) AS date, WEEKDAY(settledDate) AS week, SUM(ppn) AS total from `transorder` 
                                                  WHERE transStatus = 'A' AND year(settledDate) =".$this->db->escape(date('Y'))."
                                                  AND settledDate between date_add(".$this->db->escape(date('Y-m-d')).", INTERVAL -6 DAY) 
                                                  AND ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))."
                                                  GROUP BY YEAR(settledDate),MONTH(settledDate),DAY(settledDate)
                                                  ) tor on lt.ID =(tor.week+1) GROUP BY lt.Day 
                              ) AS a
                              ORDER BY a.year ASC, a.mon ASC, a.date ASC";
        # code...
   }
   elseif ($m == 'month') {
        $query="SELECT lt.ltID, lt.month_name as month,COALESCE(SUM(ppn),0) as total  FROM `ltmonth` lt LEFT JOIN ( select * from `transorder` WHERE transStatus = 'A' AND year(settledDate)='".date('Y')."') toa on lt.ltid=month(toa.settledDate) GROUP BY lt.month_name ORDER BY lt.ltID";
   }
   elseif ($m == 'year'){
        $query = "SELECT year(settledDate) as date, sum(ppn) as total FROM transorder where transStatus = 'A' GROUP BY date ORDER BY date";
   } else {
        $query = "SELECT a.date, a.mon, a.year, day, total FROM (
                              SELECT lt.Day , COALESCE(tor.date, day(NOW())) AS date,
                              COALESCE(tor.mon, MONTH(NOW())) AS mon, 
                              COALESCE(tor.year, YEAR(NOW())) AS year, 
                              COALESCE(SUM(tor.total),0) as total FROM ltdays lt LEFT JOIN (
                                                  select YEAR(settledDate) AS year, MONTH(settledDate) AS mon, DAY(settledDate) AS date, WEEKDAY(settledDate) AS week, SUM(ppn) AS total from `transorder` 
                                                  WHERE transStatus = 'A' AND year(settledDate) =".$this->db->escape(date('Y'))."
                                                  AND settledDate between ".$this->db->escape(date('Y-m-d', strtotime($sd)))." 
                                                  AND ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND PosID LIKE '".$pos."%'
                                                  GROUP BY YEAR(settledDate),MONTH(settledDate),DAY(settledDate)
                                                  ) tor on lt.ID =(tor.week+1) GROUP BY lt.Day 
                              ) AS a
                              ORDER BY a.year ASC, a.mon ASC, a.date ASC";
   }
   $query = $this->db->query($query);
   $data = array();
   $res = $query->result();
   foreach ($res as $obj) {
        $total = $obj->total;
        array_push($data, $total);
   }

   return $data;
}
public function get_all_branch(){
    $query = $this->db->query("SELECT COUNT(td.POSID) as total,p.latitude,p.longitude, p.POSID,p.POSNm,p.Address,p.Telp,p.Fax FROM `pos` p LEFT join `transorder` td on p.POSID = td.POSID WHERE p.active = '1' GROUP BY p.POSID");

     	/*$res = $query->result();
     	$data = array( );
     	foreach ($res as $key) {
     		$temp = (object)array("POSID" => $key->POSID, "latitude" => $key->latitude, "longitude"=> $key->longitude);
     		array_push($data, $temp);
     	}
*/
     	return $query->result();
     }

     public function get_total_omzet($m, $pos, $sd, $ed){
          if ($m=="day") {
               $str = "SELECT COALESCE(SUM(tok.total),0) as total FROM transorder tok RIGHT JOIN pos p ON tok.PosID = p.POSID WHERE tok.settledDate BETWEEN '".date("Y-m-d ")."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))." AND transStatus = 'A'";
          }
          elseif ($m=="month") {
               $str = "SELECT COALESCE(SUM(tok.total),0) as total FROM transorder tok RIGHT JOIN pos p ON tok.PosID = p.POSID WHERE month(tok.settledDate) = '".date("m")."' and year(tok.settledDate) = '".date("Y")."' AND transStatus = 'A'";
          }
          elseif ($m=="year") {
               $str = "SELECT COALESCE(SUM(tok.total),0) as total FROM transorder tok RIGHT JOIN pos p ON tok.PosID = p.POSID WHERE year(tok.settledDate) = '".date("Y")."' AND transStatus = 'A'";
          } else {
               $str = "SELECT COALESCE(SUM(tok.total),0) as total FROM transorder tok RIGHT JOIN pos p ON tok.PosID = p.POSID WHERE tok.settledDate BETWEEN '".date("Y-m-d ", strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND transStatus = 'A' AND tok.PosID LIKE '".$pos."%'";
          }
          $query = $this->db->query($str);

          if ($query->row()->total == "" || $query->row()->total == '0') {
               return 0;
          }
          else{
               return $query->row()->total;
          }
     }

     public function get_total_profit($m, $pos, $sd, $ed){
          if ($m=="day") {
               $str = "SELECT SUM((COALESCE(total,0) - COALESCE(hpp,0))) as total FROM 
                    transorder tok LEFT JOIN (
                    Select tk.transID, tk.PosID, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
                     FROM transorder tk LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                     LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                     left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                     LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                     JOIN scalar as s on s.scalarID = i.scalarID AND s.PosID = i.PosID
                     JOIN scalar as s2 on s2.scalarID = mi.scalarID AND s2.PosID = mi.PosID
                      WHERE tk.transStatus = 'A' AND tk.settledDate BETWEEN ".date("Y-m-d ")." and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")))."
                     group by tk.transID, tk.PosID
                     ) tod on tok.transID = tod.transID and tok.PosID = tod.PosID
                     RIGHT JOIN pos p ON tok.PosID = p.POSID
                     WHERE tok.transStatus = 'A' AND tok.settledDate BETWEEN ".date("Y-m-d ")." and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
          }
          elseif ($m=="month") {
               $str = "SELECT SUM((COALESCE(total,0) - COALESCE(hpp,0))) as total FROM 
                    transorder tok LEFT JOIN (
                    Select tk.transID, tk.PosID, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
                     FROM transorder tk LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                     LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                     left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                     LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                     JOIN scalar as s on s.scalarID = i.scalarID AND s.PosID = i.PosID
                     JOIN scalar as s2 on s2.scalarID = mi.scalarID AND s2.PosID = mi.PosID
                     WHERE tk.transStatus = 'A' AND month(tk.settledDate) = '".date("m")."' and year(tk.settledDate) = '".date("Y")."'
                     group by tk.transID, tk.PosID
                     ) tod on tok.transID = tod.transID and tok.PosID = tod.PosID
                     RIGHT JOIN pos p ON tok.PosID = p.POSID
                     WHERE tok.transStatus = 'A' AND month(tok.settledDate) = '".date("m")."' and year(tok.settledDate) = '".date("Y")."'";
          }
          elseif ($m=="year") {
               $str = "SELECT SUM((COALESCE(total,0) - COALESCE(hpp,0))) as total FROM 
                    transorder tok LEFT JOIN (
                    Select tk.transID, tk.PosID, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
                     FROM transorder tk LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                     LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                     left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                     LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                     JOIN scalar as s on s.scalarID = i.scalarID AND s.PosID = i.PosID
                     JOIN scalar as s2 on s2.scalarID = mi.scalarID AND s2.PosID = mi.PosID
                     WHERE tk.transStatus = 'A' AND year(tk.settledDate) = '".date("Y")."'
                     group by tk.transID, tk.PosID
                     ) tod on tok.transID = tod.transID and tok.PosID = tod.PosID
                     RIGHT JOIN pos p ON tok.PosID = p.POSID
                     WHERE tok.transStatus = 'A' AND year(tok.settledDate) = '".date("Y")."'";
          } else {
            $str = "SELECT SUM((COALESCE(total,0) - COALESCE(hpp,0))) as total FROM 
                    transorder tok LEFT JOIN (
                    Select tk.transID, tk.PosID, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
                     FROM transorder tk LEFT JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
                     LEFT JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID
                     left join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
                     LEFT JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
                     JOIN scalar as s on s.scalarID = i.scalarID AND s.PosID = i.PosID
                     JOIN scalar as s2 on s2.scalarID = mi.scalarID AND s2.PosID = mi.PosID
                      WHERE tk.transStatus = 'A' AND tk.settledDate BETWEEN '".date("Y-m-d", strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))."
                     group by tk.transID, tk.PosID
                     ) tod on tok.transID = tod.transID and tok.PosID = tod.PosID
                     RIGHT JOIN pos p ON tok.PosID = p.POSID
                     WHERE tok.transStatus = 'A' AND tok.settledDate BETWEEN '".date("Y-m-d", strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND tok.PosID LIKE '".$pos."%'";
          }
          $query = $this->db->query($str);

          if ($query->row()->total == "" || $query->row()->total == '0') {
               return 0;
          }
          else{
               return $query->row()->total;
          }
     }

     public function get_total_visit($m, $pos, $sd, $ed){
     	if ($m=="day") {
     		$string ="SELECT COUNT(td.POSID) as total FROM `pos` p join `transorder` td on p.POSID = td.POSID WHERE p.active = '1' and td.transDate BETWEEN '".date("Y-m-d ")."' and ".$this->db->escape(date('Y-m-d', strtotime("+1 days")));
     	}
     	elseif ($m=="month") {
     		$string = "SELECT COUNT(td.POSID) as total FROM `pos` p join `transorder` td on p.POSID = td.POSID WHERE p.active = '1' and month(td.transDAte) = '".date("m")."' and year(td.transDate) = '".date("Y")."'";
     	}
     	elseif ($m=="year"){
     		$string = "SELECT COUNT(td.POSID) as total FROM `pos` p join `transorder` td on p.POSID = td.POSID WHERE p.active = '1' and year(td.transDate) = '".date('Y')."'";
     	} else {
        $string ="SELECT COUNT(td.POSID) as total FROM `pos` p join `transorder` td on p.POSID = td.POSID WHERE p.active = '1' and td.transDate BETWEEN '".date("Y-m-d", strtotime($sd))."' and ".$this->db->escape(date('Y-m-d', strtotime($ed)))." AND td.PosID LIKE '".$pos."%'";
      }

     	$query = $this->db->query($string);

     	if ($query->row()->total == "" || $query->row()->total == '0') {
     		return 0;
     	}
     	else{
     		return $query->row()->total;
     	}
     }

     /*public function get_branch_revenue($POSID){
     	if($m=="day")
     					{
     						$str="transStatus='A' and transDate = '".date('Y-m-d')."' and POSID = '".$POSID."'  ";
     					}
     					else if($m=="month")
     					{
     						$str="transStatus='A' and month(transDate) = '".date('m')."' and POSID = '".$POSID."'  ";
     					}
     					else{
     						$str=" transStatus='A' and year(transDate) = '".date('Y')."' and POSID = '".$POSID."' ";
     					}
     	$query = $this->db->select("SUM(total) AS total")
     					->from("transorder")
     					->where ($str)
     					->get();
     	if ($query->row()->total == '') {
     		return 0;
     	}
     	else{
     		return $query->row()->total;
     	}
     }*/

     public function get_branch_cost($POSID)
     {
     	if($m=="day")
          {
              $str="SELECT SUM(m.price) as total FROM `transorder` tok JOIN `transorderdetail` tod ON tok.transID=tod.transID JOIN `menu` m on tod.menuID=m.menuID WHERE POSID = '".$POSID."' and tok.transDate BETWEEN '".date('Y-m-d')." 00:00:00' and '".date('Y-m-d')." 23:59:59'  ";
         }
         else if($m=="month")
         {
              $str="SELECT SUM(m.price) as total FROM `transorder` tok JOIN `transorderdetail` tod ON tok.transID=tod.transID JOIN `menu` m on tod.menuID=m.menuID WHERE POSID = '".$POSID."' and month(tok.transDate) = '".date('m')."' ";
         }
         else{
              $str="SELECT SUM(m.price) as total FROM `transorder` tok JOIN `transorderdetail` tod ON tok.transID=tod.transID JOIN `menu` m on tod.menuID=m.menuID WHERE POSID = '".$POSID."' and year(transDate) = '".date('Y')."' ";
         }
         $query = $this->db->query($str);
         if ($query->row()->total == '') {
             return 0;
        }
        else{
             return $query->row()->total;
        }
   }

   public function get_branch_revenue($POSID, $sd, $ed){
     $query = $this->db->query("SELECT SUM(COALESCE(total,0)) AS total, SUM(COALESCE(hpp,0)) AS cost, SUM((COALESCE(total,0) - COALESCE(hpp,0))) as profit FROM 
transorder tok LEFT JOIN (
Select tk.transID, tk.PosID, SUM(i.bprice * s2.ratio / s.ratio) AS hpp
 FROM transorder tk JOIN transorderdetail tod ON tk.transID=tod.transID AND tk.PosID = tod.PosID
 JOIN menu m on tod.menuID=m.menuID AND tod.PosID = m.PosID join menuing mi ON m.menuID = mi.menuID AND m.PosID = mi.PosID
 JOIN ingridients i ON mi.ingID = i.ingID AND mi.PosID = i.PosID
 JOIN scalar as s on s.scalarID = i.scalarID AND s.PosID = i.PosID
 JOIN scalar as s2 on s2.scalarID = mi.scalarID AND s2.PosID = mi.PosID
  WHERE tk.transStatus = 'A' AND tk.POSID=".$this->db->escape($POSID)."
 GROUP BY tk.transID, tk.PosID
 ) tod on tok.transID = tod.transID AND tod.PosID = tok.PosID
 RIGHT JOIN pos p ON tok.PosID = p.POSID
 WHERE tok.transStatus = 'A' AND p.POSID=".$this->db->escape($POSID)." AND tok.settledDate BETWEEN '".date('Y-m-d', strtotime($sd))."' AND '".date('Y-m-d', strtotime($ed))."'");
     return $query->row();
}
     /*
     public function get_branch_cost($POSID){
          $query = $this->db->query("SELECT p.POSID , COALESCE(SUM(i.bprice),0) as profit FROM transorder tok LEFT JOIN transorderdetail tod ON tok.transID=tod.transID LEFT JOIN menu m on tod.menuID=m.menuID left join menuing mi ON m.menuID = mi.menuID LEFT JOIN ingridients i ON mi.menuIngID = i.ingID RIGHT JOIN pos p ON tod.PosID = p.POSID GROUP BY p.POSID WHERE p.PosID = ".$this->db->escape($POSID)." ");
          return $query->row()->total;
     }
     */
     

}

?>