<?php

/**
* 
*/
class Sub_category_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT * FROM `subcategory` WHERE `subCategoryID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT sc.subCategoryID,cm.categoryNm,sc.subCategoryNm,sc.PosID FROM subcategory sc JOIN categorymenu cm on sc.categoryID = cm.categoryID AND sc.PosID = cm.PosID WHERE sc.PosID=".$this->db->escape($req));
		return $query->result();
	}

	public function get_type($pos){
		$query = $this->db->query("SELECT * FROM `categorymenu` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function checkExists($id) {
		$this->db->where("subCategoryID", $id);
		$query = $this->db->get("subcategory");
		return ($query->row() != null) ? true : false;
	}

	public function add($data){
		//$this->db->set($data);
		$this->db->insert("subcategory",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("subCategoryID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("subcategory");
	}

	public function delete($req,$pos){
		$this->db->where("subCategoryID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("subcategory");
	}

}