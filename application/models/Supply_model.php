<?php

/**
* 
*/
class Supply_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_all_supply($req){
		$query = $this->db->query("SELECT * FROM stock_check_payment AS scp 
								   JOIN supplier AS s ON scp.ID_supplier = s.ID_supplier AND scp.PosID = s.PosID  
								   JOIN users AS u ON scp.ID_user = u.userID AND scp.PosID = u.PosID 
								   JOIN employee AS e ON u.employeeID = e.employeeID AND u.PosID = e.PosID
								   WHERE s.PosID = '".$req."' ");
		return $query->result();
	}
}