<?php

/**
* 
*/
class User_model extends CI_Model
{
	
	function __construct()
	{
		# code...
		parent::__construct(); 
	}

	public function get_all_user(){
		$query = $this->db->get("user_web");
		return $query->result();
	}

	public function get_user_by_username($username) {
		$this->db->where("username", $username);
		$query = $this->db->get("users");
		$result = $query->row();
		return $result;
	}

	public function get_pos($userID) {
		$this->db->where("userID", $userID);
		$query = $this->db->get("web_pos");
		$result = $query->result();
		$pos = array();
		foreach ($result as $key) {
			array_push($pos, $key->PosID);
		}
		return $pos;
	}

	public function get_pos_auth($userID) {
		$this->db->where("userID", $userID);
		$query = $this->db->get("web_pos");
		$result = $query->result();
		$pos = array();
		foreach ($result as $key) {
			$pos[$key->PosID] = (object)$key;
		}
		return $pos;
	}

	public function get_pos_auth_branch($userID, $PosID) {
		$this->db->where("userID", $userID);
		$this->db->where("PosID", $PosID);
		$query = $this->db->get("web_pos");
		$result = $query->row();
		return $result;
	}

	public function delete_user($r){
		$this->db->where("userID",$r);
		$this->db->delete("user_web");

	}
	public function add($data){
		if ($this->db->insert('user_web',$data)) {
			return true;
		}
	}

	public function add_auth($data) {
		if ($this->db->insert('web_authority', $data)) {
			return true;
		}
	}

	public function add_pos($data) {
		foreach ($data as $d) :
			$this->db->insert("web_pos", $d);
		endforeach;
	}

	public function delete_pos($userID) {
		$this->db->where("userID", $userID);
		$this->db->delete("web_pos");
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_all_user_update($data){
		$query = $this->db->query("SELECT * FROM `user_web` WHERE `userID` ='".$this->input->get("d")."'");
		return $query->result();
	}

	public function get_all_auth_update($data){
		$query = $this->db->query("SELECT * FROM `web_authority` WHERE `userID` =".$this->db->escape($data));
		return $query->row();
	}

	public function get_all_user_id($data){
		$query = $this->db->query("SELECT * FROM `user_web` WHERE `userID` =".$this->db->escape($data));
		return $query->row();
	}

	public function update($data,$ID){
		$this->db->where("userID",$ID);
		$this->db->update("user_web", $data);
	}

	public function update_auth($data, $ID) {
		$this->db->where("userID", $ID);
		$this->db->update("web_authority", $data);
	} 

	public function get_cron() {
		$query = $this->db->get("cron_report");
		return $query->result();
	}
}