<?php

/**
* 
*/
class Staff_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $pos){
		$query = $this->db->query("SELECT * FROM `users` WHERE `userID`='".$req."' AND posID='".$pos."'");
		return $query->result();
	}

	public function get_employee($pos){
		$query = $this->db->query("SELECT DISTINCT employeeID,employee_name FROM employee WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_auth($userID, $PosID){
		$query = $this->db->query("SELECT * FROM authority WHERE userID=".$this->db->escape($userID)." AND PosID=".$this->db->escape($PosID));
		return $query->row();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `users` WHERE PosID = ".$this->db->escape($req));
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `categorymenu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function add($data){
		//$this->db->set($data);
		$this->db->insert("users",$data);
	}

	public function addAuth($data){
		//$this->db->set($data);
		$this->db->insert("authority",$data);
	}

	public function update($req,$PosID,$data){
		$this->db->set($data);
		$this->db->where("userID",$req);
		$this->db->where("PosID", $PosID);
		$this->db->update("users");
	}

	public function delete($req,$PosID){
		$this->db->where("userID",$req);
		$this->db->where("PosID", $PosID);
		$this->db->delete("users");
	}

	public function deleteAuth($req,$PosID){
		$this->db->where("userID",$req);
		$this->db->where("PosID", $PosID);
		$this->db->delete("authority");
	}
}