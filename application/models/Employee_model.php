<?php

/**
* 
*/
class Employee_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $pos){
		$query = $this->db->query("SELECT * FROM `employee` WHERE `employeeID`='".$req."' AND PosID='".$pos."'");
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `employee` WHERE PosID = ".$this->db->escape($req));
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `categorymenu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function get_position($PosID){
		$query = $this->db->query("SELECT * FROM `position` WHERE `PosID` = ".$this->db->escape($PosID));
		return $query->result();
	}

	public function add($data){
		//$this->db->set($data);
		$this->db->insert("employee",$data);
	}

	public function update($req,$PosID,$data){
		$this->db->set($data);
		$this->db->where("employeeID",$req);
		$this->db->where("PosID", $PosID);
		$this->db->update("employee");
	}

	public function delete($req, $PosID){
		$this->db->where("employeeID",$req);
		$this->db->where("PosID", $PosID);
		$this->db->delete("employee");
	}
}

?>