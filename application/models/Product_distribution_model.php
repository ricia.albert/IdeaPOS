<?php

/**
* 
*/
class Product_distribution_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_package(){
		$query = $this->db->query("SELECT * FROM `packages` WHERE `Availability`='1'");
		return $query->result();
	}

	public function get_all_product(){
		$query = $this->db->query("SELECT DISTINCT `menuID`,`menuNm`,`description`,`price` FROM `menu`");
		return $query->result();
	}

	public function get_product($req){
		$query = $this->db->query("SELECT DISTINCT menuID,menuNm FROM `menu` WHERE `menuID`='".$req."' ");
		return $query->result();
	}

	public function get_package($req){
		$query = $this->db->query("SELECT * FROM `packages` WHERE `packagesID`='".$req."' ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}

	public function get_distribute($req,$code){
		if ($req == "product") {
			$query = $this->db->query("SELECT POSID FROM `menu` WHERE `menuID`='".$code."' ");
		}
		else{
			$query = $this->db->query("SELECT POSID FROM `packages` WHERE `packagesID`='".$code."' ");
		}
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/

	public function update_product($req,$data,$state){
		//$query = $this->db->query("DELETE FROM `menu` WHERE `POSID` = '".$req."' ");
		/*if ($query->row()->menuID == null || $query->row()->menuID == "" ) {
			$this->db->insert("");
		}*/
		//$this->db->set($data);
		//$this->db->where("menuID",$req);
		if ($state == 1) {
			$this->db->delete('menu', array('menuID' => $req));
		}
		$this->db->insert("menu",$data);
	}

	public function update_package($req,$data){
		if ($state == 1) {
			$this->db->delete('packages', array('packagesID' => $req));
		}
		$this->db->insert("packages",$data);

		$this->db->set($data);
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
}

?>