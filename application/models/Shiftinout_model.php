<?php

/**
* 
*/
class Shiftinout_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req, $pos){
		$query = $this->db->query("SELECT s.*, u.username FROM shiftinout AS s
									JOIN users as u ON s.userID = u.userID AND s.PosID = u.PosID
								   WHERE `shiftID`='".$req."' AND s.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM shiftinout WHERE POSID = '".$req."' ");
		return $query->result();
	}

	public function get_type(){
		$query = $this->db->query("SELECT * FROM `menu` ");
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("menupricelist",$data);
	}

	public function update($req,$data){
		$this->db->set($data);
		$this->db->where("menuPriceID",$req);
		$this->db->update("menupricelist");
	}

	public function delete($req){
		$this->db->where("menuPriceID",$req);
		$this->db->delete("menupricelist");
	}

}

?>