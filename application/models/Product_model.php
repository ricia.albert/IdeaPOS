<?php

/**
* 
*/
class Product_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_Product($req, $pos){
		$query = $this->db->query("SELECT * FROM menu WHERE menuID ='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}
	public function get_Product_query($req, $pos){
		$query = $this->db->query("SELECT * FROM menu WHERE menuID ='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->row();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM menu WHERE POSID = '".$req."' ");
		return $query->result();
	}

	public function get_same_code($codeMenu){
		$query = $this->db->query("SELECT * FROM ingridients WHERE codeIng = '".$codeMenu."' ");
		return $query->result();
	}

	public function get_available_pos($menuCode) {
		$query = $this->db->query("SELECT m.PosID, p.PosNm FROM menu AS m JOIN pos AS p ON m.PosID = p.PosID WHERE codeMenu=".$this->db->escape($menuCode));
		return $query->result();
	}

	public function get_copy_pos($menuCode) {
		$query = $this->db->query("SELECT PosID, PosNm FROM pos
									WHERE PosID NOT IN(SELECT PosID FROM menu
									WHERE codeMenu=".$this->db->escape($menuCode).")");
		return $query->result();
	}

	public function get_ID($PosID){
		$query = $this->db->query("SELECT RIGHT(MAX(menuID),8) AS ID FROM menu WHERE PosID=".$this->db->escape($PosID));
		$row = $query->row();
		$index = $row->ID + 1;
		return "MN".str_pad($index,8,"0",STR_PAD_LEFT);
	}

	public function get_all_ing($PosID){
		$query = $this->db->query("SELECT * FROM ingridients AS i JOIN scalar AS s ON i.scalarID = s.scalarID AND i.PosID = s.PosID WHERE i.PosID=".$this->db->escape($PosID));
		return $query->result();
	}

	public function get_all_choosen($req, $pos){
		$query = $this->db->query("SELECT i.ingID,i.ingName,mi.qty, mi.scalarID FROM menuing mi 
									JOIN ingridients i on mi.ingID = i.ingID AND mi.PosID = i.PosID
									JOIN scalar AS s ON mi.scalarID = s.scalarID AND mi.PosID = s.PosID
									WHERE mi.menuID='".$req."' AND mi.PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_choosen_by_code($req, $pos, $toPos){
		$query = $this->db->query("SELECT i.ingID,i.ingName,j.qty, j.scalarID FROM ingridients
									AS i JOIN (
									SELECT i.ingID,i.ingName,i.codeIng,mi.qty, mi.scalarID FROM menuing mi 
									JOIN ingridients i on mi.ingID = i.ingID AND mi.PosID = i.PosID
									JOIN scalar AS s ON mi.scalarID = s.scalarID AND mi.PosID = s.PosID
									WHERE mi.menuID='".$req."' AND mi.PosID=".$this->db->escape($pos)."
									) AS j ON i.codeIng = j.codeIng
									WHERE PosID=".$this->db->escape($toPos));
		return $query->result();
	}

	public function get_sub($PosID){
		$this->db->where("PosID",$PosID);
		$this->db->order_by("subCategoryNm", "asc");
		$query = $this->db->get("subcategory");//SELECT `subCategoryID`,`subCategoryNm` FROM ``
		return $query->result();
	}

	public function checkExists($name) {
		$this->db->where("menuNm", $name);
		$query = $this->db->get("menu");
		return ($query->row() != null) ? true : false;
	}

	public function add($data){
		if ($this->db->insert('menu',$data)) {
			return true;
		}
	}
	public function add_sub($data){
		$this->db->insert('menuing',$data);
	}
	public function update($menuID,$PosID,$data){
		$this->db->set($data);
		$this->db->where("menuID",$menuID);
		$this->db->where("PosID", $PosID);
		$this->db->update("menu");
	}

	public function delete($data, $PosID){
		$this->db->where("menuID",$data);
		$this->db->where("PosID", $PosID);
		$this->db->delete("menu");
	}
	public function delete_sub($data){
		$this->db->where("menuID",$data);
		$this->db->delete("menuing");
	}
}