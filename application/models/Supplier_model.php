<?php

/**
* 
*/
class Supplier_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM `supplier` WHERE PosID = ".$this->db->escape($req));
		return $query->result();
	}

	public function get_member($req, $pos){
		$query = $this->db->query("SELECT * FROM supplier WHERE ID_supplier = '".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function add($data){
		if ($this->db->insert('supplier',$data)) {
			return true;
		}
	}
	public function update($data,$menuID, $PosID){
		$this->db->set($data);
		$this->db->where("ID_supplier",$menuID);
		$this->db->where("PosID",$PosID);
		$this->db->update("supplier");
	}

	public function delete($data, $pos){
		$this->db->where("ID_supplier",$data);
		$this->db->where("PosID",$pos);
		$this->db->delete("supplier");
	}
}