<?php

/**
* 
*/
class Category_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_product($req,$pos){
		$query = $this->db->query("SELECT * FROM `categorymenu` WHERE `categoryID`='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_product($pos){
		$query = $this->db->query("SELECT cm.categoryID,cm.categoryNm,tp.typeCode,cm.PosID FROM categorymenu cm JOIN menutype tp ON cm.typeID=tp.typeID AND cm.PosID = tp.PosID WHERE cm.PosID = ".$this->db->escape($pos));
		return $query->result();
	}

	public function get_type($pos){
		$query = $this->db->query("SELECT typeID, typeCode, typeText FROM `menutype` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	public function add($data){
		//$this->db->set($data);
		$this->db->insert("categorymenu",$data);
	}

	public function update($req,$pos,$data){
		$this->db->set($data);
		$this->db->where("categoryID",$req);
		$this->db->where("PosID",$pos);
		$this->db->update("categorymenu");
	}

	public function delete($req,$pos){
		$this->db->where("categoryID",$req);
		$this->db->where("PosID",$pos);
		$this->db->delete("categorymenu");
	}

}