<?php

/**
* 
*/
class Mapping_stock_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
		public function add($data){
		//$this->db->set($data);
		$this->db->insert("mapping_stock",$data);
	}
		public function update($req,$PosID,$data){
		$this->db->set($data);
		$this->db->where("ingID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->update("mapping_stock");
	}
	/*public function get_product($req, $pos){
		$query = $this->db->query("SELECT * FROM `ingridients` WHERE `ingID`='".$req."' AND PosID='".$pos."'");
		return $query->result();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_stock_opname($pos, $tgl){
		$query = $this->db->query("SELECT opnamedetID, sod.opnameID, sod.ingID, i.codeIng, i.ingName, sod.jml_temp, sod.scalarID_temp, sc1.scalarNm AS scalarTemp, sc1.ratio AS ratioTemp, sod.jml_actual, sod.scalarID_actual
									, sc.scalarNm AS scalarActual,sc.ratio AS ratioActual, sod.jml_barang, sod.scalarID_barang, sc2.scalarNm AS scalarBarang, 
									sc2.ratio AS ratioBarang, COALESCE(sod.price,0) AS bprice, sod.last_update, sod.userID FROM stockopnamedetail AS sod 
									JOIN stockopname AS sop ON sod.opnameID = sop.opnameID AND sod.PosID = sop.PosID
									JOIN ingridients AS i ON sod.ingID = i.ingID AND sod.PosID = i.PosID
									JOIN scalar AS sc1 ON sod.scalarID_temp = sc1.scalarID AND sod.PosID = sc1.PosID
									JOIN scalar AS sc ON sod.scalarID_actual = sc.scalarID AND sod.PosID = sc.PosID
									JOIN scalar AS sc2 ON sod.scalarID_barang = sc2.scalarID AND sod.PosID = sc2.PosID
									WHERE sop.posID =".$this->db->escape($pos)." AND sop.opnameDate='".date('Y-m-d', strtotime($tgl))."'");
		return $query->result();
	}

	public function get_all_product($req){
		if ($req == "") {
			$query = $this->db->query("SELECT i.ingID,ic.ingCateName,i.ingName,i.info,i.qty,i.minQty,s.scalarNm,i.bprice,i.PosID, i.codeIng 
									FROM ingridients i 
									LEFT JOIN scalar s on i.scalarID = s.scalarID AND i.PosID = s.PosID 
									LEFT JOIN ingcate ic ON i.cateIngID = ic.cateIngID AND i.PosID = ic.PosID");
		} else {
			$query = $this->db->query("SELECT i.ingID,ic.ingCateName,i.ingName,i.info,i.qty,i.minQty,s.scalarNm,i.bprice,i.PosID, i.codeIng 
									FROM ingridients i 
									LEFT JOIN scalar s on i.scalarID = s.scalarID AND i.PosID = s.PosID 
									LEFT JOIN ingcate ic ON i.cateIngID = ic.cateIngID AND i.PosID = ic.PosID
									WHERE i.PosID=".$this->db->escape($req));
		}
		return $query->result();
	}

	public function get_available_pos($menuCode) {
		$query = $this->db->query("SELECT m.PosID, p.PosNm FROM ingridients AS m JOIN pos AS p ON m.PosID = p.PosID WHERE codeIng=".$this->db->escape($menuCode));
		return $query->result();
	}

	public function get_copy_pos($menuCode) {
		$query = $this->db->query("SELECT PosID, PosNm FROM pos
									WHERE PosID NOT IN(SELECT PosID FROM ingridients
									WHERE codeIng=".$this->db->escape($menuCode).")");
		return $query->result();
	}

	public function get_ID($PosID){
		$query = $this->db->query("SELECT RIGHT(MAX(ingID),12) AS ID FROM ingridients WHERE PosID=".$this->db->escape($PosID));
		$row = $query->row();
		$index = $row->ID + 1;
		return "IG".str_pad($index,12,"0",STR_PAD_LEFT);
	}


	public function get_category($pos){
		$query = $this->db->query("SELECT distinct cateIngID, ingCateName FROM `ingcate` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_scalar($pos){
		$query = $this->db->query("SELECT distinct scalarID, scalarNm, ratioID FROM `scalar` WHERE PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM `pos` WHERE `active` = '1'");
		return $query->result();
	}*/
/*
	public function get_rid_product($req){
		$this->db->set("distribution","");
		$this->db->where("menuID",$req);
		$this->db->update("menu");
	}

	public function get_rid_package($req){
		$this->db->set("distribution","");
		$this->db->where("packagesID",$req);
		$this->db->update("packages");
	}
*/
	/*

	

	public function updateCode($code,$PosID,$data){
		$this->db->set($data);
		$this->db->where("codeIng",$code);
		$this->db->where("PosID",$PosID);
		$this->db->update("ingridients");
	}

	public function clearCode($codeIng,$data){
		$this->db->set($data);
		$this->db->where("codeIng",$codeIng);
		$this->db->update("ingridients");
	}

	public function delete($req,$PosID){
		$this->db->where("ingID",$req);
		$this->db->where("PosID",$PosID);
		$this->db->delete("ingridients");
	}

	public function fast_update($req,$id,$PosID){
		$this->db->set('qty',$req);
		$this->db->where("ingID",$id);
		$this->db->where("PosID",$PosID);
		$this->db->update("ingridients");
	}*/
}