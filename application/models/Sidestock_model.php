<?php

/**
* 
*/
class Sidestock_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_Product($req, $pos){
		$query = $this->db->query("SELECT * FROM sidestock WHERE SideStockID ='".$req."' AND PosID=".$this->db->escape($pos));
		return $query->result();
	}

	public function get_branch($pos){
		$query = $this->db->query("SELECT * FROM pos WHERE posID =".$this->db->escape($pos));
		return $query->row();
	}

	public function get_all_branch(){
		$query = $this->db->query("SELECT * FROM pos WHERE active ='1'");
		return $query->result();
	}

	public function get_available_pos($menuCode) {
		$query = $this->db->query("SELECT m.PosID, p.PosNm FROM sidestock AS m JOIN pos AS p ON m.PosID = p.PosID WHERE codeSide=".$this->db->escape($menuCode));
		return $query->result();
	}

	public function get_copy_pos($menuCode) {
		$query = $this->db->query("SELECT PosID, PosNm FROM pos
									WHERE PosID NOT IN(SELECT PosID FROM sidestock
									WHERE codeSide=".$this->db->escape($menuCode).")");
		return $query->result();
	}

	public function get_ID($PosID){
		$query = $this->db->query("SELECT RIGHT(MAX(SideStockID),8) AS ID FROM sidestock WHERE PosID=".$this->db->escape($PosID));
		$row = $query->row();
		$index = $row->ID + 1;
		return "SS".str_pad($index,8,"0",STR_PAD_LEFT);
	}

	public function get_all_product($req){
		$query = $this->db->query("SELECT * FROM sidestock WHERE POSID = ".$this->db->escape($req));
		return $query->result();
	}
	public function get_sub($PosID){
		$this->db->where("PosID",$PosID);
		$query = $this->db->get("categorysproduct");//SELECT `subCategoryID`,`subCategoryNm` FROM ``
		return $query->result();
	}
	public function add($data){
		if ($this->db->insert('sidestock',$data)) {
			return true;
		}
	}
	public function update($data,$menuID,$PosID){
		$this->db->set($data);
		$this->db->where("SideStockID",$menuID);
		$this->db->where("PosID",$PosID);
		$this->db->update("sidestock");
	}

	public function delete($data,$PosID){
		$this->db->where("SideStockID",$data);
		$this->db->where("PosID",$PosID);
		$this->db->delete("sidestock");
	}
}