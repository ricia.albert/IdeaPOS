<?php

class Approval_Model extends CI_Model
{
	static $STOCK_REQ = "S";
	static $PRICE_REQ = "P";
	static $TRANSFER_REQ = "T";
	static $STAT_NEW = "N";
	static $STAT_APPROVED = "A";
	static $STAT_REJECTED = "R";
	function __construct()
	{
		parent::__construct();
	}

	public function getStockRequest()
	{
		$sql = "SELECT approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				toPosID, requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, message, fID,
				p.PosNm As FromPOS, 
				p2.PosNm AS ToPOS,
				u.username
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				JOIN pos AS p2 ON a.toPOSID = p2.POSID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE approvalType = ".$this->db->escape(Approval_model::$STOCK_REQ)." AND approvalStats=".$this->db->escape(Approval_model::$STAT_NEW);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getAllStockRequest($stats)
	{
		$sql = "SELECT approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				toPosID, requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, message, fID,
				p.PosNm As FromPOS, 
				p2.PosNm AS ToPOS,
				u.username
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				JOIN pos AS p2 ON a.toPOSID = p2.POSID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE approvalType = ".$this->db->escape(Approval_model::$STOCK_REQ)."  AND approvalStats=".$this->db->escape($stats)."
				ORDER BY approvalID DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getDiffStockRequest()
	{
		$sql = "SELECT approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				toPosID, requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, message, fID,
				p.PosNm As FromPOS, 
				p2.PosNm AS ToPOS,
				u.username
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				JOIN pos AS p2 ON a.toPOSID = p2.POSID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE approvalType = ".$this->db->escape(Approval_model::$STOCK_REQ)."  AND approvalStats='V' 
				AND fID IN (
					SELECT DISTINCT transferID
					FROM transferdetail AS td
					JOIN ingridients AS i ON td.ingID = i.ingID AND td.PosID = i.PosID
					JOIN scalar AS s ON i.scalarID = s.scalarID AND i.PosID = s.PosID
					JOIN scalar AS s2 ON td.scalarID_real = s2.scalarID AND td.PosID = s2.PosID
					JOIN scalar AS s3 ON td.scalarID_apv = s3.scalarID AND td.PosID = s3.PosID
					WHERE (qty_real * s2.ratio / s.ratio) < (qty_apv * s3.ratio / s.ratio)
				) 
				ORDER BY approvalID DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateExpired()
	{
		$sql = "UPDATE approval SET approval.approvalStats = 'E'
				WHERE approvalID in (
					SELECT approvalID FROM (SELECT * FROM approval) AS a
					JOIN settings AS s ON a.posID = s.posID
					WHERE a.approvalStats = 'N' AND a.approvalType = ".$this->db->escape(Approval_model::$PRICE_REQ)." AND TIMESTAMPDIFF(MINUTE, a.requestDt, CONVERT_TZ(NOW(),'+00:00','+07:00')) > s.expireBargain
				)";
		$query = $this->db->query($sql);
		return $query;
	}

	public function getAllPriceRequest($stats, $pos)
	{
		$sql = "SELECT approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				WHEN 'E' THEN 'Expired'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, message, fID,
				p.PosNm As FromPOS, 
				u.username, a.transID
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE approvalType = ".$this->db->escape(Approval_model::$PRICE_REQ)."  AND approvalStats=".$this->db->escape($stats)." AND a.PosID LIKE '".$pos."%'
				ORDER BY approvalID DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getAllTransferRequest($stats)
	{
		$sql = "SELECT approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, a.toPosID, message, fID,
				p.PosNm As FromPOS, 
				p2.PosNm AS ToPOS,
				u.username, a.transID
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				JOIN pos AS p2 ON a.toPosID = p2.PosID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE approvalType = ".$this->db->escape(Approval_model::$TRANSFER_REQ)."  AND approvalStats=".$this->db->escape($stats)."
				ORDER BY approvalID DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function viewTransferReq($approvalID)
	{
		$sql = "select approvalID, approvalType, 
				CASE approvalType
				WHEN 'T' THEN 'Transfer'
				WHEN 'S' THEN 'Stock'
				END AS Type,
				toPosID, requestDt, approvalStats, 
				CASE approvalStats
				WHEN 'N' THEN 'New'
				WHEN 'A' THEN 'Approved'
				WHEN 'R' THEN 'Rejected'
				WHEN 'C' THEN 'Canceled'
				WHEN 'V' THEN 'Received'
				END AS Stats, 
				approvedDt, 
				a.last_update, a.userID, a.PosID, message, fID,
				p.PosNm As FromPOS, 
				p2.PosNm AS ToPOS,
				u.username, a.transID
				from approval AS a
				JOIN pos AS p ON a.PosID = p.PosID
				LEFT JOIN pos AS p2 ON a.toPOSID = p2.POSID
				JOIN users AS u ON a.userID = u.userID AND a.POSID = u.POSID
				WHERE a.approvalID = ".$this->db->escape($approvalID);
		$res = $this->db->query($sql);
		$transfer = $res->row();

		$sql = "SELECT td.detailID, td.transferID, td.ingID, td.qty, td.scalarID, td.PosID, i.ingName, s.scalarNm, td.qty_real, COALESCE(s2.scalarNm,'-') AS scalarID_real,
				td.qty_apv, COALESCE(s3.scalarNm,'-') AS scalarID_apv, s3.scalarID as scalar_apv, td.price, td.menuID, td.menuNm, td.price_bef, td.price_apv 
				FROM transferdetail AS td
				LEFT JOIN ingridients AS i ON td.ingID = i.ingID AND td.PosID = i.PosID
				LEFT JOIN scalar AS s ON td.scalarID = s.scalarID AND td.PosID = s.PosID
				LEFT JOIN scalar AS s2 ON td.scalarID_real = s2.scalarID AND td.PosID = s2.PosID
				LEFT JOIN scalar AS s3 ON td.scalarID_apv = s3.scalarID AND td.PosID = s3.PosID
				WHERE td.transferID=".$this->db->escape($transfer->fID);
		$query = $this->db->query($sql);
		$detail = $query->result();
		foreach ($detail as $d) :
			$d->scalarList = $this->getScalar($d->scalarID, $d->PosID);
		endforeach;

		$transfer->detail = $detail;
		return $transfer;
	}

	public function getScalar($scalarID, $posID)
	{
		$sql = "SELECT ScalarID, scalarNm, ratioID, ratio from scalar where RatioID = ".$this->db->escape($scalarID)." AND PosID=".$this->db->escape($posID);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function updateTransfer($approvalID, $transfer, $POSID)
	{
		foreach ($transfer as $t) :
			$sql = "UPDATE transferdetail SET qty_apv=".$t->Qty.", scalarID_apv=".$t->ScalarID." WHERE detailID=".$this->db->escape($t->ID);
			$this->db->query($sql);
		endforeach;
		$sql = "UPDATE approval SET approvalStats=".$this->db->escape(Approval_model::$STAT_APPROVED).", approvedDt=NOW() WHERE approvalID=".$this->db->escape($approvalID);
		$this->db->query($sql);
		$sql = "CALL sp_CreateTransGdg(".$approvalID.")";
		$this->db->query($sql);
		$sql = "CALL sp_sendNotif(".$this->db->escape($POSID).",".
								 $this->db->escape("Approval request stock").",".
								 $this->db->escape("Request stock dengan ID ".$approvalID." telah diapprove.").")";
		$this->db->query($sql);
	}

	public function rejectTransfer($approvalID, $POSID)
	{
		$sql = "UPDATE approval SET approvalStats=".$this->db->escape(Approval_model::$STAT_REJECTED).", approvedDt=NOW() WHERE approvalID=".$this->db->escape($approvalID);
		$this->db->query($sql);
		$this->db->query($sql);
		$sql = "CALL sp_sendNotif(".$this->db->escape($POSID).",".
								 $this->db->escape("Approval request stock").",".
								 $this->db->escape("Request stock dengan ID ".$approvalID." telah ditolak.").")";
	}

	public function updatePrice($approvalID, $transfer, $POSID)
	{
		foreach ($transfer as $t) :
			$sql = "UPDATE transferdetail SET price_apv=".$t->Price." WHERE detailID=".$this->db->escape($t->ID);
			$this->db->query($sql);
		endforeach;
		$sql = "UPDATE approval SET approvalStats=".$this->db->escape(Approval_model::$STAT_APPROVED).", approvedDt=NOW() WHERE approvalID=".$this->db->escape($approvalID);
		$this->db->query($sql);
		$sql = "CALL sp_UpdateBargain(".$approvalID.")";
		$this->db->query($sql);
		$sql = "CALL sp_sendNotif(".$this->db->escape($POSID).",".
								 $this->db->escape("Approval request price").",".
								 $this->db->escape("Penawaran harga dengan ID ".$approvalID." telah diapprove.").")";
		$this->db->query($sql);
	}

	public function rejectPrice($approvalID, $POSID)
	{
		$sql = "UPDATE approval SET approvalStats=".$this->db->escape(Approval_model::$STAT_REJECTED).", approvedDt=NOW() WHERE approvalID=".$this->db->escape($approvalID);
		$this->db->query($sql);
		$sql = "CALL sp_UpdateBargain(".$approvalID.")";
		$this->db->query($sql);
		$sql = "CALL sp_sendNotif(".$this->db->escape($POSID).",".
								 $this->db->escape("Approval request price").",".
								 $this->db->escape("Penawaran harga dengan ID ".$approvalID." telah ditolak.").")";
		$this->db->query($sql);
	}
}