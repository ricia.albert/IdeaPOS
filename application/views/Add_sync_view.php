<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Syncronize</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-sync hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product
        </section>

          <form method="POST" id="branchForm" action="<?php echo base_url();?>Sync/update">
		 
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Product Table</h3>
                  </div>
                  <div class="box-body">
                  </select>
                  <table id="ProductTable" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Product Name</td>
                        <td>Price</td>
                        <td>Code Menu</td>
                        <td>Store Code</td>
                        <td>Store Name</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      foreach ($viewProduct as $key) {
                        $guest = $key->menuID;
                        $pos = $key->POSID;
                        ?>
                        <tr>
                          <td>
                            <input type="hidden" readonly name="codeMenu" value="<?php echo $key->codeMenu ?>" id="codeMenu">
                            <input type="hidden" readonly name="name" value="<?php echo $key->menuNm ?>" id="name">
                            <?php echo $key->menuNm; ?> (<?php echo $key->codeMenu; ?>)
                          </td>
						              <td>
                            <input type="hidden" readonly name="price" value="<?php echo $key->price ?>" id="price">
                            <?php echo number_format($key->price); ?> 
                          </td>
						              <td>
                            <input type="hidden" readonly name="menuID" value="<?php echo $key->menuID ?>" id="menuID">
                            <?php echo ($key->codeMenu); ?> 
                          </td>	
	                     <?php
                        }
                        
                      foreach ($get_branch as $key2) {
                       
                        ?>						  
						              <td>
                            <input type="hidden" readonly name="posID" value="<?php echo $key2->POSID ?>" id="posID">
                            <?php echo $key2->POSID; ?> 
                          </td>
					
						              <td>
                             <?php echo $key2->POSNm; ?> 
                          </td>
                        </tr>
                         <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>

                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Ingridients</h3>
                    </div>
                    <div class="box-body">
					              <?php
                      foreach ($viewProduct as $key) {
                        $guest = $key->menuID;
                        
                        ?>
                      <select name="brancOption" onchange="doit('<?php echo $guest ?>')" id="brancOption">
					
                      <?php
					  
                      foreach ($view_branch as $key2) {
                        ?>

						 <option <?php echo ($key->POSID==$key2->POSID) ? "selected" : "" ; ?>  value="<?php echo $key2->POSID ?>"><?php echo $key2->POSNm ?></option>
                        
                        <?php
                      }
					  }
                      ?>
                    </select>
                      <table id="menuDetail" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Ingredients ID</td>
                            <td width="55%">Ingredients Name</td>
                            <td width="15%">Branch Code</td>
                            <td width="15%">Ingridients Code</td>
                          </tr>    
                        </thead>
                        <tbody>
                          <?php
                          foreach ($allMenu as $key => $value) {
                            ?>
                            <tr>
                              <td><?php echo  $value->ingID ?></td>
                              <td><?php echo  $value->ingName ?></td>
                              <td><?php echo  $value->PosID ?></td>
                              <td><?php echo $value->codeIng ?></td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- accepted Menu -->
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Choosen</h3>
                    </div>
                    <div class="box-body">
                      <table id="acceptedMenu" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Menu Code</td>
                            <td width="60%">Menu Name</td>
                            <td width="20%">Branch Code</td>
                            <td width="5%"></td>
                          </tr>   
                        </thead>
                        <tbody>
                          <?php
                          $rowCount = 0;
                          $indexArray= array();
                          $arrayIndex = array();
                          foreach ($menuChoosen as $key) {
                            $rowCount++;
            //echo $key->menuID;
                            array_push($indexArray, $key->ingID);
                            $arrayIndex[]+= $rowCount;
                            ?>
                            <tr>
                              <td><input type="text" class="form-control" readonly name="addCode<?php echo $rowCount ?>" id="addCode<?php echo $rowCount ?>" value="<?php echo $key->ingID ?>" ></td>

                              <td><input type="text" class="form-control" readonly name="addName<?php echo $rowCount ?>" id="addName<?php echo $rowCount ?>" value="<?php echo $key->ingName ?>" ></td>

                              <td><input type="text" class="form-control" name="addPos<?php echo $rowCount ?>" id="addPos<?php echo $rowCount ?>" readonly value="<?php echo $key->PosID ?>" /></td>
                              <td><input type="button" value = "Delete" class="form-control" onClick="Javacsript:deleteRow(this)"></td>
                            </tr>
                            <?php
                          }
                          ?>
                          <div style="display: none" name="indexArray" id="indexArray" ><?php echo json_encode($indexArray) ?></div>
                          <div style="display: none" name="arrayIndex" id="arrayIndex" ><?php echo json_encode($arrayIndex) ?></div>
                          <input type="hidden" name="indexInsert" value="<?php echo $rowCount ?>" id="indexInsert">
                        </tbody>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn" />
                    </div>
                  </div>
                </div>
                
              </div>
            </section>
          </form>

        
      </div>
      <?php include "includes/footer.php"; ?>
      <script type="text/javascript" src="<?php echo base_url();?>dist/js/sync.js"></script>
</body>
</html>
