<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Staff Account</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-staff hold-transition skin-blue sidebar-mini">
<!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
<div class="wrapper">

  <?php require("includes/header.php") ?>
  <body>
  <!-- Left side column. contains the logo and sidebar -->
  <?php require("includes/navigation.php") ?>
  <div class="content-wrapper">
    <section class="content-header">
      Staff Account
    </section>

    <form method="POST" id="productView" action="<?php echo base_url(); ?>Staff">
      <input type="hidden" name="action" id="action">
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Staff Table</h3>
              </div>
              <div class="box-body">
              <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                <table id="branchTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td>Staff ID</td>
                      <td>UserName</td>
                      <td>Active</td>
                      <td>Expired</td>
                      <td>Expired Date</td>
                      <td>PosID</td>
                      <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                        <td></td>
                      <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
<?php
                    foreach ($view_product as $key =>$value) {
                      //var_dump($value)
?>
                    <tr>
                      <td><?php echo $value->userID ?></td>
                      <td><?php echo $value->Username ?></td>
                      <td><?php echo ($value->Active == '1') ? "Active" : "Non active"; ?></td>
                      <td align="center"><input type="checkbox" disabled <?php echo ($value->Expired == 1) ? "checked" : ""; ?> /></td>
                      <td><?php echo $value->Expired_date ?></td>
                      <td><?php echo $value->PosID ?></td>
                      <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                        <td>
                          <a href="<?php echo base_url(); ?>Staff/update?d=<?php echo $value->userID ?>&p=<?php echo $value->PosID ?>"><input type="button" name="updtButton" id="updtButton" value="Update" ></a>
                          <a href="<?php echo base_url(); ?>Staff/delete?staff=<?php echo $value->userID ?>&p=<?php echo $value->PosID; ?>"><input type="button" name="dltButton" id="dltButton" onclick="confirmation('<?php echo $value->userID ?>')" value="Delete"></a>
                        </td>
                      <?php endif; ?>
                    </tr>
<?php
                    }
?>
                   </tbody>
                </table>
                <a href="<?php echo base_url(); ?>Staff/add_employee_view?b=<?php echo $temp; ?>"><input type="button" class="form-control" name="addBttn" id="addBttn" value="Add New"></a>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>

  </div>
  <?php include "includes/footer.php"; ?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/staff.js"></script>
</body>
</html>
