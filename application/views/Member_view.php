<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Customers</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-customer hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Customer
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Member">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Member Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td width="160px">Guest Name</td>
                          <td>Type</td>
                          <td width="200px">Address</td>
                          <td>Phone</td>
                          <td>Email</td>
                          <td>Status</td>
                          <td>Limit Debt</td>
                          <?php if ($this->session->userdata("customerAuth") == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->Guest_name ?></td>
                            <td><?php echo ($value->guest_type == "C") ? "Company" : "Personal"; ?></td>
                            <td><?php echo $value->Address ?></td>
                            <td><?php echo $value->Phone ?></td>
                            <td><?php echo $value->Email ?></td>
                            <td><?php echo $value->Info ?></td>
                            <td><?php echo number_format($value->limit_debt,0,",","."); ?></td>
                            <?php if ($this->session->userdata("customerAuth") == AUTH_WRITE) : ?>
                              <td>
                                <a href="<?php base_url(); ?>Member/update?menu=<?php echo $value->guestID ?>&pos=<?php echo $value->PosID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->guestID ?>','<?php echo $value->PosID; ?>')" value="Delete"></a>
                                <a href="<?php base_url(); ?>Member/special?menu=<?php echo $value->guestID ?>&pos=<?php echo $value->PosID; ?>"><input type="button" name="specialBttn" id="specialBttn" value="Special Price"></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($this->session->userdata("customerAuth") == AUTH_WRITE) : ?>
                      <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                      <?php endif; ?>
                    </div>
                  </div>
                   
                </div>
               
              </div>

            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>
      <!-- ./wrapper -->
    <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/member.js"></script>

</body>
</html>
