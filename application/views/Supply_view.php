<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Supply</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-distribute hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product
        </section>
        <form id="productView" method="POST" action="<?php echo base_url() ?>Product_distribution">
          <input type="hidden" name="action" id="action" />
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Supply Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>

                    <table id="ProductTable" class="table table-bordered table-hover" >
                      <thead>
                        <tr>
                          <td>ID Supply</td>
                          <td>Supplier</td>
                          <td>Nama Pengadaan</td>
                          <td>Tgl Pengadaan</td>
                          <td>Jml Items</td>
                          <td>Nominal</td>
                          <td>Status</td>
                          <td>Dibuat Oleh</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->ID_check_payment; ?></td>
                            <td><?php echo $value->supplier_name; ?></td>
                            <td><?php echo $value->payment_name; ?></td>
                            <td><?php echo date('d M y', strtotime($value->payment_date)); ?></td>
                            <td><?php echo number_format($value->num_of_items); ?></td>
                            <td><?php echo number_format($value->total_payment); ?></td>
                            <td><?php 
                                      switch($value->DO_status)
                                      {
                                        case "A" : echo "Accepted"; break;
                                        case "N" : echo "Not Accepted"; break; 
                                      }
                             ?></td>
                            <td><?php echo $value->employee_name; ?></td>                            
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  
                  </div>
                
                </div>
                
              </div>


            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>

  <script type="text/javascript" src="<?php echo base_url();?>dist/js/supply.js"></script>
</body>
</html>