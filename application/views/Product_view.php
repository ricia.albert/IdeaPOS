<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Product</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-product hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product
        </section>
        <form id="productView" method="POST" action="<?php echo base_url() ?>Product">
          <input type="hidden" name="action" id="action" />
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Product Table</h3>
                  </div>
                  <div class="box-body">
                    <a href="#dialog-upload" data-toggle="modal"><button type="button" class="btn btn-warning">Import Excel</button></a>
                    <br/><br/>
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>

                    <table id="ProductTable" class="table table-bordered table-hover" >
                      <thead>
                        <tr>
                          <td>Menu ID</td>
                          <td>Menu Name</td>
                          <td>Description</td>
                          <td>Price</td>
                          <td>Menu Code</td>
                          <?php if ($session->productAuth == AUTH_WRITE) : ?>
                          <td width="285px"></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->menuID ?></td>
                            <td><?php echo $value->menuNm ?></td>
                            <td><?php echo $value->description ?></td>
                            <td><?php echo number_format($value->price) ?></td>
                            <td><?php echo $value->codeMenu ?></td>
                            <?php if ($session->productAuth == AUTH_WRITE) : ?>
                              <td>
                                <a href="#"><input type="button" name="copyButton" id="copyButton" onclick="copy('<?php echo $value->codeMenu ?>','<?php echo $value->menuID ?>','<?php echo $value->POSID; ?>','<?php echo $value->menuNm ?>')" value="Copy"></a>
                                <a href="<?php base_url(); ?>Product/update?menu=<?php echo $value->menuID ?>&pos=<?php echo $value->POSID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update" /></a>
                                <a href="<?php base_url(); ?>Product_detail_price?menu=<?php echo $value->menuID ?>&pos=<?php echo $value->POSID; ?>&name=<?php echo $value->menuNm; ?>"><input type="button" name="priceBttn" id="priceBttn" value="Special Price" /></a>
                                <a href="<?php base_url(); ?>Additional_menu?menu=<?php echo $value->menuID ?>&pos=<?php echo $value->POSID; ?>&name=<?php echo $value->menuNm; ?>"><input type="button" name="addBttn" id="addBttn" value="Additional" /></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->menuID ?>','<?php echo $value->POSID; ?>')" value="Delete" /></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($session->productAuth == AUTH_WRITE) : ?>
                      <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                      <?php endif; ?>
                    </div>
                  
                  </div>
                
                </div>
                
              </div>


            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
        <?php include "includes/product_copy_dialog.php"; ?>
        <?php include "includes/upload_dialog.php"; ?>
      </div>

  <script type="text/javascript" src="<?php echo base_url();?>dist/js/product.js"></script>
</body>
</html>