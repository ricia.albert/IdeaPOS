<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Staff Account</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-staff hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Staff Account
        </section>
        <?php

        if (isset($req)) {
          ?>
          <form method="POST" id="actionForm" action="<?php echo base_url();?>Staff/update_user">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Staff Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewUser as $key) {
                            ?>

                            <tr>
                              <td width="25%">Username</td>
                              <td><input type="text" class="form-control" name="username" id="username" value="<?php echo $key->Username ?>">
                                <input type="hidden" name="userID" id="userID" value="<?php echo $key->userID ?>"/>
                                <input type="hidden" name="PosID" id="PosID" value="<?php echo $key->PosID; ?>"></td>
                              </tr>
                           <!--  <tr>
                              <td>Password</td>
                              <td><input type="Password" class="form-control" name="password" id="password" value="<?php echo $key->password ?>"></td>
                            </tr> -->
                            <tr>
                              <td>Employee</td>
                              <td><select name="employeeID" id="employeeID">
                                <?php

                                foreach ($viewEmployee as $key2) {
                                  ?>
                                  <option <?php echo ($key2->employeeID == $key->employeeID) ? "selected" : ""; ?> value="<?php echo $key2->employeeID ?>"><?php echo $key2->employeeID ?> - <?php echo $key2->employee_name ?></option>
                                  <?php
                                }

                                ?>
                              </select></td>
                            </tr>
                            <tr>
                              <td>Password </td>
                              <td><input type="password" name="passwordText" id="passwordText" value="<?php echo $key->Pass ?>" class="passwordText form-control" />
                                  <input type="password" name="passwordText" class="passwordText" id="passwordText" value="<?php echo $key->Pass ?>" style="display:none" />
                                  <input type="text" name="password" id="password" value="<?php echo $key->Pass ?>" class="form-control" style="display:none" /></td>
                              <td width="70px"><label><input type="checkbox" id="chkShow" /> Show</label></td>
                            </tr>
                            <tr>
                              <td>Active</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Active == '1') ? 'checked' : ''; ?> name="Active" id="gender1" value="1" >
                                  Active
                                </label>
                                <label>
                                  <input type="radio" <?php echo ($key->Active == '0') ? 'checked' : ''; ?> name="Active" id="gender2" value="0" >
                                  Non active
                                </label>

                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Expired ?</td>
                            <td><input type="checkbox" name="isExpired" id="datepicker" value="1" <?php echo $key->Expired == 1 ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>Expired Date</td>
                            <td><input type="text" readonly class="date form-control" name="expired" id="isExpired" value="<?php echo $key->Expired_date ?>" /></td>
                          </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th colspan="4" class="text-center">User Authorities</th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="25%">Cashier Auth</td>
                            <td width="25%"><input type="checkbox" id="Cashier_Access" name="Cashier_Access" value="1" <?php echo ($auth->Cashier_Access == 1) ? "checked" : ""; ?> /></td>
                            <td width="25%">Admin Auth</td>
                            <td width="25%"><input type="checkbox" id="Admin_access" name="Admin_access" value="1" <?php echo ($auth->Admin_access == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>User Auth</td>
                            <td><input type="checkbox" id="User_access" name="User_access" value="1" <?php echo ($auth->User_access == 1) ? "checked" : ""; ?> /></td>
                            <td>Menu Auth</td>
                            <td><input type="checkbox" id="Menu_Access" name="Menu_Access" value="1" <?php echo ($auth->Menu_Access == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>Stock Auth</td>
                            <td><input type="checkbox" id="Stock_Access" name="Stock_Access" value="1" <?php echo ($auth->Stock_Access == 1) ? "checked" : ""; ?> /></td>
                            <td>Resource Auth</td>
                            <td><input type="checkbox" id="Hrd_Employee" name="Hrd_Employee" value="1" <?php echo ($auth->Hrd_Employee == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>Transaction Auth</td>
                            <td><input type="checkbox" id="Trans_access" name="Trans_access" value="1" <?php echo ($auth->Trans_access == 1) ? "checked" : ""; ?> /></td>
                            <td>Finance Auth</td>
                            <td><input type="checkbox" id="Finance_Access" name="Finance_Access" value="1" <?php echo ($auth->Finance_Access == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>Setting Auth</td>
                            <td><input type="checkbox" id="Setting_Access" name="Setting_Access" value="1" <?php echo ($auth->Setting_Access == 1) ? "checked" : ""; ?> /></td>
                            <td>Category Auth</td>
                            <td><input type="checkbox" id="Category_Access" name="Category_Access" value="1" <?php echo ($auth->Category_Access == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          <tr>
                            <td>Report Auth</td>
                            <td><input type="checkbox" id="Report_Access" name="Report_Access" value="1" <?php echo ($auth->Report_Access == 1) ? "checked" : ""; ?> /></td>
                            <td>Guest Auth</td>
                            <td><input type="checkbox" id="Guest_Access" name="Guest_Access" value="1" <?php echo ($auth->Guest_Access == 1) ? "checked" : ""; ?> /></td>
                          </tr>
                          </tbody>
                    </table>
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th colspan="8" class="text-center">More Authorities</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td width="12.5%">Add User</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Add_User" name="Add_User" <?php echo ($auth->Add_User == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Add Menu</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Add_Menu" name="Add_Menu" <?php echo ($auth->Add_Menu == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Add Stock</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Add_Stock" name="Add_Stock" <?php echo ($auth->Add_Stock == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Add Guest</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Add_Guest" name="Add_Guest" <?php echo ($auth->Add_Guest == 1) ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td width="12.5%">Edit User</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Edit_User" name="Edit_User" <?php echo ($auth->Edit_User == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Edit Menu</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Edit_Menu" name="Edit_Menu" <?php echo ($auth->Edit_Menu == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Edit Stock</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Edit_Stock" name="Edit_Stock" <?php echo ($auth->Edit_Stock == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Edit Guest</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Edit_Guest" name="Edit_Guest" <?php echo ($auth->Edit_Guest == 1) ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td width="12.5%">Delete User</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Delete_User" name="Delete_User" <?php echo ($auth->Delete_User == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Delete Menu</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Delete_Menu" name="Delete_Menu" <?php echo ($auth->Delete_Menu == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Delete Stock</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Delete_Stock" name="Delete_Stock" <?php echo ($auth->Delete_Stock == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Delete Guest</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Delete_Guest" name="Delete_Guest" <?php echo ($auth->Delete_Guest == 1) ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td width="12.5%">Add Trans</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Add_Trans" name="Add_Trans" <?php echo ($auth->Add_Trans == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Edit Trans</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Edit_Trans" name="Edit_Trans" <?php echo ($auth->Edit_Trans == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Stock Opname</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Opname_Access" name="Opname_Access" <?php echo ($auth->Opname_Access == 1) ? "checked" : ""; ?> /></td>
                          <td width="12.5%">Cancel Trans</td>
                          <td width="12.5%"><input type="checkbox" value="1" id="Cancel_Trans" name="Cancel_Trans" <?php echo ($auth->Cancel_Trans == 1) ? "checked" : ""; ?> /></td>
                        </tr> 
                      </tbody>
                    </table>
                    <br/>
                    <input type="submit" class="form-control" onclick="subtm()" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" id="actionForm" action="<?php echo base_url();?>Staff/add_user">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">User Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <input type="hidden" value="<?php echo $branch; ?>" name="PosID" id="PosID" />
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                <!-- <tr>
                                  <td>ID</td>
                                  <td><input type="text" readonly="" class="form-control" name="userID" id="userID"></td>
                                </tr> -->
                                <tr>
                                  <td>Username</td>
                                  <td><input type="text" class="form-control" name="username" id="username"></td>
                                </tr>
                           <!--  <tr>
                              <td>Password</td>
                              <td><input type="Password" class="form-control" name="password" id="password" value="<?php echo $key->password ?>"></td>
                            </tr> -->
                            <tr>
                              <td>Employee</td>
                              <td><select name="employeeID" id="employeeID">
                                <?php

                                foreach ($viewEmployee as $key2) {
                                  ?>
                                  <option value="<?php echo $key2->employeeID ?>"><?php echo $key2->employeeID ?> - <?php echo $key2->employee_name ?></option>
                                  <?php
                                }

                                ?>
                              </select></td>
                            </tr>
                            <tr>
                              <td>Password</td>
                              <td><input type="password" name="passwordText" id="passwordText" class="passwordText form-control" />
                                  <input type="password" name="passwordText" class="passwordText" id="passwordText" style="display:none" />
                                  <input type="text" name="password" id="password" class="form-control" style="display:none" /></td>
                              <td width="70px"><label><input type="checkbox" id="chkShow" /> Show</label></td>
                            </tr>
                            <tr>
                              <td>Active</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" checked name="Active" id="gender1" value="1" >
                                  Active
                                </label> &nbsp;
                                <label>
                                  <input type="radio" name="Active" id="gender2" value="0" >
                                  Non active
                                </label>

                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Expired ?</td>
                            <td><input type="checkbox" name="isExpired" id="isExpired" value="1" /></td>
                          </tr>
                          <tr>
                            <td>Expired Date</td>
                            <td><input type="text" readonly class="date form-control" name="expired" id="datepicker"></td>
                          </tr>
                          
                        </thead>
                      </table>
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th colspan="4" class="text-center">User Authorities</th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="25%">Cashier Auth</td>
                            <td width="25%"><input type="checkbox" id="Cashier_Access" name="Cashier_Access" value="1" /></td>
                            <td width="25%">Admin Auth</td>
                            <td width="25%"><input type="checkbox" id="Admin_access" name="Admin_access" value="1" /></td>
                          </tr>
                          <tr>
                            <td>User Auth</td>
                            <td><input type="checkbox" id="User_access" name="User_access" value="1" /></td>
                            <td>Menu Auth</td>
                            <td><input type="checkbox" id="Menu_Access" name="Menu_Access" value="1" /></td>
                          </tr>
                          <tr>
                            <td>Stock Auth</td>
                            <td><input type="checkbox" id="Stock_Access" name="Stock_Access" value="1" /></td>
                            <td>Resource Auth</td>
                            <td><input type="checkbox" id="Hrd_Employee" name="Hrd_Employee" value="1" /></td>
                          </tr>
                          <tr>
                            <td>Transaction Auth</td>
                            <td><input type="checkbox" id="Trans_access" name="Trans_access" value="1" /></td>
                            <td>Report Auth</td>
                            <td><input type="checkbox" id="Report_Access" name="Report_Access" value="1" /></td>
                          </tr>
                          <tr>
                            <td>Setting Auth</td>
                            <td><input type="checkbox" id="Setting_Access" name="Setting_Access" value="1" /></td>
                            <td>Category Auth</td>
                            <td><input type="checkbox" id="Category_Access" name="Category_Access" value="1" /></td>
                          </tr>
                          <tr>
                            <td>Guest Auth</td>
                            <td><input type="checkbox" id="Guest_Access" name="Guest_Access" value="1" /></td>
                          </tr>
                          </tbody>
                      </table>
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th colspan="8" class="text-center">More Authorities</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td width="12.5%">Add User</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Add_User" name="Add_User" /></td>
                            <td width="12.5%">Add Menu</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Add_Menu" name="Add_Menu" /></td>
                            <td width="12.5%">Add Stock</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Add_Stock" name="Add_Stock" /></td>
                            <td width="12.5%">Add Guest</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Add_Guest" name="Add_Guest" /></td>
                          </tr>
                          <tr>
                            <td width="12.5%">Edit User</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Edit_User" name="Edit_User" /></td>
                            <td width="12.5%">Edit Menu</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Edit_Menu" name="Edit_Menu" /></td>
                            <td width="12.5%">Edit Stock</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Edit_Stock" name="Edit_Stock" /></td>
                            <td width="12.5%">Edit Guest</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Edit_Guest" name="Edit_Guest" /></td>
                          </tr>
                          <tr>
                            <td width="12.5%">Delete User</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Delete_User" name="Delete_User" /></td>
                            <td width="12.5%">Delete Menu</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Delete_Menu" name="Delete_Menu" /></td>
                            <td width="12.5%">Delete Stock</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Delete_Stock" name="Delete_Stock" /></td>
                            <td width="12.5%">Delete Guest</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Delete_Guest" name="Delete_Guest" /></td>
                          </tr>
                          <tr>
                            <td width="12.5%">Add Trans</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Add_Trans" name="Add_Trans" /></td>
                            <td width="12.5%">Edit Trans</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Edit_Trans" name="Edit_Trans" /></td>
                            <td width="12.5%">Stock Opname</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Opname_Access" name="Opname_Access" /></td>
                            <td width="12.5%">Cancel Trans</td>
                            <td width="12.5%"><input type="checkbox" value="1" id="Cancel_Trans" name="Cancel_Trans" /></td>
                          </tr>
                        </tbody>
                      </table>
                      <br/>
                      <input type="submit" onclick="subtm()" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                    </div>

                    <?php
                  }

                  ?>

                </div>
              </div>
            </div>
          </section>
        </form>
      </div>
      <?php include "includes/footer.php"; ?>
      <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/staff.js"></script>
    </div>
  </body>
  </html>
