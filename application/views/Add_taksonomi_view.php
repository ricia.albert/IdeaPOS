<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Taksonomi</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-taksonomi hold-transition skin-blue sidebar-mini">
<!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
<div class="wrapper">

  <!-- Header Navbar: style can be found in header.less -->
  <?php require("includes/header.php") ?>
  <body>
    <!-- Left side column. contains the logo and sidebar -->
  <?php require("includes/navigation.php") ?>
  <div class="content-wrapper">
    <section class="content-header">
      Taksonomi
    </section>

    <?php

    if (isset($req)) {
      ?>
      <form method="POST" action="<?php echo base_url();?>Taksonomi/update_category">
        <input type="hidden" name="catID" id="catID" value="<?php echo $req ?>">
        <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div align="center" class="box-header">
                  <h3 align="center" class="box-title">Taksonomi Registration</h3>
                  <?php

                  if (isset($err)) {
                    ?>
                    <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                    <?php
                  }
                  ?>
                </div>
                <div class="box-body">
                  <table id="userTable" class="table table-bordered table-hover">
                    <thead>
                      <?php
                      foreach ($viewProduct as $key) {
                        ?>
                        <tr>
                          <td>Taksonomi Code</td>
                          <td><input type="text" class="form-control" value="<?php echo $key->typeCode; ?>" name="categoryCode" id="categoryCode"></td>
                        </tr>
                        <tr>
                          <td>Taksonomi Name</td>
                          <td><input type="text" class="form-control" value="<?php echo $key->typeText; ?>" name="categoryName" id="categoryName"></td>
                        </tr>
                        <?php
                      }
                      ?>

                    </thead>
                  </table>
                  <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                </div>
                <?php
              }
              else{
                ?>
                <form method="POST" action="<?php echo base_url();?>Taksonomi/add_category">
                  <section class="content">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="box">
                          <div align="center" class="box-header">
                            <h3 align="center" class="box-title">Taksonomi Registration</h3>
                            <?php

                            if (isset($err)) {
                              ?>
                              <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                              <?php
                            }
                            ?>
                          </div>

                          <div class="box-body">
                            <input type="hidden" name="PosID" id="PosID" value="<?php echo $branch; ?>" />
                            <table id="userTable" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <td>Taksonomi Code</td>
                                  <td><input type="text" class="form-control" name="categoryCode" id="categoryCode" /></td>
                                </tr>
                                <tr>
                                  <td>Taksonomi Name</td>
                                  <td><input type="text" class="form-control" name="categoryName" id="categoryName" /></td>
                                </tr>
                              </thead>
                            </table>
                            <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                          </div>

                          <?php
                        }

                        ?>

                      </div>
                    </div>
                  </div>
                </section>
              </form>

            </div>

            <?php include "includes/footer.php"; ?>
          </div>
          
          <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/taksonomi.js"></script>
        </body>
        </html>
