<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Special Price</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Special Price
        </section>

        <?php

        if (isset($req)) {
          ?>
          <?php
        }
        else{
          ?>
          <form method="POST" action="<?php echo base_url();?>Member/add_special">
            <input type="hidden" name="guestID" id="guestID" value="<?php echo $guestID ?>">
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Special Price Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>

                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td>Guest Name</td>
                            <td><input type="text" readonly class="form-control" name="name" value="<?php echo $viewUser ?>" id="name"></td>
                          </tr>
                          <tr>
                            <td>Menu</td>
                            <td><select class="form-control" name="menuID" id="menuID">
                              <?php
                              foreach ($viewMenu as $key) {
                                ?>
                                <option value="<?php echo $key->menuID ?>"><?php echo $key->menuID ?>-<?php echo $key->menuNm ?></option>
                                <?php
                              }
                              ?>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Price</td>
                            <td><input type="number" class="form-control" name="specialPrice" id="specialPrice"></td>
                          </tr>
                        </thead>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                    </div>

                    <?php
                  }

                  ?>

                </div>
              </div>
            </div>
          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
    </div>
   
    <script type="text/javascript" src="<?php echo base_url();?>dist/js/special_price.js"></script>
  </body>
</html>
