<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Administrator</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-user hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          User Table
        </section>

        <form method="POST" action=""></form>
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">User Table</h3>
                </div>
                <div class="box-body">
                  <table id="userTable" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Address</td>
                        <td>Position</td>
                        <td>Phone</td>
                        <td>Email</td>
                        <?php if ($this->session->userdata("userAuth") == AUTH_WRITE) : ?>
                          <td></td>
                        <?php endif; ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      foreach ($view_user as $key =>$value) {
                        ?>
                        <tr>
                          <td><?php echo $value->userID ?></td>
                          <td><?php echo $value->employee_name ?></td>
                          <td><?php echo $value->Address ?></td>
                          <td><?php echo $value->Position ?></td>
                          <td><?php echo $value->Phone ?></td>
                          <td><?php echo $value->Email ?></td>
                          <?php if ($this->session->userdata("userAuth") == AUTH_WRITE) : ?>
                            <td>
                              <a href="<?php echo base_url(); ?>User/Chg_pass?d=<?php echo $value->userID ?>"><input type="button" name="updtButton" id="chgButton" value="Change Pass" ></a>
                              <a href="<?php echo base_url(); ?>User/Update?d=<?php echo $value->userID ?>"><input type="button" name="updtButton" id="updtButton" value="Update" ></a>
                              <a href="#"><input type="button" name="dltButton" id="dltButton" onclick="confirmation('<?php echo $value->userID ?>')" value="Delete"></a>
                            </td>
                          <?php endif; ?>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <?php if ($this->session->userdata("userAuth") == AUTH_WRITE) : ?>
                      <a href="<?php echo base_url(); ?>User/add_user_view"><input type="button" class="form-control" name="addBttn" id="addBttn" value="Add New"></a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </section>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>
      <!-- ./wrapper -->
      <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/user.js"></script>
</body>
</html>