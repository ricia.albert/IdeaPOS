<!-- Modal Dialog -->
<div class="modal fade" id="dialog-approval" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width:1000px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-tags"></span> &nbsp; <strong>Transfer Item</strong></h4>
      </div>
      <div class="modal-body">
        <input id="hid_id" type="hidden" />
        <input id="hid_pos" type="hidden" />
        <table class="table">
          <tr>
              <td width="40%"><strong>ID Approval</strong></td>
              <td><span id="span-id"></span></td>
          </tr>
          <tr>
              <td><strong>Tanggal Request</strong></td>
              <td><span id="span-date"></span></td>
          </tr>
          <tr>
              <td><strong>Dari Cabang</strong></td>
              <td><span id="span-from"></span></td>
          </tr>
          <tr>
              <td><strong>Ke Cabang</strong></td>
              <td><span id="span-to"></span></td>
          </tr>
          <tr>
              <td><strong>Status</strong></td>
              <td><span id="span-status"></span></td>
          </tr>
          <tr>
              <td><strong>Pesan</strong></td>
              <td><span id="span-pesan"></span></td>
          </tr>
        </table>
        <div style="clear:both"></div>
        <table class="table table-bordered">
          <thead>
            <tr>
                <th><center>ID Produk</center></th>
                <th><center>Nama Stok</center></th>
                <th><center>Jumlah</center></th>
                <th><center>Satuan</center></th>
                <th><center>Approve Jml</center></th>
                <th><center>Approve Satuan</center></th>
                <th><center>Rcv Jml</center></th>
                <th><center>Rcv Satuan</center></th>
            </tr>
          </thead>
          <tbody id="tbody">
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->