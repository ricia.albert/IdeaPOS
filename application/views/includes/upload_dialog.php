<!-- Modal Dialog -->
<div class="modal fade" id="dialog-upload" tabindex="-1" role="dialog">
  <form action="<?php echo base_url(); ?>product/import" method="POST" enctype="multipart/form-data" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><span class="glyphicon glyphicon-file"></span> &nbsp; <strong>Import From Excel </strong></h4>
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
                <td><strong>To Branch</strong></td>
                <td>
                  <select id="PosID" name="PosID">
                    <?php foreach ($view_branch as $branch) : ?>
                    <option value="<?php echo $branch->POSID ?>"><?php echo $branch->POSNm ?></option>
                    <?php endforeach; ?>
                  </select>
                </td>
            </tr>
            <tr>
                <td width="40%"><strong>File to import</strong></td>
                <td>
                  <input type="file" id="fnImport" name="fnImport" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                </td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="submit" id="btimport" class="btn btn-success">Import Data</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->