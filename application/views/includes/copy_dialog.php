<!-- Modal Dialog -->
<div class="modal fade" id="dialog-copy" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-tags"></span> &nbsp; <strong>Copy Database</strong></h4>
      </div>
      <div class="modal-body">
        <table class="table">
          <tr>
              <td width="40%"><strong>From Branch</strong></td>
              <td>
                <select id="cbFrom" name="cbFrom">
                  <?php foreach ($view_branch as $branch) : ?>
                  <option value="<?php echo $branch->POSID ?>"><?php echo $branch->POSNm ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
          </tr>
          <tr>
              <td><strong>To Branch</strong></td>
              <td>
                <select id="cbTo" name="cbTo">
                  <?php foreach ($view_branch as $branch) : ?>
                  <option value="<?php echo $branch->POSID ?>"><?php echo $branch->POSNm ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" id="btcopy" class="btn btn-success">Copy Data</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->