<!-- Modal Dialog -->
<div class="modal fade" id="dialog-trans" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width:1000px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-tags"></span> &nbsp; <strong>Detail Transaction</strong></h4>
      </div>
      <div class="modal-body">
        <input id="hid_id" type="hidden" />
        <input id="hid_pos" type="hidden" />
        <table class="table">
          <tr>
              <td width="40%"><strong>ID Trans</strong></td>
              <td><span id="span-id"></span></td>
          </tr>
          <tr>
              <td><strong>Tanggal Transaksi</strong></td>
              <td><span id="span-date"></span></td>
          </tr>
          <tr>
              <td><strong>Tanggal Settled</strong></td>
              <td><span id="span-settled"></span></td>
          </tr>
          <tr>
              <td><strong>Customer</strong></td>
              <td><span id="span-cust"></span></td>
          </tr>
          <tr>
              <td><strong>Inputted By</strong></td>
              <td><span id="span-waiter"></span></td>
          </tr>
          <tr>
              <td><strong>Slot</strong></td>
              <td><span id="span-slot"></span></td>
          </tr>
          <tr>
              <td><strong>Group</strong></td>
              <td><span id="span-group"></span></td>
          </tr>
          <tr>
              <td><strong><?php echo ($stats == "C") ? "Canceled By" : "Cashier"; ?></strong></td>
              <td><span id="span-cashier"></span></td>
          </tr>
          <tr>
              <td><strong>Status</strong></td>
              <td><span id="span-status"></span></td>
          </tr>
          <tr>
              <td><strong>Jatuh Tempo</strong></td>
              <td><span id="span-due"></span></td>
          </tr>
          <tr>
              <td><strong>Jenis Pembayaran</strong></td>
              <td><span id="span-payment"></span></td>
          </tr>
          <tr>
              <td><strong>Jenis Transaksi</strong></td>
              <td><span id="span-transtype"></span></td>
          </tr>
          <tr>
              <td><strong>Total</strong></td>
              <td><span id="span-total"></span></td>
          </tr>
          <tr>
              <td><strong>PPN</strong></td>
              <td><span id="span-ppn"></span></td>
          </tr>
          <tr>
              <td><strong>Discount</strong></td>
              <td><span id="span-discount"></span></td>
          </tr>
          <tr>
              <td><strong>Voucher</strong></td>
              <td><span id="span-voucher"></span></td>
          </tr>
          <tr>
              <td><strong>Rounding</strong></td>
              <td><span id="span-rounding"></span></td>
          </tr>
          <tr>
              <td><strong>Void ?</strong></td>
              <td><span id="span-void"></span></td>
          </tr>
          <tr>
              <td><strong>Void Date</strong></td>
              <td><span id="span-voiddt"></span></td>
          </tr>
          <tr>
              <td><strong>Catatan Void</strong></td>
              <td><span id="span-notes"></span></td>
          </tr>
        </table>
        <div style="clear:both"></div>
        <table class="table table-bordered">
          <thead>
            <tr>
                <th><center>ID Produk</center></th>
                <th><center>Nama Produk</center></th>
                <th><center>Harga</center></th>
                <th><center>Jumlah</center></th>
                <th><center>Total</center></th>
                <th><center>Discount</center></th>
                <th><center>Harga Sebelumnya</center></th>
            </tr>
          </thead>
          <tbody id="tbody">
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->