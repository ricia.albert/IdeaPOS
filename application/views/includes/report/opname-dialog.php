<!-- Modal Dialog -->
<div class="modal fade" id="dialog-opname" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-file"></span> &nbsp; <strong>Report Parameter</strong></h4>
      </div>
      <div class="modal-body">
        <table class="table">
          <tr>
            <td width="40%"><strong>Branch</strong></td>
            <td>
              <select id="opos" name="opos" class="form-control">
                 <?php
                 foreach ($view_branch as $key) {
                  ?>
                  <option value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                  <?php
                }
                ?>
              </select>
            </td>
          </tr>
          <tr>
              <td width="40%"><strong>Opname Date</strong></td>
              <td>
                <input type="text" id="txtdate" readonly class="date form-control" />
              </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" id="btopname" class="btn btn-success"><span class="glyphicon glyphicon-print"></span> &nbsp; Print Data</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->