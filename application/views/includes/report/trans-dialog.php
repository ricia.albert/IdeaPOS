<!-- Modal Dialog -->
<div class="modal fade" id="dialog-trans" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-file"></span> &nbsp; <strong>Report Parameter</strong></h4>
      </div>
      <div class="modal-body">
        <table class="table">
          <tr>
            <td width="40%"><strong>Branch</strong></td>
            <td>
              <select id="cbPOS" name="cbPOS" class="form-control">
                 <?php
                 foreach ($view_branch as $key) {
                  ?>
                  <option value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                  <?php
                }
                ?>
              </select>
            </td>
          </tr>
          <tr>
            <td width="40%"><strong>Is Debt ? </strong></td>
            <td>
              <select id="cbdebt" name="cbdebt" class="form-control">
                <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
            </td>
          </tr>
          <tr>
            <td width="40%"><strong>Status</strong></td>
            <td>
              <select id="cbstatus" name="cbstatus" class="form-control">
                <option value="A">Accepted</option>
                <option value="P">Pending</option>
                <option value="C">Canceled</option>
              </select>
            </td>
          </tr>
          <tr>
            <td width="40%"><strong>Jenis Harga</strong></td>
            <td>
              <select id="cbprice" name="cbprice" class="form-control">
                <option value="">All</option>
                <option value="Normal">Normal</option>
                <?php foreach ($view_price as $price) : ?>
                  <option value="<?php echo $price->info; ?>"><?php echo $price->info; ?></option>
                <?php endforeach; ?>
              </select>
            </td>
          </tr>
          <tr>
              <td width="40%"><strong>From Date</strong></td>
              <td>
                <input id="txtsd" type="text" readonly class="form-control" />
              </td>
          </tr>
          <tr>
              <td><strong>To Date</strong></td>
              <td>
                <input id="txted" type="text" readonly class="form-control" />
              </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" id="btprint" class="btn btn-success"><span class="glyphicon glyphicon-print"></span> &nbsp; Print Data</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->