
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a id="mn-dashboard" href="<?php echo base_url(); ?>home">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php if ($this->session->userdata("userAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-user" href="<?php echo base_url(); ?>user">
          <i class="fa fa-user"></i>
          <span>Users</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("approvalAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-approval" href="#">
          <i class="fa fa-handshake-o"></i>
          <span>Approval</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-approval" class="treeview-menu">
          <li><a id="mn-stock" href="<?php echo base_url(); ?>approval"><i class="fa fa-circle-o"></i> Approval Stock</a></li>
          <li><a id="mn-price" href="<?php echo base_url(); ?>approval/price"><i class="fa fa-circle-o"></i> Approval Price</a></li>
          <li><a id="mn-transfer" href="<?php echo base_url(); ?>approval/transfer"><i class="fa fa-circle-o"></i> View Transfer</a></li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("branchAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-branches" href="#">
          <i class="fa fa-institution"></i>
          <span>Branches</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-branch" class="treeview-menu">
          <li><a id="mn-view" href="<?php echo base_url(); ?>branch"><i class="fa fa-circle-o"></i> View</a></li>
          <li><a id="mn-shift" href="<?php echo base_url(); ?>shiftinout"><i class="fa fa-circle-o"></i> Shift In/Out</a></li>
          <li><a id="mn-pettycash" href="<?php echo base_url(); ?>pettycash"><i class="fa fa-circle-o"></i> Petty Cash</a></li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("branchAuth") != AUTH_NONE) : ?>
      <li class="treeview">
          <a id="mn-resources" href="#">
            <i class="fa fa-wrench"></i>
            <span>Resources</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul id="group-resources" class="treeview-menu">
            <li><a id="mn-employee" href="<?php echo base_url(); ?>employee"><i class="fa fa-circle-o"></i> Employee</a></li>
            <li><a id="mn-staff" href="<?php echo base_url(); ?>staff"><i class="fa fa-circle-o"></i> Staff Account</a></li>
            <li><a id="mn-position" href="<?php echo base_url(); ?>position"><i class="fa fa-circle-o"></i> Position</a></li>
            <li><a id="mn-payment" href="<?php echo base_url(); ?>payment"><i class="fa fa-circle-o"></i> Payment</a></li>
            <li><a id="mn-supplier" href="<?php echo base_url(); ?>supplier"><i class="fa fa-circle-o"></i> Supplier</a></li>
          </ul>
        </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("productAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-products" href="#">
          <i class="fa fa-shopping-bag"></i>
          <span>Products</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-product" class="treeview-menu">
          <?php if ($this->session->userdata("productAuth") != AUTH_NONE) : ?>
            <li><a id="mn-product" href="<?php echo base_url(); ?>product"><i class="fa fa-circle-o"></i> Produk</a></li>
            <li><a id="mn-package" href="<?php echo base_url(); ?>package"><i class="fa fa-circle-o"></i> Package</a></li>
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("stockAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-stock" href="#">
          <i class="fa fa-database"></i>
          <span>Inventory</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-stock" class="treeview-menu">
          <?php if ($this->session->userdata("stockAuth") != AUTH_NONE) : ?>
            <li><a id="mn-sidestock" href="<?php echo base_url(); ?>sidestock"><i class="fa fa-circle-o"></i> Side Stock</a></li>
            <li><a id="mn-ingridient" href="<?php echo base_url(); ?>ingridients"><i class="fa fa-circle-o"></i> Ingridients</a></li>
            <li><a id="mn-distribute" href="<?php echo base_url(); ?>product_distribution"><i class="fa fa-circle-o"></i> Distribution</a></li>
            <li><a id="mn-progress" href="<?php echo base_url(); ?>stock"><i class="fa fa-circle-o"></i> Stock Mutation</a></li>
            <!-- <li><a id="mn-additional" href="<?php echo base_url(); ?>additional_menu"><i class="fa fa-circle-o"></i> Additional Menu</a></li> -->
            <!-- <li><a id="mn_detailprice" href="<?php echo base_url(); ?>product_detail_price"><i class="fa fa-circle-o"></i> Product Detail Price</a></li> -->
          <?php endif; ?>
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("stockAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-sync" href="<?php echo base_url(); ?>sync">
          <i class="fa fa-link"></i>
          <span>Stock Syncronize</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("customerAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-customer" href="<?php echo base_url(); ?>member">
          <i class="fa fa-user-plus"></i>
          <span>Customers</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("categoryAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-category" href="#">
          <i class="fa fa-cubes"></i>
          <span>Category</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-category" class="treeview-menu">
          <li><a id="mn-taksonomi" href="<?php echo base_url(); ?>taksonomi"><i class="fa fa-circle-o"></i> Taksonomi</a></li>
          <li><a id="mn-brand" href="<?php echo base_url(); ?>brand"><i class="fa fa-circle-o"></i> Brand</a></li>
          <li><a id="mn-categories" href="<?php echo base_url(); ?>category"><i class="fa fa-circle-o"></i> Product Category</a></li>
          <li><a id="mn-subcategory" href="<?php echo base_url(); ?>sub_category"><i class="fa fa-circle-o"></i> Sub-Category</a></li>
          <li><a id="mn-ingcategory" href="<?php echo base_url(); ?>ing_category"><i class="fa fa-circle-o"></i> Ingredients Category</a></li>       
          <li><a id="mn-sidecategory" href="<?php echo base_url(); ?>side_category"><i class="fa fa-circle-o"></i> Side Stock Category</a></li>       
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("transactionAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-trans" href="#">
          <i class="fa fa-money"></i>
          <span>Transaction</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul id="group-trans" class="treeview-menu">
          <li><a id="mn-direct" href="<?php echo base_url(); ?>Transaction_detail"><i class="fa fa-circle-o"></i> Daftar Transaksi</a></li>
          <li><a id="mn-debt" href="<?php echo base_url(); ?>Transaction_detail/debt"><i class="fa fa-circle-o"></i> Daftar Hutang</a></li>
        </ul>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("settingAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-settings" href="<?php echo base_url(); ?>setting">
          <i class="fa fa-cog"></i>
          <span>Configurations</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php endif; ?>
      <?php if ($this->session->userdata("reportAuth") != AUTH_NONE) : ?>
      <li class="treeview">
        <a id="mn-report" href="<?php echo base_url(); ?>report">
          <i class="fa fa-print"></i>
          <span>Reports</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      <?php endif; ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>