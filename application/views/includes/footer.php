<!-- Footer -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.1.3
  </div>
  <strong>Copyright &copy; 2016-<?php echo date("Y"); ?> <a href="http://idea-pos.com">Idea-POS</a>.</strong> All rights
  reserved.
</footer>