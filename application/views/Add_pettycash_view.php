<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Petty Cash</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-pettycash hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Petty Cash 
        </section>

        <?php

        if (isset($req)) {
         $price="";
         $rowCount=0;
         $indexArray=array();
         $arrayIndex = array();
         ?>
         <form method="POST" action="<?php echo base_url();?>Pettycash/update_pettycash">
          <input type="hidden" name="req" id="req" value="<?php echo $req ?>">
          <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Petty Cash Detail
                        <?php if (isset($posNm)) : ?>
                          - Copy to <strong><?php echo $posNm; ?></strong>
                        <?php endif; ?>
                    </h3>
                    <?php
                    if(isset($err)){
                      ?>
                      <h3 style="color : red"><?php echo $err ?></h3>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table id="detailPackage" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewProduct as $key) {
                          ?>
                          <tr>
                           <td>Information</td>
                           <td><input type="text" class="form-control" value="<?php echo $key->pettyName ?>" name="pettyName" id="pettyName"></td>
                         </tr>
                         <tr>
                           <td>Petty Date</td>
                           <td><input type="text" readonly class="date form-control" name="pettyDate" id="pettyDate" value="<?php echo date('Y-m-d',strtotime($key->pettyDate)); ?>" /></td>
                         </tr>
                         <tr>
                           <td>Type</td>
                           <td>
                              <select id="pettyType" name="pettyType" class="form-control">
                                  <?php foreach ($view_menu as $p) : ?>
                                    <option value="<?php echo $p->pettyTypeID; ?>" <?php echo ($key->pettyTypeID == $p->pettyTypeID) ? 'selected' : ''; ?> ><?php echo $p->typeName;?></option>
                                  <?php endforeach; ?>
                                </select>
                           </td>
                         </tr>
                         <tr>
                           <td>Petty Amount</td>
                           <td><input type="number" class="form-control" value="<?php echo $key->pettyAmt ?>" name="pettyAmt" value="0" id="pettyAmt"</td>
                         </tr>
                         <tr>
                           <td>Description</td>
                           <td>
                             <textarea class="form-control" name="description" id="description"><?php echo $key->pettyDescr; ?></textarea>
                           </td>
                         </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>  
                     <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                  </div>
                </div>
              </div>
            </div>
      
     </section>
   </form>
   <?php
 }
 else{
  ?>
  <form method="POST" id="branchForm" action="<?php echo base_url(); ?>Pettycash/add_pettycash">
    <section class="content">
      <input type="hidden" name="indexInsert" value="0" id="indexInsert">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Package Detail</h3>
              <?php
              if(isset($err)){
                ?>
                <h3 style="color : red"><?php echo $err ?></h3>
                <?php
              }
              ?>
            </div>
            <div class="box-body">
              <input type="hidden" value="<?php echo $pos; ?>" name="pos" id="pos" />
              <table id="detailPackage" class="table table-bordered table-hover">
                <thead>
                   <tr>
                     <td>Information</td>
                     <td><input type="text" class="form-control" name="pettyName" id="pettyName"></td>
                   </tr>
                   <tr>
                     <td>Petty Date</td>
                     <td><input type="text" readonly class="date form-control" name="pettyDate" id="pettyDate" /></td>
                   </tr>
                   <tr>
                     <td>Type</td>
                     <td>
                        <select id="pettyType" name="pettyType" class="form-control">
                          <?php foreach ($view_menu as $p) : ?>
                          <option value="<?php echo $p->pettyTypeID; ?>" ><?php echo $p->typeName;?></option>
                          <?php endforeach; ?>
                        </select>
                      </td>
                   </tr>
                    <tr>
                       <td>Petty Amount</td>
                       <td><input type="number" class="form-control" name="pettyAmt" value="0" id="pettyAmt"</td>
                     </tr>
                     <tr>
                       <td>Description</td>
                       <td>
                         <textarea class="form-control" name="description" id="description"></textarea>
                       </td>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>  
                <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
              </div>

        </div>
      </div>
  </section>
</form>
<?php
}

?>
</div>
<?php include "includes/footer.php"; ?>
</div>

<script type="text/javascript" src="<?php echo base_url();?>dist/js/pettycash.js"></script>
</body>
</html>