<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Detail Shift In Out</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-shift hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product 
        </section>

        <?php

        if (isset($req)) {
          $price="";
          $rowCount=0;
          $indexArray=array();
          $arrayIndex = array();
          ?>
          <form method="POST" action="<?php echo base_url();?>Product/update_product">
            <input type="hidden" name="req" id="req" value="<?php echo $req ?>">
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Product Detail</h3>
                    </div>
                    <div class="box-body">
                      <table id="branchTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                    //var_dump($viewBranch)
                            ?>
                            <tr>
                              <td width="40%">Shift Date</td>
                              <td><?php echo date('d M y',strtotime($key->shiftDate)) ?></td>
                            </tr>
                            <tr>
                              <td>Branch</td>
                              <td><?php echo $key->PosID ?></td>
                            </tr>
                            <tr>
                              <td>In / Out</td>
                              <td><?php echo ($key->isInOut=='0') ? "In" : "Out" ; ?></td>
                            </tr>
                            <tr>
                              <td>Notes</td>
                              <td><?php echo $key->notes ?></td>
                            </tr>
                            <tr>
                              <td>User Name</td>
                              <td><?php echo $key->username ?></td>
                            </tr>
                            <?php
                          }

                          ?>

                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Cash Drawer Money</h3>
                    </div>
                    <div class="box-body">
                      <table id="menuDetail" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td>Money</td>
                            <td>Value</td>
                            <td>Qty</td>
                            <td>Total</td>
                          </tr>    
                        </thead>
                        <tbody>
                          <?php
                          foreach ($cashDrawer as $key) {
                            ?>
                            <tr>
                              <td><?php echo $key->moneyNm ?></td>
                              <td><?php echo number_format($key->value,2,",",".") ?></td>
                              <td><?php echo $key->qty ?></td>
                              <td><?php echo number_format($key->total,2,",",".") ?></td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                
            </section>
          </form>
          <?php
        }
          ?>
        </div>

        <?php include "includes/footer.php"; ?>
      </div>
      
      <script type="text/javascript" src="<?php echo base_url();?>dist/js/add_product.js"></script>
</body>
</html>
