<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Group Transfer</title>
  <?php include "includes/include_js_css.php";?>
</head>
<body class="bd-customer hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Group Transfer
        </section>
        <form method="POST" action="<?php echo base_url();?>Branch/addGroup">
          <input type="hidden" name="branchCode" id="branchCode" value="<?php echo $req ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div align="center" class="box-header">
                    <h3 align="center" class="box-title">Branch Data</h3>
                    <?php

                    if (isset($err)) {
                      ?>
                      <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                          <?php

                          foreach ($viewBranch as $key) {
                    //var_dump($viewBranch)
                            ?>
                            <tr>
                              <td width="40%">Branch Code</td>
                              <td><?php echo $key->POSID ?></td>
                            </tr>
                            <tr>
                              <td>Branch Name</td>
                              <td><?php echo $key->POSNm ?></td>
                            </tr>
                            <tr>
                              <td>Is Franchise ?</td>
                              <td>
                                <?php echo ($key->isFranchise == 1) ? "Yes" : "No"; ?>
                            </tr>
                            <tr>
                              <td>Is Storage ?</td>
                              <td><?php echo ($key->isGudang == 1) ? "Yes" : "No"; ?></td>
                            </tr>
                            <?php
                          }
                          ?>

                        </thead>
                      </table>
                      <table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th colspan="8" class="text-center">Transfer To</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($branch as $b) : 
                                if ($b->POSID != $req) {
                            ?>
                            <tr>
                              <td width="40%"><?php echo $b->POSNm; ?> 
                                <?php if ($b->isFranchise == 1) echo " (Franchise)"?>
                                <?php if ($b->isGudang == 1) echo " (Storage)"?>
                              </td>
                              <td><input type="checkbox" value="1" id="<?php echo $b->POSID; ?>" <?php echo (in_array($b->POSID, $branch_pos)) ? "checked" : ""; ?> name="<?php echo $b->POSID; ?>" /></td>
                            </tr>
                          <?php }
                          endforeach; ?>
                        </tbody>
                      </table>
                    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                </div>
              </div>
            </div> 
          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
    </div>
    <script type="text/javascript" src="<?php echo base_url()?>dist/js/branch.js"></script>
  </body>
  </html>
