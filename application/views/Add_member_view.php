<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Customers</title>
  <?php include "includes/include_js_css.php";?>
</head>
<body class="bd-customer hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

  <!-- Header Navbar: style can be found in header.less -->
  <?php require("includes/header.php") ?>
  <body>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require("includes/navigation.php") ?>
    <div class="content-wrapper">
      <section class="content-header">
        Customers
      </section>
      <?php

      if (isset($req)) {
        ?>
        <form method="POST" action="<?php echo base_url();?>Member/update_member">
          <input type="hidden" name="guestID" id="guestID" value="<?php echo $req ?>">
          <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div align="center" class="box-header">
                    <h3 align="center" class="box-title">Member Registration</h3>
                    <?php

                    if (isset($err)) {
                      ?>
                      <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table id="userTable" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewUser as $key) {
                          ?>

                          <tr>
                            <td>Guest Name</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->Guest_name ?>" name="guestName" id="guestName"></td>
                          </tr>
                          <tr>
                            <td>Guest Type</td>
                            <td>
                              <select name="guestType" id="guestType" class="form-control">
                                <option value="C" <?php echo ($key->guest_type == "C") ? "selected" : ""; ?> >Company</option>
                                <option value="P" <?php echo ($key->guest_type == "P") ? "selected" : ""; ?> >Personal</option>
                              </select>
                            </td>
                          </tr>
                          <tr class="personal">
                              <td>Sex</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Sex=="L") ? "checked" : "" ?> name="gender" id="gender1" value="L">
                                  Male
                                </label> &nbsp;
                                <label>
                                  <input type="radio" <?php echo ($key->Sex=="P") ? "checked" : "" ?> name="gender" id="gender2" value="P">
                                  Female
                                </label>

                              </div>
                            </td>
                          </tr>
                          <tr class="personal">
                            <td>Born</td>
                            <td><input type="text" readonly class="date form-control" value="<?php echo $key->Born ?>" name="guestBorn" id="guestBorn"></td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->Address ?>" name="guestAddress" id="guestAddress"></td>
                          </tr>
                          <tr>
                            <td>Phone</td>
                            <td><input type="text" value="<?php echo $key->Phone ?>" name="guestPhone" id="guestPhone" class="form-control">
                            </tr>
                            <tr>
                              <td>Email</td>
                              <td><input type="email" class="form-control" value="<?php echo $key->Email ?>" name="guestEmail" id="guestEmail"></td>
                            </tr>
                            <tr>
                              <td>Info</td>
                              <td><select class="form-control" name="statusGuest" id="statusGuest">
                                <option <?php echo ($key->Info=="Pelanggan Baru") ? "selected" : "" ?> value="Pelanggan Baru">Pelanggan Baru</option>
                                <option <?php echo ($key->Info=="Pelanggan Lama") ? "selected" : "" ?> value="Pelanggan Lama">Pelanggan Lama</option>
                                <option <?php echo ($key->Info=="Pelanggan Setia") ? "selected" : "" ?> value="Pelanggan Setia">Pelanggan Setia</option>
                              </select></td>
                            </tr>
                            <tr>
                              <td>Limit Debt</td>
                              <td><input type="number" class="form-control" value="<?php echo $key->limit_debt ?>" name="guestLimit" id="guestLimit" /></td>
                            </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" action="<?php echo base_url();?>Member/add_member">
                    <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">Customer Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <td>Guest Name</td>
                                    <td><input type="text" class="form-control" name="guestName" id="guestName"></td>
                                  </tr>
                                  <tr>
                                    <td>Guest Type</td>
                                    <td>
                                      <select name="guestType" id="guestType" class="form-control">
                                        <option value="C">Company</option>
                                        <option value="P">Personal</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr class="personal">
                                      <td>Sex</td>
                                      <td>  <div class="radio">
                                        <label>
                                          <input type="radio" name="gender" id="gender1" checked value="L">
                                          Male
                                        </label> &nbsp;
                                        <label>
                                          <input type="radio" name="gender" id="gender2" value="P">
                                          Female
                                        </label>

                                      </div>
                                    </td>
                                  </tr>
                                  <tr class="personal">
                                    <td>Born</td>
                                    <td><input type="text" readonly class="date form-control" name="guestBorn" id="guestBorn"></td>
                                  </tr>
                                  <tr>
                                    <td>Address</td>
                                    <td><input type="text" class="form-control" name="guestAddress" id="guestAddress"></td>
                                  </tr>
                                  <tr>
                                    <td>Phone</td>
                                    <td><input type="text" name="guestPhone" id="guestPhone" class="form-control">
                                    </tr>
                                    <tr>
                                      <td>Email</td>
                                      <td><input type="email" class="form-control" name="guestEmail" id="guestEmail"></td>
                                    </tr>
                                    <tr>
                                      <td>Info</td>
                                      <td><select class="form-control" name="statusGuest" id="statusGuest">
                                        <option value="Pelanggan Baru">Pelanggan Baru</option>
                                        <option value="Pelanggan Lama">Pelanggan Lama</option>
                                        <!--<option value="Pelanggan Setia">Pelanggan Setia</option>-->
                                      </select></td>
                                    </tr>
                                    <tr>
                                      <td>Limit Debt</td>
                                      <td><input type="number" class="form-control" name="guestLimit" id="guestLimit" /></td>
                                    </tr>
                                </thead>
                              </table>
                              <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                            </div>

                            <?php
                          }

                          ?>

                        </div>
                      </div>
                    </div>
                  </section>
                </form>

              </div>
              <?php include "includes/footer.php"; ?>
            </div>
            <script type="text/javascript" src="<?php echo base_url()?>dist/js/member.js"></script>
          </body>
          </html>
