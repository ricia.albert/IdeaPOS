<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Ingridients</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-ingridients hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Ingridients
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Ingridients">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Ingridients Table</h3>
                    <h5>(Double Click in qty to fast update)</h5>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Ingridients Code</td>
                          <td>Ingridients Name</td>
                          <td>ingridients Category</td>
                          <td>Info</td>
                          <td>Price</td>
                          <td>Qty</td>
                          <td>Minimum Qty</td>
                          <td>Scalar</td>
                          <?php if ($session->stockAuth == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->ingID ?></td>
                            <td><?php echo $value->ingName ?></td>
                            <td><?php echo $value->ingCateName ?></td>
                            <td><?php echo $value->info ?></td>
                            <td><?php echo number_format($value->bprice) ?></td>
                            <td ondblclick = "functi('<?php echo $value->ingID ?>')"><?php echo $value->qty ?></td>
                            <td><?php echo $value->minQty ?></td>
                            <td><?php echo ($value->scalarNm == "") ? "-" : $value->scalarNm; ?></td>
                            <?php if ($session->stockAuth == AUTH_WRITE) : ?>
                              <td>
                                <a href="#"><input type="button" name="copyButton" id="copyButton" onclick="copy('<?php echo $value->codeIng ?>','<?php echo $value->ingID ?>','<?php echo $value->PosID; ?>','<?php echo $value->ingName ?>')" value="Copy"></a>
                                <a href="<?php base_url(); ?>Ingridients/update?menu=<?php echo $value->ingID ?>&pos=<?php echo $value->PosID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->ingID ?>','<?php echo $value->PosID; ?>')" value="Delete"></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($session->stockAuth == AUTH_WRITE) : ?>
                      <a href="<?php base_url(); ?>Ingridients/add_ingridients_view?pos=<?php echo $temp; ?>"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </form>

        </div>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/product_copy_dialog.php"; ?>
      </div>
     
  <script type="text/javascript" src="<?php echo base_url();?>dist/js/ingridient.js"></script>

</body>
</html>
