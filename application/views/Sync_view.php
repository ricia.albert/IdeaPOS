<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Stock Syncronize</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-sync hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product
        </section>
        <form id="productView" method="POST" action="<?php echo base_url() ?>Sync">
          <input type="hidden" name="action" id="action" />
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Product Table</h3>
                  </div>
                  <div class="box-body">
                    <a href="<?php echo base_url();?>Sync/syncall?pos=<?php echo $temp ?>">
                    <button id="btSyncAll" type="button" class="btn btn-warning">Sync All</button></a>
                    <br/><br/>
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>

                    <table id="ProductTable" class="table table-bordered table-hover" >
                      <thead>
                        <tr>
                          <td>Menu ID</td>
                          <td>Menu Name</td>
                          <td>Description</td>
                          <td>Price</td>
                          <td>Menu Code</td>
                          <td width="285px"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->menuID ?></td>
                            <td><?php echo $value->menuNm ?></td>
                            <td><?php echo $value->description ?></td>
                            <td><?php echo number_format($value->price) ?></td>
                            <td><?php echo $value->codeMenu ?></td>
                            <td>
                              <a href="<?php echo base_url();?>Sync/add?req=<?php echo $value->menuID; ?>&pos=<?php echo $value->POSID; ?>"><input type="button" name="syncButton" id="syncButton" value="Syncronize"></a>
                            </td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($this->session->userdata("productAuth") == AUTH_WRITE) : ?>
                      <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                      <?php endif; ?>
                    </div>
                  
                  </div>
                
                </div>
                
              </div>


            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
        <?php include "includes/product_copy_dialog.php"; ?>
        <?php include "includes/upload_dialog.php"; ?>
      </div>

  <script type="text/javascript" src="<?php echo base_url();?>dist/js/product.js"></script>
</body>
</html>