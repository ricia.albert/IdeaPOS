<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Distribute</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-distribute hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product Distribute
        </section>

        <form method="POST" action="<?php echo base_url() ?>Product_distribution/confirm">
          <section class="content">
            <input type="hidden" name="state" id="state" value="<?php echo $state ?>">
            <div class="row">    
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Product Table (klik kode)</h3>
                  </div>
                  <div class="box-body">
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewProduct as $key) {
                          if ($state=="product") {
                            ?> 
                            <tr>
                              <td>Menu Code</td>
                              <td>Menu Name</td>
                            </tr>
                            <?php
                          }
                          else{
                            ?> 
                            <tr>
                              <td>Package Code</td>
                              <td>Package Name</td>
                            </tr>
                            <?php
                          }
                        }
                        ?>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($viewProduct as $key) {
                          if ($state=="product") {
                            ?> 
                            <tr>
                              <td><input type="hidden" name="menuID" id="menuID" value="<?php echo $key->menuID ?>"><?php echo $key->menuID ?></td>
                              <td><?php echo $key->menuNm ?></td>
                            </tr>
                            <?php
                          }
                          else{
                            ?> 
                            <tr>
                              <td><input type="hidden" name="menuID" id="menuID" value="<?php echo $key->packagesID ?>"><?php echo $key->packagesID ?></td>
                              <td><?php echo $key->Packages_name ?></td>
                            </tr>
                            <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Package Table (klik kode)</h3>
                  </div>
                  <div class="box-body">
                    <table id="branchTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Branch Code</td>
                          <td>Branch Name</td>
                          <td>Branch Address</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        $temp = array();
                        foreach ($distribute as $key) {
                      //$temp[]+=$key->distribution;
                          if ($key->POSID != "") {
                            array_push($temp, $key->POSID);
                        //$temp = explode(",", $key->POSID);
                          }
                        }
                    //var_dump($temp);
                        foreach ($viewBranch as $key =>$value) {
                      //var_dump($value)
                          $checked = "";
                          if (count($temp)!=0) {
                            if (in_array($value->POSID, $temp)) {
                              $checked="checked";
                            }else{
                              $checked="";
                            }
                          }
                          $i++;
                          ?>
                          <tr>
                            <td><input type="checkbox" <?php echo $checked ?> name="POSID<?php echo $i ?>" id="POSID<?php echo $i ?>" value="<?php echo $value->POSID ?>"></td>
                            <td><?php echo $value->POSID ?></td>
                            <td><?php echo $value->POSNm ?></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <input type="hidden" name="index" id="index" value="<?php echo $i ?>">
              </div>
              <input type="submit" class="form-control" name="sbmntBttn" id="sbmntBttn">
            </div>
          </section>
        </form>

      </div>
      <?php require("includes/footer.php"); ?>
    </div>
  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/add_distribute.js"></script>
</body>
</html>