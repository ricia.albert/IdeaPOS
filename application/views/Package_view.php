<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Packages</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-package hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Package
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Package">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Package Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Package Code</td>
                          <td>Package Name</td>
                          <td>Info</td>
                          <td>Price</td>
                          <td>Package Price</td>
                          <?php if ($this->session->userdata("productAuth") == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->packagesID ?></td>
                            <td><?php echo $value->Packages_name ?></td>
                            <td><?php echo $value->Info ?></td>
                            <td><?php echo number_format($value->Total_price) ?></td>
                            <td><?php echo number_format($value->Packages_price) ?></td>
                            <?php if ($this->session->userdata("productAuth") == AUTH_WRITE) : ?>
                              <td>
                                <a href="#"><input type="button" name="copyButton" id="copyButton" onclick="copy('<?php echo $value->Packages_code ?>','<?php echo $value->packagesID ?>','<?php echo $value->POSID; ?>','<?php echo $value->Packages_name ?>')" value="Copy"></a>
                                <a href="<?php base_url(); ?>Product_detail_price?menu=<?php echo $value->packagesID ?>&pos=<?php echo $value->POSID; ?>&name=<?php echo $value->Packages_name; ?>"><input type="button" name="priceBttn" id="priceBttn" value="Special Price" /></a>
                                <a href="<?php base_url(); ?>Package/update?menu=<?php echo $value->packagesID ?>&pos=<?php echo $value->POSID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->packagesID ?>','<?php echo $value->POSID; ?>')" value="Delete"></a>
                              </td>
                            <?php endif; ?>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <?php if ($this->session->userdata("productAuth") == AUTH_WRITE) : ?>
                    <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                    <?php endif; ?>
                  </div>
                  
                  </div>
                
                </div>
                
              </div>

          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
      <?php include "includes/product_copy_dialog.php"; ?>
    </div>

  <script type="text/javascript" src="<?php echo base_url();?>dist/js/package.js"></script>

</body>
</html>
