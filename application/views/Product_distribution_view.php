<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Product</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-distribute hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product Distribute
        </section>

        <form method="POST" action="">
          <section class="content">
            <div class="row">    
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Product Table (klik kode)</h3>
                  </div>
                  <div class="box-body">
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Menu Code</td>
                          <td>Menu Name</td>
                          <td>Description</td>
                          <td>Price</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                          $i++;
                          ?>
                          <tr>
                            <td><a href="<?php echo base_url(); ?>Product_distribution/add_distribute?state=product&code=<?php echo $value->menuID ?>" target="_blank"><?php echo $value->menuID ?></a></td>
                            <td><?php echo $value->menuNm ?></td>
                            <td><?php echo $value->description ?></td>
                            <td><?php echo $value->price ?></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Package Table (klik kode)</h3>
                  </div>
                  <div class="box-body">
                    <table id="packageTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Package Code</td>
                          <td>Package Name</td>
                          <td>Description</td>
                          <td>Price</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_package as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><a href="<?php echo base_url(); ?>Product_distribution/add_distribute?state=package&code=<?php echo $value->packagesID ?>" target="_blank"><?php echo $value->packagesID ?></a></td>
                            <td><?php echo $value->Packages_name ?></td>
                            <td><?php echo $value->Info ?></td>
                            <td><?php echo $value->Packages_price ?></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </section>
        </form>
      </div>

      <?php include "includes/footer.php"; ?>
    </div>
 
  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/distribution.js"></script>

</body>
</html>
