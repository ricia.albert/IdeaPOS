<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Ingridients</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-ingridients hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

  <!-- Header Navbar: style can be found in header.less -->
  <?php require("includes/header.php") ?>
  <body>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require("includes/navigation.php") ?>
    <div class="content-wrapper">
      <section class="content-header">
        Ingridients
      </section>

      <?php

      if (isset($req)) {
        ?>
        <?php if ($copy != 1) { ?>
        <form method="POST" id="branchForm" action="<?php echo base_url();?>Ingridients/update_ingridients">
        <?php } else { ?>
        <form method="POST" id="branchForm" action="<?php echo base_url();?>Ingridients/add_ingridients">
        <?php } ?>
          <input type="hidden" name="ingID" id="ingID" value="<?php echo $req ?>">
          <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div align="center" class="box-header">
                    <h3 align="center" class="box-title">Ingridients Registration</h3>
                    <?php
                    if (isset($err)) {
                      ?>
                      <h3 align="center" style="color:red"><?php echo $err; ?></h3>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table id="userTable" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewProduct as $key) {
                          ?>
                          <tr>
                            <td>Ingridients Code</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->codeIng ?>" name="ingCode" id="ingCode"></td>
                          </tr>
                          <tr>
                            <td>Ingridients Name</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->ingName ?>" name="ingName" id="ingName"></td>
                          </tr>
                          <tr>
                            <td>Ingridients Category</td>
                            <td><select class="form-control" name="ingCat" id="ingCat">
                              <?php
                              foreach ($category as $key2) {
                                ?>
                                <option <?php echo ($key->cateIngID == $key2->cateIngID) ? "selected" : "" ; ?> value="<?php echo $key2->cateIngID ?>"><?php echo $key2->ingCateName ?></option>
                                <?php
                              }
                              ?>
                            </select></td>
                          </tr>
                          <tr>
                            <td>Info</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->info ?>" name="ingInfo" id="ingInfo"></td>
                          </tr>
                          <tr>
                            <td>Ingridients Price</td>
                            <td><input type="number" name="ingPrice" id="ingPrice" value="<?php echo $key->bprice ?>" class="form-control">
                            </tr>
                            <tr>
                              <td>Qty</td>
                              <td><input type="number" class="form-control" value="<?php echo $key->qty ?>" name="ingQty" id="ingQty"></td>
                            </tr>
                            <tr>
                              <td>Minimum Qty</td>
                              <td><input type="number" class="form-control" value="<?php echo $key->minQty ?>" name="ingMin" id="ingMin"></td>
                            </tr>
                            <tr>
                              <td>Scalar</td>
                              <td><select class="form-control" name="ingScalar" id="ingScalar">
                                <?php
                                foreach ($scalar as $key2) {
                                  ?>
                                  <option <?php echo ($key->scalarID == $key2->scalarID) ? "selected" : "" ; ?> value="<?php echo $key2->scalarID ?>"><?php echo $key2->scalarNm ?></option>
                                  <?php
                                }
                                ?>
                              </select></td>
                            </tr>
                            <?php
                          }
                          ?>

                        </thead>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <form method="POST" action="<?php echo base_url();?>Ingridients/add_ingridients">
                      <section class="content">
                        <div class="row">
                          <div class="col-xs-12">
                            <div class="box">
                              <div align="center" class="box-header">
                                <h3 align="center" class="box-title">Ingridients Registration</h3>
                                <?php

                                if (isset($err)) {
                                  ?>
                                  <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                  <?php
                                }
                                ?>
                              </div>

                              <div class="box-body">
                                <input type="hidden" value="<?php echo $pos; ?>" name="pos" id="pos" />
                                <table id="userTable" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <td>Ingridients Code</td>
                                      <td><input type="text" class="form-control" name="ingCode" id="ingCode"></td>
                                    </tr>
                                    <tr>
                                      <td>Ingridients Name</td>
                                      <td><input type="text" class="form-control" name="ingName" id="ingName"></td>
                                    </tr>
                                    <tr>
                                      <td>Ingridients Category</td>
                                      <td><select class="form-control" name="ingCat" id="ingCat">
                                        <?php
                                        foreach ($category as $key) {
                                          ?>
                                          <option value="<?php echo $key->cateIngID ?>"><?php echo $key->ingCateName ?></option>
                                          <?php
                                        }
                                        ?>
                                      </select></td>
                                    </tr>
                                    <tr>
                                      <td>Info</td>
                                      <td><input type="text" class="form-control" name="ingInfo" id="ingInfo"></td>
                                    </tr>
                                    <tr>
                                      <td>Ingridients Price</td>
                                      <td><input type="number" name="ingPrice" id="ingPrice" class="form-control">
                                      </tr>
                                      <tr>
                                        <td>Qty</td>
                                        <td><input type="number" class="form-control" name="ingQty" id="ingQty"></td>
                                      </tr>
                                      <tr>
                                        <td>Minimum Qty</td>
                                        <td><input type="number" class="form-control" name="ingMin" id="ingMin"></td>
                                      </tr>
                                      <tr>
                                        <td>Scalar</td>
                                        <td><select class="form-control" name="ingScalar" id="ingScalar">
                                          <?php
                                          foreach ($scalar as $key) {
                                            ?>
                                            <option value="<?php echo $key->scalarID ?>"><?php echo $key->scalarNm ?></option>
                                            <?php
                                          }
                                          ?>
                                        </select></td>
                                      </tr>
                                    </thead>
                                  </table>
                                  <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                                </div>

                                <?php
                              }

                              ?>

                            </div>
                          </div>
                        </div>
                      </section>
                    </form>

                  </div>


                  <?php include "includes/footer.php"; ?>
                </div>
                
                <script type="text/javascript" src="<?php echo base_url();?>dist/js/ingridient.js"></script>

              </body>
              </html>
