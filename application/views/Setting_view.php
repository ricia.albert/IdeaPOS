<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Configuration</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-settings hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Configurations
        </section>
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Configurations</h3>
                  </div>
                  <div class="box-body">
                    <form method="GET" id="productView" action="<?php echo base_url();?>Setting">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    </form>
                    <form method="POST" action="<?php echo base_url();?>Setting/update">
                    <input type="hidden" id="pos" name="pos" value="<?php echo $temp; ?>" />
                    <div class="clear"><br/></div>
                    <table id="userTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td width="50%">Aktifkan notifikasi untuk stok habis ?</td>
                          <td>
                            <input type="checkbox" value="True" name="Notif" id="Notif" <?php echo ($view_setting->Notif == "True") ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Beri penanda untuk supplier yang diblacklist ?</td>
                          <td>
                            <input type="checkbox" value="1" name="signBlackList" id="signBlackList" <?php echo ($view_setting->signBlackList == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Aktifkan penanda ketika jml stok di bawah stok minimum ?</td>
                          <td>
                            <input type="checkbox" value="True" name="Min_stock" id="Min_stock" <?php echo ($view_setting->Min_stock == "True") ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Aktifkan sistem PPN ?</td>
                          <td><input type="checkbox" value="True" name="e_ppn" id="e_ppn" <?php echo ($view_setting->e_ppn == "True") ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td>Persentase PPN</td>
                          <td><input type="number" name="ppn_value" id="ppn_value" value="<?php echo $view_setting->ppn_value; ?>" /> %</td>
                        </tr>
                        <tr>
                          <td>Rounding / Pembulatan</td>
                          <td>
                            <input type="number" value="<?php echo $view_setting->rounding; ?>" class="form-control" name="rounding" id="rounding" />
                          </td>
                        </tr>
                        <tr>
                          <td>Jumlah maksimal no. antrian service</td>
                          <td><input type="number" name="maxTA" id="maxTA" value="<?php echo $view_setting->maxTA; ?>" /></td>
                        </tr>
                        <tr>
                          <td>Kurangi stok ketika terjadi pesanan</td>
                          <td><input type="checkbox" value="1" name="affectStock" id="affectStock" <?php echo ($view_setting->affectStock == 1) ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td>Konfirmasi admin ketika hapus / kurangi menu pesanan (cashier)</td>
                          <td><input type="checkbox" value="1" name="StrictPolicy" id="StrictPolicy" <?php echo ($view_setting->StrictPolicy == 1) ? "checked" : ""; ?> /></td>
                        </tr>
                        <tr>
                          <td>Jenis harga opname</td>
                          <td>
                            <select class="form-control" name="opnamePrice" id="opnamePrice">
                              <option value="Harga Jual" <?php echo ($view_setting->opnamePrice == "Harga Jual") ? "selected" : ""; ?>>Harga Jual</option>
                              <option value="Harga Beli" <?php echo ($view_setting->opnamePrice == "Harga Beli") ? "selected" : ""; ?>>Harga Beli</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Jenis laba</td>
                          <td>
                            <select class="form-control" name="labatype" id="labatype">
                              <option value="Amount" <?php echo ($view_setting->labatype == "Amount") ? "selected" : ""; ?>>Amount</option>
                              <option value="Percentage" <?php echo ($view_setting->labatype == "Percentage") ? "selected" : ""; ?>>Percentage</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Amount laba</td>
                          <td>
                            <input type="number" class="form-control" id="deflaba" name="deflaba" value="<?php echo $view_setting->deflaba; ?>" />
                          </td>
                        </tr>
                        <tr>
                          <td>Kurangi harga setelah PPN dengan voucher ?</td>
                          <td>
                            <input type="checkbox" id="vCutAfterTax" value="1" name="vCutAfterTax" <?php echo ($view_setting->vCutAfterTax == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Dapat melakukan penawaran harga ?</td>
                          <td>
                            <input type="checkbox" id="canBargain" value="1" name="canBargain" <?php echo ($view_setting->canBargain == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <!-- <tr>
                          <td>Penawaran harga membutuhkan approval ke server ?</td>
                          <td>
                            <input type="checkbox" id="bargainConfirm" value="1" name="bargainConfirm" <?php echo ($view_setting->bargainConfirm == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr> -->
                        <tr>
                          <td>Penawaran expired setelah </td>
                          <td>
                            <input type="number" id="expireBargain" name="expireBargain" value="<?php echo $view_setting->expireBargain; ?>" /> &nbsp;Detik
                          </td>
                        </tr>
                        <tr>
                          <td>Dapat mencicil pembayaran tagihan ?</td>
                          <td>
                            <input type="checkbox" id="allowCredit" value="1" name="allowCredit" <?php echo ($view_setting->allowCredit == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Gunakan harga spesial pelanggan ?</td>
                          <td>
                            <input type="checkbox" id="allowPriceOffer" value="1" name="allowPriceOffer" <?php echo ($view_setting->allowPriceOffer == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Tandai hutang yang sudah melewati jatuh tempo ?</td>
                          <td>
                            <input type="checkbox" id="overdue" value="1" name="overdue" <?php echo ($view_setting->overdue == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Tandai transaksi dari approval ?</td>
                          <td>
                            <input type="checkbox" id="debtSign" value="1" name="debtSign" <?php echo ($view_setting->debtSign == 1) ? "checked" : ""; ?> />
                          </td>
                        </tr>
                        <tr>
                          <td>Primary Tab ?</td>
                          <td>
                            <input type="primaryTitle" name="primaryTitle" value="<?php echo $view_setting->primaryTitle; ?>" id="txtPrimary" />
                          </td>
                        </tr>
                        <tr>
                          <td>Secondary Tab ?</td>
                          <td>
                            <input type="checkbox" id="secondaryTab" value="1" name="secondaryTab" <?php echo ($view_setting->secondaryTab == 1) ? "checked" : ""; ?> />
                            &nbsp;
                            <input type="secondaryTitle" name="secondaryTitle" value="<?php echo $view_setting->secondaryTitle; ?>" id="txtSecondary" />
                          </td>
                        </tr>
                        <tr>
                          <td>Tersier Tab ?</td>
                          <td>
                            <input type="checkbox" id="tersierTab" value="1" name="tersierTab" <?php echo ($view_setting->tersierTab == 1) ? "checked" : ""; ?> />
                            &nbsp;
                            <input type="tersierTitle" name="tersierTitle" value="<?php echo $view_setting->tersierTitle; ?>" id="txtTersier" />
                          </td>
                        </tr>
                        <tr>
                          <td>In Debt Tab ?</td>
                          <td>
                            <input type="checkbox" id="debtTab" value="1" name="debtTab" <?php echo ($view_setting->debtTab == 1) ? "checked" : ""; ?> />
                            &nbsp;
                            <input type="text" value="<?php echo $view_setting->debtTitle; ?>" name="debtTitle" id="debtTitle" />
                          </td>
                        </tr>
                         <tr>
                          <td>Waktu Transisi Display </td>
                          <td>
                            <input type="number" name="transTime" id="transTime" value="<?php echo $view_setting->transTime; ?>" /> &nbsp;Detik
                          </td>
                        </tr>
                        <tr>
                          <td>Produk Primer </td>
                          <td>
                            <input type="text" value="<?php echo $view_setting->codePrimary; ?>" class="form-control" name="codePrimary" id="codePrimary" />
                          </td>
                        </tr>
                        <tr>
                          <td>Produk Secondary </td>
                          <td>
                            <input type="text" value="<?php echo $view_setting->codeSecondary; ?>" class="form-control" name="codeSecondary" id="codeSecondary" />
                          </td>
                        </tr>
                        <tr>
                          <td>Produk Lainnya </td>
                          <td>
                            <input type="text" value="<?php echo $view_setting->codeEtc; ?>" class="form-control" name="codeEtc" id="codeEtc" />
                          </td>
                        </tr>
                      </thead>
                    </table>
                    <?php if ($session->settingAuth == AUTH_WRITE) : ?>
                      <input type="submit" class="form-control"></input>
                    <?php endif; ?>
                  </form>

                    </div>
                  </div>
                  
                </div>
                
              </div>

            </section>

        </div>
        <?php include "includes/footer.php"; ?>
      </div>
      <!-- ./wrapper -->

  <script type="text/javascript" src="<?php echo base_url()?>dist/js/settings.js"></script>

</body>
</html>
