<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Ing-Category</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-ingcate hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Ingredients Category
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Ing_category">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Ingredients Category Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Category Name</td>
                          <?php if ($session->categoryAuth == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->ingCateName ?></td>
                            <?php if ($session->categoryAuth == AUTH_WRITE) : ?>
                              <td>
                                <a href="#" onclick = "functiU('<?php echo $value->cateIngID ?>','<?php echo $value->PosID ?>')"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->cateIngID ?>','<?php echo $value->PosID ?>')" value="Delete"></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($session->categoryAuth == AUTH_WRITE) : ?>
                      <div onclick = "functi()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></div>
                   <?php endif; ?>
                    </div>
                  </div>
                  
                </div>
                
              </div>

            </section>
          </form>

        </div>
        <?php include "includes/footer.php";?>
      </div>
    
  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/ing_cate.js"></script>

</body>
</html>
