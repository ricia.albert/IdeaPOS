<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Product Detail Price</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="<?php
                if (strpos($temp, "MN") === 0) {
                  echo "bd-product";
                } else if (strpos($temp, "PK") === 0) {
                  echo "bd-package";
                } else if (strpos($temp, "SS") === 0) {
                  echo "bd-sidestock";
                } 
              ?> hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product Detail Price
        </section>

        <?php

        if (isset($req)) {
          ?>
          <form method="POST" id="detailForm" action="<?php echo base_url();?>Product_detail_price/update_category">
            <input type="hidden" name="catID" id="catID" value="<?php echo $req ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Product Detail Price Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                            ?>
                            <tr>
                              <td>Menu</td>
                              <td>
                                <select name="menuID" id="menuID" class="form-control">
                                  <?php
                                  foreach ($view_menu as $key2) {
                                    ?>
                                    <option <?php echo ($key->menuID==$key2->menuID) ? "selected" : "" ; ?> value="<?php echo $key2->menuID ?>"><?php echo $key2->menuID ?>-<?php echo $key2->menuNm ?>-<?php echo number_format($key2->price) ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td>Detail Info</td>
                              <td><input type="text" class="form-control" value='<?php echo $key->Info ?>' name="detailInfo" id="detailInfo"></td>
                            </tr>
                            <tr>
                              <td>Detail Price</td>
                              <td><input type="number" value='<?php echo $key->Price ?>' class="form-control" name="detailPrice" id="detailPrice"></td>
                            </tr>
                            <?php
                          }
                          ?>

                        </thead>
                      </table>
                      <input type="button" onclick="submite()" value="Submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <form method="POST" id="detailForm" action="<?php echo base_url();?>Product_detail_price/add_category">
                      <section class="content">
                        <div class="row">
                          <div class="col-xs-12">
                            <div class="box">
                              <div align="center" class="box-header">
                                <h3 align="center" class="box-title">Product Detail Price Registration</h3>
                                <?php

                                if (isset($err)) {
                                  ?>
                                  <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                  <?php
                                }
                                ?>
                              </div>

                              <div class="box-body">
                                <table id="userTable" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <td>Menu</td>
                                      <td>
                                        <select name="menuID" id="menuID" class="form-control">
                                          <?php
                                          foreach ($view_menu as $key) {
                                            ?>
                                            <option <?php echo ($temp==$key->menuID) ? "selected" : "" ; ?> value="<?php echo $key->menuID ?>"><?php echo $key->menuID ?>-<?php echo $key->menuNm ?>-<?php echo number_format($key->price) ?></option>
                                            <?php
                                          }
                                          ?>
                                        </select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Detail Info</td>
                                      <td><input type="text" class="form-control" name="detailInfo" id="detailInfo"></td>
                                    </tr>
                                    <tr>
                                      <td>Detail Price</td>
                                      <td><input type="number" value='0' class="form-control" name="detailPrice" id="detailPrice"></td>
                                    </tr>
                                  </thead>
                                </table>
                                <input type="button" onclick="submite()" value="Submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                              </div>

                              <?php
                            }

                            ?>

                          </div>
                        </div>
                      </div>
                    </section>
                  </form>

                </div>


                <?php include "includes/footer.php"; ?>
              </div>
              <!-- ./wrapper -->

              <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/additional_prod.js"></script>
  </body>
  </html>
