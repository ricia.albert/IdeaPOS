<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Special Price</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-customer hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Special Price
        </section>

        <form id="productView" method="POST" >
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Member Table</h3>
                  </div>
                  <div class="box-body">
                  </select>
                  <table id="ProductTable" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>Guest Name</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      foreach ($viewUser as $key) {
                        $guest = $key->guestID;
                        $pos = $key->PosID;
                        ?>
                        <tr>
                          <td><input type="hidden" readonly name="guestID" value="<?php echo $key->guestID ?>" id="guestID"><?php echo $key->Guest_name ?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Special Price Table</h3>
                </div>
                <div class="box-body">
                </select>
                <table id="menuTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td>Menu Code</td>
                      <td>Menu Name</td>
                      <td>Special Price</td>
                      <td>From</td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($viewMenu as $key) {
                      ?>
                      <tr>
                        <td><?php echo $key->menuID ?></td>
                        <td><?php echo $key->menuNm ?></td>
                        <td ondblclick = "functi('<?php echo $key->priceID ?>','<?php echo $pos ?>','<?php echo $key->guestID ?>')"><?php echo number_format($key->priceOffer,0) ?></td>
                        <td><?php echo $key->POSNm ?></td>
                        <td><!--<a href="<?php base_url(); ?>member/../update_special_view?menu=<?php echo $key->priceID ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>-->
                          <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $key->priceID ?>','<?php echo $key->PosID ?>','<?php echo $key->guestID ?>')" value="Delete"></a></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                  <a href="<?php base_url(); ?>add_special_view?guestID=<?php echo $guest ?>&pos=<?php echo $pos ?>"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                </div>
                
              </div>
            </div>
          </div>

        </section>
      </form>

    </div>
    <?php include "includes/footer.php"; ?>
  </div>
  <script type="text/javascript" src="<?php echo base_url();?>dist/js/special_price.js"></script>

</body>
</html>
