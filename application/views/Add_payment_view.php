<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Payment</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-position hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Payment
        </section>

        <?php
        if (isset($req)) {
          ?>
          <form method="POST" action="<?php echo base_url();?>Payment/update_position">
            <input type="hidden" name="paymentID" id="paymentID" value="<?php echo $req ?>" />
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>" />
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Payment Registration</h3>
                      <?php
                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                            ?>
                            <tr>
                              <td>Payment Name</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->paymentName; ?>" name="paymentName" id="paymentName"></td>
                            </tr>
                            <tr>
                              <td>Percent Discount</td>
                              <td><input type="number" class="form-control" value="<?php echo $key->percentDiscount; ?>" name="percentDiscount" id="percentDiscount"></td>
                            </tr>
                            <tr>
                              <td>Discount</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->discount; ?>" name="discount" id="discount"></td>
                            </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" action="<?php echo base_url();?>Payment/add_position">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">Payment Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <input type="hidden" value="<?php echo $pos; ?>" name="PosID" id="PosID" />
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <td>Payment Name</td>
                                    <td><input type="text" class="form-control" name="paymentName" id="paymentName"></td>
                                  </tr>
                                  <tr>
                                    <td>Percent Discount</td>
                                    <td><input type="number" class="form-control" name="percentDiscount" id="percentDiscount"></td>
                                  </tr>
                                  <tr>
                                    <td>Discount</td>
                                    <td><input type="text" class="form-control" name="discount" id="discount"></td>
                                  </tr>
                              </thead>
                            </table>
                            <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                          </div>

                          <?php
                        }

                        ?>

                      </div>
                    </div>
                  </div>
                </section>
              </form>

            </div>

            <?php include "includes/footer.php"; ?>
          </div>
          
          <script type="text/javascript" src="<?php echo base_url();?>dist/js/payment.js"></script>
  </body>
  </html>