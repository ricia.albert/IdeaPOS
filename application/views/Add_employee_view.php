<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Employee</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-employee hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Employee
        </section>
        <?php

        if (isset($req)) {
          ?>
          <form method="POST" id="actionForm" action="<?php echo base_url();?>Employee/update_user">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">User Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewUser as $key) {
                            ?>

                            <tr>
                              <td>Employee Name</td>
                              <td><input type="text" class="form-control" name="employeeName" id="employeeName" value="<?php echo $key->employee_name ?>">
                              <input type="hidden" name="employeeID" id="employeeID" value="<?php echo $key->employeeID ?>" />
                              <input type="hidden" name="PosID" id="PosID" value="<?php echo $key->PosID; ?>" /></td>
                            </tr>
                           <!--  <tr>
                              <td>Password</td>
                              <td><input type="Password" class="form-control" name="password" id="password" value="<?php echo $key->password ?>"></td>
                            </tr> -->
                            <tr>
                              <td>Employee Position</td>
                              <td>
                                <select id="employeePosition" name="employeePosition" class="form-control">
                                  <?php foreach ($position as $p) : ?>
                                    <option value="<?php echo $p->Position_name; ?>" <?php echo ($key->Position == $p->Position_name) ? 'selected' : ''; ?> ><?php echo $p->Position_name;?></option>
                                  <?php endforeach; ?>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td>Type</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Type == 'P') ? 'checked' : ''; ?> name="employeeType" id="gender1" value="P" >
                                  Permanent
                                </label>
                                &nbsp;
                                <label>
                                  <input type="radio" <?php echo ($key->Type == 'C') ? 'checked' : ''; ?> name="employeeType" id="gender2" value="C" >
                                  Contract
                                </label>
                                &nbsp;
                                <label>
                                  <input type="radio" <?php echo ($key->Type == 'T') ? 'checked' : ''; ?> name="employeeType" id="gender3" value="T" >
                                  Terminated
                                </label>
                              </div>
                            </td>
                            </tr>
                            <tr>
                              <td>Employee KTP</td>
                              <td><input type="text" class="form-control" name="employeeKTP" id="employeeKTP" value="<?php echo $key->No_KTP ?>"></td>
                            </tr>
                            <tr>
                              <td>Employee Rek</td>
                              <td><input type="text" class="form-control" name="employeeRek" id="employeeRek" value="<?php echo $key->No_Rek ?>"></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><input type="text" class="form-control" name="employeeAddress" id="employeeAddress" value="<?php echo $key->Address ?>"></td>
                            </tr>
                            <tr>
                              <td>Status</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Status == 'M') ? 'checked' : ''; ?> name="employeeStatus" id="gender1" value="M" >
                                  Menikah
                                </label>
                                <label>
                                  <input type="radio" <?php echo ($key->Status == 'S') ? 'checked' : ''; ?> name="employeeStatus" id="gender2" value="S" >
                                  single
                                </label>

                              </div>
                            </td>
                          </tr>
                            <tr>
                              <td>Sex</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Sex == 'L') ? 'checked' : ''; ?> name="gender" id="gender1" value="L" >
                                  Pria
                                </label>
                                <label>
                                  <input type="radio" <?php echo ($key->Sex == 'P') ? 'checked' : ''; ?> name="gender" id="gender2" value="P" >
                                  Wanita
                                </label>

                              </div>
                            </td>
                          </tr>
                        <tr>
                          <td>Phone</td>
                          <td><input type="text" class="form-control" name="employeePhone" id="employeePhone" value="<?php echo $key->Phone ?>"></td>
                        </tr>
                        <tr>
                          <td>Work Date</td>
                          <td><input type="text" readonly class="date form-control" name="work" value="<?php echo $key->Work_date ?>"></td>
                        </tr>
                        <tr>
                          <td>End Date</td>
                          <td><input type="text" readonly class="date form-control" name="end" value="<?php echo $key->End_date ?>"></td>
                        </tr>
                        <?php
                      }
                      ?>

                    </thead>
                  </table>
                  <input type="submit" class="form-control" onclick="subtm()" name="sbmtBttn" id="sbmtBttn"></input>
                </div>
                <?php
              }
              else{
                ?>
                <form method="POST" id="actionForm" action="<?php echo base_url();?>Employee/add_user">
                  <section class="content">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="box">
                          <div align="center" class="box-header">
                            <h3 align="center" class="box-title">User Registration</h3>
                            <?php

                            if (isset($err)) {
                              ?>
                              <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                              <?php
                            }
                            ?>
                          </div>

                          <div class="box-body">
                            <input type="hidden" value="<?php echo $branch; ?>" name="PosID" id="PosID" />
                            <table id="userTable" class="table table-bordered table-hover">
                              <thead>
                                <!-- <tr>
                                  <td>ID</td>
                                  <td><input type="text" readonly="" class="form-control" name="userID" id="userID"></td>
                                </tr> -->
                                <tr>
                              <td>Employee Name</td>
                              <td><input type="text" class="form-control" name="employeeName" id="employeeName">
                              <input type="hidden" name="employeeID" id="employeeID" ></td>
                            </tr>
                           <!--  <tr>
                              <td>Password</td>
                              <td><input type="Password" class="form-control" name="password" id="password" value="<?php echo $key->password ?>"></td>
                            </tr> -->
                            <tr>
                              <td>Employee Position</td>
                              <td>  
                                 <select id="employeePosition" name="employeePosition" class="form-control">
                                  <?php foreach ($position as $p) : ?>
                                    <option value="<?php echo $p->Position_name; ?>" ><?php echo $p->Position_name;?></option>
                                  <?php endforeach; ?>
                                </select>
                            </td>
                            </tr>
                            <tr>
                              <td>Type</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" checked name="employeeType" id="gender1" value="P" >
                                  Permanent
                                </label>
                                &nbsp;
                                <label>
                                  <input type="radio" name="employeeType" id="gender2" value="C" >
                                  Contract
                                </label>
                                &nbsp;
                                <label>
                                  <input type="radio" name="employeeType" id="gender3" value="T" >
                                  Terminated
                                </label>
                              </div>
                            </td>
                            </tr>
                            <tr>
                              <td>Employee KTP</td>
                              <td><input type="text" class="form-control" name="employeeKTP" id="employeeKTP" ></td>
                            </tr>
                            <tr>
                              <td>Employee Rek</td>
                              <td><input type="text" class="form-control" name="employeeRek" id="employeeRek" ></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><input type="text" class="form-control" name="employeeAddress" id="employeeAddress" ></td>
                            </tr>
                            <tr>
                              <td>Status</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" checked name="employeeStatus" id="gender1" value="M" >
                                  Menikah
                                </label>
                                <label>
                                  <input type="radio" name="employeeStatus" id="gender2" value="S" >
                                  single
                                </label>

                              </div>
                            </td>
                          </tr>
                            <tr>
                              <td>Sex</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" checked name="gender" id="gender1" value="L" >
                                  L
                                </label>
                                <label>
                                  <input type="radio" name="gender" id="gender2" value="P" >
                                  P
                                </label>

                              </div>
                            </td>
                          </tr>
                        <tr>
                          <td>Phone</td>
                          <td><input type="text" class="form-control" name="employeePhone" id="employeePhone" ></td>
                        </tr>
                        <tr>
                          <td>Work Date</td>
                          <td><input type="text" readonly class="date form-control" name="work" /></td>
                        </tr>
                        <tr>
                          <td>End Date</td>
                          <td><input type="text" readonly class="date form-control" name="end" /></td>
                        </tr>
                          </thead>
                        </table>
                        <input type="submit" onclick="subtm()" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                      </div>

                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/Add_employee_view.js"></script>
      </div>
</body>
</html>
