<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Cash Drawer</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-cashdrawer hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Cash Drawer
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Cashdrawer">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Cash Drawer Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Branch Code</td>
                          <td>Shift</td>
                          <td>Money</td>
                          <td>Qty</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = "a";
                        foreach ($view_product as $key) {
                      //var_dump($view_product);
                      //echo "$i";
                          ?>
                          <tr>
                            <td><?php echo $key->PosID ?></td>
                            <td><?php echo $key->shiftDate ?></td>
                            <td><?php echo $key->moneyNm?></td>
                            <td><?php echo $key->qty?></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!--<a href="<?php base_url(); ?>product_detail_price/add_detail_view"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>-->
            </div>


          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
    </div>
  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/cashdrawer.js"></script>

</body>
</html>
