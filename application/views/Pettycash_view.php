<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Petty Cash</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-pettycash hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Petty Cash
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Pettycash">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Petty Cash Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <div class="pull-right">
                      <input type="text" class="date" value="<?php echo $sd; ?>" readonly name="sd" id="startDt" placeholder="Start Date" />&nbsp; <strong>s/d</strong> &nbsp; 
                      <input type="text" class="date" value="<?php echo $ed; ?>" readonly name="ed" id="endDt" placeholder="End Date" />
                      <button type="button" id="btfilter" name="btfilter" onclick="doit()">Filter</button>
                    </div>
                    <div style="clear:both"><br/></div>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Petty Date</td>
                          <td>Information</td>
                          <td>Description</td>
                          <td>Type</td>
                          <td>Amount</td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key) {
                      //var_dump($value);
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo date('d M Y', strtotime($key->pettyDate)); ?></td>
                            <td><?php echo $key->pettyName; ?></td>
                            <td><?php echo $key->pettyDescr; ?></td>
                            <td><?php echo $key->typeName; ?></td>
                            <td><?php echo number_format($key->pettyAmt,0,",","."); ?></td>
                            <td>
                              <a href="<?php base_url(); ?>Pettycash/update?menu=<?php echo $key->pettyCashID ?>&pos=<?php echo $key->PosID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                              <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $key->pettyCashID ?>','<?php echo $key->PosID; ?>')" value="Delete"></a>
                            </td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                    <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                    <?php endif; ?>
                  </div>

                </div>

              </div>
              
            </div>
          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
    </div>
</body>
</html>
<script type="text/javascript" src="<?php echo base_url();?>dist/js/pettycash.js"></script>

