<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Product</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-product hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product
        </section>

        <div style="display:none" name="scalar" id="scalar"><?php echo $scalarJson ?></div>
        <?php

        if (isset($req)) {
          $price="";
          $rowCount=0;
          $indexArray=array();
          $arrayIndex = array();
          ?>
          <?php if ($copy != 1) { ?>
          <form method="POST" id="branchForm" onsubmit="return validate()" action="<?php echo base_url();?>Product/update_product">
          <?php } else { ?>
          <form method="POST" id="branchForm" onsubmit="return validate()" action="<?php echo base_url(); ?>Product/add_product">
          <?php } ?>
            <input type="hidden" name="req" id="req" value="<?php echo $req ?>">
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">

            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Product Detail 
                        <?php if (isset($posNm)) : ?>
                          - Copy to <strong><?php echo $posNm; ?></strong>
                        <?php endif; ?></h3>
                      <?php
                      if(isset($err)){
                        ?>
                        <h3 style="color : red"><?php echo $err ?></h3>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="branchTable" class="table table-bordered table-hover">
                        <thead>
                          <?php

                          foreach ($viewProduct as $key) {
                    //var_dump($viewBranch)
                            ?>
                            <tr>
                              <td>Menu Code</td>
                              <td><input type="text" class="form-control" name="menuID" id="menuID" value="<?php echo $key->codeMenu ?>"></td>
                            </tr>
                            <tr>
                              <td>Menu Name</td>
                              <td><input type="text" class="form-control" name="nameMenu" id="nameMenu" value="<?php echo $key->menuNm ?>"></td>
                            </tr>
                            <tr>
                              <td>Description</td>
                              <td><input type="text" class="form-control" name="Description" id="Description" value="<?php echo $key->description ?>"></td>
                            </tr>
                            <tr>
                              <td>Price</td>
                              <td><input type="number" class="form-control" name="menuPrice" id="menuPrice" value="<?php echo $key->price ?>"></td>
                            </tr>
                            <tr>
                              <td>Visible</td>
                              <td>
                                <div class="radio">
                                  <label>
                                    <input type="radio" <?php echo ($key->visible == '1') ? 'checked' : ''; ?> name="menuVisible" id="menuVisible1" value="1" >
                                    Yes
                                  </label>
                                  <label>
                                    <input type="radio" <?php echo ($key->visible == '0') ? 'checked' : ''; ?> name="menuVisible" id="menuVisible2" value="0" >
                                    No
                                  </label>

                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Sub Kategori</td>
                              <td>
                                <select class="form-control" name="menuSub" id="menuSub">
                                  <?php
                                  foreach ($menuSub as $key2) {
                                    ?>
                                    <option <?php echo ($key->subCategoryID==$key2->subCategoryID)? "selected" : "" ; ?> value="<?php echo $key2->subCategoryID ?>"><?php echo $key2->subCategoryNm ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td>Brand</td>
                              <td>
                                <select class="form-control" name="brandID" id="brandID">
                                  <?php
                                  foreach ($allBrand as $key3) {
                                    ?>
                                    <option <?php echo ($key->brandID==$key3->brandID)? "selected" : "" ; ?> value="<?php echo $key3->brandID ?>"><?php echo $key3->brandNm ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                            <?php
                          }

                          ?>

                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Ingridients</h3>
                    </div>
                    <div class="box-body">
                      <table id="menuDetail" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Ingredients Code</td>
                            <td width="50%">Ingredients Name</td>
                            <td width="20%">Ingredients Qty</td>
                            <td width="15%">Scalar</td>
                          </tr>    
                        </thead>
                        <tbody>
                          <?php
                          foreach ($allMenu as $key) {
                            ?>
                            <tr>
                              <td><?php echo $key->ingID ?></td>
                              <td><?php echo $key->ingName ?></td>
                              <td><?php echo $key->qty ?></td>
                              <td><?php echo $key->scalarNm ?></td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- accepted Menu -->
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Choosen</h3>
                    </div>
                    <div class="box-body">
                      <table id="acceptedMenu" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Menu Code</td>
                            <td width="50%">Menu Name</td>
                            <td width="20%">Qty</td>
                            <td width="10%">Scalar</td>
                            <td width="5%"></td>
                          </tr>   
                        </thead>
                        <tbody>
                          <?php
                          foreach ($menuChoosen as $key) {
                            $rowCount++;
            //echo $key->menuID;
                            array_push($indexArray, $key->ingID);
                            $arrayIndex[]+= $rowCount;
                            ?>
                            <tr>
                              <td><input type="text" class="form-control" readonly name="addCode<?php echo $rowCount ?>" id="addCode<?php echo $rowCount ?>" value="<?php echo $key->ingID ?>" /></td>

                              <td><input type="text" class="form-control" readonly name="addName<?php echo $rowCount ?>" id="addName<?php echo $rowCount ?>" value="<?php echo $key->ingName ?>" ></td>

                              <td><input type="text" class="num form-control" name="addQty<?php echo $rowCount ?>" id="addQty<?php echo $rowCount ?>" value="<?php echo $key->qty ?>" ></td>
                              <td>
                                <select class="form-control" name="addScalar<?php echo $rowCount ?>" id="addScalar<?php echo $rowCount ?>">
                                    <?php foreach ($scalar as $s => $c) { ?>
                                      <option value="<?php echo $c->scalarID; ?>" <?php echo ($c->scalarID == $key->scalarID) ? "selected" : ""; ?>><?php echo $c->scalarNm; ?></option>
                                    <?php }?>
                                </select>
                              </td>
                              <td><input type="button" value = "Delete" class="form-control" onClick="Javacsript:deleteRow(this)"></td>
                            </tr>
                            <?php
                          }
                          ?>
                          <div style="display: none" name="indexArray" id="indexArray" ><?php echo json_encode($indexArray) ?></div>
                          <div style="display: none" name="arrayIndex" id="arrayIndex" ><?php echo json_encode($arrayIndex) ?></div>
                          <input type="hidden" name="indexInsert" value="<?php echo $rowCount ?>" id="indexInsert">
                        </tbody>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn" />
                    </div>
                  </div>
                </div>
                
              </div>
            </section>
          </form>
          <?php
        }
        else{
          ?>
          <form method="POST" id="branchForm" onsubmit="return validate()" action="<?php echo base_url(); ?>Product/add_product">
            <input type="hidden" name="indexInsert" value="0" id="indexInsert">
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Product Detail</h3>
                      <?php
                      if(isset($err)){
                        ?>
                        <h3 style="color : red"><?php echo $err ?></h3>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="branchTable" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td>Menu Code</td>
                            <td><input type="text" class="form-control" name="menuID" id="menuID"></td>
                          </tr>
                          <tr>
                            <td>Menu Name</td>
                            <td><input type="text" class="form-control" name="nameMenu" id="nameMenu"></td>
                          </tr>
                          <tr>
                            <td>Description</td>
                            <td><input type="text" class="form-control" name="Description" id="Description"></td>
                          </tr>
                          <tr>
                            <td>Price</td>
                            <td><input type="number" class="form-control" name="menuPrice" id="menuPrice"></td>
                          </tr>
                          <tr>
                            <td>Visible</td>
                            <td><!--<input type="text" class="form-control" name="menuVisible" id="menuVisible" value="<?php echo $key->latitude ?>">-->
                              <div class="radio">
                                <label>
                                  <input type="radio" name="menuVisible" checked id="menuVisible1" value="1" >
                                  Yes
                                </label>
                                <label>
                                  <input type="radio" name="menuVisible" id="menuVisible2" value="0" >
                                  No
                                </label>

                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Sub Kategori</td>
                            <td><!--<input type="text" class="form-control" name="menuSub" id="menuSub" value="<?php echo $key->longitude ?>">-->
                              <select class="form-control" name="menuSub" id="menuSub">
                                <?php
                                foreach ($menuSub as $key) {
                                  ?>
                                  <option value="<?php echo $key->subCategoryID ?>"><?php echo $key->subCategoryNm ?></option>
                                  <?php
                                }
                                ?>
                              </select>
                            </td>
                          </tr>
                          <tr>
                              <td>Brand</td>
                              <td>
                                <select class="form-control" name="brandID" id="brandID">
                                  <?php
                                  foreach ($allBrand as $key3) {
                                    ?>
                                    <option value="<?php echo $key3->brandID ?>"><?php echo $key3->brandNm ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Ingredients</h3>
                    </div>
                    <div class="box-body">
                      <table id="menuDetail" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Ingredients Code</td>
                            <td width="60%">Ingredients Name</td>
                            <td width="20%">Ingredients Qty</td>
                            <td width="15%">Scalar</td>
                          </tr>   
                        </thead>
                        <tbody>
                          <?php
                          foreach ($allMenu as $key) {
                            ?>
                            <tr>
                              <td><?php echo $key->ingID ?></td>
                              <td><?php echo $key->ingName ?></td>
                              <td><?php echo $key->qty ?></td>
                              <td><?php echo $key->scalarNm ?></td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- accepted Menu -->
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Choosen</h3>
                    </div>
                    <div class="box-body">
                      <table id="acceptedMenu" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td width="15%">Menu Code</td>
                            <td width="50%">Menu Name</td>
                            <td width="20%">Qty</td>
                            <td width="10%">Scalar</td>
                            <td width="5%"></td>
                          </tr>   
                        </thead>
                        <tbody>
                        </tbody>
                      </table>

                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                    </div>

                  </div>
                </div>
                
              </div>
              
            </form>
            <?php
          }

          ?>
        </div>

        <?php include "includes/footer.php"; ?>
      </div>
      
      <script type="text/javascript" src="<?php echo base_url();?>dist/js/add_product.js"></script>
</body>
</html>
