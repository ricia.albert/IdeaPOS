<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Shift in / out</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-shift hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Shift in / out
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Shiftinout">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Shift in / out Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Branch Code</td>
                          <td>Shift Date</td>
                          <td>In/Out</td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key) {
                      //var_dump($value);
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $key->PosID ?></td>
                            <td><?php echo $key->shiftDate ?></td>
                            <td><?php echo ($key->isInOut=='0') ? "In" : "Out" ; ?></td>
                            <td><a target="_blank" href="<?php echo base_url(); ?>Shiftinout/detail?menu=<?php echo $key->shiftID ?>&pos=<?php echo $key->PosID ?>">
                              <input type="button" name="cashButton" id="cashButton" value="Cash Drawer" ></a></td>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!--<a href="<?php base_url(); ?>product_detail_price/add_detail_view"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>-->
            </div>


          </section>
        </form>

      </div>
      <?php include "includes/footer.php"; ?>
    </div>

  <script type="text/javascript">
  $(function () {
    $('#branchTable').DataTable({
      "columnDefs": [{
        "defaultContent": "-",
        "targets": "_all"
      }],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  $(function () {
    $('#ProductTable').DataTable({
      "columnDefs": [{
        "defaultContent": "-",
        "targets": "_all"
      }],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  function confirmation(d){
    var base = "<?php base_url(); ?>product_detail_price/delete?menu="+d;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
      window.location.assign(base);
    }
  }

  function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }
  </script>

</body>
</html>
