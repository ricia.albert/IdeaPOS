<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Ing-Category</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-ingcate hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Ingredients Category
        </section>
        <?php
        if (isset($req)) {
          ?>
          <form method="POST" action="<?php echo base_url();?>Sub_category/update_category">
            <input type="hidden" name="catID" id="catID" value="<?php echo $req ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Ingredients Category Registration</h3>
                      <?php
                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                            ?>
                            <tr>
                              <td>Sub Category Name</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->subCategoryNm ?>" name="categoryName" id="categoryName"></td>
                            </tr>
                            <tr>
                              <td>From Category</td>
                              <td><select class="form-control" name="typeID" id="typeID">
                                <?php
                                foreach ($type as $key2) {
                                  ?>
                                  <option <?php echo ($key->categoryID==$key2->categoryID) ? "selected" : "" ; ?>  value="<?php echo $key2->categoryID ?>"><?php echo $key2->categoryNm ?></option>
                                  <?php
                                }
                                ?>
                              </select></td>
                            </tr>
                            <tr>
                              <td>Visible</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->visible=="1") ? "checked" : "" ; ?> name="visible" id="gender1" value="1">
                                  Yes
                                </label>
                                <label>
                                  <input type="radio" <?php echo ($key->visible=="0") ? "checked" : "" ; ?> name="visible" id="gender2" value="0">
                                  No
                                </label>

                              </div>
                            </td>
                          </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" action="<?php echo base_url();?>Sub_category/add_category">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">Ingredients Category Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <td>Sub Category Name</td>
                                    <td><input type="text" class="form-control" name="categoryName" id="categoryName"></td>
                                  </tr>
                                  <tr>
                                    <td>Visible</td>
                                    <td>  <div class="radio">
                                      <label>
                                        <input type="radio" checked name="visible" id="gender1" value="1" checked>
                                        Yes
                                      </label>
                                      <label>
                                        <input type="radio" name="visible" id="gender2" value="0">
                                        No
                                      </label>

                                    </div>
                                  </td>
                                </tr>
                              </thead>
                            </table>
                            <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                          </div>

                          <?php
                        }
                        ?>

                      </div>
                    </div>
                  </div>
                </section>
              </form>

            </div>



          </div>

    <script type="text/javascript" src="<?php echo base_url();?>dist/js/ing_cate.js"></script>

  </body>
  </html>
