<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Brand</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-brand hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Brand
        </section>

        <?php
        if (isset($req)) {
          ?>
          <form method="POST" id="brandForm" action="<?php echo base_url();?>Brand/update_category">
            <input type="hidden" name="catID" id="catID" value="<?php echo $req ?>">
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Brand Registration</h3>
                      <?php
                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                            ?>
                            <tr>
                              <td>Brand Name</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->brandNm; ?>" name="brandNm" id="brandNm"></td>
                            </tr>
                            <tr>
                              <td>Visible</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->visible=="1") ? "checked" : "" ; ?> name="visible" id="gender1" value="1">
                                  Yes
                                </label>
                                <label>
                                  <input type="radio" <?php echo ($key->visible=="0") ? "checked" : "" ; ?> name="visible" id="gender2" value="0">
                                  No
                                </label>

                              </div>
                            </td>
                          </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <input type="button" onclick="check()" class="form-control" name="sbmtBttn" id="sbmtBttn" value="Submit"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" id="brandForm" action="<?php echo base_url();?>Brand/add_category">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">Brand Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <input type="hidden" value="<?php echo $branch; ?>" name="PosID" id="PosID" />
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <td>Brand Name</td>
                                    <td><input type="text" class="form-control" name="brandNm" id="brandNm"></td>
                                  </tr>
                                  <tr>
                                    <td>Visible</td>
                                    <td>  <div class="radio">
                                      <label>
                                        <input type="radio" name="visible" id="gender1" value="1" checked>
                                        Yes
                                      </label>
                                      <label>
                                        <input type="radio" name="visible" id="gender2" value="0">
                                        No
                                      </label>

                                    </div>
                                  </td>
                                </tr>
                              </thead>
                            </table>
                            <input type="button" onclick="check()" class="form-control" name="sbmtBttn" id="sbmtBttn" value="Submit"></input>
                          </div>

                          <?php
                        }

                        ?>

                      </div>
                    </div>
                  </div>
                </section>
              </form>

            </div>

            <?php include "includes/footer.php"; ?>
          </div>
        
          <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/brand.js"></script>
  </body>
  </html>