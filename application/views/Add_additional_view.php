<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Additional</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-product hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Additional Product
        </section>

        <?php

        if (isset($req)) {
          ?>
          <form method="POST" id="additionalForm" action="<?php echo base_url();?>Additional_menu/update_category">
            <input type="hidden" name="catID" id="catID" value="<?php echo $req ?>">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">Additional Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewProduct as $key) {
                            ?>
                            <tr>
                              <td>Additional Product</td>
                              <td><select class="form-control" name="additionalCode" id="additionalCode">
                                <?php
                                foreach ($view_menu as $key2) {
                                  ?>
                                  <option <?php echo ($key->toppingID == $key2->menuID) ? "selected" : "" ; ?> value="<?php echo $key2->menuID ?>" ><?php echo $key2->menuID ?>-<?php echo $key2->menuNm ?></option>
                                  <?php
                                }
                                ?>
                              </select></td>
                            </tr>
                            <tr>
                              <td>Special Price</td>
                              <td><input type="number" class="form-control" name="specialPrice" id="specialPrice" value="<?php echo $key->specialPrice; ?>" /></td>
                            </tr>
                            <tr>
                              <td>Use Special Price ?</td>
                              <td><input type="checkbox" value="1" name="useSpecial" id="useSpecial" <?php echo ($key->useDefaultPrice == 1) ? "checked" : "" ; ?> /></td>
                            </tr>
                            <?php
                          }
                          ?>

                        </thead>
                      </table>
                      <input type="button" value="Submit" onclick="submits()" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <form method="POST" id="additionalForm" action="<?php echo base_url();?>Additional_menu/add_category">
                      <section class="content">
                        <div class="row">
                          <div class="col-xs-12">
                            <div class="box">
                              <div align="center" class="box-header">
                                <h3 align="center" class="box-title">Additional Registration</h3>
                                <?php

                                if (isset($err)) {
                                  ?>
                                  <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                  <?php
                                }
                                ?>
                              </div>

                              <div class="box-body">
                                <table id="userTable" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <td>Additional Product</td>
                                      <td><select class="form-control" name="additionalCode" id="additionalCode">
                                        <?php
                                        foreach ($view_menu as $key => $value) {
                                          ?>
                                          <option value="<?php echo $value->menuID ?>" ><?php echo $value->menuID ?>-<?php echo $value->menuNm ?></option>
                                          <?php
                                        }
                                        ?>
                                      </select></td>
                                    </tr>
                                    <tr>
                                      <td>Special Price</td>
                                      <td><input type="number" class="form-control" name="specialPrice" id="specialPrice" /></td>
                                    </tr>
                                    <tr>
                                      <td>Use Special Price ?</td>
                                      <td><input type="checkbox" value="1" name="useSpecial" id="useSpecial" /></td>
                                    </tr>
                                  </thead>
                                </table>
                                <input type="button" value="Submit" onclick="submits()" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                              </div>

                              <?php
                            }

                            ?>

                          </div>
                        </div>
                      </div>
                    </section>
                  </form>

                </div>


                <?php include "includes/footer.php"; ?>
              </div>
           
              <script type="text/javascript" src="<?php echo base_url();?>dist/js/additional_prod.js"></script>
  </body>
  </html>
