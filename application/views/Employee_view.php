<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Employee</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-employee hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Employee
        </section>

        <form method="POST" id="productView" action="<?php echo base_url()?>Employee">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Employee Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="branchTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Employee ID</td>
                          <td>Employee Name</td>
                          <td>Employee Position</td>
                          <td>Employee Type</td>
                          <td>Employee Identity(KTP)</td>
                          <td>Employee Rek</td>
                          <td>Employee Sex</td>
                          <td>Employee Status</td>
                          <td>Employee Phone</td>
                          <td>Employee PosID</td>
                          <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($view_product as $key =>$value) {
                          ?>
                          <tr>
                            <td><?php echo $value->employeeID ?></td>
                            <td><?php echo $value->employee_name ?></td>
                            <td><?php echo $value->Position ?></td>
                            <td>
                              <?php 
                                switch ($value->Type) {
                                  case "P" : echo "Permanent"; break;
                                  case "C" : echo "Contract"; break;
                                  case "T" : echo "Terminated"; break;
                                }
                              ?>
                            </td>
                            <td><?php echo $value->No_KTP ?></td>
                            <td><?php echo $value->No_Rek ?></td>
                            <td><?php echo ($value->Sex == "M") ? "Pria" : "Wanita" ?></td>
                            <td><?php echo ($value->Status == "S") ? "Single" : "Married" ?></td>
                            <td><?php echo $value->Phone ?></td>
                            <td><?php echo $value->PosID ?></td>
                            <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                              <td>
                                <a href="<?php echo base_url(); ?>Employee/update?d=<?php echo $value->employeeID ?>&p=<?php echo $value->PosID ?>"><input type="button" name="updtButton" id="updtButton" value="Update" ></a>
                                <a href="<?php echo base_url(); ?>Employee/delete?d=<?php echo $value->employeeID ?>&p=<?php echo $value->PosID ?>"><input type="button" name="dltButton" id="dltButton" onclick="confirmation('<?php echo $value->employeeID ?>')" value="Delete"></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <a href="<?php echo base_url(); ?>Employee/add_employee_view?b=<?php echo $temp; ?>"><input type="button" class="form-control" name="addBttn" id="addBttn" value="Add New"></a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </form>

        </div>
        <?php include "includes/footer.php"; ?>
      </div>
      <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/employee.js"></script>
    </body>
    </html>
