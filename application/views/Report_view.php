<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Report</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-report hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Reports
        </section>
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Reports</h3>
                </div>
                <div class="box-body">
                  <table class="table">
                    <thead>
                      <tr>
                          <th width="30px">No. </th>
                          <th>Report Name</th>
                          <th>Files</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td align="center">1.</td>
                          <td>Laporan Penjualan Total</td>
                          <td><a class="repo" data-mod="exportTrans" href="#dialog-trans" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">2.</td>
                          <td>Laporan Petty Cash (Kas Kecil)</td>
                          <td><a class="repo" data-mod="exportPettyCash" href="#dialog-date" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">3.</td>
                          <td>Laporan Harga Pokok Penjualan per Transaksi</td>
                          <td><a class="repo" data-mod="exportHPP" href="#dialog-trans" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">4.</td>
                          <td>Laporan Persediaan (Stock)</td>
                          <td><a class="repo" data-mod="exportStock" href="#dialog-branch" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">5.</td>
                          <td>Laporan Stock Opname</td>
                          <td><a class="repo" data-mod="exportOpname" href="#dialog-opname" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">6.</td>
                          <td>Laporan Penjualan Per Produk</td>
                          <td><a class="repo" data-mod="exportProductSell" href="#dialog-date" data-toggle="modal" ><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">7.</td>
                          <td>Laporan Omset &amp; Kunjungan Per Hari</td>
                          <td><a class="repo" data-mod="exportOmset" href="#dialog-date" data-toggle="modal" ><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">8.</td>
                          <td>Laporan Penggunaan Stock</td>
                          <td><a class="repo" data-mod="exportUsage" href="#dialog-date" data-toggle="modal" ><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">9.</td>
                          <td>Laporan Mutasi Stock</td>
                          <td><a class="repo" data-mod="exportMutasi" href="#dialog-date" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <tr>
                          <td align="center">10.</td>
                          <td>Laporan Rangkuman Penjualan</td>
                          <td><a class="repo" data-mod="exportSummary" href="#dialog-date" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr>
                      <!-- <tr>
                          <td align="center">10.</td>
                          <td>Laporan Perubahan Transaksi</td>
                          <td><a class="repo" data-mod="exportChange" href="#dialog-date" data-toggle="modal"><span class="fa fa-download"></span> &nbsp;Download</a></td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>

            </div>

          </div>

        </section>

      </div>
      <?php include "includes/footer.php"; ?>
      <?php include "includes/report/trans-dialog.php"; ?>
      <?php include "includes/report/date-dialog.php"; ?>
      <?php include "includes/report/branch-dialog.php"; ?>
      <?php include "includes/report/opname-dialog.php"; ?>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="<?php echo base_url()?>dist/js/report.js"></script>

  </body>
  </html>
