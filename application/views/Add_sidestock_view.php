<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Side Stock</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-sidestock hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Side Stock 
        </section>

        <?php
        if (isset($req)) {
          ?>
          <?php if ($copy != 1) { ?>
          <form method="POST" id="branchForm" action="<?php echo base_url();?>Sidestock/update_product">
          <?php } else { ?>
          <form method="POST" id="branchForm" action="<?php echo base_url(); ?>Sidestock/add_product">
          <?php } ?>
            <input type="hidden" name="req" id="req" value="<?php echo $req ?>" />
            <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>" />
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Product Detail</h3>
                      <?php
                      if(isset($err)){
                        ?>
                        <h3 style="color : red"><?php echo $err ?></h3>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="branchTable" class="table table-bordered table-hover">
                        <thead>
                          <?php

                          foreach ($viewProduct as $key) {
                    //var_dump($viewBranch)
                            ?>
                            <tr>
                              <td>Side Stock Code</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->codeSide ?>" name="sideCode" id="sideCode"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Name</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->Side_name ?>" name="sideName" id="sideName"></td>
                            </tr>
                            <tr>
                              <td>Info</td>
                              <td><input type="text" class="form-control" value="<?php echo $key->Info ?>" name="Description" id="Description"></td>
                            </tr>
                            <tr>
                              <td>Qty</td>
                              <td><input type="number" class="form-control" value='<?php echo $key->Qty ?>' name="sideQty" id="sideQty"></td>
                            </tr>
                            <tr>
                              <td>HPP</td>
                              <td><input type="number" class="form-control" value="<?php echo $key->Bought_price ?>" name="sideHPP" id="sideHPP"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Price</td>
                              <td><input type="number" class="form-control" value='<?php echo $key->Side_price ?>' name="sidePrice" id="sidePrice"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Minimum Stock</td>
                              <td><input type="number" class="form-control" value='<?php echo $key->Minimum_stock ?>' name="sideMin" id="sideMin"></td>
                            </tr>
                            <tr>
                              <td>Avaible</td>
                              <td>
                                <div class="radio">
                                  <label>
                                    <input type="radio" <?php echo ($key->Availability == "yes") ? "checked" : "" ; ?> name="menuVisible" checked id="menuVisible1" value="yes" >
                                    Yes
                                  </label>
                                  <label>
                                    <input type="radio" <?php echo ($key->Availability == "no") ? "checked" : "" ; ?> name="menuVisible" id="menuVisible2" value="no" >
                                    No
                                  </label>

                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Category</td>
                              <td>
                                <select id="sideCat" name="sideCat" class="form-control">
                                  <?php
                                  foreach ($menuSub as $key2) {
                                    ?>
                                    <option <?php echo ($key->categoryID == $key2->categoryID ) ? "selected" : "" ; ?> value="<?php echo $key2->categoryID ?>"><?php echo $key2->Category_name ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                            <?php
                          }

                          ?>

                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <?php
          }
          else{
            ?>
            <form method="POST" id="branchForm" action="<?php echo base_url(); ?>Sidestock/add_product">
              <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>" />
              <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Product Detail</h3>
                        <?php
                        if(isset($err)){
                          ?>
                          <h3 style="color : red"><?php echo $err ?></h3>
                          <?php
                        }
                        ?>
                      </div>
                      <div class="box-body">
                        <table id="branchTable" class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <td>Side Stock Code</td>
                              <td><input type="text" class="form-control" name="sideCode" id="sideCode"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Name</td>
                              <td><input type="text" class="form-control" name="sideName" id="sideName"></td>
                            </tr>
                            <tr>
                              <td>Info</td>
                              <td><input type="text" class="form-control" name="Description" id="Description"></td>
                            </tr>
                            <tr>
                              <td>Qty</td>
                              <td><input type="number" class="form-control" value='0' name="sideQty" id="sideQty"></td>
                            </tr>
                            <tr>
                              <td>HPP</td>
                              <td><input type="number" class="form-control" value='0' name="sideHPP" id="sideHPP"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Price</td>
                              <td><input type="number" class="form-control" value='0' name="sidePrice" id="sidePrice"></td>
                            </tr>
                            <tr>
                              <td>Side Stock Minimum Stock</td>
                              <td><input type="number" class="form-control" value='0' name="sideMin" id="sideMin"></td>
                            </tr>
                            <tr>
                              <td>Avaible</td>
                              <td>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="menuVisible" checked id="menuVisible1" value="yes" >
                                    Yes
                                  </label>
                                  <label>
                                    <input type="radio" name="menuVisible" id="menuVisible2" value="no" >
                                    No
                                  </label>

                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Category</td>
                              <td>
                                <select id="sideCat" name="sideCat" class="form-control">
                                  <?php
                                  foreach ($menuSub as $key) {
                                    ?>
                                    <option value="<?php echo $key->categoryID ?>"><?php echo $key->Category_name ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </td>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                        <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <?php
            }

            ?>
          </div>

          <?php include "includes/footer.php"; ?>
        </div>
        

</body>
</html>
