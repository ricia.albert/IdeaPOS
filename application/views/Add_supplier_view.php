<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Suppliers</title>
  <?php include "includes/include_js_css.php";?>
</head>
<body class="bd-supplier hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

  <!-- Header Navbar: style can be found in header.less -->
  <?php require("includes/header.php") ?>
  <body>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require("includes/navigation.php") ?>
    <div class="content-wrapper">
      <section class="content-header">
        Suppliers
      </section>
      <?php

      if (isset($req)) {
        ?>
        <form method="POST" action="<?php echo base_url();?>Supplier/update_member">
          <input type="hidden" name="guestID" id="guestID" value="<?php echo $req ?>">
          <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div align="center" class="box-header">
                    <h3 align="center" class="box-title">Supplier Registration</h3>
                    <?php

                    if (isset($err)) {
                      ?>
                      <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table id="userTable" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewUser as $key) {
                          ?>

                          <tr>
                            <td>Supplier Name</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->supplier_name; ?>" name="guestName" id="guestName"></td>
                          </tr>
                          <tr>
                            <td>Supplier Type</td>
                            <td>
                              <select name="guestType" id="guestType" class="form-control">
                                <option value="C" <?php echo ($key->Type == "C") ? "selected" : ""; ?> >Company</option>
                                <option value="P" <?php echo ($key->Type == "P") ? "selected" : ""; ?> >Personal</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td><input type="text" class="form-control" value="<?php echo $key->Address ?>" name="guestAddress" id="guestAddress"></td>
                          </tr>
                          <tr>
                            <td>Phone</td>
                            <td><input type="text" value="<?php echo $key->Phone ?>" name="guestPhone" id="guestPhone" class="form-control">
                            </tr>
                            <tr>
                              <td>Email</td>
                              <td><input type="email" class="form-control" value="<?php echo $key->CP_email ?>" name="guestEmail" id="guestEmail"></td>
                            </tr>
                            <tr>
                              <td>Info</td>
                              <td><textarea id="info" class="form-control" name="info"><?php echo $key->Info ?></textarea></td>
                            </tr>
                            <tr>
                              <td>Active ?</td>
                              <td><input type="checkbox" value="1" name="active" id="active" <?php echo ($key->active) ? "checked" : ""; ?> /></td>
                            </tr>
                            <tr>
                              <td>Is Blacklist ?</td>
                              <td><input type="checkbox" value="1" name="blacklist" id="blacklist" <?php echo ($key->isBlackList) ? "checked" : ""; ?> /></td>
                            </tr>
                          <?php
                        }
                        ?>

                      </thead>
                    </table>
                    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                  </div>
                  <?php
                }
                else{
                  ?>
                  <form method="POST" action="<?php echo base_url();?>Supplier/add_member">
                    <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
                    <section class="content">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div align="center" class="box-header">
                              <h3 align="center" class="box-title">Customer Registration</h3>
                              <?php

                              if (isset($err)) {
                                ?>
                                <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                                <?php
                              }
                              ?>
                            </div>

                            <div class="box-body">
                              <table id="userTable" class="table table-bordered table-hover">
                                <thead>
                                 <tr>
                                  <td>Supplier Name</td>
                                  <td><input type="text" class="form-control" name="guestName" id="guestName"></td>
                                </tr>
                                <tr>
                                  <td>Supplier Type</td>
                                  <td>
                                    <select name="guestType" id="guestType" class="form-control">
                                      <option value="C">Company</option>
                                      <option value="P">Personal</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Address</td>
                                  <td><input type="text" class="form-control" name="guestAddress" id="guestAddress"></td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td><input type="text" name="guestPhone" id="guestPhone" class="form-control">
                                  </tr>
                                  <tr>
                                    <td>Email</td>
                                    <td><input type="email" class="form-control" name="guestEmail" id="guestEmail"></td>
                                  </tr>
                                  <tr>
                                    <td>Info</td>
                                    <td><textarea id="info" class="form-control" name="info"></textarea></td>
                                  </tr>
                                  <tr>
                                    <td>Active ?</td>
                                    <td><input type="checkbox" value="1" name="active" id="active" /></td>
                                  </tr>
                                  <tr>
                                    <td>Is Blacklist ?</td>
                                    <td><input type="checkbox" value="1" name="blacklist" id="blacklist" /></td>
                                  </tr>
                                </thead>
                              </table>
                              <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                            </div>

                            <?php
                          }

                          ?>

                        </div>
                      </div>
                    </div>
                  </section>
                </form>

              </div>
              <?php include "includes/footer.php"; ?>
            </div>
            <script type="text/javascript" src="<?php echo base_url()?>dist/js/supplier.js"></script>
          </body>
          </html>
