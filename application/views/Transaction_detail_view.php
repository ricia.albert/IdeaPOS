<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Detail</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-trans hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Detail 
        </section>

        <form method="POST" id="productView" action="<?php echo base_url();?>Transaction_detail">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <?php
                    if ($req == "cabang") {
                      ?>
                      <h3 class="box-title">Branch Table</h3>
                      <?php
                    }
                    elseif ($req=="totalTransaksi") {
                      ?>
                      <h3 class="box-title">Transaksi Table</h3>
                      <?php
                    }
                    elseif ($req == "newMember") {
                      ?>
                      <h3 class="box-title">Member Table</h3>
                      <?php
                    }
                    elseif ($req == "sumTransaki") {
                      ?>
                      <h3 class="box-title">Transaksi Count</h3>
                      <?php
                    }
                    else{
                      ?>
                      <h3 class="box-title">Product Detail</h3>
                      <?php
                }//($param == "day") ? "selected" : "";
                ?>
                <!-- <div class="btn-default pull-right"><a href="<?php echo base_url(); ?>Csv_export?d=<?php echo $_GET['d'] ?>&time=<?php echo $_GET['time'] ?>"><input type="button" name="csvBttn" id="csvBttn" value="Export to Excel"></a></div> -->
              </div>
              <div class="box-body">
               <select name="brancOption" onchange="doit()" id="brancOption">
                <?php
                foreach ($view_branch as $key) {
                  ?>
                  <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                  <?php
                }
                ?>
              </select>&nbsp;
              <select name="stats" id="stats" onchange="doit()">
                <option value="A" <?php echo ($stats == "A") ? "selected" : ""; ?>>Accepted</option>
                <option value="P" <?php echo ($stats == "P") ? "selected" : ""; ?>>Pending</option>
                <option value="C" <?php echo ($stats == "C") ? "selected" : ""; ?>>Canceled</option>
              </select>
              <div class="pull-right">
                <input type="text" class="date" value="<?php echo $sd; ?>" readonly name="sd" id="startDt" placeholder="Start Date" />&nbsp; <strong>s/d</strong> &nbsp; 
                <input type="text" class="date" value="<?php echo $ed; ?>" readonly name="ed" id="endDt" placeholder="End Date" />
                <button type="button" id="btfilter" name="btfilter" onclick="doit()">Filter</button>
              </div>
              <div style="clear:both"></div>
              <center><h2>Total : <?php echo number_format($total, 2, ",", "."); ?></h2></center>
              <div style="clear:both"><br/></div>
                <table id="branchesTable" class="table table-bordered table-hover">
                  <thead>
                    <?php
                    if ($req=="cabang") {
                      ?>
                      <tr>
                        <td>Branch Code</td>
                        <td>Branch Name</td>
                        <td>Branch Address</td>
                        <td>Branch Phone</td>
                        <td>Branch Fax</td>
                      </tr>
                      <?php
                    }
                    elseif ($req=="totalTransaksi") {
                      ?>
                      <tr>
                        <!-- <td>Branch ID</td> -->
                        <th>Trans Code</th>
                        <th>Trans Date</th>
                        <th>Trans Settled</th>
                        <th class="text-center">Slot</th>
                        <th class="text-center">Status</th>
                        <th>Trans Total</th>
                        <th>Trans PPN</th>
                        <th>Discount</th>
                        <th>Voucher</th>
                        <th>Sub Total</th>
                      </tr>
                      <?php
                    }
                    elseif ($req=="newMember") {
                      ?>
                      <tr>
                        <td>Guest Name</td>
                        <td>Sex</td>
                        <td>Born</td>
                        <td>Address</td>
                        <td>Phone</td>
                        <td>Email</td>
                      </tr>
                      <?php
                    }
                    elseif ($req=="sumTransaki") {
                      ?>
                      <tr>
                        <td>POSID</td>
                        <td>Total</td>
                      </tr>
                      <?php
                    }
                    else{
                      ?>
                      <tr>
                        <td>Branch ID</td>
                        <td>Trans Code</td>
                        <td>Trans Date</td>
                        <td>Trans Settled</td>
                      </tr>
                      <?php
                    }

                    ?>
                  </thead> 
                  <tbody>
                    <?php
                    if ($req=="cabang") {
                      foreach ($value as $key) {
                        ?>
                        <tr>
                          <td><?php echo $key->POSID ?></td>
                          <td><?php echo $key->POSNm ?></td>
                          <td><?php echo $key->Address ?></td>
                          <td><?php echo $key->Telp ?></td>
                          <td><?php echo $key->Fax ?></td>
                        </tr>
                        <?php
                      }
                    }
                    elseif ($req=="totalTransaksi") {
                      $index = 0;
                      foreach ($value as $key) {
                      ?>
                          <tr>
                            <!-- <td><?php echo $key->PosID ?></td> -->
                            <td><a class="a_trans" data-pos="<?php echo $key->PosID ?>" data-id="<?php echo $key->transID ?>" href="#"><?php echo $key->transID ?></a></td>
                            <td><?php echo $key->transDate ?></td>
                            <td><?php echo ($key->settledDate == null) ? "-" : $key->settledDate; ?></td>
                            <td align="center"><?php echo $key->tableNm; ?></td>
                            <td align="center">
                              <?php 
                                switch ($key->transStatus)
                                {
                                  case "A" : echo "Accepted";break;
                                  case "C" : echo "Canceled";break;
                                  case "P" : echo "Pending";break;
                                }
                                ?>
                            </td>
                            <td><?php echo number_format($key->total,2,",",".") ?></td>
                            <td><?php echo number_format($key->ppn,2,",",".") ?></td>
                            <td><?php echo number_format($key->discount,2,",",".") ?></td>
                            <td><?php echo number_format($key->totalVoucher,2,",",".") ?></td>
                            <td><?php echo number_format($key->total + $key->ppn - $key->discount - $key->totalVoucher,2,",",".") ?></td>
                          </tr>
                          <?php
                      }
                    }
                    elseif ($req=="newMember") {
                      foreach ($value as $key) {
                        ?>
                        <tr>
                          <td><?php echo $key->Guest_name ?></td>
                          <td><?php echo $key->Sex ?></td>
                          <td><?php echo $key->Born ?></td>
                          <td><?php echo $key->Address ?></td>
                          <td><?php echo $key->Phone ?></td>
                          <td><?php echo $key->Email ?></td>
                        </tr>
                        <?php
                      }
                    }

                    elseif ($req=="sumTransaki") {
                      foreach ($value as $key) {
                        //var_dump($value);
                        ?>
                        <tr>
                          <td><?php echo $key->POSID ?></td>
                          <td><?php echo $key->total ?></td>
                        </tr>
                        <?php
                      }
                    }
                    else{
                      $index = 0;
                      foreach ($value as $key) {
                        $index++;
                        if ($index == 1) {
                          ?>
                          <tr>
                            <td><?php echo $key->POSID ?></td>
                            <td><?php echo $key->transID ?></td>
                            <td><?php echo $key->transDate ?></td>
                            <td><?php echo $key->settledDate ?></td>
                            
                          </tr>
                          <?php
                        }
                        else{
                          ?>
                          <tr>
                            <td></td>
                            <td><?php echo $key->transID ?></td>
                            <td><?php echo $key->transDate ?></td>
                            <td><?php echo $key->settledDate ?></td>
                          </tr>
                          <?php
                        }

                      }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>

  </div>


  <?php include "includes/footer.php"; ?>
</div>
<?php include "includes/detail_dialog.php"; ?>
<script type="text/javascript" src="<?php echo base_url();?>dist/js/trans_detail.js"></script>
</body>
</html>
