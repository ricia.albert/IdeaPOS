<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Stock Mutation</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-progress hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Stock Mutation
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Stock">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Progress Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                     <div class="pull-right">
                        <input type="text" class="date" readonly name="sd" value="<?php echo $sd; ?>" id="startDt" placeholder="Start Date" />&nbsp; <strong>s/d</strong> &nbsp; 
                        <input type="text" class="date" readonly name="ed" value="<?php echo $ed; ?>" id="endDt" placeholder="End Date" />
                        <button type="button" id="btfilter" name="btfilter" onclick="doit()">Filter</button>
                      </div>
                    <div style="clear:both"><br/></div>
                    <table id="branchesTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Ingridients Code</td>
                          <td>Ingridients Name</td>
                          <td width="105px">Current Qty</td>
                          <td width="105px">Stock In</td>
                          <td width="105px">Sold Out</td>
                          <td width="105px">Request Stock</td>
                          <td width="105px">Transfer In</td>
                          <td width="105px">Transfer Out</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ($value as $key) {
                        ?>
                          <tr>
                            <td><?php echo $key->codeIng; ?></td>
                            <td><?php echo $key->ingName; ?></td>
                            <td><?php echo number_format($key->qty,0,",",".")." ".$key->scalarNm; ?></td>
                            <td><?php echo number_format($key->inventory_in,0,",",".")." ".$key->scalarNm; ?></td>
                            <td><?php echo number_format($key->sales_stock,0,",",".")." ".$key->scalarNm; ?></td>
                            <td><?php echo number_format($key->request_in,0,",",".")." ".$key->scalarNm; ?></td>
                            <td><?php echo number_format($key->transfer_in,0,",",".")." ".$key->scalarNm; ?></td>
                            <td><?php echo number_format($key->transfer_out,0,",",".")." ".$key->scalarNm; ?></td>
                          </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </form>

        </div>
        <?php include "includes/footer.php"; ?>
      </div>
     
  <script type="text/javascript" src="<?php echo base_url();?>dist/js/stock.js"></script>

</body>
</html>
