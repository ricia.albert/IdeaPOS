<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | User</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-user hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <!-- Header Navbar: style can be found in header.less -->
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          User
        </section>
        <?php

        if (isset($req)) {
          ?>
          <form method="POST" action="<?php echo base_url();?>User/update_user">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div align="center" class="box-header">
                      <h3 align="center" class="box-title">User Registration</h3>
                      <?php

                      if (isset($err)) {
                        ?>
                        <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="userTable" class="table table-bordered table-hover">
                        <thead>
                          <?php
                          foreach ($viewUser as $key) {
                            ?>

                            <tr>
                              <td>User Name</td>
                              <td>
                                <input type="hidden" name="userID" id="userID" value="<?php echo $key->userID; ?>" />
                                <input type="text" class="form-control" name="username" id="username" value="<?php echo $key->username ?>">
                              </td>
                            </tr>
                            <tr>
                              <td>Global Username</td>
                              <td>
                                <input type="text" class="form-control" name="globalUser" id="globalUser" value="<?php echo $key->globaluser ?>">
                              </td>
                            </tr>
                            <tr>
                              <td>Name</td>
                              <td><input type="text" class="form-control" name="nameUser" id="nameUser" value="<?php echo $key->employee_name ?>"></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><input type="text" class="form-control" name="addressUser" id="addressUser" value="<?php echo $key->Address ?>"></td>
                            </tr>
                            <tr>
                              <td>Sex</td>
                              <td>  <div class="radio">
                                <label>
                                  <input type="radio" <?php echo ($key->Sex == 'L') ? 'checked' : ''; ?> name="gender" id="gender1" value="L" >
                                  Pria
                                </label> &nbsp;
                                <label>
                                  <input type="radio" <?php echo ($key->Sex == 'P') ? 'checked' : ''; ?> name="gender" id="gender2" value="P" >
                                  Wanita
                                </label>

                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Position</td> 
                            <td>  <div class="radio">
                              <label>
                                <input type="radio" <?php echo ($key->Position == 'Admin') ? 'checked' : ''; ?> name="position" id="position1" value="Admin" >
                                Admin
                              </label> &nbsp;
                              <label>
                                <input type="radio" <?php echo ($key->Position == 'Manager') ? 'checked' : ''; ?> name="position" id="position2" value="Manager" >
                                Manager
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Phone</td>
                          <td><input type="text" class="form-control" name="phoneUser" id="phoneUser" value="<?php echo $key->Phone ?>"></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td><input type="text" class="form-control" name="emailUser" id="emailUser" value="<?php echo $key->Email ?>"></td>
                        </tr>
                        <tr>
                          <td>User Authority</td>
                          <td>
                            <select class="form-control" name="userAuth" id="userAuth">
                              <option value="N" <?php echo ($viewAuth->userAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->userAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->userAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Branch Authority</td>
                          <td>
                            <select class="form-control" name="branchAuth" id="branchAuth">
                              <option value="N" <?php echo ($viewAuth->branchAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->branchAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->branchAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Category Authority</td>
                          <td>
                            <select class="form-control" name="categoryAuth" id="categoryAuth">
                              <option value="N" <?php echo ($viewAuth->categoryAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->categoryAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->categoryAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Stock Authority</td>
                          <td>
                            <select class="form-control" name="stockAuth" id="stockAuth">
                              <option value="N" <?php echo ($viewAuth->stockAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->stockAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->stockAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Product Authority</td>
                          <td>
                            <select class="form-control" name="productAuth" id="productAuth">
                              <option value="N" <?php echo ($viewAuth->productAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->productAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->productAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Transaction Authority</td>
                          <td>
                            <select class="form-control" name="transAuth" id="transAuth">
                              <option value="N" <?php echo ($viewAuth->transactionAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->transactionAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->transactionAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Setting Authority</td>
                          <td>
                            <select class="form-control" name="settingAuth" id="settingAuth">
                              <option value="N" <?php echo ($viewAuth->settingAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->settingAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->settingAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Customer Authority</td>
                          <td>
                            <select class="form-control" name="customerAuth" id="customerAuth">
                              <option value="N" <?php echo ($viewAuth->customerAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->customerAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->customerAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Report Authority</td>
                          <td>
                            <select class="form-control" name="reportAuth" id="reportAuth">
                              <option value="N" <?php echo ($viewAuth->reportAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->reportAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->reportAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Request Authority</td>
                          <td>
                            <select class="form-control" name="requestAuth" id="requestAuth">
                              <option value="N" <?php echo ($viewAuth->requestAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->requestAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->requestAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Approval Authority</td>
                          <td>
                            <select class="form-control" name="approvalAuth" id="approvalAuth">
                              <option value="N" <?php echo ($viewAuth->approvalAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo ($viewAuth->approvalAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo ($viewAuth->approvalAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>

                    </thead>
                  </table>

                <div class="nav-tabs-custom">

                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <?php foreach ($branch as $b) : ?>
                      <li role="presentation" >
                        <input type="checkbox" value="1" class="check" data-target="<?php echo $b->POSID; ?>" id="<?php echo $b->POSID; ?>" <?php echo (in_array($b->POSID, $pos)) ? "checked" : ""; ?> name="<?php echo $b->POSID; ?>" />
                        <a href="#tab-<?php echo $b->POSID; ?>" role="tab" data-toggle="tab">
                          <label>
                            <?php echo $b->POSNm; ?>
                          </label>
                        </a>
                      </li>
                    <?php endforeach; ?>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <?php foreach ($branch as $b) : ?>
                      <div role="tabpanel" class="tab-pane" id="tab-<?php echo $b->POSID; ?>">
                        <table class="table table-bordered table-hover">
                        <tr>
                          <td>Category Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="categoryAuth_<?php echo $b->POSID; ?>" id="categoryAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->categoryAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->categoryAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->categoryAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Stock Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="stockAuth_<?php echo $b->POSID; ?>" id="stockAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->stockAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->stockAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->stockAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Product Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="productAuth_<?php echo $b->POSID; ?>" id="productAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->productAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->productAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->productAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Transaction Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="transAuth_<?php echo $b->POSID; ?>" id="transAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->transactionAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->transactionAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->transactionAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Setting Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="settingAuth_<?php echo $b->POSID; ?>" id="settingAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->settingAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->settingAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->settingAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Customer Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="customerAuth_<?php echo $b->POSID; ?>" id="customerAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->customerAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->customerAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->customerAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Report Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="reportAuth_<?php echo $b->POSID; ?>" id="reportAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->reportAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->reportAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->reportAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Request Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="requestAuth_<?php echo $b->POSID; ?>" id="requestAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->requestAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->requestAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->requestAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>Approval Authority</td>
                          <td>
                            <select class="form-control auth <?php echo $b->POSID; ?>" name="approvalAuth_<?php echo $b->POSID; ?>" id="approvalAuth_<?php echo $b->POSID; ?>">
                              <option value="N" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->approvalAuth == 'N') ? 'selected' : ''; ?>>None</option>
                              <option value="R" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->approvalAuth == 'R') ? 'selected' : ''; ?>>Read Only</option>
                              <option value="W" <?php echo (isset($posAuth[$b->POSID]) && $posAuth[$b->POSID]->approvalAuth == 'W') ? 'selected' : ''; ?>>Read / Write</option>
                            </select>
                          </td>
                        </tr>
                        </table>
                      </div>
                    <?php endforeach; ?>
                  </div>

              </div>
                <br/>
                  <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                </div>
                <?php
              }
              else{
                ?>
                <form method="POST" action="<?php echo base_url();?>User/add_user">
                  <section class="content">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="box">
                          <div align="center" class="box-header">
                            <h3 align="center" class="box-title">User Registration</h3>
                            <?php

                            if (isset($err)) {
                              ?>
                              <h3 align="center" style="color:red"><?php echo $err; ?></h3>

                              <?php
                            }
                            ?>
                          </div>

                          <div class="box-body">
                            <table id="userTable" class="table table-bordered table-hover">
                              <thead>
                                <!-- <tr>
                                  <td>ID</td>
                                  <td><input type="text" readonly="" class="form-control" name="userID" id="userID"></td>
                                </tr> -->
                                <tr>
                                  <td width="40%">User Name</td>
                                  <td><input type="text" class="form-control" name="username" id="username"></td>
                                </tr>
                                <tr>
                                  <td>Global Username</td>
                                  <td>
                                    <input type="text" class="form-control" name="globalUser" id="globalUser">
                                  </td>
                                </tr>
                                <tr>
                                  <td>Password</td>
                                  <td><input type="Password" class="form-control" name="password" id="password"></td>
                                </tr>
                                <tr>
                                  <td>Name</td>
                                  <td><input type="text" class="form-control" name="nameUser" id="nameUser"></td>
                                </tr>
                                <tr>
                                  <td>Address</td>
                                  <td><input type="text" class="form-control" name="addressUser" id="addressUser"></td>
                                </tr>
                                <tr>
                                  <td>Sex</td>
                                  <td>  <div class="radio">
                                    <label>
                                      <input type="radio" name="gender" id="gender1" value="L" checked>
                                      Pria
                                    </label> &nbsp;
                                    <label>
                                      <input type="radio" name="gender" id="gender2" value="P" checked>
                                      Wanita
                                    </label>

                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>Position</td> 
                                <td>  <div class="radio">
                                  <label>
                                    <input type="radio" name="position" id="position1" value="Admin" checked>
                                    Admin
                                  </label> &nbsp;
                                  <label>
                                    <input type="radio" name="position" id="position2" value="Manager" checked>
                                    Manager
                                  </label>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>Phone</td>
                              <td><input type="text" class="form-control" name="phoneUser" id="phoneUser"></td>
                            </tr>
                            <tr>
                              <td>Email</td>
                              <td><input type="text" class="form-control" name="emailUser" id="emailUser"></td>
                            </tr>
                            <tr>
                                <td>User Authority</td>
                                <td>
                                  <select class="form-control" name="userAuth" id="userAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Branch Authority</td>
                                <td>
                                  <select class="form-control" name="branchAuth" id="branchAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Category Authority</td>
                                <td>
                                  <select class="form-control" name="categoryAuth" id="categoryAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Stock Authority</td>
                                <td>
                                  <select class="form-control" name="stockAuth" id="stockAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Product Authority</td>
                                <td>
                                  <select class="form-control" name="productAuth" id="productAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Transaction Authority</td>
                                <td>
                                  <select class="form-control" name="transAuth" id="transAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Setting Authority</td>
                                <td>
                                  <select class="form-control" name="settingAuth" id="settingAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Customer Authority</td>
                                <td>
                                  <select class="form-control" name="customerAuth" id="customerAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Report Authority</td>
                                <td>
                                  <select class="form-control" name="reportAuth" id="reportAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Request Authority</td>
                                <td>
                                  <select class="form-control" name="requestAuth" id="requestAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Approval Authority</td>
                                <td>
                                  <select class="form-control" name="approvalAuth" id="approvalAuth">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                          </thead>
                        </table>
                        
                        <!-- Nav tabs -->
                        <div class="nav-tabs-custom">
                          <ul class="nav nav-tabs">
                            <?php foreach ($branch as $b) : ?>
                            <li>
                              <input type="checkbox" value="1" class="check" data-target="<?php echo $b->POSID; ?>" id="<?php echo $b->POSID; ?>" name="<?php echo $b->POSID; ?>" />
                              <a href="#tab-<?php echo $b->POSID; ?>" class="tab" data-toggle="tab">
                                <label>
                                  <?php echo $b->POSNm; ?>
                                </label>
                              </a>
                            </li>
                          <?php endforeach; ?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                          <?php foreach ($branch as $b) : ?>
                          <div role="tabpanel" class="tab-pane" id="tab-<?php echo $b->POSID; ?>">
                            <table class="table table-bordered table-hover">
                              <tr>
                                <td>Category Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="categoryAuth_<?php echo $b->POSID; ?>" id="categoryAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Stock Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="stockAuth_<?php echo $b->POSID; ?>" id="stockAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Product Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="productAuth_<?php echo $b->POSID; ?>" id="productAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Transaction Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="transAuth_<?php echo $b->POSID; ?>" id="transAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Setting Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="settingAuth_<?php echo $b->POSID; ?>" id="settingAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Customer Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="customerAuth_<?php echo $b->POSID; ?>" id="customerAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Report Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="reportAuth_<?php echo $b->POSID; ?>" id="reportAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Request Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="requestAuth_<?php echo $b->POSID; ?>" id="requestAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td>Approval Authority</td>
                                <td>
                                  <select class="form-control auth <?php echo $b->POSID; ?>" name="approvalAuth_<?php echo $b->POSID; ?>" id="approvalAuth_<?php echo $b->POSID; ?>">
                                    <option value="N">None</option>
                                    <option value="R">Read Only</option>
                                    <option value="W">Read / Write</option>
                                  </select>
                                </td>
                              </tr>
                            </table>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                        <br/>
                        <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn"></input>
                      </div>

                      <?php
                    }

                    ?>

                  </div>
                </div>
              </div>
            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>
</body>
</html>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/add_user.js"></script>
