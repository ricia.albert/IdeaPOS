<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Product Detail Price</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="<?php
                if (strpos($menu, "MN") === 0) {
                  echo "bd-product";
                } else if (strpos($menu, "PK") === 0) {
                  echo "bd-package";
                } else if (strpos($menu, "SS") === 0) {
                  echo "bd-sidestock";
                } 
              ?> hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Product Detail Price
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>taksonomi">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title"><?php echo $name; ?> - Detail Price</h3>
                  </div>
                  <div class="box-body">
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td width="450px">Info</td>
                          <td>Price</td>
                          <td>User</td>
                          <td width="160px"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->Info ?></td>
                            <td><?php echo number_format($value->Price) ?></td>
                            <td><?php echo $value->Username ?></td>
                            <td><a href="<?php base_url(); ?>Product_detail_price/update?menu=<?php echo $value->menuPriceID ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                              <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->menuPriceID ?>')" value="Delete"></a></td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <a href="<?php base_url(); ?>Product_detail_price/add_detail_view"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                </div>
                
              </div>


            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>
      
  <script type="text/javascript" src="<?php echo base_url();?>dist/js/product_price.js"></script>

</body>
</html>
