<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Package</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-package hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Package 
        </section>

        <?php

        if (isset($req)) {
         $price="";
         $rowCount=0;
         $indexArray=array();
         $arrayIndex = array();
         ?>
         <?php if ($copy != 1) { ?>
         <form method="POST" action="<?php echo base_url();?>Package/update_product">
          <?php } else { ?>
          <form method="POST" action="<?php echo base_url(); ?>Package/add_package">
          <?php } ?>
          <input type="hidden" name="req" id="req" value="<?php echo $req ?>">
          <input type="hidden" name="pos" id="pos" value="<?php echo $pos ?>">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Package Detail
                        <?php if (isset($posNm)) : ?>
                          - Copy to <strong><?php echo $posNm; ?></strong>
                        <?php endif; ?>
                    </h3>
                    <?php
                    if(isset($err)){
                      ?>
                      <h3 style="color : red"><?php echo $err ?></h3>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="box-body">
                    <table id="detailPackage" class="table table-bordered table-hover">
                      <thead>
                        <?php
                        foreach ($viewProduct as $key) {
                          ?>
                          <tr>
                           <td>Package Code</td>
                           <td><input type="text" class="form-control" value="<?php echo $key->Packages_code ?>" name="packageCode" id="packageCode"></td>
                         </tr>
                         <tr>
                           <td>Package Name</td>
                           <td><input type="text" class="form-control" value="<?php echo $key->Packages_name ?>" name="nameMenu" id="nameMenu"></td>
                         </tr>
                         <tr>
                           <td>Info</td>
                           <td><input type="text" class="form-control" value="<?php echo $key->Info ?>" name="Description" id="Description"></td>
                         </tr>
                         <tr>
                           <td>Combine Price</td>
                           <td><input type="number" class="form-control" value="<?php echo $key->Total_price ?>" readonly name="totalPrice" value="0" id="totalPrice"</td>
                         </tr>
                         <tr>
                           <td>Price</td>
                           <td>
                             <input type="number" class="form-control" value="<?php echo $key->Packages_price ?>" name="menuPrice" value="0" id="menuPrice">
                           </td>
                         </tr>
                         <tr>
                           <td>Avaible</td>
                           <td>
                             <div class="radio">
                               <label>
                                 <input type="radio" <?php echo ($key->Availability == "1") ? "checked" : "" ; ?> name="menuVisible" checked id="menuVisible1" value="1" >
                                 Yes
                               </label>
                               <label>
                                 <input type="radio" <?php echo ($key->Availability == "0") ? "checked" : "" ; ?> name="menuVisible" id="menuVisible2" value="0" >
                                 No
                               </label>

                             </div>
                           </td>
                         </tr>
                         <tr>
                          <?php
                        }
                        ?>

                      </thead>
                      <tbody>
                      </tbody>
                    </table>  
                  </div>
                </div>
              </div>
              <!-- detail menu -->
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Menu</h3>
                  </div>
                  <div class="box-body">
                    <table id="menuDetail" class="table table-bordered table-hover">
                     <thead>
                       <tr>
                        <td>Menu Code</td>
                        <td>Menu Name</td>
                        <td>Menu Price</td>
                      </tr>   
                    </thead>
                    <tbody>
                      <?php
                      foreach ($menuSub as $key) {
                        ?>
                        <tr>
                          <td><?php echo $key->menuID ?></td>
                          <td><?php echo $key->menuNm ?></td>
                          <td><?php echo $key->price ?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- accepted Menu -->
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Choosen</h3>
                </div>
                <div class="box-body">
                  <table id="acceptedMenu" class="table table-bordered table-hover">
                    <thead>
                     <tr>
                      <td>Menu Code</td>
                      <td>Menu Name</td>
                      <td>Qty</td>
                      <td></td>
                    </tr>   
                  </thead>
                  <tbody>
                    <?php
                    foreach ($menuChoosen as $key) {
                      $rowCount++;
						//echo $key->menuID;
                      array_push($indexArray, $key->menuID);
                      $arrayIndex[]+= $rowCount;
                      ?>
                      <tr>
                       <td><input type="text" class="form-control" readonly name="addCode<?php echo $rowCount ?>" id="addCode<?php echo $rowCount ?>" value="<?php echo $key->menuID ?>" ></td>

                       <td><input type="text" class="form-control" readonly name="addName<?php echo $rowCount ?>" id="addName<?php echo $rowCount ?>" value="<?php echo $key->menuNm ?>" ></td>

                       <td><input type="number" class="form-control" name="addQty<?php echo $rowCount ?>" id="addQty<?php echo $rowCount ?>" onchange="recalculate()" value="<?php echo $key->qty ?>" > <input type="hidden" class="form-control" name="addPrice<?php echo $rowCount ?>" id="addPrice<?php echo $rowCount ?>" value="<?php echo $key->price ?>" ></td>
                       <td><input type="button" value = "Delete" class="form-control" onClick="Javacsript:deleteRow(this)"></td>
                     </tr>
                     <?php
                   }
                   ?>
                   <div style="display: none" name="indexArray" id="indexArray" ><?php echo json_encode($indexArray) ?></div>
                   <div style="display: none" name="arrayIndex" id="arrayIndex" ><?php echo json_encode($arrayIndex) ?></div>
                   <input type="hidden" name="indexInsert" value="<?php echo $rowCount ?>" id="indexInsert">
                 </tbody>
               </table>
             </div>
           </div>
         </div>
       </div>
       <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
     </section>
   </form>
   <?php
 }
 else{
  ?>
  <form method="POST" id="branchForm" action="<?php echo base_url(); ?>Package/add_package">
    <section class="content">
      <input type="hidden" name="indexInsert" value="0" id="indexInsert">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Package Detail</h3>
              <?php
              if(isset($err)){
                ?>
                <h3 style="color : red"><?php echo $err ?></h3>
                <?php
              }
              ?>
            </div>
            <div class="box-body">
              <input type="hidden" value="<?php echo $pos; ?>" name="pos" id="pos" />
              <table id="detailPackage" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <td>Package Code</td>
                    <td><input type="text" class="form-control" name="packageCode" id="packageCode"></td>
                  </tr>
                  <tr>
                    <td>Package Name</td>
                    <td><input type="text" class="form-control" name="nameMenu" id="nameMenu"></td>
                  </tr>
                  <tr>
                    <td>Info</td>
                    <td><input type="text" class="form-control" name="Description" id="Description"></td>
                  </tr>
                  <tr>
                    <td>Combine Price</td>
                    <td><input type="number" class="form-control" readonly name="totalPrice" value="0" id="totalPrice"></td>
                  </tr>
                  <tr>
                    <td>Price</td>
                    <td>
                      <input type="number" class="form-control" name="menuPrice" value="0" id="menuPrice">
                    </td>
                  </tr>
                  <tr>
                    <td>Avaible</td>
                    <td>
                      <div class="radio">
                        <label>
                          <input type="radio" <?php //echo ($key->Availability == "yes") ? "checked" : "" ; ?> name="menuVisible" checked id="menuVisible1" value="1" >
                          Yes
                        </label>
                        <label>
                          <input type="radio" <?php //echo ($key->Availability == "no") ? "checked" : "" ; ?> name="menuVisible" id="menuVisible2" value="0" >
                          No
                        </label>

                      </div>
                    </td>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>  
              </div>
            </div>
          </div>
          <!-- detail menu -->
          <div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Menu</h3>
              </div>
              <div class="box-body">
                <table id="menuDetail" class="table table-bordered table-hover">
                	<thead>
                   <tr>
                    <td>Menu Code</td>
                    <td>Menu Name</td>
                    <td>Menu Price</td>
                  </tr>   
                </thead>
                <tbody>
                  <?php
                  foreach ($menuSub as $key) {
                    ?>
                    <tr>
                      <td><?php echo $key->menuID ?></td>
                      <td><?php echo $key->menuNm ?></td>
                      <td><?php echo $key->price ?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- accepted Menu -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Choosen</h3>
            </div>
            <div class="box-body">
              <table id="acceptedMenu" class="table table-bordered table-hover">
                <thead>
                 <tr>
                  <td>Menu Code</td>
                  <td>Menu Name</td>
                  <td>Qty</td>
                  <td></td>
                </tr>   
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
  </section>
</form>
<?php
}

?>

</div>

<?php include "includes/footer.php"; ?>
</div>

<script type="text/javascript" src="<?php echo base_url();?>dist/js/add_package.js"></script>
</body>
</html>