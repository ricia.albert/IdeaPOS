<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Side Stock</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-sidestock hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Side Stock
        </section>

        <form id="productView" method="POST" action="<?php echo base_url() ?>Sidestock">
          <input type="hidden" name="action" id="action">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Side Stock Table</h3>
                  </div>
                  <div class="box-body">
                    <select name="brancOption" onchange="doit()" id="brancOption">
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <table id="ProductTable" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Side Stock Code</td>
                          <td>Side Stock Name</td>
                          <td>Info</td>
                          <td>Qty</td>
                          <td>HPP</td>
                          <td>Side Price</td>
                          <td>Minimum Stock</td>
                          <td>Avaible</td>
                          <?php if ($this->session->userdata("stockAuth") == AUTH_WRITE) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 0;
                        foreach ($view_product as $key =>$value) {
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->SideStockID ?></td>
                            <td><?php echo $value->Side_name ?></td>
                            <td><?php echo $value->Info ?></td>
                            <td><?php echo $value->Qty ?></td>
                            <td><?php echo number_format($value->Bought_price) ?></td>
                            <td><?php echo number_format($value->Side_price) ?></td>
                            <td><?php echo $value->Minimum_stock ?></td>
                            <td><?php echo $value->Availability ?></td>
                            <?php if ($this->session->userdata("stockAuth") == AUTH_WRITE) : ?>
                              <td>
                                <a href="#"><input type="button" name="copyButton" id="copyButton" onclick="copy('<?php echo $value->codeSide ?>','<?php echo $value->SideStockID ?>','<?php echo $value->PosID; ?>','<?php echo $value->Side_name ?>')" value="Copy"></a>
                                <a href="<?php base_url(); ?>Product_detail_price?menu=<?php echo $value->SideStockID ?>&pos=<?php echo $value->PosID; ?>&name=<?php echo $value->Side_name; ?>"><input type="button" name="priceBttn" id="priceBttn" value="Special Price" /></a>
                                <a href="<?php echo base_url(); ?>Sidestock/update?menu=<?php echo $value->SideStockID ?>&pos=<?php echo $value->PosID; ?>"><input type="button" name="updateBttn" id="updateBttn" value="Update"></a>
                                <a href="#"><input type="button" name="deleteButton" id="deleteButton" onclick="confirmation('<?php echo $value->SideStockID ?>','<?php echo $value->PosID; ?>')" value="Delete"></a>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php if ($this->session->userdata("stockAuth") == AUTH_WRITE) : ?>
                        <a onclick="add()"><input type="button" name="submitBttn" value="Add New" id="submitBttn" class="form-control"></a>
                      <?php endif; ?>
                    </div>
                  </div>
                  
                </div>
                
              </div>

            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
        <?php include "includes/product_copy_dialog.php"; ?>
      </div>

  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/sidestock.js"></script>

</body>
</html>