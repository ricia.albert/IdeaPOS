<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Branches</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-branch hold-transition skin-blue sidebar-mini">
<!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
<div class="wrapper">

  <?php require("includes/header.php") ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php require("includes/navigation.php") ?>
  <div class="content-wrapper">
    <section class="content-header">
      Branch
    </section>

    <form method="POST" action="">
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Branch Table</h3>
              </div>
              <div class="box-body">
                <a href="#dialog-copy" data-toggle="modal"><button type="button" class="btn btn-warning">Copy Database</button></a>
                <table id="branchTable" class="table table-bordered table-hover" style="margin-top:20px">
                  <thead>
                    <tr>
                      <td>Branch Code</td>
                      <td>Branch Name</td>
                      <td>Address</td>
                      <td>Phone</td>
                      <td>Franchise ?</td>
                      <td>Free Stock ?</td>
                      <td>Storage ?</td>
                      <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                        <td></td>
                      <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
<?php
                    foreach ($view_branch as $key =>$value) {
                      //var_dump($value)
?>
                    <tr>
                      <td><?php echo $value->POSID ?></td>
                      <td><?php echo $value->POSNm ?></td>
                      <td><?php echo $value->Address ?></td>
                      <td><?php echo $value->Telp ?></td>
                      <td><?php echo ($value->isFranchise == 1) ? "Yes" : "No"; ?></td>
                      <td><?php echo ($value->isFree == 1) ? "Yes" : "No"; ?></td>
                      <td><?php echo ($value->isGudang == 1) ? "Yes" : "No"; ?></td>
                      <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                        <td width="150px">
                          <a href="<?php echo base_url(); ?>Branch/update?d=<?php echo $value->POSID ?>"><input type="button" name="updtButton" id="updtButton" value="Update" ></a>
                          <a href="#"><input type="button" name="dltButton" id="dltButton" onclick="confirmation('<?php echo $value->POSID ?>')" value="Delete"></a>
                          <a href="<?php echo base_url(); ?>Branch/group?d=<?php echo $value->POSID ?>"><input type="button" name="grpButton" id="grpButton" value="Group Trf"></a>
                          <a href="#"><input type="button" name="resetButton" id="resetButton" onclick="confirmationReset('<?php echo $value->POSID ?>')" value="Reset"></a>
                        </td>
                      <?php endif; ?>
                    </tr>
<?php
                    }
?>
                   </tbody>
                </table>
                <?php if ($this->session->userdata("branchAuth") == AUTH_WRITE) : ?>
                  <a href="<?php echo base_url(); ?>Branch/add_branch_view"><input type="button" class="form-control" name="addBttn" id="addBttn" value="Add New"></a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>

  </div>
  <?php include "includes/footer.php"; ?>
</div>
<?php include "includes/copy_dialog.php"; ?>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/branch.js"></script>
</body>
</html>
