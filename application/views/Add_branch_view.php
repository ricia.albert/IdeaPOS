<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Branch</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-branch hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">
    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Branch 
        </section>
        <?php

        if (isset($req)) {
          ?>
          <form method="POST" action="<?php echo base_url();?>Branch/update_branch">
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Branch Table</h3>
                      <?php
                      if(isset($err)){
                        ?>
                        <h3 style="color : red"><?php echo $err ?></h3>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="box-body">
                      <table id="branchTable" class="table table-bordered table-hover">
                        <thead>
                          <?php

                          foreach ($viewBranch as $key) {
                    //var_dump($viewBranch)
                            ?>
                            <tr>
                              <td>Branch Code</td>
                              <td><input type="text" class="form-control" readonly name="codeBranch" id="codeBranch" value="<?php echo $key->POSID ?>"></td>
                            </tr>
                            <tr>
                              <td>Branch Name</td>
                              <td><input type="text" class="form-control" name="nameBranch" id="nameBranch" value="<?php echo $key->POSNm ?>"></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><input type="text" class="form-control" name="addressBranch" id="addressBranch" value="<?php echo $key->Address ?>"></td>
                            </tr>
                            <tr>
                              <td>latitude</td>
                              <td><input type="text" class="form-control" name="latitude" id="latitude" value="<?php echo $key->latitude ?>"></td>
                            </tr>
                            <tr>
                              <td>longitude</td>
                              <td><input type="text" class="form-control" name="longitude" id="longitude" value="<?php echo $key->longitude ?>"></td>
                            </tr>
                            <tr>
                              <td>Phone</td>
                              <td><input type="text" class="form-control" name="phoneBranch" id="phoneBranch" value="<?php echo $key->Telp ?>"></td>
                            </tr>
                            <tr>
                              <td>Fax</td>
                              <td><input type="text" class="form-control" name="faxBranch" id="faxBranch" value="<?php echo $key->Fax ?>"></td>
                            </tr>
                            <tr>
                              <td>Is Franchise ?</td>
                              <td><input type="checkbox" value="1" name="franchise" id="franchise" <?php echo ($key->isFranchise == 1) ? "checked" : ""; ?> /></td>
                            </tr>
                            <tr>
                              <td>Free Stock ?</td>
                              <td><input type="checkbox" value="1" name="freestock" id="freestock" <?php echo ($key->isFree == 1) ? "checked" : ""; ?> /></td>
                            </tr>
                            <tr>
                              <td>Is Storage ?</td>
                              <td><input type="checkbox" value="1" name="storage" id="storage" <?php echo ($key->isGudang == 1) ? "checked" : ""; ?> /></td>
                            </tr>
                            <?php
                          }
                          ?>

                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <?php
          }
          else{
            ?>
            <form method="POST" id="branchForm" action="<?php echo base_url(); ?>Branch/add_branch">
              <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Branch Table</h3>
                        <?php
                        if(isset($err)){
                          ?>
                          <h3 style="color : red"><?php echo $err ?></h3>
                          <?php
                        }
                        ?>
                      </div>
                      <div class="box-body">
                        <table id="branchTable" class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <td>Branch Code</td>
                              <td><input type="text" class="form-control" name="codeBranch" id="codeBranch"></td>
                            </tr>
                            <tr>
                              <td>Branch Name</td>
                              <td><input type="text" class="form-control" name="nameBranch" id="nameBranch"></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><input type="text" class="form-control" name="addressBranch" id="addressBranch"></td>
                            </tr>
                            <tr>
                              <td>latitude</td>
                              <td><input type="text" class="form-control" name="latitude" id="latitude"></td>
                            </tr>
                            <tr>
                              <td>longitude</td>
                              <td><input type="text" class="form-control" name="longitude" id="longitude"></td>
                            </tr>
                            <tr>
                              <td>Phone</td>
                              <td><input type="text" class="form-control" name="phoneBranch" id="phoneBranch"></td>
                            </tr>
                            <tr>
                              <td>Fax</td>
                              <td><input type="text" class="form-control" name="faxBranch" id="faxBranch"></td>
                            </tr>
                            <tr>
                              <td>Is Franchise ?</td>
                              <td><input type="checkbox" value="1" name="franchise" id="franchise" /></td>
                            </tr>
                            <tr>
                              <td>Free Stock ?</td>
                              <td><input type="checkbox" value="1" name="freestock" id="freestock" /></td>
                            </tr>
                            <tr>
                              <td>Is Storage ?</td>
                              <td><input type="checkbox" value="1" name="storage" id="storage" /></td>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                        <input type="submit" class="form-control" name="sbmtBttn" id="sbmtBttn">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <?php
            }

            ?>
          </div>
        </div>
      </body>
      </html>
