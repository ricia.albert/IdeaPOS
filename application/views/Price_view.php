<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME; ?> | Approval Price</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-price hold-transition skin-blue sidebar-mini">
  <!--<div id="jsonValue"  style="display: none"><?php echo $monthly_trans; ?></div>-->
  <div class="wrapper">

    <?php require("includes/header.php") ?>
    <body>
      <!-- Left side column. contains the logo and sidebar -->
      <?php require("includes/navigation.php") ?>
      <div class="content-wrapper">
        <section class="content-header">
          Approval Price
        </section>

        <form id="approvalView" method="POST" action="<?php echo base_url() ?>Approval/Price">
          <input type="hidden" name="action" id="action">
          <input type="hidden" name="b" id="b">
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Approval Price</h3>
                  </div>
                  <div class="box-body">

                    <select name="brancOption" onchange="doit()" id="brancOption" style="margin:10px 0px">
                      <option value="" <?php echo ($b == "") ? "selected" : ""; ?>>All Branches</option>
                      <?php
                      foreach ($view_branch as $key) {
                        ?>
                        <option <?php echo ($b == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
                        <?php
                      }
                      ?>
                    </select>
                    <select name="stateOption" onchange="doit()" id="stateOption">
                      <option value="N" <?php echo ($temp == "N") ? "selected" : ""; ?>>New</option>
                      <option value="A" <?php echo ($temp == "A") ? "selected" : ""; ?>>Approved</option>
                      <option value="R" <?php echo ($temp == "R") ? "selected" : ""; ?>>Rejected</option>
                      <option value="E" <?php echo ($temp == "E") ? "selected" : ""; ?>>Expired</option>
                    </select>
                    <div class="pull-right">
                      There are <strong id="spdata"><?php echo count($view_approval); ?></strong> data on display.
                    </div>
                    <table id="ProductTable" class="table table-bordered table-hover" style="margin-top:10px">
                      <thead>
                        <tr>
                          <th width="100px">Approval ID</th>
                          <th width="175px">Request Date</th>
                          <th width="185px">From</th>
                          <th width="185px">Trans ID</th>
                          <th width="280px">Message</th>
                          <th width="110px">Status</th>
                          <?php if ($session == AUTH_WRITE) : ?>
                            <th></th>
                          <?php endif; ?>
                        </tr>
                      </thead>
                      <tbody id="table-approval">
                        <?php
                        $i = 0;
                        foreach ($view_approval as $key =>$value) {
                      //var_dump($value)
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $value->approvalID; ?></td>
                            <td><?php echo $value->requestDt; ?></td>
                            <td><?php echo $value->FromPOS; ?></td>
                            <td><?php echo $value->transID; ?></td>
                            <td><?php echo $value->message; ?></td>
                            <td><?php echo $value->Stats; ?></td>
                            <?php if ($session == AUTH_WRITE) : ?>
                              <td align="center">
                                <button type="button" data-id="<?php echo $value->approvalID; ?>" class="btn btn-primary btnDetail">
                                  <span class="fa fa-envelope-open"></span> &nbsp; Detail
                                </button>
                              </td>
                            <?php endif; ?>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>
              </div>

            </section>
          </form>

        </div>

        <?php include "includes/footer.php"; ?>
      </div>
    <?php include "includes/price_dialog.php"; ?>
  <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/price.js"></script>

</body>
</html>