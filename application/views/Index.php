<?php
$tempDate=array();
$tempValue=array();
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo APP_NAME; ?> | Dashboard</title>
  <?php include "includes/include_js_css.php"; ?>
</head>
<body class="bd-dashboard hold-transition skin-blue sidebar-mini">
  <?php include "includes/hidden.php"; ?>
  <div class="wrapper">
    <!-- Header Navbar: style can be found in header.less -->

    <?php require("includes/header.php"); ?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php require("includes/navigation.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
        <!-- <div class="pull-right">
          
        </div> -->
        <form id="productView" action="<?php echo base_url(); ?>home/index" method="GET">
          <input type="hidden" name="action" id="action">
          <select name="brancOption" onchange="doit()" id="brancOption" style="margin:10px 0px">
            <option value="">All Branches</option>
            <?php
            foreach ($view_branch as $key) {
              ?>
              <option <?php echo ($temp == $key->POSID) ? "selected" : ""; ?> value="<?php echo $key->POSID ?>"><?php echo $key->POSNm ?></option>
              <?php
            }
            ?>
          </select>
          <div class="pull-right" style="margin-top:10px">
            <input type="text" class="date" value="<?php echo $sd; ?>" readonly name="sd" id="startDt" placeholder="Start Date" />&nbsp; <strong>s/d</strong> &nbsp; 
            <input type="text" class="date" value="<?php echo $ed; ?>" readonly name="ed" id="endDt" placeholder="End Date" />
            <div class="btn-group">
              <div class="form-group">
                <select name="t" id="t" style="margin-top:10px">
                  <option <?php echo ($param == "day") ? "selected" : ""; ?> value="day">Day</option>
                  <option <?php echo ($param == "month") ? "selected" : ""; ?> value="month">Month</option>
                  <option <?php echo ($param == "year") ? "selected" : ""; ?> value="year">Year</option>
                </select>
              </div>

            </div>
            <button type="button" id="btfilter" name="btfilter" onclick="doit()">Filter</button>
          </div>
        </form>
        <ol class="breadcrumb pull-right">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
        <div style="clear:both"></div>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- <a href="<?php echo base_url(); ?>Transaction_detail?d=cabang&time=<?php echo (isset($_GET["t"]))? $_GET["t"] : ""; ?>" target = "_blank"> -->
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>


                <div class="info-box-content">
                  <span class="info-box-text">Cabang</span>
                  <span class="info-box-number"><?php echo $branch_count; ?></span>
                </div>
              <!-- </a> -->
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- <a href="<?php echo base_url(); ?>Transaction_detail?d=totalTransaksi&time=<?php echo (isset($_GET["t"]))? $_GET["t"] : "" ; ?>" target = "_blank"> -->
                <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-pie-chart"></i></span>


                  <div class="info-box-content">
                    <span class="info-box-text">Total Transaksi</span>
                    <span class="info-box-number"><?php echo number_format($transaction_sum); ?></span>
                  </div>
                <!-- </a> -->
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->

            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <!-- <a href="<?php echo base_url(); ?>Transaction_detail?d=sumTransaki&time=<?php echo (isset($_GET["t"]))? $_GET["t"] : "" ; ?>" target = "_blank"> -->
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-database"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Jumlah Transaksi</span>
                    <span class="info-box-number"><?php echo number_format($transaction_count) ?></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            <!-- </a> -->
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <!-- <a href="<?php echo base_url(); ?>Transaction_detail?d=newMember&time=<?php echo (isset($_GET["t"]))? $_GET["t"] : "" ; ?>" target = "_blank"> -->
                  <div class="info-box-content">
                    <span class="info-box-text">Pelanggan Baru</span>
                    <span class="info-box-number"><?php echo number_format($member_count) ?></span>
                  </div>
                <!-- </a> -->
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

          <?php

          if(isset($_GET['t'])){
            ?>
            <input type="hidden" name="dayAct" id="dayAct" value="<?php echo $_GET['t'] ?>" />
            <?php
          }
          ?>


          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Recapitulation Report</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-8">
                        <p class="text-center">
                          <strong><span id="dayText"><?php if ($this->input->get("t")!==null) {
                            echo ucfirst($this->input->get("t"));
                          }  ?></span></strong>
                        </p>

                        <div class="chart">
                          <!-- Sales Chart Canvas -->
                          <div id="chartsa" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                        </div>
                        <!-- /.chart-responsive -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-4">
                        <p class="text-center">
                          <strong>Goal Completion</strong>
                        </p>

                        <div class="progress-group">
                          <span class="progress-text">Penjualan/Berhasil</span>
                          <span class="progress-number"><b><?php echo number_format($transaction_sccs) ?></b>/<?php echo number_format($transaction_limit) ?></span>

                          <div class="progress sm">
                            <?php 

                            $total=($transaction_sccs/$transaction_limit)*100;
                            if ($total>=100) {
                             $total='100';
                           }
                           else if ($total < 0) {
                             $total = '0';
                           } ?>
                           <div class="progress-bar progress-bar-green" style="width: <?php echo $total ?>%"></div>
                         </div>
                       </div>
                       <!-- /.progress-group -->
                       <div class="progress-group">
                        <span class="progress-text">Penjualan/Pending</span>
                        <span class="progress-number"><b><?php echo number_format($transaction_pending) ?></b>/<?php echo number_format($transaction_limit) ?></span>

                        <div class="progress sm">
                          <?php $total=($transaction_pending/$transaction_limit)*100;
                          if ($total>=100) {
                           $total='100';
                         }
                         else if ($total < 0) {
                           $total = '0';
                         } ?>
                         <div class="progress-bar progress-bar-yellow" style="width: <?php echo $total ?>%"></div>
                       </div>
                     </div>
                     <!-- /.progress-group -->
                     <div class="progress-group">
                      <span class="progress-text">Penjualan/Batal</span>
                      <span class="progress-number"><b><?php echo number_format($transaction_cancel) ?></b>/<?php echo number_format($transaction_limit) ?></span>

                      <div class="progress sm">
                        <?php $total=($transaction_cancel/$transaction_limit)*100;
                        if ($total>=100) {
                         $total='100';
                       }
                       else if ($total < 0) {
                         $total = '0';
                       } ?>
                       <div class="progress-bar progress-bar-cancel" style="width: <?php echo $total ?>%"></div>
                     </div>
                   </div>
                   <!-- /.progress-group -->
                   <div class="progress-group">
                      <span class="progress-text">Penjualan/Hutang</span>
                      <span class="progress-number"><b><?php echo number_format($transaction_debt) ?></b>/<?php echo number_format($transaction_limit) ?></span>

                      <div class="progress sm">
                        <?php $total=($transaction_debt/$transaction_limit)*100;
                        if ($total>=100) {
                         $total='100';
                       }
                       else if ($total < 0) {
                         $total = '0';
                       } ?>
                       <div class="progress-bar progress-bar-lightblue" style="width: <?php echo $total ?>%"></div>
                     </div>
                   </div>
                 <!-- /.progress-group -->
                   <!-- /.progress-group -->
                   <div class="progress-group">
                    <span class="progress-text">Penjualan/Semua</span>
                    <span class="progress-number"><b><?php echo number_format($transaction_limit) ?></b>/<?php echo number_format($transaction_limit) ?></span>

                    <div class="progress sm">
                      <?php $total=($transaction_limit/$transaction_limit)*100;
                      if ($total>=100) {
                       $total='100';
                     }
                     else if ($total < 0) {
                       $total = '0';
                     } ?>
                     <div class="progress-bar progress-bar-aqua" style="width: <?php echo $total ?>%"></div>
                   </div>
                 </div>
                 
               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
           </div>
           <!-- ./box-body -->
           <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Rp.<?php echo number_format($transaction_sccs,2) ?></h5>
                  <span class="description-text">TOTAL REVENUE</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Rp.<?php echo number_format($total_cost,2) ?></h5>
                  <span class="description-text">TOTAL COST</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Rp.<?php $a=$transaction_sccs- $total_cost;echo number_format($a,2)?></h5>
                  <span class="description-text">TOTAL PROFIT</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Statistic Revenue &amp; Profit Each Branches</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-9 col-sm-8">
                <div class="pad">
                  <!-- Map will be created here -->
                  <div id="map"></div>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-3 col-sm-8">
                <div class="pad box-pane-right bg-green" style="min-height: 280px">
                  <div class="description-block margin-bottom">
                    <h5 class="description-header"><?php echo number_format($total_visit) ?></h5>
                    <span class="description-text">Visits</span>
                  </div>
                  <!-- /.description-block -->
                  <div class="description-block margin-bottom">
                    <h5 class="description-header"><?php echo number_format($total_omzet) ?></h5>
                    <span class="description-text">Omset</span>
                  </div>
                  <!-- /.description-block -->
                  <div class="description-block">
                    <h5 class="description-header"><?php echo number_format($total_profit) ?></h5>
                    <span class="description-text">Profit</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <div class="row">
          <div class="col-md-6">
            <!-- DIRECT CHAT -->
            <div class="box box-warning direct-chat direct-chat-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Request Stock</h3>

                <div class="box-tools pull-right">
                  <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow"><?php echo count($request_stock); ?></span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                  <?php if (count($request_stock) <= 0) { ?>
                  <div class="direct-chat-msg">
                    [Tidak ada data]
                  </div>
                  <?php } else { 
                      $i = 0;
                    ?>
                    <div id="div-request">
                  <?php foreach ($request_stock as $s) : ?>
                    <!-- Message. Default to the left -->
                    <a href="#dialog-approval" class="request_dialog" data-toggle="modal" data-id="<?php echo $s->approvalID; ?>">
                      <div class="direct-chat-msg">
                        <div class="direct-chat-info clearfix">
                          <span class="direct-chat-name pull-left"><?php echo $s->FromPOS; ?></span>
                          <span class="direct-chat-timestamp pull-right"><?php echo date("d M y H:i",strtotime($s->requestDt)); ?></span>
                        </div>
                        
                        <div class="direct-chat-text">
                          <?php echo $s->message; ?>
                        </div>
                      </div>
                    </a>
                     <!-- /.direct-chat-msg -->
                  <?php 
                    $i++; 
                    if ($i >= 5) break;
                    endforeach; ?>
                  </div>
                  <?php } ?>

                </div>
                <!--/.direct-chat-messages-->
              </div>
              <div class="box-footer text-center">
                <a href="<?php echo base_url(); ?>approval" class="uppercase">Lihat selengkapnya</a>
              </div>
            </div>
            <!--/.direct-chat -->
          </div>
          <!-- /.col -->

          <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Pelanggan</h3>

                <div class="box-tools pull-right">
                  <span class="label label-success"><?php echo count($guest); ?> Pelanggan</span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <tbody>
                      <?php
                      if (count($guest)==0) {
                        ?>
                        <tr>
                          <td align="center">[Tidak ada data]</td>
                        </tr>
                      <?php 
                      }
                      else{
                        $i = 1;
                        foreach ($guest as $key => $value) {
                          if ($i > 6) break;
                          ?>
                          <tr>
                            <td><span class="fa fa-user"></span> &nbsp; <strong><?php echo $value->Guest_Name ?> - (<?php echo $value->Email; ?>)</strong></td>
                          </tr>
                          <?php
                          $i++;
                        }
                      }
                      
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.users-list -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?php echo base_url(); ?>member" class="uppercase">Lihat selengkapnya</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!--/.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Popular Product</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                  <tr>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th style="text-align:center">Order Frequency</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (count($product_tren)==0) {
                  ?>
                      <tr>
                        <td colspan="3" align="center">[Tidak ada data]</td>
                      </tr>
                  <?php
                  }
                  else{
                    foreach ($product_tren as $key => $value) {
                      //var_dump($value);
                      ?>
                      <tr>
                        <td><?php echo $value['codeMenu'] ?></a></td>
                        <td><?php echo $value['menuNM'] ?></td>
                        <td align="center">
                          <!-- <a href="<?php base_url(); ?>transaction_detail?d=<?php echo $value["menuID"] ?>&time=<?php echo (isset($_GET["t"]))? $_GET["t"] : "" ; ?>" target="_blank"> -->
                            <?php echo number_format($value["jml"]) ?>
                          <!-- </a> -->
                        </td>
                      </tr>
                      <?php
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-yellow">
          <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Branch Revenue</span>
            <span class="info-box-number" id="branchRevenue">- Select Branch -</span>

            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <span class="progress-description">

            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Branch Cost</span>
            <span class="info-box-number" id="branchCost">- Select Branch -</span>

            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <span class="progress-description">

            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        <div class="info-box bg-aqua">
          <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Branch Profit</span>
            <span class="info-box-number" id="branchProfit">- Select Branch -</span>

            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <span class="progress-description">

            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->

        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Customer Transaction</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="chart-responsive">
                  <div id="piechartcustomer" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->

              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->

          <!-- /.footer -->
        </div>
        <!-- /.box -->

        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Item Purchase</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="chart-responsive">
                  <div id="piechartitem" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <!-- ./chart-responsive -->
              </div>
              <!-- /.col -->
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->
          <!-- /.footer -->
        </div>
        <!-- /.box -->
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include "includes/footer.php"; ?>
<?php include "includes/approval_dialog.php"; ?>
<!-- Control Sidebar -->

<!-- /.control-sidebar -->
<div class="control-sidebar-bg"></div>
</div>
</body>
</html>
<script src="<?php echo base_url(); ?>dist/js/home.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVVSqxxlhct_UCU6GGEpJqAl5fSxTGojc&callback=initMap"
async defer></script>
