// Javascript
var TYPE_PERSONAL = "P";
var TYPE_COMPANY = "C";

$(function () {
	$('#branchTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});
$(function () {
	$('#ProductTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});

function confirmation(d, pos){
	var base = base_url + "Member/delete?menu="+d+"&pos=" + pos;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
    	window.location.assign(base);
    }
}

function doit(){
	var temp = $("#brancOption").val();
	$("#action").val(temp);
	$("#productView").submit();
}

function add()
  {
      var pos = $("#brancOption").val();
     var base = base_url + "Member/add_member_view?pos="+pos;
    //alert(base)
      window.location = base
  }

//Datemask dd/mm/yyyy
// $("#guestBorn").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
$("#guestPhone").inputmask("(9999) 9999-9999", {"placeholder": "(____) ____-____"});

// Event
$(document).ready(function(e)
{
	// Show Type field
	var val = $("#guestType").val();
	if (val == TYPE_PERSONAL) {
		$(".personal").show();
	} else if (val == TYPE_COMPANY) {
		$(".personal").hide();
	}


	$("#guestType").change(function(e){
		var val = $(this).val();
		if (val == TYPE_PERSONAL) {
			$(".personal").show();
		} else if (val == TYPE_COMPANY) {
			$(".personal").hide();
		}
	});
});
