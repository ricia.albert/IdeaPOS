// Javascript Document

$(document).ready(function(e) {
	$(".auth").prop("disabled", true);
	var checks = $(".check:checked");
	for(var i=0;i<checks.length;i++) {
		var target = $(checks[i]).data("target");
		$("." + target).prop("disabled", false);
	}
	$(".check").change(function(e) {
		var target = $(this).data("target");
		var checked = $(this).prop("checked");
		$("." + target).prop("disabled", !checked);
		if (!checked)
			$("." + target).val($("." + target + " option:first").val());
	});
});