// Javascript
$("#arrange").change(function(e){
	window.location = base_url + "Home?t=" + $("#arrange").val();
});

function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }


var jsonValueV = JSON.parse($("#jsonValue").html());
var jsonDebt = JSON.parse($("#jsonDebt").html());
var customer = JSON.parse($("#itemCharts").html());
var item = JSON.parse($("#customerCharts").html());
var timeStamp = JSON.parse($("#timeStamp").html());
var ppn = JSON.parse($("#ppn").html());
var branch = JSON.parse($("#branch").html());
    //var branch_revenue = JSON.parse($("#branch_revenue").html());
    var tempBranch = [];

    for (var x in branch) {
    	tempBranch.push(branch[x]);
    }

    var index = 0;
    var map;
    var infoWindow = [];
    function initMap() {
    	var index = 0;

    	var current = {lat: -6.1745 , lng:106.8227}

    	map = new google.maps.Map(document.getElementById('map'), {
    		center: current,
    		scrollwheel: false,
    		zoom: 7
    	});

    	for (var i = 0; i < tempBranch.length; i++) {
    		index++;
    		var POSID = tempBranch[i]["POSID"];
    		var name = tempBranch[i]["POSNm"];
    		var address = tempBranch[i]["Address"];
    		var telp = tempBranch[i]["Telp"];
    		var fax = tempBranch[i]["Fax"];
    		var latitude = tempBranch[i]["latitude"];
    		var longitude = tempBranch[i]["longitude"];
    		var total = tempBranch[i]["total"];
    		var marker = [];
    		var myPosition = {lat: latitude, lng: longitude};
            // Create a map object and specify the DOM element for display.
            
            var contentString = "<div id='content'><b>"+POSID+
            "</b><br>"+name+"</br>"+address+"<br>"+telp+"/"+fax+
            "<br>Visit : "+ total+
            "</div>";
            infoWindow[i] = addMarker(POSID, myPosition, contentString);
        }

        map.addListener("click", function()
        {
        	clearInfoWindow();
        });
    }

    function clearInfoWindow()
    {
    	for (var i=0; i<infoWindow.length;i++)
    	{
    		infoWindow[i].close();
    	}
    }

    function getBranchInfo(POSID)
    {
    	$.ajax({
    		url : base_url + "Home/get_info_branch",
    		type : "POST",
    		dataType : "html",
    		data : "POSID=" + POSID + "&sd=" + $("#startDt").val() + "&ed=" + $("#endDt").val(),
    		success : function(result)
    		{
    			var json = $.parseJSON(result);
    			$("#branchRevenue").html("Rp. " + json.total);
    			$("#branchCost").html("Rp. " + json.cost);
    			$("#branchProfit").html("Rp. " + json.profit);
    		},
    		error : function(result) 
    		{
    			console.log("AJAX Error : " + result);
    		}
    	})
    }

    function addMarker(POSID, myPosition, contentString)
    {
    	var infoWindow = new google.maps.InfoWindow({
    		content : contentString
    	});

    	var marker = new google.maps.Marker({
    		map : map,
    		position : myPosition,
    		title : POSID
    	});

    	marker.addListener('click', function() {
    		clearInfoWindow();
    		infoWindow.open(map, marker);
    		getBranchInfo(marker.title);
    	});

    	return infoWindow;
    }


    $(function () {
    	Highcharts.chart('piechartcustomer', {
    		chart: {
    			plotBackgroundColor: null,
    			plotBorderWidth: null,
    			plotShadow: false,
    			type: 'pie'
    		},
    		title: {
    			text: ''
    		},
    		tooltip: {
    			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    		},
    		plotOptions: {
    			pie: {
    				allowPointSelect: true,
    				cursor: 'pointer',
    				dataLabels: {
    					enabled: true,
    					format: '<b>{point.name}</b>:<br/> {point.percentage:.1f} %',
    					style: {
    						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    					}
    				}
    			}
    		},
    		series: [{
    			name: 'Brands',
    			colorByPoint: true,
    			data: customer

    		}]
    	});
    }); 

    $(function () {
    	Highcharts.chart('piechartitem', {
    		chart: {
    			plotBackgroundColor: null,
    			plotBorderWidth: null,
    			plotShadow: false,
    			type: 'pie'
    		},
    		title: {
    			text: ''
    		},
    		tooltip: {
    			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    		},
    		plotOptions: {
    			pie: {
    				allowPointSelect: true,
    				cursor: 'pointer',
    				dataLabels: {
    					enabled: true,
    					format: '<b>{point.name}</b>:<br/> {point.percentage:.1f} %',
    					style: {
    						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    					}
    				}
    			}
    		},
    		series: [{
    			name: 'Brands',
    			colorByPoint: true,
    			data: item
    		}]
    	});
    });

    $(function () {
    	Highcharts.chart('chartsa', {
    		chart: {
    			type: 'line'
    		},
    		title: {
    			text: 'Transaction Chart'
    		},
    		subtitle: {
    			text: 'Source: Idea-POS'
    		},
    		xAxis: {
    			categories: timeStamp
    		},
    		yAxis: {
    			title: {
    				text: 'Rp.'
    			}
    		},
    		plotOptions: {
    			line: {
    				dataLabels: {
    					enabled: true
    				},
    				enableMouseTracking: false
    			}
    		},
    		series: [{
    			name: 'Sales',
    			data: jsonValueV
    		}, {
                name: 'In Debt',
                data: jsonDebt
            }, {
    			name: 'PPN',
    			data: ppn
    		}]
    	});
    });

function refresh()
{
    $.ajax({
        url : base_url + "Home/get_new_request",
        type : "GET",
        dataType : "html",
        success : function(result)
        {
            console.log(result);
            var json = $.parseJSON(result);
            var html = "";
            for (var i = 0; i < json.length; i++) {
                html += "<a href='#dialog-approval' class='request_dialog' data-toggle='modal' data-id='" + json[i].approvalID + "'>" + 
                        "<div class='direct-chat-msg'>" + 
                        "<div class='direct-chat-info clearfix'>" +
                        "<span class='direct-chat-name pull-left'>" + json[i].FromPOS + "</span>" +
                        "<span class='direct-chat-timestamp pull-right'>" + formatDate(json[i].requestDt) + "</span>" +
                        "</div>" +
                        "<div class='direct-chat-text'>" +
                          json[i].message +
                        "</div></div></a>";
            }
            $("#div-request").html(html);
            $(".request_dialog").click(function(e)
            {
                var id = $(this).attr("data-id");
                $.ajax({
                    url : base_url + "Home/view_request",
                    type : "POST",
                    dataType : "html",
                    data : "approvalID=" + id,
                    success : function(result)
                    {
                        console.log(result);
                        var json = $.parseJSON(result);
                        $("#hid_id").val(json.approvalID);
                        $("#hid_pos").val(json.PosID);
                        $("#span-id").html(json.approvalID);
                        $("#span-date").html(formatDate(json.requestDt));
                        $("#span-from").html(json.FromPOS);
                        $("#span-to").html(json.ToPOS);
                        $("#span-pesan").html(json.message);
                        $("#span-status").html(json.Stats);
                        var html = "";
                        for (var i=0; i<json.detail.length; i++) {
                            html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                                    "<td>" + json.detail[i].ingName + "</td>" + 
                                    "<td align='center'>" + json.detail[i].qty + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarNm + "</td>";
                            if (json.approvalStats == "N") {
                                html += "<td align='center'><input type='number' class='txtqtyapv' value='" + json.detail[i].qty_apv + "' /></td>" +
                                        "<td align='center'><select class='cbscalarapv'>";
                                for (var j=0; j<json.detail[i].scalarList.length; j++) {
                                    html += "<option value='" +json.detail[i].scalarList[j].ScalarID + "' " + ((json.detail[i].scalar_apv == json.detail[i].scalarList[j].ScalarID) ? "selected" : "") + ">" + json.detail[i].scalarList[j].scalarNm + "</option>";
                                }
                            } else {
                                html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
                                        "<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                            }
                            html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                            html += "</select>" +
                                    "</tr>";
                        }
                        $("#tbody").html(html);
                        $("#div-wait").hide();
                        if (json.approvalStats != "N") {
                            $("#pnl-operation").hide();
                        } else {
                            $("#pnl-operation").show();
                        }
                    }
                });
            });
        },
        error : function(result) 
        {
            console.log("AJAX Error : " + result);
        }
    })
}

$(document).ready(function(e)
{
    $(".request_dialog").click(function(e)
    {
        $("#div-wait").hide();
        var id = $(this).attr("data-id");
        $.ajax({
            url : base_url + "Home/view_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + id,
            success : function(result)
            {
                console.log(result);
                var json = $.parseJSON(result);
                $("#hid_id").val(json.approvalID);
                $("#hid_pos").val(json.PosID);
                $("#span-id").html(json.approvalID);
                $("#span-date").html(formatDate(json.requestDt));
                $("#span-from").html(json.FromPOS);
                $("#span-to").html(json.ToPOS);
                $("#span-pesan").html(json.message);
                $("#span-status").html(json.Stats);
                var html = "";
                for (var i=0; i<json.detail.length; i++) {
                    html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                            "<td>" + json.detail[i].ingName + "</td>" + 
                            "<td align='center'>" + json.detail[i].qty + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarNm + "</td>";
                    if (json.approvalStats == "N") {
                        html += "<td align='center'><input type='number' class='txtqtyapv' value='" + json.detail[i].qty_apv + "' /></td>" +
                                "<td align='center'><select class='cbscalarapv'>";
                        for (var j=0; j<json.detail[i].scalarList.length; j++) {
                            html += "<option value='" +json.detail[i].scalarList[j].ScalarID + "' " + ((json.detail[i].scalar_apv == json.detail[i].scalarList[j].ScalarID) ? "selected" : "") + ">" + json.detail[i].scalarList[j].scalarNm + "</option>";
                        }
                    } else {
                        html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
                                "<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                    }

                    html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                    html += "</select>" +
                            "</tr>";
                }
                $("#tbody").html(html);
                $("#div-wait").hide();
                if (json.approvalStats != "N") {
                    $("#pnl-operation").hide();
                } else {
                    $("#pnl-operation").show();
                }
            }
        });
    });

    $("#btApprove").click(function(e)
    {
        $("#div-wait").show();
        $("#pnl-operation").hide();
        var approvalID = $("#hid_id").val();
        var posID = $("#hid_pos").val();
        var row = $(".row-items");
        var qtyapv = 0;
        var scalarapv, detailID, x;
        var dataList = new Array();
        for (var i = 0;i<row.length; i++) {
            detailID = $(row[i]).attr("data-id");
            qtyapv = $(row[i]).find(".txtqtyapv").val();
            scalarapv = $(row[i]).find(".cbscalarapv").val();
            x = {
                    ID : detailID,
                    Qty : qtyapv,
                    ScalarID : scalarapv
            };
            dataList.push(x);
        }
        var json = JSON.stringify(dataList);
        
        $.ajax({
            url : base_url + "Home/approve_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + approvalID + "&POSID=" + posID + "&json=" + json,
            success : function(result)
            {
                console.log(result);
                refresh();
                alert("Request has been approved.");
                $("#dialog-approval").modal("hide");
            }
        });
    });

    $("#btReject").click(function(e)
    {
        $("#div-wait").show();
        $("#pnl-operation").hide();
        var approvalID = $("#hid_id").val();
        $.ajax({
            url : base_url + "Home/reject_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + approvalID,
            success : function(result)
            {
                console.log(result);
                refresh();
                alert("Request has been rejected.");
                $("#dialog-approval").modal("hide");
            }
        });
    });
});