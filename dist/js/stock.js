// Javascript
$(function () {
	$('#branchesTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"responsive" : true
	});
});

  function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }
