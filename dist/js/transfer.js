// Javascript Document

function doit(){
    var temp = $("#stateOption").val();
    $("#action").val(temp);
    $("#approvalView").submit();
}
  
function refresh()
{
    var stats = $("#stateOption").val();
    $.ajax({
        url : base_url + "Home/get_transfer?s=" + stats,
        type : "GET",
        dataType : "html",
        success : function(result)
        {
            console.log(result);
            var json = $.parseJSON(result);
            var html = "";
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    html += "<tr>" +
                            "<td>" + json[i].approvalID + "</td>" + 
                            "<td>" + formatDate(json[i].requestDt) + "</td>" + 
                            "<td>" + json[i].FromPOS + "</td>" +
                            "<td>" + json[i].ToPOS + "</td>" + 
                            "<td>" + json[i].message + "</td>" + 
                            "<td>" + json[i].Stats + "</td>" + 
                            "<td align='center'>" +
                            "<button type='button' data-id='"+json[i].approvalID + "' class='btn btn-primary btnDetail'><span class='fa fa-envelope-open'></span> &nbsp; Detail</button></td>" +
                            "</tr>";
                }
            } else {
                html += "<tr><td colspan='7' align='center'><strong>[No Data Found]</strong></td></tr>";
            }
            $("#spdata").html(json.length);
            $("#table-approval").html(html);

            $(".btnDetail").click(function(e)
            {
                var id = $(this).attr("data-id");
                $.ajax({
                    url : base_url + "Home/view_request",
                    type : "POST",
                    dataType : "html",
                    data : "approvalID=" + id,
                    success : function(result)
                    {
                        console.log(result);
                        var json = $.parseJSON(result);
                        $("#hid_id").val(json.approvalID);
                        $("#hid_pos").val(json.PosID);
                        $("#span-id").html(json.approvalID);
                        $("#span-date").html(formatDate(json.requestDt));
                        $("#span-from").html(json.FromPOS);
                        $("#span-to").html(json.ToPOS);
                        $("#span-pesan").html(json.message);
                        $("#span-status").html(json.Stats);
                        var html = "";
                        for (var i=0; i<json.detail.length; i++) {
                            html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                                    "<td>" + json.detail[i].ingName + "</td>" + 
                                    "<td align='center'>" + json.detail[i].qty + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarNm + "</td>";
                            html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                            html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                            html += "</select>" +
                                    "</tr>";
                        }
                        $("#tbody").html(html);
                        $("#div-wait").hide();
                        if (json.approvalStats != "N") {
                            $("#pnl-operation").hide();
                        } else {
                            $("#pnl-operation").show();
                        }
                        $("#dialog-approval").modal("show");
                    }
                });
            });
        },
        error : function(result) 
        {
            console.log("AJAX Error : " + result);
        }
    });
}

$(document).ready(function(e){

    setInterval(refresh, 1000);
	$(".btnDetail").click(function(e)
	{
        $("#div-wait").hide();
		var id = $(this).attr("data-id");
		$.ajax({
            url : base_url + "Home/view_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + id,
            success : function(result)
            {
                console.log(result);
                var json = $.parseJSON(result);
                $("#hid_id").val(json.approvalID);
                $("#hid_pos").val(json.PosID);
                $("#span-id").html(json.approvalID);
                $("#span-date").html(formatDate(json.requestDt));
                $("#span-from").html(json.FromPOS);
                $("#span-to").html(json.ToPOS);
                $("#span-pesan").html(json.message);
                $("#span-status").html(json.Stats);
                var html = "";
                for (var i=0; i<json.detail.length; i++) {
                    html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                            "<td>" + json.detail[i].ingName + "</td>" + 
                            "<td align='center'>" + json.detail[i].qty + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarNm + "</td>";          
            		html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
            				"<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                    html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                    html += "</select>" +
                            "</tr>";
                }
                $("#tbody").html(html);
                $("#div-wait").hide();
                if (json.approvalStats != "N") {
                    $("#pnl-operation").hide();
                } else {
                    $("#pnl-operation").show();
                }
                $("#dialog-approval").modal("show");
            }
        });
		
	});
})