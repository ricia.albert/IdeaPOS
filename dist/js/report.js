// Javascript Document
var module = "";
$(document).ready(function()
{
	$("#dialog-trans").on('show', function (e) {
	    $("#txtsd").val("");
	    $("#txted").val("");
	});

	$(".repo").click(function(e){
		module = $(this).data("mod");
	});

	$("#txtsd").datepicker({
		dateFormat:"yy-mm-dd",
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            var dt2 = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            dt2.setDate(dt2.getDate() + 31);
            $("#txted").datepicker("option", "minDate", dt);
            $("#txted").datepicker("option", "maxDate", dt2);
        }
    });

    $("#txted").datepicker({
    	dateFormat:"yy-mm-dd",
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#txtsd").datepicker("option", "maxDate", dt);
        }
    });

	$("#btprint").click(function(e){
		var pos = $("#cbPOS").val();
		var brc = $("#cbPOS option:selected").text();
		var debt = $("#cbdebt").val();
		var stats = $("#cbstatus").val();
		var price = $("#cbprice").val();
		var sd = $("#txtsd").val();
		var ed = $("#txted").val();
		if (sd == "")
			alert("Please fill start date first.");
		else if (ed == "")
			alert("Please fill end date first.");
		else {
			$.ajax({
				url : base_url + "report/" + module,
				type : "POST",
				dataType : "html",
				data : "pos=" + pos + "&brc=" + brc + "&debt=" + debt + "&stats=" + stats + "&price=" + price + "&sd=" + sd + "&ed=" + ed,
				success : function(result) {
					console.log(result);
					var url = result;
					window.location = url;
				},
				error : function(result) {
					console.log("AJAX Error on Export Report");
				}
			});
		}
	});

	$("#btsimple").click(function(e){
		var pos = $("#pos").val();
		var brc = $("#pos option:selected").text();
		var sd = $("#txtfrom").val();
		var ed = $("#txtend").val();
		if (sd == "")
			alert("Please fill start date first.");
		else if (ed == "")
			alert("Please fill end date first.");
		else {
			$.ajax({
				url : base_url + "report/" + module,
				type : "POST",
				dataType : "html",
				data : "pos=" + pos + "&brc=" + brc + "&sd=" + sd + "&ed=" + ed,
				success : function(result) {
					console.log(result);
					var url = result;
					window.location = url;
				},
				error : function(result) {
					console.log("AJAX Error on Export Report");
				}
			});
		}
	});

	$("#btbranch").click(function(e){
		var pos = $("#cpos").val();
		var brc = $("#cpos option:selected").text();
		$.ajax({
			url : base_url + "report/" + module,
			type : "POST",
			dataType : "html",
			data : "pos=" + pos + "&brc=" + brc,
			success : function(result) {
				var url = result;
				window.location = url;
			},
			error : function(result) {
				console.log("AJAX Error on Export Report");
			}
		});
	});

	$("#btopname").click(function(e){
		var pos = $("#opos").val();
		var brc = $("#opos option:selected").text();
		var sd = $("#txtdate").val();
		if (sd == "")
			alert("Please fill opname date first.");
		else {
			$.ajax({
				url : base_url + "report/" + module,
				type : "POST",
				dataType : "html",
				data : "pos=" + pos + "&brc=" + brc + "&sd=" + sd,
				success : function(result) {
					var url = result;
					window.location = url;
				},
				error : function(result) {
					console.log("AJAX Error on Export Report");
				}
			});
		}
	});
});
