// Javascript Document
String.prototype.paddingLeft = function (paddingValue) {
   return String(paddingValue + this).slice(-paddingValue.length);
};

function monthName(x)
{
    var mon = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
                "Agustus", "September", "Oktober", "November", "Desember"];
    return mon[x];
}

function formatNumber(biaya) {
	var n = parseFloat(biaya);
	var value = n.toLocaleString(
	  undefined, // use a string like 'en-US' to override browser locale
	  { minimumFractionDigits: 2 }
	);
	return value;
}

function formatDate(x)
{
    var dt = new Date(x);
    var hours = dt.getHours();
    var minutes = dt.getMinutes();
    var dates = dt.getDate();
    return dates.toString().paddingLeft("00") + " " + monthName(dt.getMonth()) + " " + dt.getFullYear() + " " + hours.toString().paddingLeft("00") + ":" + minutes.toString().paddingLeft("00");
}

function validateNum() {
	$(".num").keydown(function(e){
		if ((e.keyCode < 48 || e.keyCode > 57 ) && e.keyCode != 190 &&
			 e.keyCode != 8 && e.keyCode != 46 &&
			 e.keyCode != 37 && e.keyCode != 38 && 
			 e.keyCode != 39 && e.keyCode != 40 && (e.keyCode < 96 || e.keyCode > 105)) 
		 {
    	 	return false;
    	 }
	});
}

$(document).ready(function(e){
	$(".num").keydown(function(e){
		if ((e.keyCode < 48 || e.keyCode > 57 ) && e.keyCode != 190 &&
			 e.keyCode != 8 && e.keyCode != 46 &&
			 e.keyCode != 37 && e.keyCode != 38 && 
			 e.keyCode != 39 && e.keyCode != 40 && (e.keyCode < 96 || e.keyCode > 105)) 
		 {
    	 	return false;
    	 }
	});

	$(".date").datepicker({
		dateFormat:"yy-mm-dd"
	});
});