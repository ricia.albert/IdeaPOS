// Javascript
var tempProd;
var tempID;
var tempPos;
$(function () {
  $('#branchTable').DataTable({
    "columnDefs": [{
      "defaultContent": "-",
      "targets": "_all"
    }],
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false
  });
});
$(function () {
  $('#ProductTable').DataTable({
    "columnDefs": [{
      "defaultContent": "-",
      "targets": "_all"
    }],
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false
  });
});

function confirmation(d, pos){
  var base = base_url + "Package/delete?menu="+d+"&pos=" + pos;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
      window.location.assign(base);
    }
  }


function copy(d, id, pos, name){
  tempProd = d;
  tempID = id;
  tempPos = pos;
  $("#product").html(name);
  $.ajax({
    url : base_url + "Package/get_available_branch",
    type : "POST",
    dataType : "html",
    data : "codeMenu=" + tempProd,
    success : function(result) {
      console.log(result);
      var res = result.split("<-sp->");
      var json = $.parseJSON(res[0]);
      var json2 = $.parseJSON(res[1]);
      var options = document.createElement("option");
      var cbTo = document.getElementById("cbTo");
      var branches = "";
      if (json.length > 0) {
        for (var i=0; i<json.length;i++) {
          if (i > 0) 
            branches += ", ";
          branches += json[i].PosNm;
        }
      } else {
        branches = "No branches copied.";
      }
      cbTo.options.length = 0;
      if (json2.length > 0) {
        for (var i=0; i<json2.length;i++) {
          options = document.createElement("option");
          options.text = json2[i].PosNm;
          options.value = json2[i].PosID;
          cbTo.options.add(options);
        } 
      } else {
        options = document.createElement("option");
        options.text = "All branches has been copied.";
        options.value = "";
        cbTo.options.add(options);
      }
      
      $("#td_copied").html(branches);
      $("#dialog-prod-copy").modal("show");

    },
    error : function(result) {
      console.log("AJAX Error on : " + result);
    }
  });
  
}

  function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }

  function add()
  {
      var pos = $("#brancOption").val();
     var base = base_url + "Package/add_package_view?pos="+pos;
    //alert(base)
      window.location = base
  }

   $(document).ready(function(e) {
      $("#btcopy").click(function(e) {
          var to = $("#cbTo").val();
          if (to == null || to == "") {
            alert("Harap pilih target cabang terlebih dahulu !");
          } else {
            var base = base_url + "Package/update?menu="+tempID + "&pos=" + to + "&copy=1&fromPos="+ tempPos;
            window.location.assign(base);
          }
      });
  })