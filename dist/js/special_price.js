// Javascript Document
$(function () {
	$('#menuTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});
$(function () {
	$('#ProductTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"autoWidth": false
	});
});

function confirmation(d, pos, guest){
	var base = base_url + "member/delete_special?menu="+d+"&pos=" + pos + '&guest='+guest;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
    	window.location.assign(base);
    }
}

function functi(d, pos, guest){
	var qty = prompt("Insert the Price","0");

	if (isNaN(qty)) {
		alert("Input is incorrect");
	}
	else{
		var base = base_url + "member/fast_update?menu="+qty+"&ID="+d+"&pos="+pos+"&guest="+guest;
		window.location.assign(base);
	}
}

function doit(){
	var temp = $("#brancOption").val();
	$("#action").val(temp);
	$("#productView").submit();
}

$("#guestBorn").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
$("#guestPhone").inputmask("(___) ___-____", {"placeholder": "(___) ___-____"});