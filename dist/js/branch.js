// Javascript
$(function () {
	$('#branchTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});

function confirmation(d){
	var base = base_url + "Branch/Delete?d="+d;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
    	window.location.assign(base);
    }
}

function confirmationReset(pos){
	var base = base_url + "Branch/Reset?pos="+pos;
    //alert(base)
    var r = confirm("Are you sure want to reset this branch data ?");
    if (r == true) {
    	window.location.assign(base);
    }
}

$(document).ready(function(e)
{
	$("#btcopy").click(function(e)
	{
		var from = $("#cbFrom").val();
		var to = $("#cbTo").val();
		$.ajax({
			url : base_url + "Branch/Copy",
			dataType : "html",
			type : "POST",
			data : "from=" + from + "&to=" + to,
			success : function(result) {
				console.log(result);
				if (parseInt(result) == 0) {
					alert("Copy Database Successfully.");
					$("#dialog-copy").modal("hide");
				}
			},
			error : function(result) {
				alert("There is still data in destination branch.\nPlease clean the destination branch data first by clicking reset.");
				console.log("AJAX Error : " + result.responseText);
			}
		})
	});
});