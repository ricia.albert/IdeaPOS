// Javascript
$(document).ready(function(e) {
	$('#branchTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
	window.setTimeout(function() {
		if ($("#password").val() != "") {
			$(".passwordText").val($("#password").val());	
		}
		$(".passwordText").change(function() {
				$("#password").val($(this).val());
			});
	}, 200);

	$("#password").change(function() {
		$(".passwordText").val($(this).val());
	});

	$("#chkShow").change(function(){
		var checked = $(this).prop("checked");
		if (checked) {
			$("#passwordText").hide();
			$("#password").show();
		} else {
			$("#passwordText").show();
			$("#password").hide();
		}
	});
});


  function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }

function confirmation(d){
	var base = base_url + "Employee/Delete?d="+d;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
    	window.location.assign(base);
    }
}