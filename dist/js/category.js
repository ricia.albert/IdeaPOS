// Javascript
$(function () {
	$('#branchTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});
$(function () {
	$('#ProductTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});
});

function confirmation(d,pos){
	var base = base_url + "Category/delete?menu="+d + "&pos="+pos;
    //alert(base)
    var r = confirm("Are you sure?");
    if (r == true) {
    	window.location.assign(base);
    }
}

function doit(){
	var temp = $("#brancOption").val();
	$("#action").val(temp);
	$("#productView").submit();
}

function functi(d){
	var qty = prompt("Insert the quantity","0");

	if (isNaN(qty)) {
		alert("Input is incorrect");
	}
	else{
		var base = "<?php base_url(); ?>ingridients/fast_update?menu="+qty+"&ID="+d;
		window.location.assign(base);
	}
}

 //Datemask dd/mm/yyyy
 $("#guestBorn").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
 $("#guestPhone").inputmask("(___) ___-____", {"placeholder": "(___) ___-____"});
