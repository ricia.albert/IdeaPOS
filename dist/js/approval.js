// Javascript Document

function doit(){
    var temp = $("#stateOption").val();
    $("#action").val(temp);
    $("#approvalView").submit();
}
  
function refresh()
{
    var stats = $("#stateOption").val();
    $.ajax({
        url : base_url + "Home/get_request?s=" + stats,
        type : "GET",
        dataType : "html",
        success : function(result)
        {
            console.log(result);
            var json = $.parseJSON(result);
            var html = "";
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    html += "<tr>" +
                            "<td>" + json[i].approvalID + "</td>" + 
                            "<td>" + formatDate(json[i].requestDt) + "</td>" + 
                            "<td>" + json[i].FromPOS + "</td>" +
                            "<td>" + json[i].ToPOS + "</td>" + 
                            "<td>" + json[i].message + "</td>" + 
                            "<td>" + json[i].Stats + "</td>" + 
                            "<td align='center'>" +
                            "<button type='button' data-id='"+json[i].approvalID + "' class='btn btn-primary btnDetail'><span class='fa fa-envelope-open'></span> &nbsp; Detail</button></td>" +
                            "</tr>";
                }
            } else {
                html += "<tr><td colspan='7' align='center'><strong>[No Data Found]</strong></td></tr>";
            }
            $("#spdata").html(json.length);
            $("#table-approval").html(html);

            $(".btnDetail").click(function(e)
            {
                var id = $(this).attr("data-id");
                $.ajax({
                    url : base_url + "Home/view_request",
                    type : "POST",
                    dataType : "html",
                    data : "approvalID=" + id,
                    success : function(result)
                    {
                        console.log(result);
                        var json = $.parseJSON(result);
                        $("#hid_id").val(json.approvalID);
                        $("#hid_pos").val(json.PosID);
                        $("#span-id").html(json.approvalID);
                        $("#span-date").html(formatDate(json.requestDt));
                        $("#span-from").html(json.FromPOS);
                        $("#span-to").html(json.ToPOS);
                        $("#span-pesan").html(json.message);
                        $("#span-status").html(json.Stats);
                        var html = "";
                        for (var i=0; i<json.detail.length; i++) {
                            html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                                    "<td>" + json.detail[i].ingName + "</td>" + 
                                    "<td align='center'>" + json.detail[i].qty + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarNm + "</td>";
                            if (json.approvalStats == "N") {
                                html += "<td align='center'><input type='number' class='txtqtyapv' value='" + json.detail[i].qty_apv + "' /></td>" +
                                        "<td align='center'><select class='cbscalarapv'>";
                                for (var j=0; j<json.detail[i].scalarList.length; j++) {
                                    html += "<option value='" +json.detail[i].scalarList[j].ScalarID + "' " + ((json.detail[i].scalar_apv == json.detail[i].scalarList[j].ScalarID) ? "selected" : "") + ">" + json.detail[i].scalarList[j].scalarNm + "</option>";
                                }
                            } else {
                                html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
                                        "<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                            }
                            html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                                    "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                            html += "</select>" +
                                    "</tr>";
                        }
                        $("#tbody").html(html);
                        $("#div-wait").hide();
                        if (json.approvalStats != "N") {
                            $("#pnl-operation").hide();
                        } else {
                            $("#pnl-operation").show();
                        }
                        $("#dialog-approval").modal("show");
                    }
                });
            });
        },
        error : function(result) 
        {
            console.log("AJAX Error : " + result);
        }
    });
}

$(document).ready(function(e){

    setInterval(refresh, 1000);
	$(".btnDetail").click(function(e)
	{
        $("#div-wait").hide();
		var id = $(this).attr("data-id");
		$.ajax({
            url : base_url + "Home/view_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + id,
            success : function(result)
            {
                console.log(result);
                var json = $.parseJSON(result);
                $("#hid_id").val(json.approvalID);
                $("#hid_pos").val(json.PosID);
                $("#span-id").html(json.approvalID);
                $("#span-date").html(formatDate(json.requestDt));
                $("#span-from").html(json.FromPOS);
                $("#span-to").html(json.ToPOS);
                $("#span-pesan").html(json.message);
                $("#span-status").html(json.Stats);
                var html = "";
                for (var i=0; i<json.detail.length; i++) {
                    html += "<tr class='row-items' data-id='" + json.detail[i].detailID + "'><td>" + json.detail[i].ingID + "</td>" +
                            "<td>" + json.detail[i].ingName + "</td>" + 
                            "<td align='center'>" + json.detail[i].qty + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarNm + "</td>";
                    if (json.approvalStats == "N") {
	                    html += "<td align='center'><input type='number' class='txtqtyapv' value='" + json.detail[i].qty_apv + "' /></td>" +
	                            "<td align='center'><select class='cbscalarapv'>";
	                    for (var j=0; j<json.detail[i].scalarList.length; j++) {
	                        html += "<option value='" +json.detail[i].scalarList[j].ScalarID + "' " + ((json.detail[i].scalar_apv == json.detail[i].scalarList[j].ScalarID) ? "selected" : "") + ">" + json.detail[i].scalarList[j].scalarNm + "</option>";
	                    }
                	} else {
                		html += "<td align='center'>" + json.detail[i].qty_apv + "</td>" +
                				"<td align='center'>" + json.detail[i].scalarID_apv + "</td>";
                	}
                    html += "<td align='center'>" + json.detail[i].qty_real + "</td>" +
                            "<td align='center'>" + json.detail[i].scalarID_real + "</td>";
                    html += "</select>" +
                            "</tr>";
                }
                $("#tbody").html(html);
                $("#div-wait").hide();
                if (json.approvalStats != "N") {
                    $("#pnl-operation").hide();
                } else {
                    $("#pnl-operation").show();
                }
                $("#dialog-approval").modal("show");
            }
        });
		
	});

    $("#btApprove").click(function(e)
    {
        $("#div-wait").show();
        $("#pnl-operation").hide();
        var approvalID = $("#hid_id").val();
        var posID = $("#hid_pos").val();
        var row = $(".row-items");
        var qtyapv = 0;
        var scalarapv, detailID, x;
        var dataList = new Array();
        for (var i = 0;i<row.length; i++) {
            detailID = $(row[i]).attr("data-id");
            qtyapv = $(row[i]).find(".txtqtyapv").val();
            scalarapv = $(row[i]).find(".cbscalarapv").val();
            x = {
                    ID : detailID,
                    Qty : qtyapv,
                    ScalarID : scalarapv
            };
            dataList.push(x);
        }
        var json = JSON.stringify(dataList);
        
        $.ajax({
            url : base_url + "Home/approve_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + approvalID + "&POSID=" + posID + "&json=" + json,
            success : function(result)
            {
                console.log(result);
                alert("Request has been approved.");
                $("#dialog-approval").modal("hide");
                refresh();
            }
        });
    });

    $("#btReject").click(function(e)
    {
        $("#div-wait").show();
        $("#pnl-operation").hide();
        var approvalID = $("#hid_id").val();
        var posID = $("#hid_pos").val();
        $.ajax({
            url : base_url + "Home/reject_request",
            type : "POST",
            dataType : "html",
            data : "approvalID=" + approvalID + "&POSID=" + posID,
            success : function(result)
            {
                console.log(result);
                alert("Request has been rejected.");
                $("#dialog-approval").modal("hide");
                refresh();
            }
        });
    });
})