// Javascript
$(function () {
	$('#branchesTable').DataTable({
		"columnDefs": [{
			"defaultContent": "-",
			"targets": "_all"
		}],
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"responsive" : true
	});
});

  function doit(){
    var temp = $("#brancOption").val();
    $("#action").val(temp);
    $("#productView").submit();
  }

 $(document).ready(function(e)
 {
 	$("#branchesTable").on("click",".a_trans", function(e)
 	{
 		var id = $(this).data("id");
 		var pos = $(this).data("pos");
 		$.ajax({
 			url : base_url + "transaction_detail/detail?transID=" + id+"&pos=" + pos,
 			dataType : "html",
 			type : "GET",
 			success : function(result) {
 				console.log(result);
 				var json = $.parseJSON(result);
 				$("#span-id").html(json.transID);
 				$("#span-date").html(formatDate(json.transDate));
 				if (json.settledDate != null && json.settledDate != "") {
 					$("#span-settled").html(formatDate(json.settledDate));
 				} else {
 					$("#span-settled").html("-");
 				}
 				$("#span-cust").html(json.atasNama);
 				$("#span-waiter").html(json.waiterName);
 				$("#span-cashier").html(json.employee_name);
 				$("#span-slot").html(json.tableNm);
 				$("#span-group").html(json.roomNm);
 				$("#span-status").html(json.Status);
 				$("#span-due").html((json.DueDt == null) ? "-" : formatDate(json.DueDt));
 				$("#span-payment").html((json.paymentName == null) ? "Kontan" : json.paymentName );

 				if (json.tableID != null) {
 					$("#span-transtype").html("Transaction");
 				} else if (json.isTakeAway == 1)  {
 					$("#span-transtype").html("Service");
 				} else if (json.isDelivery == 1)  {
 					$("#span-transtype").html("Delivery");
 				} else if (json.isDebt == 1)  {
 					$("#span-transtype").html("In Debt");
 				}
 				$("#span-total").html(formatNumber(json.total));
 				$("#span-ppn").html(formatNumber(json.ppn));
 				$("#span-discount").html(formatNumber(json.totalDiscount));
 				$("#span-voucher").html(formatNumber(json.totalVoucher));
 				$("#span-rounding").html(formatNumber(json.rounding));
 				$("#span-void").html((json.hasVoid == 1) ? "Ya" : "Tidak");
 				$("#span-voiddt").html((json.voidDt == null) ? "-" : formatDate(json.voidDt));
 				$("#span-notes").html((json.notes == null) ? "-" : json.notes);

 				var html = "";
 				var detail = json.detail;
 				for (var d in detail) {
 					html+="<tr><td align='center'>" + detail[d].menuID + "</td>" + 
 						  "<td align='center'>" + detail[d].menuNm + "</td>" + 
 						  "<td align='center'>" + formatNumber(detail[d].price) + "</td>" + 
 						  "<td align='center'>" + detail[d].qty + "</td>" + 
 						  "<td align='center'>" + formatNumber((detail[d].price * detail[d].qty)) + "</td>" + 
 						  "<td align='center'>" + formatNumber(detail[d].valueDiscount) + "</td>" + 
 						  "<td align='center'>" + formatNumber(detail[d].fixed_price) + "</td></tr>";
 				}
 				$("#tbody").html(html);
 				$("#dialog-trans").modal("show");
 			},
 			error : function(result) {
 				console.log("AJAX Error : " + result);
 			}
 		})
 	});
 })
